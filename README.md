# NE:ONE - opeN sourcE: ONE record server software

NE:ONE is an initiative by the Digital Testbed Air Cargo (DTAC), a research project funded by the German Federal Ministry for Digital and Transport. https://www.linkedin.com/company/digitales-testfeld-air-cargo-dtac/

It's goal is to provide and open source and free to use ONE Record server software package, that helps you implement the IATA ONE Record standard, the new data sharing standard in air cargo and beyond. NE:ONE is fully compliant with the IATA ONE Record API description (v2.0) and data model (v3.0.0) to help you get started with ONE Record.

To find out more about ONE Record, check out the IATA GitHub:
- current API description including how to query (e.g. POST, GET, PATCH) any ONE Record server including NE:ONE:
  https://iata-cargo.github.io/ONE-Record
- current data model:
  https://github.com/IATA-Cargo/ONE-Record/tree/api_2.0.0-dev/working_draft/API

While NE:ONE currently is being developed through public funds within the DTAC, it’s supposed to be jointly used & maintained by the players of the air cargo community and beyond. Let's push Digital Air Cargo to the next level togehter!

If you want to contribute see [CONTRIBUTING.md](~/CONTRIBUTING.md) file in this repository.

For more info on the work being done on NE:ONE, DTAC and the OLF Working Group you can contact oliver.ditz@iml.fraunhofer.de

Currently the NE:ONE server is based on the [CARGO ontology Version 3.0.0](https://github.com/IATA-Cargo/ONE-Record/blob/master/2023-12-standard/Data-Model/IATA-1R-DM-Ontology.ttl)
at commit [b9d7aaf944d321aeda42110e4a7dd45a0bb4ca02](https://github.com/IATA-Cargo/ONE-Record/commit/b9d7aaf944d321aeda42110e4a7dd45a0bb4ca02)
and [API ontology 2.0.0-dev](https://github.com/IATA-Cargo/ONE-Record/blob/master/working_draft/API/ONE-Record-API-Ontology.ttl)
at commit [c0b17f35899f1009392fc01bf0f2e3b8a728bba1](https://github.com/IATA-Cargo/ONE-Record/commit/c0b17f35899f1009392fc01bf0f2e3b8a728bba1).

---

[[_TOC_]]

# Getting Started

- To get started with NE:ONE there are first of all different ways to run (install) it. See chapter [Installation](#installation).
- To start developing for NE:ONE you can have a look at the chapter [NE:ONE Development - Getting Started](#neone-development-getting-started).
- If you want start configuring your own NE:ONE server, e.g. to setup notification, you can take a look at [Configuration](#configuration).

## Installation

### Plain docker usage

The NE:ONE project releases docker containers containing the NE:ONE server. The published containers are available from
the [Gitlab container registry](https://git.openlogisticsfoundation.org/wg-digitalaircargo/ne-one/container_registry)
the images named `ne-one-mtls` have mandatory mTLS access configured.


The NE:ONE server can be started as plain Docker container using the following command:

```shell script
docker run -d --name neone \
    -p 8080:8080 \
    git.openlogisticsfoundation.org:5050/wg-digitalaircargo/ne-one:latest
```

The `latest` tag always points to the latest release version. Use the tag `dev` for the latest development
version.

This starts a bare minimum instance of the NE:ONE server using an in memory repository.

The server information endpoint is populated with values configured in the `application.properties`. Defaults are:

```properties
neone-server.supported-content-types[0]=application/ld+json
neone-server.supported-content-types[1]=application/turtle
neone-server.supported-content-types[2]=text/turtle
neone-server.supported-languages[0]=${default-language}
neone-server.supported-logistics-object-types[0]=https://onerecord.iata.org/ns/cargo#Piece
neone-server.supported-logistics-object-types[1]=https://onerecord.iata.org/ns/cargo#Waybill
neone-server.supported-logistics-object-types[2]=https://onerecord.iata.org/ns/cargo#Shipment
neone-server.data-holder.name=ACME Corporation
neone-server.data-holder.id=_data-holder
```

The `neone-server.data-holder.id` defines the URI of the data holder `LogisticsObject`, it defaults to `_data-holder` so
as per default configuration the object can be retrieved using the URI
`http://localhost:8080/logistics-objects/_data_holder`.

The configuration of the NE:ONE server follows the default Quarkus configuration possibilities. Please refer to the
[Quarkus configuration guide](https://quarkus.io/guides/config-reference). Possible configurations and defaults are
located in the `src/main/resources/application.conf` file.

There is also a mTLS enabled docker image available named
`git.openlogisticsfoundation.org:5050/wg-digitalaircargo/ne-one/ne-one-mtls` with the same tags as the non-mTLS version.
In order to use this version it is recommended to use the docker compose method for starting the server.

For accessing the services the NE:ONE server provides, a JWT token is needed. See also the section about the
[Keycloak setup](#starting-keycloak-for-authentication). As per default one valid token issuer is configured with
the issuer url set to `http://localhost:8989/realms/neone` and the corresponding JWKS endpoint
`http://localhost:8989/realms/neone/protocol/openid-connect/certs`.

For adding additional valid issuers a mapping in the form of

```properties
auth.valid-issuers.<ISSUER_KEY>=<ISSUER_URL>
auth.issuers.<ISSUER_KEY>.publickey.location=<PUBLIC_KEY_LOCATION>
```

has to be set up and provided via the different Quarkus configuration mechanisms (properties file, Java system
properties, environment variables, ...).

The `<PUBLIC_KEY_LOCATION>` may either be a JWKS url, a public key as string or point to a file containing the public
key in PEM format.

> **_NOTE:_** To disable the retrieval of the `one-record-ìd` from the supplied JWT, set the `disable.access.subject`
> property to `true`. When doing so, a fixed value for the access subject is used with the value set to the data holder
> of the server. This does not disable the ACL authorization, ACLs need to be created anyway. See
> [Authorization API](#authorization-api) for details.

### Docker Compose

The NE:ONE project comes with several Docker Compose files that may be used to start the server in different
configurations. The Docker Compose files are located in the `src/main/docker-compose` folder of the source repository.

Starting the NE:ONE server in its minimal configuration without persistence is as easy as running:

```shell script
docker compose up -d
```

This starts the NE:ONE server and maps the internal port `8080` to the host port `8080`.

Default configurations are provided in `.env` files and are set up to configure the server for a local deployment. In
order to configure the server for external access, at least the `lo-id.env` file has to be adapted to the external
available host and port configuration.

| Environment Variable     | Description                                                                                                                      |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `LO_ID_CONFIG_HOST`      | The external host name of the server                                                                                             |
| `LO_ID_CONFIG_SCHEME`    | The scheme of the host, either `http` or `https`                                                                                 |
| `LO_ID_CONFIG_PORT`      | The external port of the server                                                                                                  |
| `LO_ID_CONFIG_ROOT_PATH` | If there is a reverse proxy wich serves the endpoints using a different root path than the NE:ONE server (optional), default `/` |

These configuration properties are used to build up the base for all externally resolvable entities in the NE:ONE
server, so the base URL has the format:
`<LO_ID_CONFIG_SCHEME>://<LO_ID_CONFIG_HOST>[:<LO_ID_CONFIG_PORT>]/[<LO_ID_CONFIG_ROOT_PATH>/]`

As the NE:ONE server calls external services for notifications and subscriptions, the `client.env` is used to configure
these endpoints. If left as is the NE:ONE server expects a mock server running in the same network as the NE:ONE
server that handles subscription proposals and notifications.

Docker Compose files are provided to use GraphDB persistence, TLS and mTLS configuration.

| Docker Compose file                 | Purpose                                                                                                                  | Environment files     |
|-------------------------------------|--------------------------------------------------------------------------------------------------------------------------|-----------------------|
| `docker-compose.graphdb.yml`        | Enable GraphDB persistence, may be used together with `docker-compose.graphdb-server.yml`                                | `graph-db.env`        |
| `docker-compose.graphdb-server.yml` | Starts a GraphDB instance                                                                                                | none                  |
| `docker-compose.tls.yml`            | Starts the server with TLS enabled on port 8443                                                                          | `tls.env`             |
| `docker-compose.mtls.yml`           | Starts the server with mTLS (and TLS) enabled on port 8443. **MUST NOT** be used together with `docker-compose.tls.yml`. | `tls.env`, `mtls.env` |


#### Enabling GraphDB persistence

Starting the server with GraphDB persistence enabled is done using the following Docker Compose command.

```shell script
docker compose -f docker-compose.yml -f docker-compose.graphdb.yml -f docker-compose.graphdb-server.yml up -d
```

#### Enabling TLS

When enabling TLS the used certificate is included in the repository, however the certificates subject is `localhost`.
Please refer to the [mTLS configuration section](accessing-the-server-using-mtls), which includes a step-by-step guide
on creating certificates.

The `tls.env` file alters the configuration for the Logistics Object IDs to point to the TLS port and scheme of the
server.

> **_NOTE:_** When using your own certificates the `tls.env` file and the `docker-compose.tls.yml` file have to be
> adapted to point to the right certificate file and include the correct password.

```shell script
docker compose -f docker-compose.yml -f docker-compose.tls.yml up -d
```

#### Enabling mTLS

The mTLS configuration includes the TLS configuration as mTLS is an addition to TLS. As for mTLS the configuration
included is using the certificates located in this repository. The
[mTLS configuration section](accessing-the-server-using-mtls) describes how to use your own certificates.

> **_NOTE:_** When using your own certificates the `tls.env`, `mtls.env` files and the `docker-compose.tls.yml` file
> have to be adapted to point to the right certificate files and include the correct passwords.

```shell script
docker compose -f docker-compose.yml -f docker-compose.mtls.yml up -d
```

#### Starting the mock server for subscriptions and notifications

```shell script
docker compose -f docker-compose.mockserver.yml up -d
```

This may also be combined with other Docker Compose files, see [Combining configurations](combining-configurations).
The started mock server also exposes a dashboard where expectations and requests can be viewed. The dashboard can be
accessed here: [http://localhost:1080/mockserver/dashboard](http://localhost:1080/mockserver/dashboard)

#### Starting Keycloak for authentication

The NE:ONE server protects its resources using JWT tokens. In order to start Keycloak as identity provider Docker
Compose files are provided. As the NE:ONE server implements the ONE Record specification, it supports JWTs from
different identity providers. Therefore, the Docker Compose file starts a Keycloak instance with two configured realms
which act as separate token issuers.

*Keycloak instance with two configured realms*:

|                          |                                                                   |
|--------------------------|-------------------------------------------------------------------|
| **URL**                  | http://localhost:8989                                             |
| **Username**             | `admin`                                                           |
| **Password**             | `admin`                                                           |
| **Token URL (Realm #1)** | http://localhost:8989/realms/neone/protocol/openid-connect/token  |
| **Token URL (Realm #2)** | http://localhost:8989/realms/neone2/protocol/openid-connect/token |

Additionally, this instance has a preconfigured user for downloading files using the blob API.

|              |                |
|--------------|----------------|
| **Username** | `neone-viewer` |
| **Password** | `viewer`       |


```shell script
docker compose -f docker-compose.keycloak.yml up -d
```

Realm #1 is configured with the realm name `neone`. Also, a preconfigured client is provided with the client_id
`neone-client` and client_secret `lx7ThS5aYggdsMm42BP3wMrVqKm9WpNY`.

Realm #2 is configured with the realm name `neone2`. Also, a preconfigured client is provided with the client id
`neone-client` and client_secret `lx7ThS5aYggdsMm42BP3wMrVqKm9WpNY`. In addition, a preconfigured client is provided
that can be used to simulate an external ONE Record client. The client_id is `external-one-record-client` and the
client_secret is `H58gdLkGofh0BaTQxBjvzIBlZONfIpHy`.

Both realms are configured to issue tokens including the ONE Record custom claim `logistics_agent_uri`.
For `neone-client`, its value is `http://localhost:8080/logistics-objects/_data-holder` and for
`external-one-record-client`, its value is `http://external-one-record-server/logistics-objects/_data-holder`.

#### Starting Minio as S3 compatible service

The NE:ONE server stores blobs in any Amazon S3 compatible storage provider. For the sake of simplicity a Docker
Compose file is provided to start a [Minio server](https://minio.io). See
[Combining Configurations](#combinig-configurations) on how to start the Minio server together with the NE:ONE server
using Docker Compose.

When running the NE:ONE server as standalone docker container configure the network to include the Minio server.
The easiest way to do this is by setting the `QUARKUS_S3_ENDPOINT_OVERRIDE` environment variable to
`http://host.docker.internal:9000` and when running docker on linux adding
`--add-host=host.docker.internal:host-gateway` to your docker command that starts the NE:ONE server. I.e.:

``` shell
docker run -d --name neone \
  -p 8080:8080 \
  --add-host=host.docker.internal:host-gateway \
  -e "QUARKUS_S3_ENDPOINT_OVERRIDE=http://host.docker.internal:9000" \
  git.openlogisticsfoundation.org:5050/digital-air-cargo/ne-one:dev`
```

And additionally start the Minio server, either using the Docker Compose file

```shell script
docker compose -f docker-compose.minio.yml up -d
```

or by starting the Minio server using the docker command

```shell
docker run -d \
  -p 9000:9000  -p 9090:9090 \
  --name minio \
  -e "MINIO_ROOT_USER=admin" \
  -e "MINIO_ROOT_PASSWORD=admin123" \
  quay.io/minio/minio server /data --console-address ":9090"
```

The NE:ONE server defaults to using a local Minio server, as started by the supplied Docker Compose file,
for blob storage. In order to configure other S3 object stores, please refer to the
[documentation of Quarkus S3 extension](https://docs.quarkiverse.io/quarkus-amazon-services/dev/amazon-s3.html).

The default configuration creates a bucket named `neone` during startup. The following properties control this
behaviour:

| Property                  | Description                                | Default |
|---------------------------|--------------------------------------------|---------|
| `blobstore.bucket-name`   | The bucket name to store blobs             |         |
| `blobstore.create-bucket` | Whether to create the bucket if not exists | false   |

#### Starting Grafana and Prometheus for monitoring

The NE:ONE server exposes metrics for Prometheus at `/q/metrics` endpoint. In order touse the data for monitoring a
Docker Compose file is provided. The setup starts a Prometheus and a Grafana instance. Both preconfigured to directly
see the measured values.

Prometheus connects to the host network port 8080, so it is assumed that a NE:ONE server is running and exposes its
HTTP port on all interfaces on port 8080 of the host network.

The monitoring stack is started with the following command:

```shell
docker compose -f docker-compose.monitoring.yml up -d
```

#### Combinig configurations

The configurations for (m)TLS and GraphDB may be combined to start the server., I.e.:

```shell script
docker compose -f docker-compose.yml -f docker-compose.tls.yml -f docker-compose.graphdb.yml \
  -f docker-compose.graphdb-server.yml -f docker-compose.mockserver.yml up -d
```

> **_NOTE:_** in every case the `docker-compose.yml` has to be the first file in the list.

### Testing interoperability between two NE:ONE server

To test notifications and subscriptions two NE:ONE server and the previously mentioned mock server are needed. Starting
the mock server is done using docker compose.

```shell script
docker compose -f docker-compose.mockserver.yml up -d
```
This starts the mock server and exposes it on port 1080 on localhost, be sure that port 1080 is not already in use.

Afterwards two NE:ONE server can be started using the NE:ONE docker image. The easiest way to connect the two servers
is to use the host networking driver of docker.

Ensure that ports 8080 and 8081 are not already in use on the host system.

```shell script
docker run -d --name ne-one-1 --net=host \
  -e QUARKUS_REST_CLIENT_SUBSCRIPTION_CLIENT_URL=http://localhost:1080 \
  git.openlogisticsfoundation.org:5050/wg-digitalaircargo/ne-one:dev

docker run -d --name ne-one-2 --net=host \
  -e LO_ID_CONFIG_PORT=8081 \
  -e QUARKUS_HTTP_PORT=8081 \
  -e QUARKUS_REST_CLIENT_SUBSCRIPTION_CLIENT_URL=http://localhost:1080 \
  git.openlogisticsfoundation.org:5050/wg-digitalaircargo/ne-one:dev
```

The mock server is set up to accept all publisher initiated subscriptions.

## Caching of remote Logistics Objects in Redis

The NE:ONE server includes a service to fetch remote logistics objects. If a subscription for a logistics object or
logistics object type is present for the remote object it is put into a Redis cache server. The cache entry gets
evicted if a change on the object has happened and a notification about the change is received.

The cache is disabled by default. In order to enable the cache the `quarkus.cache.enabled` property has to be set to
`true` using one of the Quarkus configuration possibilities.

The configuration of the redis cache server is documented in the
[Quarkus redis cache guide](https://quarkus.io/guides/cache-redis-reference).

In order to also enable the Redis health check a [re-augmentation](https://quarkus.io/guides/reaugmentation) of the
NE:ONE server is necessary. The NE:ONE docker image is built to enable this process. A custom docker file has to be
written for this process to work.

**Example:**

```dockerfile
FROM git.openlogisticsfoundation.org:5050/wg-digitalaircargo/ne-one as builder

# enable redis health check
ENV QUARKUS_REDIS_HEALTH_ENABLED=true
# enable health-ui
ENV QUARKUS_SMALLRYE_HEALTH_UI_ALWAYS_INCLUDE=true
# enable caching
ENV QUARKUS_CACHE_ENABLED=true
# set redis host
ENV QUARKUS_REDIS_HOSTS=redis://localhost:6379

WORKDIR /deployments/quarkus-app

# run re-augmentation
RUN java -jar -Dquarkus.launch.rebuild=true quarkus-run.jar

# create new custom image from the re-augmented jar
FROM git.openlogisticsfoundation.org:5050/wg-digitalaircargo/ne-one

# copy re-augmented application to original destination
COPY --from=builder /deployments/quarkus-app/ /deployments/quarkus-app/
```

Then build the image with `docker build -t ne-one-redis .`. The resulting image now is configured to use Redis as
caching system and enables the health checks for redis.


# NE:ONE Development - Getting Started

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/.

The NE:ONE server is developed using Java Version 17 and also uses Docker in order to start the
[Quarkus dev services](https://quarkus.io/guides/dev-services). Please ensure that your development environment is
set up to meet these requirements.

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

### Use persistent RDF datastore for development

The NE:ONE server defaults to using an in memory RDF store. However, it is possible to use
[ontotext GraphDB](https://www.ontotext.com) or
[Blazepgraph](https://www.ontotext.com) or
[RDF4J native store](https://rdf4j.org/documentation/programming/repository/#native-rdf-repository) to have a persistent
RDF store.

The type of repository is specified by setting the application property `repository-type` to:

* `in-memory` (default) for the in memory store
* `native` for the RDF4J native store
* `http` for the GraphDB store

**Using RDF4J native store**

The native store stores all triples on disk in a binary format for compact storage and retrieval. This store is suited
for up to 100 million triples.

Configuration:

| Property              | Value               |
|-----------------------|---------------------|
| `repository-type`     | `native`            |
| `repository-data-dir` | `PATH_TO_DIRECTORY` |

**Using GraphDB**

In order to use GraphDB it must be up and running before the NE:ONE server is started. A `docker compose` file is
provided that may be used to start a GraphDB instance.

```shell script
cd src/main/docker-compose
docker docker compose -f docker-compose.graphdb-server.yml up -d
```

This command also configures a repository with the id `neone` within the GraphDB instance.

Now the `repository-type` property has to be set to `http`. This can be achieved using quarkus default configuration
possibilities (`application.properties`, Java system property, environment variable).

I.e.:

```shell script
./mvnw compile quarkus:dev -Drepository-type=http
```

> **_NOTE:_**  The configuration property `http-repository-url` defaults to `http://localhost:7200/repositories/neone`
> which is the repository URL for the repository created by the docker compose file and has to be adjusted if a GraphDB
> is started using a different method than the provided docker compose file.

Configuration:

| Property              | Value                 |
|-----------------------|-----------------------|
| `repository-type`     | `http`                |
| `http-repository-url` | `URL_OF_GRAPHDB_REPO` |

**Using SPARQL compatible endpoint**

In order to use a SPARQL compatible endpoint the `repository-type` property has to be set to `sparql`. This can be
achieved using quarkus default configuration possibilities (`application.properties`, Java system property,
environment variable). Furthermore, the properties `sparql-query-endpoint` and `sparql-update-endpoint` have to be set.

Configuration:

| Property                 | Value                 |
|--------------------------|-----------------------|
| `repository-type`        | `sparql`              |
| `sparql-query-endpoint`  | `SPARQL_QUERY_URL`    |
| `sparql-update-endpoint` | `SPARQL_UPDATE_URL`   |

> **_NOTE:_**  GraphDB, for example, also provides a SPARQL endpoint.
> With the default configuration of the [GraphDB docker-compose file](src/main/docker-compose/docker-compose.graphdb-server.yml)
> the SPARQL query endpoint is `http://localhost:7200/repositories/neone` and the SPARQL update endpoint is `http://localhost:7200/repositories/neone/statements`.
> The SPARQL query endpoint is used for read operations and the SPARQL update endpoint is used for write operations.

# Authorization API

The authorization of the NE:ONE server is implemented using the [W3C ACL Ontology](https://www.w3.org/ns/auth/acl).

## Create ACLs

### Granting access to a specific lo

|              |                                     |
|--------------|-------------------------------------|
| Endpoint     | `/internal/acls/grant`              |
| HTTP Method  | `POST`                              |
| Content Type | `application/x-www-form-urlencoded` |

Form parameter:

| Name       | Description                                        | Remark                                                                                                     |
|------------|----------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| `accessTo` | The IRI of the object to grant access to           |                                                                                                            |
| `agent`    | The IRI of the agent that should be granted access |                                                                                                            |
| `modes`    | The mode of access                                 | Can be either `acl:Read` or `acl:Write`, can occure multiple times to grant both modes in a single request |


### Create ACL for a specific requester

|              |                       |
|--------------|-----------------------|
| Endpoint     | `/internal/acls`      |
| HTTP Method  | `POST`                |
| Content Type | `application/ld+json` |

Request Body:

```json
{
  "@context": {
    "@vocab": "http://www.w3.org/ns/auth/acl#",
    "acl": "http://www.w3.org/ns/auth/acl#"
  },
  "@type": "acl:Authorization",
  "agent": {
    "@id": "<ACCESS_SUBJECT_IRI>"
  },
  "accessTo": {
    "@id": "<ACCESS_OBJECT_IRI>"
  },
  "mode": [
    {
      "@id": "<ACL_MODE>"
    }
  ]
}
```

| Variable             | Value                                                                                                                      |
|----------------------|----------------------------------------------------------------------------------------------------------------------------|
| `ACCESS_SUBJECT_IRI` | The IRI of a `cargo:LogisticsAgent` that should be granted access                                                          |
| `ACCESS_OBJECT_IRI`  | The IRI of either a `cargo:LogisticsObject`, `cargo:LogisticsEvent` or `api:ActionRequest` the access should be granted to |
| `ACL_MODE`           | Either `acl:Read`, `acl:Write` or both                                                                                     |

Response Header:

| Header     | Value                  |
|------------|------------------------|
| `Location` | URL of the created ACL |

### Create single ACL for all authenticated requesters

For creating an ACL for all authenticated requesters, the special IRI `http://www.w3.org/ns/auth/acl#AuthenticatedAgent`
can be used as subject of the `acl:agent` predicate.

```json
{
  "@context": {
    "@vocab": "http://www.w3.org/ns/auth/acl#",
    "acl": "http://www.w3.org/ns/auth/acl#"
  },
  "@type": "acl:Authorization",
  "agent": {
    "@id": "http://www.w3.org/ns/auth/acl#AuthenticatedAgent"
  },
  "accessTo": {
    "@id": "<ACCESS_OBJECT_IRI>"
  },
  "mode": [
    {
      "@id": "<ACL_MODE>"
    }
  ]
}
```

## Get ACL

|             |                              |
|-------------|------------------------------|
| Endpoint    | `/internal/acls/<unique-id>` |
| HTTP Method | `POST`                       |
| Accept      | `application/ld+json`        |

Response:

ACL as JSON-LD.

## Update ACL

|             |                              |
|-------------|------------------------------|
| Endpoint    | `/internal/acls/<unique-id>` |
| HTTP Method | `POST`                       |
| Accept      | `application/ld+json`        |

Request Body:

```json
{
  "@context": {
    "@vocab": "http://www.w3.org/ns/auth/acl#",
    "acl": "http://www.w3.org/ns/auth/acl#"
  },
  "@type": "acl:Authorization",
  "agent": {
    "@id": "<ACCESS_SUBJECT_IRI>"
  },
  "accessTo": {
    "@id": "<ACCESS_OBJECT_IRI>"
  },
  "mode": [
    {
      "@id": "<ACL_MODE>"
    }
  ]
}
```

| Variable             | Value                                                                                                                      |
|----------------------|----------------------------------------------------------------------------------------------------------------------------|
| `ACCESS_SUBJECT_IRI` | The IRI of a `cargo:LogisticsAgent` that should be granted access                                                          |
| `ACCESS_OBJECT_IRI`  | The IRI of either a `cargo:LogisticsObject`, `cargo:LogisticsEvent` or `api:ActionRequest` the access should be granted to |
| `ACL_MODE`           | Either `acl:Read`, `acl:Write` or both                                                                                     |

The existing ACL will be overwritten with the one supplied as request body.

Response:

HTTP Status 200 for success or 404 if the ACL to be updated was not found.

## Delete ACL


|             |                              |
|-------------|------------------------------|
| Endpoint    | `/internal/acls/<unique-id>` |
| HTTP Method | `DELETE`                     |
| Accept      | `application/ld+json`        |

Response:

HTTP Status 200 for success or 404 if the ACL to be updated was not found.

## Find ACLs

|             |                       |
|-------------|-----------------------|
| Endpoint    | `/internal/acls`      |
| HTTP Method | `POST`                |
| Accept      | `application/ld+json` |

Query parameters:

| Parameter  | Value                                                                         |
|------------|-------------------------------------------------------------------------------|
| `agent`    | The subject that has access to some object. Mandatory if `accessTo` is unset. |
| `accessTo` | The object that ACLs is linked to. Mandatory if `agent` is unset.             |

Response Body:

The ACL list that match the given query.

# Blobstore API

The NE:ONE server allows to store blobs and apply defined ACLs to the objects stored. The ACLs are inherited from
the `cargo:ExternalReference` logistics object they are linked to.

## Upload blob

|              |                              |
|--------------|------------------------------|
| Endpoint     | `files/upload/<SHORT_LO_ID>` |
| HTTP Method  | `POST`                       |
| Content-Type | `multipart/form-data`        |

Form paramter:

| Name       | Description               |
|------------|---------------------------|
| `mimetype` | The MIME type of the file |
| `filename` | The filename              |
| `data`     | The file to upload        |


The `<SHORT_LO_ID>` must be the UUID part of a URI of an existing `cargo:ExternalReference`.

Response Header:

| Header     | Value                    |
|------------|--------------------------|
| `Location` | URL of the uploaded file |

## Download file

|              |                                           |
|--------------|-------------------------------------------|
| Endpoint     | `files/download/<SHORT_LO_ID>/<filename>` |
| HTTP Method  | `GET`                                     |

The `<SHORT_LO_ID>` must be the UUID part of a URI of an existing `cargo:ExternalReference`.

HTTP Response:

The requested file.

# Remote Logistics Object Proxy API

The NE:ONE server is able to proxy requests to third party Logistics Objects. In order to fetch remote objects the
NE:ONE server needs to authenticate against a trusted identity provider using the following properties:

```properties
quarkus.oidc-client.auth-server-url=<trusted_idp_url>
quarkus.oidc-client.client-id=<client_id>
quarkus.oidc-client.credentials.secret=<client_secret>
```

> **_NOTE:_**  According to the IATA ONE Record specification

For development these properties are preconfigured to use the provided Keycloak instance.

|             |                       |
|-------------|-----------------------|
| Endpoint    | `/internal/proxy`     |
| HTTP Method | `GET`                 |
| Accept      | `application/ld+json` |

Query paramter:

| Name       | Description                            |
|------------|----------------------------------------|
| `iri`      | The IRI of the remote Logistics Object |

The response is the logistics object normalized using the JSON-LD context
`"@vocab": "https://onerecord.iata.org/ns/cargo#"`.

# Getting Started with the first Logistics Object

> **_NOTE:_** All values for data holder properties are assumed to be taken from the section about the
> [provided Keycloak configuration](#starting-keycloak-for-authentication).

1. Create a Logistics Object using the API as described in the IATA ONE Record specification. As the creation of
   Logistics Objects is strictly spoken not part of the ONE Record API, no authentication is needed. Securing the
   endpoint is out of scope of the NE:ONE server.

   An example is:

   ```shell
    curl --request POST \
    --url http://localhost:8080/logistics-objects \
    --header 'Accept: application/ld+json' \
    --header 'Content-Type: application/ld+json' \
    --data '{
      "@context": {
      "@vocab": "https://onerecord.iata.org/ns/cargo#",
      "cargo": "https://onerecord.iata.org/ns/cargo#"
    },
      "@type": "cargo:Piece",
      "handlingInstructions": [
        {
          "@type": "cargo:HandlingInstructions",
          "hasDescription": "Valuable Cargo",
          "isOfHandlingInstructionsType": "SPH",
          "isOfHandlingInstructionsTypeCode": "VAL"
        }
      ]
    }'
   ```

2. Use the `Location` response header to create an ACL entry for the created `Logistics Object` using the
   [ACL API](#authorization-api), i.e.

    ```shell
    curl --request POST \
      --url http://localhost:8080/internal/acls \
      --header 'Content-Type: application/ld+json' \
      --data '{
      "@context": {
        "@vocab": "http://www.w3.org/ns/auth/acl#",
        "acl": "http://www.w3.org/ns/auth/acl#"
      },
      "@type": "acl:Authorization",
      "agent": {
        "@id": "http://localhost:8080/logistics-objects/_data-holder"
      },
      "accessTo": {
        "@id": "[LOCATION_OF_CREATED_LOGISTICS_OBJECT]"
      },
      "mode": [
        {
          "@id": "acl:Read"
        },
        {
          "@id": "acl:Write"
        }
      ]
    }
    '
    ```

3. Use the IATA ONE Record API to interact with the Logistics Object. As the interaction through the ONE Record API
   is secured by default ensure that a JWT token obtained from the
   [provided Keycloak instance](#starting-keycloak-for-authentication) is included in every request.

# Configuration

## Configuring Subscriber Validation, Notification Forwarding, and Subscription Proposal Approval

To enable subscriber validation for notifications the property `validate-subscribers` can be enabled (defaults to
`false`). If enabled every event that happens on a logistics object will trigger a request to a third party application
which should respond with the list of partner servers to notify. The request is issued as `GET` request to the
endpoint defined as `${quarkus.rest-client.notification-client.url}/notifications/partner?loid=${loid}`.
This allows managing the servers to notify from a third party application.

The server defaults to not forward the received notifications to a third party server. In order to enable forwarding
of notifications, the property `forward-notifications` has to be set to `true`. To configure the endpoint where the
notifications should be forwarded to, the property `quarkus.rest-client.notification-client.url` has to be set to
point to the base URL of a server that accepts `POST` requests containing the received notification with the path
`/notifications` appended.

The server is configured to automatically accept all subscription proposals that expire after 60 minutes.
To change this behaviour, the property `auto-accept-subscription-proposals` property must be set to `false`.
If set to `false`, the server will send a `GET` request to a configured endpoint to approve the subscription proposal.
Use the property `quarkus.rest-client.subscription-client.url` to configure this endpoint to which the subscription
proposal is sent. This endpoint is the base URL of a web service that accepts `GET` requests with the query parameters
`topicType` and `topic` under the path `/proposal`.
To change the expiry time of automatically created subscription proposal responses, the property
`default-subscription-lifespan` can be used.

## Internal IRIs

The NE:ONE server replaces all blank nodes with internal IRIs during processing of requests. The used scheme of the
IRI is configurable using the property `lo-id-config.internal-iri-scheme`.

| Property                           | Default | Description                             |
|------------------------------------|---------|-----------------------------------------|
| `lo-id-config.internal-iri-scheme` | `neone` | The scheme for the internal IRIs to use |

The replacement takes a blank node and converts it to `<lo-id-config.internal-iri-scheme>:<UUID>`, for instance the
blank node `_:b0`, with default settings, will be converted to `neone:87d2d6b1-af8b-45de-b25c-98264a7ddd22`. All links
will be maintained.

## Using short UIDs

As an alternative to UUIDs as identifiers for entities [NanoIDs](https://github.com/ai/nanoid) may be used. The NE:ONE
server uses the default Nano ID configuration. See  the [Nano ID Collision Calculator](https://zelark.github.io/nano-id-cc/)
for further information about the collision probability. In order to use Nano ID set the `lo-id-config.random-id-strategy`
property to `nanoid`. The configuration uses the Java built in `java.util.UUID` class to generate IDs.

## Setting JSON-LD mode

JSON-LD form can be set with the property `jsonld.mode`.

| Property      | Default   | Description                                                                                                                                             |
|---------------|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| `jsonld.mode` | `compact` | The JSON-LD form to use, can be one of `compact`, `flatten` or `expand`, see also [Forms of JSON-LD](https://www.w3.org/TR/json-ld11/#forms-of-json-ld) |

## Accessing the server using mTLS

The NE:ONE server may be accessed using TLS. The default TLS port is `8443`. The `src/test/tls` folder contains all
necessary keys and certificates ready to use for **test** and **development**.

> **_WARNING:_** Do **NOT** use these files for production, use them solely for testing purposes.

In dev mode the server uses the provided keys and certificates from the `src/test/tls` folder by default. If not using
the provided files, the server can be configured using the properties `quarkus.http.ssl.certificate.key-store-file`,
`quarkus.http.ssl.certificate.key-store-password`, `quarkus.http.ssl.certificate.trust-store-file` and
`quarkus.http.ssl.certificate.trust-store-password`.

### Production mTLS configuration

| Property                                            | Default                 | Description                                         |
|-----------------------------------------------------|-------------------------|-----------------------------------------------------|
| `quarkus.http.ssl.client-auth`                      | `required`              | Deny all clients without trusted client certificate |
| `quarkus.http.ssl.certificate.trust-store-file`     | `config/truststore.p12` | Path to the truststore                              |
| `quarkus.http.ssl.certificate.trust-store-password` | `changeit`              | The truststore password                             |
| `quarkus.http.ssl.certificate.key-store-file`       | `config/keystore.p12`   | Path to the server keystore                         |
| `quarkus.http.ssl.certificate.key-store-password`   | `changeit`              | The password for the keystore                       |
| `quarkus.http.port`                                 | `0`                     | Disables the non TLS http port                      |


### Creating your own certificates

The following example uses the `openssl` and the java `keytool` binaries to create alle necessary files to run a
NE:ONE server with mTLS enabled.

1. Create a root CA capable of signing CSRs (we use the CA to sign both the server and client certificate)
   ```shell script
   openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout rootCA.key -out rootCA.crt -subj "/CN=NEONE CA"
   ```
2. Create a certificate signing request (CSR) for the server
   ```shell script
   openssl req -new -newkey rsa:4096 -nodes \
       -keyout localhost.key -out localhost.csr \
       -subj "/CN=localhost" \
       -addext "subjectAltName=DNS:localhost,IP:127.0.0.1"
   ```
3. Sign the CSR to obtain the server certificate
   ```shell script
   openssl x509 -req -CA rootCA.crt -CAkey rootCA.key \
       -in localhost.csr -out localhost.crt \
       -days 365 -CAcreateserial -copy_extensions copyall
   ```
4. Create the server keystore (password is `changeit`)
   ```shell script
   openssl pkcs12 -export -out localhost.p12 -name "localhost" \
       -inkey localhost.key -in localhost.crt \
       -passout pass:changeit
   ```
5. Create a truststore that trusts all certificates issued by the earlier created root CA (password is `changeit`)
   ```shell script
   keytool -import -file rootCA.crt -alias neoneCA -keystore truststore.p12 -storepass changeit
   ```
6. Create a certificate signing request for the client certificate
   ```shell script
   openssl req -new -newkey rsa:4096 -nodes \
       -keyout client.key -out client.csr \
       -subj "/CN=neone-client" \
       -addext "extendedKeyUsage=clientAuth"
   ```
7. Sign the client CSR with the root CA to obtain the client certificate
   ```shell script
   openssl x509 -req -CA rootCA.crt -CAkey rootCA.key \
       -in client.csr -out client.crt \
       -days 365 -CAcreateserial -copy_extensions copyall
   ```
8. Create a client keystore (password is `changeit`)
   ```shell script
   openssl pkcs12 -export -out client.p12 -name "neone-client" \
       -inkey client.key -in client.crt -passout pass:changeit
   ```

### Configure Clients

When interacting with the server the client has to be configured to use a trusted certificate. When using
[Insomnia](https://insomnia.rest) this is configured using the _Collection Settings_. For example, using the
provided certificates, follow the next steps to configure Insomnia for mTLS.

1. Open the collection settings
   ![Collection settings](documentation/assets/insomnia-collection-settings.png)
2. Switch to client certificates section to add the CA certificate and client certificate
   ![Client certificate section](documentation/assets/insomnia-collection-settings-certs1.png)
  * Click on _Choose PEM file_  and select the `rootCA.crt` file
  * Click on _New Certificate_
3. Add client certificate
   ![Add certificates](documentation/assets/insomnia-collection-settings-certs2.png)
  * Set _Host_ to `localhost:8443`
  * Click on _Choose Cert_ and select the `client.crt` file
  * Click on _Choose Key_ and select the `client.key` file
  * (On MacOS click on _Choose File_ and select the `client.p12` file and set _Passphrase_ to `changeit`)

### Testing with an untrusted client certificate

For testing the mTLS configuration with an untrusted certificate, the `src/test/tls` folder contains a self-signed
certificate that may be used as client certificate.

To create an untrusted self-signed certificate `openssl` can be used.

```shell script
# create CSR and key
openssl req -newkey rsa:4096 -nodes -keyout self-signed.key -out self-signed.csr -subj "/CN=not-known"

# use created key to sign the CSR
openssl x509 -signkey self-signed.key -in self-signed.csr -req -days 3650 -out self-signed.crt
```

### Disable mTLS

mTLS client authentication can be disabled by setting the `quarkus.http.ssl.client-auth` property to `none`. This does
not disable TLS, so the server is still serving TLS endpoint (port 8443) but does not ask for a client certificate.

# Packaging and running the application

The application can be packaged using:

```shell script
./mvnw package
```

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

# Creating a native executable

You can create a native executable using:

```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/ne-one-1.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

# Related Guides

- RESTEasy Reactive ([guide](https://quarkus.io/guides/resteasy-reactive)): A JAX-RS implementation utilizing build time
  processing and Vert.x. This extension is not compatible with the quarkus-resteasy extension, or any of the extensions
  that depend on it.

# Performance Tests

A basic JMeter performance test plan is provided. It can be used to get an idea of the required runtime environment for
your expected workload or to examine the load compatibility of a particular runtime setup.


1. Install Apache JMeter. It can freely be downloaded.
2. Open `Performace Test Plan.jmx`
3. Configure the parameters within the test plan.
4. Startup NE:ONE and all required services.
5. Start the test plan in JMeter.

![Test Plan](documentation/assets/jmeter.png)

In essence the conducted steps are:

* Set global configuration properties
* Login to the identity provider (Keycloak)
* Create a logistics object
* Fetch this logistics object
* Create subscription on the logistics object
* Modify the logistics object via change request
* Create a logistics object event
* Output the result using a couple of different views

It's recommended to adapt the test plan to your prevailing use case for meaningful results.

In order to run the test plan you have to enter the valid parameter values in the `Set global properties` test step.
You can control the generated load by adjusting the "request per second" curve in the `Throuput shaping timer` Be aware
that the `Main Thread Group` has to provide sufficient `Number of threads (users)` for the shaping timer, since it
only can limit the load, but not generate it. So increasing or decreasing the maximum number of requests per second in the shaping timer go hand in hand with a matching thread
count in the thread group.
