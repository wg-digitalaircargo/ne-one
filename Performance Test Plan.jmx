<?xml version="1.0" encoding="UTF-8"?>
<jmeterTestPlan version="1.2" properties="5.0" jmeter="5.6.2">
  <hashTree>
    <TestPlan guiclass="TestPlanGui" testclass="TestPlan" testname="Neon Performance Test Plan" enabled="true">
      <boolProp name="TestPlan.functional_mode">false</boolProp>
      <boolProp name="TestPlan.tearDown_on_shutdown">false</boolProp>
      <boolProp name="TestPlan.serialize_threadgroups">true</boolProp>
      <elementProp name="TestPlan.user_defined_variables" elementType="Arguments" guiclass="ArgumentsPanel" testclass="Arguments" testname="User Defined Variables" enabled="true">
        <collectionProp name="Arguments.arguments"/>
      </elementProp>
      <stringProp name="TestPlan.comments">Performance test</stringProp>
    </TestPlan>
    <hashTree>
      <Arguments guiclass="ArgumentsPanel" testclass="Arguments" testname="Set global properties" enabled="true">
        <collectionProp name="Arguments.arguments">
          <elementProp name="IDP_HOST" elementType="Argument">
            <stringProp name="Argument.name">IDP_HOST</stringProp>
            <stringProp name="Argument.value">localhost</stringProp>
            <stringProp name="Argument.metadata">=</stringProp>
            <stringProp name="Argument.desc">Keycloak address</stringProp>
          </elementProp>
          <elementProp name="IDP_PORT" elementType="Argument">
            <stringProp name="Argument.name">IDP_PORT</stringProp>
            <stringProp name="Argument.value">8989</stringProp>
            <stringProp name="Argument.metadata">=</stringProp>
            <stringProp name="Argument.desc">Keycloak port</stringProp>
          </elementProp>
          <elementProp name="CLIENT_ID" elementType="Argument">
            <stringProp name="Argument.name">CLIENT_ID</stringProp>
            <stringProp name="Argument.metadata">=</stringProp>
            <stringProp name="Argument.value">neone-client</stringProp>
            <stringProp name="Argument.desc">Client id for Keycloak communication</stringProp>
          </elementProp>
          <elementProp name="CLIENT_SECRET" elementType="Argument">
            <stringProp name="Argument.name">CLIENT_SECRET</stringProp>
            <stringProp name="Argument.metadata">=</stringProp>
            <stringProp name="Argument.value">lx7ThS5aYggdsMm42BP3wMrVqKm9WpNY</stringProp>
            <stringProp name="Argument.desc">Client secret for Keycloak communication</stringProp>
          </elementProp>
          <elementProp name="NEONE_HOST" elementType="Argument">
            <stringProp name="Argument.name">NEONE_HOST</stringProp>
            <stringProp name="Argument.value">localhost</stringProp>
            <stringProp name="Argument.metadata">=</stringProp>
            <stringProp name="Argument.desc">Address of Neone server</stringProp>
          </elementProp>
          <elementProp name="NEONE_PORT" elementType="Argument">
            <stringProp name="Argument.name">NEONE_PORT</stringProp>
            <stringProp name="Argument.value">8080</stringProp>
            <stringProp name="Argument.metadata">=</stringProp>
            <stringProp name="Argument.desc">Port of Neone server</stringProp>
          </elementProp>
        </collectionProp>
        <stringProp name="TestPlan.comments">Host addresses, port number etc.</stringProp>
      </Arguments>
      <hashTree/>
      <SetupThreadGroup guiclass="SetupThreadGroupGui" testclass="SetupThreadGroup" testname="setUp Thread Group" enabled="true">
        <stringProp name="ThreadGroup.on_sample_error">continue</stringProp>
        <elementProp name="ThreadGroup.main_controller" elementType="LoopController" guiclass="LoopControlPanel" testclass="LoopController" testname="Loop Controller" enabled="true">
          <stringProp name="LoopController.loops">1</stringProp>
          <boolProp name="LoopController.continue_forever">false</boolProp>
        </elementProp>
        <stringProp name="ThreadGroup.num_threads">1</stringProp>
        <stringProp name="ThreadGroup.ramp_time">0</stringProp>
        <boolProp name="ThreadGroup.scheduler">false</boolProp>
        <stringProp name="ThreadGroup.duration"></stringProp>
        <stringProp name="ThreadGroup.delay"></stringProp>
        <boolProp name="ThreadGroup.same_user_on_next_iteration">true</boolProp>
        <stringProp name="TestPlan.comments">Retrieve the access token from the identity provider and prepare to hand it over to the main thread group.</stringProp>
      </SetupThreadGroup>
      <hashTree>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="Get Access Token" enabled="true">
          <boolProp name="HTTPSampler.postBodyRaw">false</boolProp>
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" guiclass="HTTPArgumentsPanel" testclass="Arguments" testname="User Defined Variables" enabled="true">
            <collectionProp name="Arguments.arguments">
              <elementProp name="client_id" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">true</boolProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">client_id</stringProp>
                <stringProp name="Argument.value">${CLIENT_ID}</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
              </elementProp>
              <elementProp name="client_secret" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">true</boolProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">client_secret</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <stringProp name="Argument.value">${CLIENT_SECRET}</stringProp>
              </elementProp>
              <elementProp name="grant_type" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">true</boolProp>
                <stringProp name="Argument.metadata">=</stringProp>
                <boolProp name="HTTPArgument.use_equals">true</boolProp>
                <stringProp name="Argument.name">grant_type</stringProp>
                <stringProp name="Argument.value">client_credentials</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.domain">${IDP_HOST}</stringProp>
          <stringProp name="HTTPSampler.port">${IDP_PORT}</stringProp>
          <stringProp name="HTTPSampler.protocol">http</stringProp>
          <stringProp name="HTTPSampler.path">/realms/neone/protocol/openid-connect/token</stringProp>
          <stringProp name="HTTPSampler.method">POST</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.BROWSER_COMPATIBLE_MULTIPART">false</boolProp>
          <boolProp name="HTTPSampler.image_parser">false</boolProp>
          <boolProp name="HTTPSampler.concurrentDwn">false</boolProp>
          <stringProp name="HTTPSampler.concurrentPool">6</stringProp>
          <boolProp name="HTTPSampler.md5">false</boolProp>
          <intProp name="HTTPSampler.ipSourceType">0</intProp>
        </HTTPSamplerProxy>
        <hashTree>
          <HeaderManager guiclass="HeaderPanel" testclass="HeaderManager" testname="HTTP Header Manager" enabled="true">
            <collectionProp name="HeaderManager.headers">
              <elementProp name="" elementType="Header">
                <stringProp name="Header.name">Content-Type</stringProp>
                <stringProp name="Header.value">application/x-www-form-urlencoded</stringProp>
              </elementProp>
            </collectionProp>
          </HeaderManager>
          <hashTree/>
        </hashTree>
        <JSONPostProcessor guiclass="JSONPostProcessorGui" testclass="JSONPostProcessor" testname="Extract access token" enabled="true">
          <stringProp name="JSONPostProcessor.referenceNames">accessToken</stringProp>
          <stringProp name="JSONPostProcessor.jsonPathExprs">$.access_token</stringProp>
          <stringProp name="JSONPostProcessor.match_numbers">0</stringProp>
          <stringProp name="Scope.variable">bearerToken</stringProp>
        </JSONPostProcessor>
        <hashTree/>
        <BeanShellAssertion guiclass="BeanShellAssertionGui" testclass="BeanShellAssertion" testname="Set bearerToken property" enabled="true">
          <stringProp name="TestPlan.comments">Property to be used in the main thread group</stringProp>
          <stringProp name="BeanShellAssertion.query">${__setProperty(&quot;bearerToken&quot;,${accessToken})}</stringProp>
          <stringProp name="BeanShellAssertion.filename"></stringProp>
          <stringProp name="BeanShellAssertion.parameters"></stringProp>
          <boolProp name="BeanShellAssertion.resetInterpreter">false</boolProp>
        </BeanShellAssertion>
        <hashTree/>
      </hashTree>
      <ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" testname="Main Thread Group" enabled="true">
        <stringProp name="ThreadGroup.on_sample_error">continue</stringProp>
        <elementProp name="ThreadGroup.main_controller" elementType="LoopController" guiclass="LoopControlPanel" testclass="LoopController" testname="Loop Controller" enabled="true">
          <intProp name="LoopController.loops">-1</intProp>
          <boolProp name="LoopController.continue_forever">false</boolProp>
        </elementProp>
        <stringProp name="ThreadGroup.num_threads">50</stringProp>
        <stringProp name="ThreadGroup.ramp_time">0</stringProp>
        <boolProp name="ThreadGroup.delayedStart">false</boolProp>
        <boolProp name="ThreadGroup.scheduler">false</boolProp>
        <stringProp name="ThreadGroup.duration"></stringProp>
        <stringProp name="ThreadGroup.delay"></stringProp>
        <boolProp name="ThreadGroup.same_user_on_next_iteration">true</boolProp>
        <stringProp name="TestPlan.comments">Fetches the access token from the setup thread group. Conducts a series of NEONE requests.</stringProp>
      </ThreadGroup>
      <hashTree>
        <kg.apc.jmeter.timers.VariableThroughputTimer guiclass="kg.apc.jmeter.timers.VariableThroughputTimerGui" testclass="kg.apc.jmeter.timers.VariableThroughputTimer" testname="jp@gc - Throughput Shaping Timer" enabled="true">
          <collectionProp name="load_profile">
            <collectionProp name="1815798158">
              <stringProp name="1">1</stringProp>
              <stringProp name="1598">20</stringProp>
              <stringProp name="1598">20</stringProp>
            </collectionProp>
            <collectionProp name="-238471552">
              <stringProp name="1598">20</stringProp>
              <stringProp name="1598">20</stringProp>
              <stringProp name="1722">60</stringProp>
            </collectionProp>
            <collectionProp name="-1971522920">
              <stringProp name="1598">20</stringProp>
              <stringProp name="49">1</stringProp>
              <stringProp name="1598">20</stringProp>
            </collectionProp>
          </collectionProp>
        </kg.apc.jmeter.timers.VariableThroughputTimer>
        <hashTree/>
        <HeaderManager guiclass="HeaderPanel" testclass="HeaderManager" testname="HTTP Header Manager" enabled="true">
          <collectionProp name="HeaderManager.headers">
            <elementProp name="" elementType="Header">
              <stringProp name="Header.name">Accept</stringProp>
              <stringProp name="Header.value">application/ld+json</stringProp>
            </elementProp>
            <elementProp name="" elementType="Header">
              <stringProp name="Header.name">Content-Type</stringProp>
              <stringProp name="Header.value">application/ld+json</stringProp>
            </elementProp>
            <elementProp name="" elementType="Header">
              <stringProp name="Header.name">Authorization</stringProp>
              <stringProp name="Header.value">Bearer ${__property(&quot;bearerToken&quot;)}</stringProp>
            </elementProp>
          </collectionProp>
          <stringProp name="TestPlan.comments">Header for all NEONE requests in the main thread group.</stringProp>
        </HeaderManager>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="Create LogisticsObject (Piece)" enabled="true">
          <boolProp name="HTTPSampler.postBodyRaw">true</boolProp>
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments">
            <collectionProp name="Arguments.arguments">
              <elementProp name="" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value">{&#xd;
    &quot;@context&quot;: {&#xd;
			  &quot;@vocab&quot;: &quot;https://onerecord.iata.org/ns/cargo#&quot;,&#xd;
        &quot;cargo&quot;: &quot;https://onerecord.iata.org/ns/cargo#&quot;&#xd;
    },&#xd;
    &quot;@type&quot;: &quot;cargo:Piece&quot;,&#xd;
    &quot;hasHandlingInstructions&quot;: [&#xd;
        {&#xd;
            &quot;@type&quot;: &quot;cargo:HandlingInstructions&quot;,            &#xd;
            &quot;hasDescription&quot;: &quot;Valuable Cargo&quot;,&#xd;
            &quot;isOfHandlingInstructionsType&quot;: &quot;SPA&quot;,&#xd;
            &quot;isOfHandlingInstructionsTypeCode&quot;: &quot;VAL&quot;&#xd;
        }&#xd;
    ]&#xd;
}&#xd;
</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.domain">${NEONE_HOST}</stringProp>
          <stringProp name="HTTPSampler.port">${NEONE_PORT}</stringProp>
          <stringProp name="HTTPSampler.protocol">http</stringProp>
          <stringProp name="HTTPSampler.path">/logistics-objects</stringProp>
          <stringProp name="HTTPSampler.method">POST</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.BROWSER_COMPATIBLE_MULTIPART">false</boolProp>
          <boolProp name="HTTPSampler.image_parser">false</boolProp>
          <boolProp name="HTTPSampler.concurrentDwn">false</boolProp>
          <stringProp name="HTTPSampler.concurrentPool">6</stringProp>
          <boolProp name="HTTPSampler.md5">false</boolProp>
          <intProp name="HTTPSampler.ipSourceType">0</intProp>
        </HTTPSamplerProxy>
        <hashTree>
          <RegexExtractor guiclass="RegexExtractorGui" testclass="RegexExtractor" testname="Extract location header" enabled="true">
            <stringProp name="RegexExtractor.useHeaders">true</stringProp>
            <boolProp name="RegexExtractor.default_empty_value">true</boolProp>
            <stringProp name="RegexExtractor.refname">location</stringProp>
            <stringProp name="RegexExtractor.regex">Location: (.*?)\n</stringProp>
            <stringProp name="RegexExtractor.template">$1$</stringProp>
            <stringProp name="RegexExtractor.match_number">0</stringProp>
          </RegexExtractor>
          <hashTree/>
        </hashTree>
        <JSR223Sampler guiclass="TestBeanGUI" testclass="JSR223Sampler" testname="Set loIRI from location" enabled="true">
          <stringProp name="scriptLanguage">groovy</stringProp>
          <stringProp name="parameters"></stringProp>
          <stringProp name="filename"></stringProp>
          <stringProp name="cacheKey">true</stringProp>
          <stringProp name="script">vars.put(&quot;loIRI&quot;, vars.get(&quot;location&quot;))</stringProp>
        </JSR223Sampler>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="Get LogisticsObject" enabled="true">
          <boolProp name="HTTPSampler.postBodyRaw">false</boolProp>
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments" guiclass="HTTPArgumentsPanel" testclass="Arguments" testname="User Defined Variables" enabled="true">
            <collectionProp name="Arguments.arguments"/>
          </elementProp>
          <stringProp name="HTTPSampler.path">${loIRI}</stringProp>
          <stringProp name="HTTPSampler.method">GET</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.BROWSER_COMPATIBLE_MULTIPART">false</boolProp>
          <boolProp name="HTTPSampler.image_parser">false</boolProp>
          <boolProp name="HTTPSampler.concurrentDwn">false</boolProp>
          <stringProp name="HTTPSampler.concurrentPool">6</stringProp>
          <boolProp name="HTTPSampler.md5">false</boolProp>
          <intProp name="HTTPSampler.ipSourceType">0</intProp>
          <stringProp name="TestPlan.comments">Get Logistics Object previously created in step &quot;Create LogisticsObject (Piece)&quot;</stringProp>
        </HTTPSamplerProxy>
        <hashTree>
          <RegexExtractor guiclass="RegexExtractorGui" testclass="RegexExtractor" testname="Extract revision number" enabled="true">
            <stringProp name="RegexExtractor.useHeaders">true</stringProp>
            <boolProp name="RegexExtractor.default_empty_value">true</boolProp>
            <stringProp name="RegexExtractor.refname">revision</stringProp>
            <stringProp name="RegexExtractor.regex">Latest-Revision: (.*?)\n</stringProp>
            <stringProp name="RegexExtractor.template">$1$</stringProp>
            <stringProp name="RegexExtractor.match_number">0</stringProp>
          </RegexExtractor>
          <hashTree/>
        </hashTree>
        <JSR223Sampler guiclass="TestBeanGUI" testclass="JSR223Sampler" testname="Set latest revision from revision" enabled="true">
          <stringProp name="scriptLanguage">groovy</stringProp>
          <stringProp name="parameters"></stringProp>
          <stringProp name="filename"></stringProp>
          <stringProp name="cacheKey">true</stringProp>
          <stringProp name="script">vars.put(&quot;latestRevision&quot;, vars.get(&quot;revision&quot;))</stringProp>
        </JSR223Sampler>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="Create Subscription" enabled="true">
          <boolProp name="HTTPSampler.postBodyRaw">true</boolProp>
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments">
            <collectionProp name="Arguments.arguments">
              <elementProp name="" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value">{&#xd;
    &quot;@context&quot;: {&#xd;
      &quot;cargo&quot;: &quot;https://onerecord.iata.org/ns/cargo#&quot;,&#xd;
      &quot;api&quot;: &quot;https://onerecord.iata.org/ns/api#&quot;,&#xd;
			&quot;xsd&quot;: &quot;http://www.w3.org/2001/XMLSchema#&quot;&#xd;
    },    &#xd;
    &quot;@type&quot;: &quot;api:Subscription&quot;,&#xd;
    &quot;api:hasContentType&quot;: &quot;application/ld+json&quot;,&#xd;
    &quot;api:hasSubscriber&quot;: {&#xd;
      &quot;@id&quot;: &quot;http://${NEONE_HOST}:${NEONE_PORT}/&quot;&#xd;
    },&#xd;
    &quot;api:hasTopicType&quot;: {&#xd;
      &quot;@id&quot;: &quot;api:LOGISTICS_OBJECT_IDENTIFIER&quot;&#xd;
    },&#xd;
    &quot;api:hasTopic&quot;: {&#xd;
      &quot;@id&quot;: &quot;${loIRI}&quot;&#xd;
    },&#xd;
    &quot;api:sendLogisticsObjectBody&quot;: true,&#xd;
	  &quot;https://onerecord.iata.org/ns/api#expiresAt&quot;: &quot;2024-10-30T11:48:00Z&quot;&#xd;
  }</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.domain">${NEONE_HOST}</stringProp>
          <stringProp name="HTTPSampler.port">${NEONE_PORT}</stringProp>
          <stringProp name="HTTPSampler.protocol">http</stringProp>
          <stringProp name="HTTPSampler.path">/subscriptions</stringProp>
          <stringProp name="HTTPSampler.method">POST</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.BROWSER_COMPATIBLE_MULTIPART">false</boolProp>
          <boolProp name="HTTPSampler.image_parser">false</boolProp>
          <boolProp name="HTTPSampler.concurrentDwn">false</boolProp>
          <stringProp name="HTTPSampler.concurrentPool">6</stringProp>
          <boolProp name="HTTPSampler.md5">false</boolProp>
          <intProp name="HTTPSampler.ipSourceType">0</intProp>
          <stringProp name="TestPlan.comments">Create subscription on previously created logistcs object</stringProp>
        </HTTPSamplerProxy>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="Create Change Request (add coload to piece)" enabled="true">
          <boolProp name="HTTPSampler.postBodyRaw">true</boolProp>
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments">
            <collectionProp name="Arguments.arguments">
              <elementProp name="" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value">{&#xd;
    &quot;@context&quot;: {&#xd;
			  &quot;@vocab&quot;: &quot;https://onerecord.iata.org/ns/api#&quot;,&#xd;
        &quot;cargo&quot;: &quot;https://onerecord.iata.org/ns/cargo#&quot;,&#xd;
        &quot;api&quot;: &quot;https://onerecord.iata.org/ns/api#&quot;&#xd;
    },&#xd;
    &quot;@type&quot;: &quot;api:Change&quot;,&#xd;
    &quot;hasLogisticsObject&quot;: {&#xd;
        &quot;@id&quot;: &quot;${loIRI}&quot;&#xd;
    },&#xd;
    &quot;hasDescription&quot;: &quot;Add canBeColoaded&quot;,&#xd;
    &quot;hasOperation&quot;: {&#xd;
        &quot;@type&quot;: &quot;api:Operation&quot;,&#xd;
        &quot;op&quot;: {&#xd;
            &quot;@id&quot;: &quot;api:ADD&quot;&#xd;
        },&#xd;
        &quot;s&quot;: &quot;${loIRI}&quot;,&#xd;
        &quot;p&quot;: &quot;https://onerecord.iata.org/ns/cargo#canBeColoaded&quot;,&#xd;
        &quot;o&quot;: {&#xd;
            &quot;@type&quot;: &quot;api:OperationObject&quot;,&#xd;
            &quot;hasDatatype&quot;: &quot;http://www.w3.org/2001/XMLSchema#boolean&quot;,&#xd;
            &quot;hasValue&quot;: true&#xd;
        }&#xd;
    },&#xd;
    &quot;hasRevision&quot;: ${latestRevision}&#xd;
}</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.path">${loIRI}</stringProp>
          <stringProp name="HTTPSampler.method">PATCH</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.BROWSER_COMPATIBLE_MULTIPART">false</boolProp>
          <boolProp name="HTTPSampler.image_parser">false</boolProp>
          <boolProp name="HTTPSampler.concurrentDwn">false</boolProp>
          <stringProp name="HTTPSampler.concurrentPool">6</stringProp>
          <boolProp name="HTTPSampler.md5">false</boolProp>
          <intProp name="HTTPSampler.ipSourceType">0</intProp>
        </HTTPSamplerProxy>
        <hashTree/>
        <HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="Create LO Event" enabled="true">
          <boolProp name="HTTPSampler.postBodyRaw">true</boolProp>
          <elementProp name="HTTPsampler.Arguments" elementType="Arguments">
            <collectionProp name="Arguments.arguments">
              <elementProp name="" elementType="HTTPArgument">
                <boolProp name="HTTPArgument.always_encode">false</boolProp>
                <stringProp name="Argument.value">{        &#xd;
    &quot;@context&quot;: {&#xd;
        &quot;cargo&quot;: &quot;https://onerecord.iata.org/ns/cargo#&quot;&#xd;
    },&#xd;
		&quot;@type&quot;: &quot;https://onerecord.iata.org/Event&quot;,    &#xd;
    &quot;cargo:eventDate&quot;: &quot;2023-07-20T10:38:01.000Z&quot;,&#xd;
    &quot;cargo:eventCode&quot;: &quot;DEP&quot;,&#xd;
    &quot;cargo:eventName&quot;: &quot;Consignment departed on a specific flight&quot;,&#xd;
    &quot;cargo:eventTimeType&quot;: &quot;Actual&quot;,&#xd;
    &quot;cargo:linkedObject&quot;: {&#xd;
        &quot;@type&quot;: &quot;cargo:Piece&quot;,&#xd;
        &quot;@id&quot;: &quot;${loIRI}&quot;&#xd;
    },&#xd;
    &quot;cargo:recordedBy&quot;: {&#xd;
        &quot;@type&quot;: &quot;https://onerecord.iata.org/Company&quot;,&#xd;
        &quot;@id&quot;: &quot;http://${NEONE_HOST}:${NEONE_PORT}/logistics-objects/_data-holder&quot;&#xd;
    },&#xd;
      &quot;cargo:recordedByActor&quot;: {&#xd;
        &quot;@type&quot;: &quot;https://onerecord.iata.org/Person&quot;,&#xd;
        &quot;@id&quot;: &quot;http://${NEONE_HOST}:${NEONE_PORT}/logistics-objects/_data-holder&quot;&#xd;
    }&#xd;
}&#xd;
</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
              </elementProp>
            </collectionProp>
          </elementProp>
          <stringProp name="HTTPSampler.path">${loIRI}/logistics-events</stringProp>
          <stringProp name="HTTPSampler.method">POST</stringProp>
          <boolProp name="HTTPSampler.follow_redirects">true</boolProp>
          <boolProp name="HTTPSampler.auto_redirects">false</boolProp>
          <boolProp name="HTTPSampler.use_keepalive">true</boolProp>
          <boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>
          <boolProp name="HTTPSampler.BROWSER_COMPATIBLE_MULTIPART">false</boolProp>
          <boolProp name="HTTPSampler.image_parser">false</boolProp>
          <boolProp name="HTTPSampler.concurrentDwn">false</boolProp>
          <stringProp name="HTTPSampler.concurrentPool">6</stringProp>
          <boolProp name="HTTPSampler.md5">false</boolProp>
          <intProp name="HTTPSampler.ipSourceType">0</intProp>
        </HTTPSamplerProxy>
        <hashTree/>
      </hashTree>
      <ResultCollector guiclass="RespTimeGraphVisualizer" testclass="ResultCollector" testname="Response Time Graph" enabled="true">
        <boolProp name="ResultCollector.error_logging">false</boolProp>
        <objProp>
          <name>saveConfig</name>
          <value class="SampleSaveConfiguration">
            <time>true</time>
            <latency>true</latency>
            <timestamp>true</timestamp>
            <success>true</success>
            <label>true</label>
            <code>true</code>
            <message>true</message>
            <threadName>true</threadName>
            <dataType>true</dataType>
            <encoding>false</encoding>
            <assertions>true</assertions>
            <subresults>true</subresults>
            <responseData>false</responseData>
            <samplerData>false</samplerData>
            <xml>false</xml>
            <fieldNames>true</fieldNames>
            <responseHeaders>false</responseHeaders>
            <requestHeaders>false</requestHeaders>
            <responseDataOnError>false</responseDataOnError>
            <saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>
            <assertionsResultsToSave>0</assertionsResultsToSave>
            <bytes>true</bytes>
            <sentBytes>true</sentBytes>
            <url>true</url>
            <threadCounts>true</threadCounts>
            <idleTime>true</idleTime>
            <connectTime>true</connectTime>
          </value>
        </objProp>
        <stringProp name="filename"></stringProp>
      </ResultCollector>
      <hashTree/>
      <ResultCollector guiclass="StatGraphVisualizer" testclass="ResultCollector" testname="Aggregate Graph" enabled="true">
        <boolProp name="ResultCollector.error_logging">false</boolProp>
        <objProp>
          <name>saveConfig</name>
          <value class="SampleSaveConfiguration">
            <time>true</time>
            <latency>true</latency>
            <timestamp>true</timestamp>
            <success>true</success>
            <label>true</label>
            <code>true</code>
            <message>true</message>
            <threadName>true</threadName>
            <dataType>true</dataType>
            <encoding>false</encoding>
            <assertions>true</assertions>
            <subresults>true</subresults>
            <responseData>false</responseData>
            <samplerData>false</samplerData>
            <xml>false</xml>
            <fieldNames>true</fieldNames>
            <responseHeaders>false</responseHeaders>
            <requestHeaders>false</requestHeaders>
            <responseDataOnError>false</responseDataOnError>
            <saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>
            <assertionsResultsToSave>0</assertionsResultsToSave>
            <bytes>true</bytes>
            <sentBytes>true</sentBytes>
            <url>true</url>
            <threadCounts>true</threadCounts>
            <idleTime>true</idleTime>
            <connectTime>true</connectTime>
          </value>
        </objProp>
        <stringProp name="filename"></stringProp>
      </ResultCollector>
      <hashTree/>
      <ResultCollector guiclass="ViewResultsFullVisualizer" testclass="ResultCollector" testname="View Results Tree" enabled="true">
        <boolProp name="ResultCollector.error_logging">false</boolProp>
        <objProp>
          <name>saveConfig</name>
          <value class="SampleSaveConfiguration">
            <time>true</time>
            <latency>true</latency>
            <timestamp>true</timestamp>
            <success>true</success>
            <label>true</label>
            <code>true</code>
            <message>true</message>
            <threadName>true</threadName>
            <dataType>true</dataType>
            <encoding>false</encoding>
            <assertions>true</assertions>
            <subresults>true</subresults>
            <responseData>false</responseData>
            <samplerData>false</samplerData>
            <xml>false</xml>
            <fieldNames>true</fieldNames>
            <responseHeaders>false</responseHeaders>
            <requestHeaders>false</requestHeaders>
            <responseDataOnError>false</responseDataOnError>
            <saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>
            <assertionsResultsToSave>0</assertionsResultsToSave>
            <bytes>true</bytes>
            <sentBytes>true</sentBytes>
            <url>true</url>
            <threadCounts>true</threadCounts>
            <idleTime>true</idleTime>
            <connectTime>true</connectTime>
          </value>
        </objProp>
        <stringProp name="filename"></stringProp>
      </ResultCollector>
      <hashTree/>
    </hashTree>
  </hashTree>
</jmeterTestPlan>
