// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone;

import io.quarkus.redis.datasource.codecs.Codecs;
import io.quarkus.runtime.StartupEvent;
import io.vertx.mutiny.core.eventbus.EventBus;
import io.vertx.mutiny.core.eventbus.MessageConsumer;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.service.EventService;
import org.openlogisticsfoundation.neone.service.internal.BlobStoreConfig;
import org.openlogisticsfoundation.neone.service.internal.InitializationService;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectBlobStoreService;
import org.openlogisticsfoundation.neone.service.internal.NeoneIdCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;

@ApplicationScoped
public class NeoneStartup {

    private static final Logger log = LoggerFactory.getLogger(NeoneStartup.class);

    private final EventService eventService;

    private final InitializationService initializationService;

    private final BlobStoreConfig blobStoreConfig;

    private final LogisticsObjectBlobStoreService blobStoreService;

    private final EventBus eventBus;

    @Inject
    public NeoneStartup(EventService eventService, InitializationService initializationService,
                        BlobStoreConfig blobStoreConfig, LogisticsObjectBlobStoreService blobStoreService,
                        EventBus eventBus) {
        this.eventService = eventService;
        this.initializationService = initializationService;
        this.blobStoreConfig = blobStoreConfig;
        this.blobStoreService = blobStoreService;
        this.eventBus = eventBus;
    }

    void onStart(@Observes StartupEvent event) {
        log.info("Pausing LO event consumer until initialisation finished");
        MessageConsumer<Object> loEventConsumer = eventBus.consumer(NeoneEvent.ADDRESS).pause();

        // workaround for https://github.com/quarkusio/quarkus/issues/35661
        Codecs.register(NeoneIdCodec.getNeoneIdCodec());

        eventService.triggerMissedEvents();
        initializationService.initializeServerInformation();
        if (blobStoreConfig.createBucket()) {
            log.info("Creating bucket");
            blobStoreService.createBucket(blobStoreConfig.bucketName());
        }

        log.info("Resuming LO event consumer");
        loEventConsumer.resume();
    }
}
