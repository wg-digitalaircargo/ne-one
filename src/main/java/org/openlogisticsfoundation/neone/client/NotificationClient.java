// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.client;

import org.openlogisticsfoundation.neone.controller.NotificationMessage;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

public interface NotificationClient {

    @GET
    @Path("/notifications/partner")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    CompanyIds shouldNotify(@QueryParam("loid") String logisticsObjectId);

    @POST
    @Path("/notifications")
    @Consumes({"application/ld+json"})
    void notify(NotificationMessage notification);
}
