// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.client;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/proposal/")
@Produces(MediaType.APPLICATION_JSON)
public interface SubscriptionClient {

    @GET
    ProposalResponse handleSubscription(@QueryParam("topicType") String topicType,
                                        @QueryParam("topic") String topic);
}
