// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.client;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipse.rdf4j.model.Model;
import org.openlogisticsfoundation.neone.controller.ModelBodyHandler;
import org.openlogisticsfoundation.neone.controller.NotificationMessage;
import org.openlogisticsfoundation.neone.model.Subscription;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

@Consumes(ModelBodyHandler.APPLICATION_LD_JSON)
@OidcClientFilter
@RegisterProvider(OneRecordClientExceptionMapper.class)
@RegisterRestClient(configKey = "one-record-client")
public interface OneRecordClient {

    @GET
    @Path("/subscriptions")
    Subscription proposeSubscription(@QueryParam("topic") String topic, @QueryParam("topicType") String topicType);

    @POST
    @Path("/notifications")
    Response notifySubscriber(NotificationMessage notification);

    @GET
    @Path("/logistics-objects/{id}")
    Uni<Model> getLogisticsObject(@PathParam("id") String id);
}
