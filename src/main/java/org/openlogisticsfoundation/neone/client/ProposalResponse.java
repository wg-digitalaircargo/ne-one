// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.client;

import java.time.Instant;

public record ProposalResponse(boolean subscribe,
                               boolean sendLoBody,
                               String includeEventType,
                               Instant expiresAt) {
}
