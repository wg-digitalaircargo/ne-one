//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.client;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;
import org.openlogisticsfoundation.neone.model.ErrorDetail;

import jakarta.ws.rs.core.Response;
import java.util.Collections;
import java.util.Optional;

public abstract class OneRecordClientException extends RuntimeException {

    public static final String ERROR_PREFIX = "1R_CLIENT_";

    private final IRI errorIri;
    private final IRI errorDetailIri;
    private final String errorMessage;
    private final IRI resource;

    public OneRecordClientException(IRI errorIri, IRI errorDetailIri) {
        this(errorIri, errorDetailIri, null, null);
    }

    public OneRecordClientException(IRI errorIri, IRI errorDetailIri, String errorMessage) {
        this(errorIri, errorDetailIri, errorMessage, null);
    }

    public OneRecordClientException(IRI errorIri, IRI errorDetailIri, String errorMessage, IRI resource) {
        this.errorIri = errorIri;
        this.errorDetailIri = errorDetailIri;
        this.errorMessage = errorMessage;
        this.resource = resource;
    }

    public IRI getErrorIri() {
        return errorIri;
    }

    public IRI getErrorDetailIri() {
        return errorDetailIri;
    }

    public Optional<String> getErrorMessage() {
        return Optional.ofNullable(errorMessage);
    }

    public Optional<IRI> getResource() {
        return Optional.ofNullable(resource);
    }

    protected abstract String getCode();

    protected abstract Response.Status getResponseStatus();

    public Error asError(String title) {
        ErrorDetail errorDetail = new ErrorDetail(getErrorDetailIri(), ERROR_PREFIX + getCode(),
                getErrorMessage(), Optional.empty(), getResource()
        );
        return new Error(getErrorIri(), title, Collections.singleton(errorDetail));
    }

    public Response asErrorResponse(String title) {
        return Response.status(getResponseStatus()).entity(asError(title)).build();
    }

    public static OneRecordClientException requestFailed(IdProvider idProvider, Response response) {
        return new RequestFailedException(
                idProvider.createInternalIri().getIri(),
                idProvider.createInternalIri().getIri(),
                response.getStatus()
        );
    }

    public static LogisticsObjectNotFoundException notFound(IdProvider idProvider) {
        return new LogisticsObjectNotFoundException(
                idProvider.createInternalIri().getIri(),
                idProvider.createInternalIri().getIri()
        );
    }

    public static UnauthorizedException unauthorized(IdProvider idProvider) {
        return new UnauthorizedException(
                idProvider.createInternalIri().getIri(),
                idProvider.createInternalIri().getIri()
        );
    }

    public static UnauthenticatedException unauthenticated(IdProvider idProvider) {
        return new UnauthenticatedException(
                idProvider.createInternalIri().getIri(),
                idProvider.createInternalIri().getIri()
        );
    }


    public static class RequestFailedException extends OneRecordClientException {

        private static final String message = "The request to the upstream server failed with status [%s]";

        public RequestFailedException(IRI errorIri, IRI errorDetailIri, int status) {
            super(errorIri, errorDetailIri, message.formatted(status));
        }

        @Override
        protected String getCode() {
            return "10000";
        }

        @Override
        protected Response.Status getResponseStatus() {
            return Response.Status.INTERNAL_SERVER_ERROR;
        }
    }

    public static class LogisticsObjectNotFoundException extends OneRecordClientException {

        private static final String message = "LogisticsObject not found on upstream server";
        public LogisticsObjectNotFoundException(IRI errorIri, IRI errorDetailIri) {
            super(errorIri, errorDetailIri, message);
        }

        @Override
        protected String getCode() {
            return "10001";
        }

        @Override
        protected Response.Status getResponseStatus() {
            return Response.Status.INTERNAL_SERVER_ERROR;
        }
    }

    public static class UnauthorizedException extends OneRecordClientException {

        private static final String message = "Unauthorized to view requested LogisticsObject on upstream server";

        private UnauthorizedException(IRI errorIri, IRI errorDetailIri) {
            super(errorIri, errorDetailIri, message);
        }

        @Override
        protected String getCode() {
            return "10002";
        }

        @Override
        protected Response.Status getResponseStatus() {
            return Response.Status.FORBIDDEN;
        }
    }

    public static class UnauthenticatedException extends OneRecordClientException {

        private static final String message = "Upstream server could not authenticate the request";

        private UnauthenticatedException(IRI errorIri, IRI errorDetailIri) {
            super(errorIri, errorDetailIri, message);
        }

        @Override
        protected String getCode() {
            return "10003";
        }

        @Override
        protected Response.Status getResponseStatus() {
            return Response.Status.UNAUTHORIZED;
        }
    }
}
