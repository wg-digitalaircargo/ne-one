// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.client;

import io.quarkus.rest.client.reactive.runtime.RestClientBuilderImpl;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RestClientBuilderProducer {

    public RestClientBuilder restClientBuilder() {
        return new RestClientBuilderImpl();
    }
}
