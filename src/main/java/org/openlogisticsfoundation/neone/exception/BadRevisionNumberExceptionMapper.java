//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class BadRevisionNumberExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<BadRevisionNumberException> {

    @Inject
    protected BadRevisionNumberExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(BadRevisionNumberException exception) {
        var status = 422;
        var title = "Unprocessable: Bad revision number.";
        var message = String.valueOf(exception.badRevisionNumber);

        var error = Error.createError(errorIri(), title, errorDetailIri(), String.valueOf(status), message);
        return Response.status(status, title).entity(error).build();
    }
}
