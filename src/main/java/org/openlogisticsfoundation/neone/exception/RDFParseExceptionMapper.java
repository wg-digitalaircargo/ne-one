//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.eclipse.rdf4j.rio.RDFParseException;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class RDFParseExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<RDFParseException> {

    @Inject
    protected RDFParseExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(RDFParseException exception) {
        var status = Response.Status.BAD_REQUEST; // 400
        var title = "Invalid RDF model";
        var error = Error.createError(errorIri(), title);

        return Response.status(status).entity(error).build();
    }
}
