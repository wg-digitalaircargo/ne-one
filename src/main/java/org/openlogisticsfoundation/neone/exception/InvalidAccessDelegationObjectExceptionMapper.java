//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import java.util.stream.Collectors;

@Provider
public class InvalidAccessDelegationObjectExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<InvalidAccessDelegationObjectException> {

    @Inject
    protected InvalidAccessDelegationObjectExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(InvalidAccessDelegationObjectException e) {
        var status = Response.Status.BAD_REQUEST; // 400
        var title = "Invalid Access Delegation object(s).";
        var message = e.loIris.stream().map(IRI::stringValue).collect(Collectors.joining(", "));

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        return Response.status(status).entity(error).build();
    }
}
