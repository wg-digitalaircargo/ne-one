//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

public class InvalidLogisticsObjectTypeException extends RuntimeException {

    private final String logisticsObject;

    private final String actual;

    private final String expected;

    public InvalidLogisticsObjectTypeException(String logisticsObject, String actual, String expected) {
        this.logisticsObject = logisticsObject;
        this.actual = actual;
        this.expected = expected;
    }

    public String getLogisticsObject() {
        return logisticsObject;
    }

    public String getActual() {
        return actual;
    }

    public String getExpected() {
        return expected;
    }
}
