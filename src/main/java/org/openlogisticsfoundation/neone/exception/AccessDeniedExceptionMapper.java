//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class AccessDeniedExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<AccessDeniedException> {

    @Inject
    protected AccessDeniedExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(AccessDeniedException e) {
        var status = Response.Status.FORBIDDEN; // 403
        var title = "Access Denied";
        String message;
        if (e.agent.isPresent()) {
            message = e.agent.map(IRI::toString).orElse("") + " has no access to " + e.accessTo.orElse(null);
        } else {
            message = "Not authenticated";
        }

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        return Response.status(status).entity(error).build();
    }
}
