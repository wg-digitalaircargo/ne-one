//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

public class InvalidNotificationException extends RuntimeException {

    public InvalidNotificationException(String msg) {
        super(msg);
    }
}
