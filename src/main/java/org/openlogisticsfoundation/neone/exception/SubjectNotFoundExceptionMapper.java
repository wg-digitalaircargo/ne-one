//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class SubjectNotFoundExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<SubjectNotFoundException> {

    @Inject
    public SubjectNotFoundExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(SubjectNotFoundException e) {
        var status = Response.Status.NOT_FOUND; // 404
        var title = "Subject not found";
        var message = e.getSubject();

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        return Response.status(status).entity(error).build();
    }
}
