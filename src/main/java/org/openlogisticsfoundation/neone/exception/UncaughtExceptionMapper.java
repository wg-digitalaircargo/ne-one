//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
public class UncaughtExceptionMapper extends BaseExceptionMapper implements ExceptionMapper<Throwable> {

    private final Logger log = LoggerFactory.getLogger(UncaughtExceptionMapper.class);

    private final UriInfo uriInfo;

    @Inject
    protected UncaughtExceptionMapper(IdProvider idProvider, @Context UriInfo uriInfo) {
        super(idProvider);
        this.uriInfo = uriInfo;
    }

    @Override
    public Response toResponse(Throwable exception) {
        log.error("Uncaught exception for path [{}]", uriInfo.getAbsolutePath(), exception);
        var title = "Internal server error";

        var error = Error.createError(errorIri(), title);
        return Response.serverError().entity(error).build();
    }
}
