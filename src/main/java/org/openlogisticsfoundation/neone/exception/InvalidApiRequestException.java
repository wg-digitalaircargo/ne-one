//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

public class InvalidApiRequestException extends RuntimeException {

    public InvalidApiRequestException() {
    }

    public InvalidApiRequestException(String message) {
        super(message);
    }
}
