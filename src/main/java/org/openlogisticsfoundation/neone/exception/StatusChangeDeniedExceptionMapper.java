//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class StatusChangeDeniedExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<StatusChangeDeniedException> {

    @Inject
    protected StatusChangeDeniedExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(StatusChangeDeniedException e) {
        var status = Response.Status.NOT_ACCEPTABLE; // 406
        var title = "Action request not longer PENDING";
        var message = e.getActionRequestIri() != null ? e.getActionRequestIri().toString() : "";

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        return Response.status(status).entity(error).build();
    }
}
