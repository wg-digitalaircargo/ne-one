//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class InvalidLogisticsObjectTypeExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<InvalidLogisticsObjectTypeException> {


    protected InvalidLogisticsObjectTypeExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(InvalidLogisticsObjectTypeException exception) {
        var status = Response.Status.BAD_REQUEST; // 400
        var title = "Invalid logistics object type";
        var message = """
            LogisticsObject [%s] of type [%s], expected [%s]
            """.formatted(exception.getLogisticsObject(), exception.getActual(), exception.getExpected());

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        return Response.status(status).entity(error).build();
    }
}
