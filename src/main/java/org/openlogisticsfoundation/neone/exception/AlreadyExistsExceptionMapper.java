//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
public class AlreadyExistsExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<AlreadyExistsException> {

    @Inject
    protected AlreadyExistsExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(AlreadyExistsException e) {
        var status = Response.Status.CONFLICT; // 409
        var title = "The object already exists.";
        var message = e.getResource();

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        // HTTP code 409
        return Response.status(status).entity(error).build();
    }
}
