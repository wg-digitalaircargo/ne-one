//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class InvalidNotificationExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<InvalidNotificationException> {

    @Inject
    protected InvalidNotificationExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(InvalidNotificationException e) {
        var status = Response.Status.BAD_REQUEST; // 400
        var title = "Invalid Notification";
        var message = e.getMessage();

        var error = Error.createError(errorIri(), title, errorDetailIri(), status.toString(), message);
        return Response.status(status).entity(error).build();
    }
}
