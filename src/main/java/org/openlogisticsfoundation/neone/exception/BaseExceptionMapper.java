//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.controller.IdProvider;

public abstract class BaseExceptionMapper {

    private final IdProvider idProvider;

    protected BaseExceptionMapper(IdProvider idProvider) {
        this.idProvider = idProvider;
    }

    public IdProvider getIdProvider() {
        return idProvider;
    }

    public IRI errorIri() {
        return idProvider.createInternalIri().getIri();
    }

    public IRI errorDetailIri() {
        return errorIri();
    }
}
