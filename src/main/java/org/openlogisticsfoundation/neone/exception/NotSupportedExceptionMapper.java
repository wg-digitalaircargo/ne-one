//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;

import jakarta.inject.Inject;
import jakarta.ws.rs.NotSupportedException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class NotSupportedExceptionMapper extends BaseExceptionMapper
    implements ExceptionMapper<NotSupportedException> {

    @Inject
    protected NotSupportedExceptionMapper(IdProvider idProvider) {
        super(idProvider);
    }

    @Override
    public Response toResponse(NotSupportedException e) {
        var status = Response.Status.UNSUPPORTED_MEDIA_TYPE; // 415
        var title = "Unsupported Media Type";

        var error = Error.createError(errorIri(), title);
        return Response.status(status).entity(error).build();
    }
}
