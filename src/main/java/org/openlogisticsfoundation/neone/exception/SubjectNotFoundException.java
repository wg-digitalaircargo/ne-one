//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.exception;

public class SubjectNotFoundException extends RuntimeException {

    private final String subject;

    public SubjectNotFoundException(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }
}
