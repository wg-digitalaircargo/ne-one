// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone;

import io.smallrye.config.ConfigMapping;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;

import java.util.Set;

@ConfigMapping(prefix = "neone-server")
public interface NeoneServerConfig {

    Set<String> supportedContentTypes();

    Set<String> supportedLanguages();

    DataHolder dataHolder();

    default Model toServerInformationModel(IRI iri, IRI dataHolderIri) {
        Model model = new DynamicModelFactory().createEmptyModel();
        model.add(iri, RDF.TYPE, API.ServerInformation);
        model.add(iri, API.hasServerEndpoint, Values.literal(iri.stringValue()));
        supportedContentTypes().forEach(contentType ->
            model.add(iri, API.hasSupportedContentType, Values.literal(contentType))
        );
        supportedLanguages().forEach(lang ->
            model.add(iri, API.hasSupportedLanguage, Values.literal(lang))
        );
        model.add(iri, API.hasDataHolder, dataHolderIri);

        return model;
    }

    interface DataHolder {
        String id();

        String name();
    }
}
