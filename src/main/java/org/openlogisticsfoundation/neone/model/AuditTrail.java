// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.util.List;

public record AuditTrail(IRI iri,
                         List<ChangeRequest> changeRequests,
                         int latestRevision) implements Referencable {
}
