// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;
import org.iata.onerecord.api.API;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

public record Subscription(
    IRI iri,
    IRI subscriber,
    TopicType topicType,
    String topic,
    Optional<String> description,
    Optional<Instant> expiresAt,
    Optional<Boolean> sendLoBody,
    Optional<String> includeSubscriptionEventType,
    Optional<Boolean> notifyRequestStatusChange
) implements Referencable {
    public static List<String> contentTypes() {
        return List.of("application/ld+json", "application/x-turtle", "text/turtle");
    }

    public enum TopicType {
        LOGISTICS_OBJECT_TYPE(API.LOGISTICS_OBJECT_TYPE),
        LOGISTICS_OBJECT_IDENTIFIER(API.LOGISTICS_OBJECT_IDENTIFIER);

        private static final Map<IRI, Subscription.TopicType> reverseLookup = new HashMap<>();
        private final IRI value;

        TopicType(IRI value) {
            this.value = value;
        }

        public IRI getValue() {
            return value;
        }

        public static Subscription.TopicType from(IRI value) {
            Subscription.TopicType type = reverseLookup.get(value);
            if (type == null) {
                throw new NoSuchElementException(value.stringValue());
            }

            return type;
        }

        static {
            for (Subscription.TopicType type : Subscription.TopicType.values()) {
                reverseLookup.put(type.getValue(), type);
            }
        }
    }
}
