// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

/**
 * Some generic logistics related event, not to be confused with LogisticsObjectEvent.
 */
public record LogisticsEvent(IRI iri,
                             Optional<Instant> creationDate,
                             Optional<Instant> eventDate,
                             Optional<IRI> eventCode,
                             Optional<String> eventName,
                             Optional<IRI> eventTimeType,
                             Set<IRI> externalReferences,
                             Optional<IRI> eventFor,
                             Optional<IRI> eventLocation,
                             Optional<IRI> recordingOrganization,
                             Optional<IRI> recordingActor,
                             Optional<Model> embeddedLo,
                             Optional<Model> embeddedEventCodeModel,
                             Optional<Boolean> partialEventIndicator) implements Referencable {
    /*
     * Complete the LogisticsEvent. The client might not have the link to the LO properly set, so it is established here.
     */
    public LogisticsEvent withLinkedObjectAndCreatedAt(IRI loIri, Instant createdAt) {
        return new LogisticsEvent(this.iri, Optional.ofNullable(createdAt),
            this.eventDate, this.eventCode, this.eventName,
            this.eventTimeType, this.externalReferences, Optional.ofNullable(loIri),
            this.eventLocation, this.recordingOrganization, this.recordingActor,
            this.embeddedLo, this.embeddedEventCodeModel, this.partialEventIndicator);
    }

    public LogisticsEvent withEmbeddedLoModel(Model loModel) {
        return new LogisticsEvent(this.iri, this.creationDate, this.eventDate, this.eventCode, this.eventName,
            this.eventTimeType, this.externalReferences, this.eventFor,
            this.eventLocation, this.recordingOrganization, this.recordingActor,
            Optional.of(loModel), this.embeddedEventCodeModel, this.partialEventIndicator);
    }

    public LogisticsEvent withId(IRI id) {
        return new LogisticsEvent(id, this.creationDate, this.eventDate, this.eventCode, this.eventName,
            this.eventTimeType, this.externalReferences, this.eventFor,
            this.eventLocation, this.recordingOrganization, this.recordingActor,
            this.embeddedLo, this.embeddedEventCodeModel, this.partialEventIndicator);
    }
}
