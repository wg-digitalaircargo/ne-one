// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.time.Instant;
import java.util.Optional;

public record ChangeRequest(
    IRI iri,
    Optional<Error> error,
    RequestStatus requestStatus,
    Instant requestedAt,
    IRI requestedBy,
    Optional<Instant> revokedAt,
    Optional<IRI> revokedBy,
    Change change
) implements ActionRequest {

    public ChangeRequest withStatus(RequestStatus status) {
        return new ChangeRequest(this.iri, this.error, status, this.requestedAt,
            this.requestedBy, this.revokedAt, this.revokedBy, this.change);
    }

    public ChangeRequest withRevocation(IRI revokedBy, Instant revokedAt) {
        return new ChangeRequest(this.iri, this.error, RequestStatus.REQUEST_REVOKED, this.requestedAt,
            this.requestedBy, Optional.ofNullable(revokedAt), Optional.ofNullable(revokedBy),
            this.change);
    }
}
