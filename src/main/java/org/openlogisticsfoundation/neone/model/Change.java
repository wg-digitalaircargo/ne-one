// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.util.Optional;
import java.util.Set;

public record Change(
    IRI iri,
    Optional<String> description,
    Set<Operation> operations,
    Integer revision,
    IRI hasLogisticsObject,
    Optional<Boolean> notifyRequestStatusChange
) implements Referencable {
}
