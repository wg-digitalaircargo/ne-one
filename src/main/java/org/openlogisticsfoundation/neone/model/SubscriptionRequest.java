//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.time.Instant;
import java.util.Optional;

public record SubscriptionRequest(
    IRI iri,
    RequestStatus requestStatus,
    Instant requestedAt,
    IRI requestedBy,
    Optional<Instant> revokedAt,
    Optional<IRI> revokedBy,
    Optional<Error> error,
    Subscription subscription) implements ActionRequest {

    public SubscriptionRequest withStatus(RequestStatus status) {
        return new SubscriptionRequest(this.iri, status, this.requestedAt,
            this.requestedBy, this.revokedAt, this.revokedBy, this.error, this.subscription);
    }

    public SubscriptionRequest withRevocation(IRI revokedBy, Instant revokedAt) {
        return new SubscriptionRequest(this.iri, RequestStatus.REQUEST_REVOKED, this.requestedAt,
            this.requestedBy, Optional.ofNullable(revokedAt), Optional.ofNullable(revokedBy),
            this.error, this.subscription);
    }
}

