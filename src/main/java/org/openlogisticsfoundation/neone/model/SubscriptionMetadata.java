//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.net.URL;

public record SubscriptionMetadata(IRI iri,
                                   IRI describes, // => Subscription IRI
                                   URL callbackUrl) implements Metadata {
}
