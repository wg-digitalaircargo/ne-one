// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

public record Operation(IRI iri,
                        OperationObject o,
                        PatchOperation op,
                        IRI p,
                        String s // TODO: maybe use rdf4j.Resource as it can either be a BNode or a IRI
) implements Referencable { }
