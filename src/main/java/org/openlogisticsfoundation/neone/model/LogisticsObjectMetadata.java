//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.time.Instant;
import java.util.Optional;

public record LogisticsObjectMetadata(IRI iri,
                                      IRI describes,
                                      Integer revision, // latest revision
                                      Instant createdAt,
                                      Optional<Boolean> hasPredefinedIri)
implements Metadata { }
