// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

/**
 * Event that is raised for certain cargo actions. Also transports the state of processing
 * (NEW, PENDING, PROCESSED).
 */
public record NeoneEvent(IRI iri,
                         IRI loId,
                         IRI notificationEventType,
                         State state,
                         Optional<IRI> triggeredBy,
                         Set<IRI> changedProperties,
                         Set<IRI> loTypes) implements Referencable {

    public static final String ADDRESS = "lo-event";


    public enum State {
        NEW(NEONE.NEW),
        PENDING(NEONE.PENDING),
        PROCESSED(NEONE.PROCESSED);

        private static final Map<IRI, State> reverseLookup = new HashMap<>();

        private final IRI value;

        State(IRI value) {
            this.value = value;
        }

        public IRI getValue() {
            return value;
        }

        public static State from(IRI value) {
            State state = reverseLookup.get(value);
            if (state == null) {
                throw new NoSuchElementException(value.stringValue());
            }

            return state;
        }

        static {
            for (State state : State.values()) {
                reverseLookup.put(state.getValue(), state);
            }
        }
    }
}
