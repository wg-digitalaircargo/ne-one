// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import java.time.Instant;

/**
 * Snapshot of some LO for a given point in time.
 */
public record Snapshot(IRI iri,
                       IRI loIri,
                       String payload,
                       Instant at,
                       Integer revision) implements Referencable {
}
