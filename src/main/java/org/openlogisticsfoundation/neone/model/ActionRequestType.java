//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

public class ActionRequestType {
    public final static String CHANGE_REQUEST = "https://onerecord.iata.org/ns/api#ChangeRequest";
    public final static String SUBSCRIPTION_REQUEST = "https://onerecord.iata.org/ns/api#SubscriptionRequest";
    public final static String ACCESS_DELEGATION_REQUEST = "https://onerecord.iata.org/ns/api#AccessDelegationRequest";
}
