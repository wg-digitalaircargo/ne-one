//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;


import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;
import java.time.Instant;

@ApplicationScoped
public class SnapshotHandler extends ModelHandler<Snapshot> {

    @Override
    public Snapshot fromModel(IRI iri, Model model) {
        var loIri = getObject(iri, NEONE.referencesLogisticsObject, model);
        var payload = getObjectString(iri, NEONE.payload, model);
        var at = Instant.parse(getObjectString(iri, NEONE.isCreatedAt, model));
        var revision = getObjectLiteral(iri, NEONE.hasRevision, model).intValue();

        return new Snapshot(iri, loIri, payload, at, revision);
    }

    @Override
    public Model fromJava(Snapshot entity) {
        var ts = Values.literal(entity.at().toString(), CoreDatatype.XSD.DATETIME);
        return new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, NEONE.Snapshot)
            .add(NEONE.referencesLogisticsObject, entity.loIri())
            .add(NEONE.payload, entity.payload())
            .add(NEONE.isCreatedAt, ts)
            .add(NEONE.hasRevision, entity.revision())
            .build();
    }
}
