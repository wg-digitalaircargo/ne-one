//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.AccessDelegation;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.stream.Collectors;

@ApplicationScoped
public class AccessDelegationHandler extends ModelHandler<AccessDelegation> {

    @Override
    public AccessDelegation fromModel(IRI subject, Model model) {
        var description = findObjectString(subject, API.hasDescription, model);
        var permissions = findObjects(subject, API.hasPermission, model).stream()
            .map(AccessDelegation.Permission::from).collect(Collectors.toSet());
        var isRequestedFor = getObjects(subject, API.isRequestedFor, model);
        var hasLogisticsObject = getObjects(subject, API.hasLogisticsObject, model);
        var notifyRequestStatusChange = findObjectLiteral(subject, API.notifyRequestStatusChange, model)
            .map(Literal::booleanValue);
        return new AccessDelegation(subject, description, permissions, isRequestedFor,
            hasLogisticsObject, notifyRequestStatusChange);
    }

    @Override
    public Model fromJava(AccessDelegation entity) {
        ModelBuilder builder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, API.AccessDelegation)
            .add(API.notifyRequestStatusChange, entity.notifyRequestStatusChange().orElse(false));
        entity.description().ifPresent(description -> builder.add(API.hasDescription, description));
        entity.isRequestedFor().forEach(iri -> builder.add(API.isRequestedFor, iri));
        entity.hasLogisticsObject().forEach(iri -> builder.add(API.hasLogisticsObject, iri));
        entity.permissions().stream().map(AccessDelegation.Permission::iri)
            .forEach(iri -> builder.add(API.hasPermission, iri));
        return builder.build();
    }
}
