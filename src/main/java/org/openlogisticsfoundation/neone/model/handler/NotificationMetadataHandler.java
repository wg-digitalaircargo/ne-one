//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.model.NotificationMetadata;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

@ApplicationScoped
public class NotificationMetadataHandler extends ModelHandler<NotificationMetadata> {

    @Override
    public NotificationMetadata fromModel(IRI subject, Model model) {
        try {
            IRI notificationIri = getObject(subject, NEONE.describes, model);
            URL callbackUrl = new URI(getObjectString(subject, NEONE.callbackUrl, model)).toURL();
            Boolean includeLoBody = findObjectString(subject, NEONE.includeLogisticsObject, model).map(Boolean::valueOf).orElse(false);
            IRI eventIri = getObject(subject, NEONE.hasEvent, model);
            return new NotificationMetadata(subject, notificationIri, callbackUrl, includeLoBody, eventIri);
        } catch (MalformedURLException | URISyntaxException e) {
            throw new NeoneException("Unable to deserialize Notification metadata model", e);
        }
    }

    @Override
    public Model fromJava(NotificationMetadata entity) {
        ModelBuilder modelBuilder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, NEONE.NotificationMetadata)
            .add(NEONE.describes, entity.describes())
            .add(NEONE.callbackUrl, entity.callbackUrl())
            .add(NEONE.includeLogisticsObject, entity.includeLoBody())
            .add(NEONE.hasEvent, entity.loEventIri());
        return modelBuilder.build();
    }
}
