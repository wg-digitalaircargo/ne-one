//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.Notification;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class NotificationHandler extends ModelHandler<Notification> {

    @Override
    public Notification fromModel(IRI iri, Model model) {
        var eventType = getObject(iri, API.hasEventType, model);
        var loIri = findObject(iri, API.hasLogisticsObject, model);
        var loType = findObjectUri(iri, API.hasLogisticsObjectType, model);
        var triggeredBy = findObject(iri, API.isTriggeredBy, model);
        var changedProps = findObjects(iri, API.hasChangedProperty, model);
        return new Notification(iri, eventType, loType, loIri, triggeredBy, changedProps);
    }

    @Override
    public Model fromJava(Notification entity) {
        var builder = new ModelBuilder();
        builder.subject(entity.iri())
            .add(RDF.TYPE, API.Notification)
            .add(API.hasEventType, entity.eventType());
        entity.hasLogisticsObjectType().ifPresent(type ->
            builder.add(API.hasLogisticsObjectType, Values.literal(type, CoreDatatype.XSD.ANYURI))
        );
        entity.hasLogisticsObject().ifPresent(p -> builder.add(API.hasLogisticsObject, p));
        entity.triggeredBy().ifPresent(t -> builder.add(API.isTriggeredBy, t));
        entity.changedProperties().forEach(p -> builder.add(API.hasChangedProperty, p));
        return builder.build();
    }
}
