//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.SubscriptionRequest;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;

@ApplicationScoped
public class SubscriptionRequestHandler extends ModelHandler<SubscriptionRequest> {

    private final ErrorHandler errorHandler;

    private final SubscriptionHandler subscriptionHandler;

    @Inject
    public SubscriptionRequestHandler(ErrorHandler errorHandler,
                                      SubscriptionHandler subscriptionHandler) {
        this.errorHandler = errorHandler;
        this.subscriptionHandler = subscriptionHandler;
    }

    @Override
    public SubscriptionRequest fromModel(IRI iri, Model model) {
        var subscriptionIri = getObject(iri, API.hasSubscription, model);
        var requestStatus = RequestStatus.from(getObject(iri, API.hasRequestStatus, model));
        var requestedAt = Instant.parse(getObjectString(iri, API.isRequestedAt, model));
        var requestedBy = getObject(iri, API.isRequestedBy, model);
        var revokedAt = findObjectString(iri, API.isRevokedAt, model).map(Instant::parse);
        var revokedBy = findObject(iri, API.isRevokedBy, model);
        var error = findObject(iri, API.hasError, model).map(errorIri -> errorHandler.fromModel(errorIri, model));
        var subscription = subscriptionHandler.fromModel(subscriptionIri, model);

        return new SubscriptionRequest(iri, requestStatus, requestedAt, requestedBy, revokedAt, revokedBy,
            error, subscription);
    }

    @Override
    public Model fromJava(SubscriptionRequest entity) {
        var builder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, API.SubscriptionRequest)
            .add(API.hasSubscription, entity.subscription().iri())
            .add(API.hasRequestStatus, entity.requestStatus().iri())
            .add(API.isRequestedAt, Values.literal(entity.requestedAt().toString(), CoreDatatype.XSD.DATETIME))
            .add(API.isRequestedBy, entity.requestedBy());
        entity.revokedAt().ifPresent(instant ->
            builder.add(API.isRevokedAt, Values.literal(instant.toString(), CoreDatatype.XSD.DATETIME)));
        entity.revokedBy().ifPresent(iri -> builder.add(API.isRevokedBy, iri));
        entity.error().ifPresent(e -> builder.add(API.hasError, e.iri()));

        var subRqModel = builder.build();
        var subsriptionModel = subscriptionHandler.fromJava(entity.subscription());

        return merge(subRqModel, subsriptionModel);
    }
}
