//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.AccessDelegationRequest;
import org.openlogisticsfoundation.neone.model.RequestStatus;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;

@ApplicationScoped
public class AccessDelegationRequestHandler extends ModelHandler<AccessDelegationRequest> {

    private final ErrorHandler errorHandler;

    private final AccessDelegationHandler accessDelegationHandler;

    @Inject
    public AccessDelegationRequestHandler(ErrorHandler errorHandler,
                                          AccessDelegationHandler accessDelegationHandler) {
        this.errorHandler = errorHandler;
        this.accessDelegationHandler = accessDelegationHandler;
    }

    @Override
    public AccessDelegationRequest fromModel(IRI iri, Model model) {
        var accessDelegationIri = getObject(iri, API.hasAccessDelegation, model);
        var requestStatus = RequestStatus.from(getObject(iri, API.hasRequestStatus, model));
        var requestedAt = Instant.parse(getObjectString(iri, API.isRequestedAt, model));
        var requestedBy = getObject(iri, API.isRequestedBy, model);
        var revokedAt = findObjectString(iri, API.isRevokedAt, model).map(Instant::parse);
        var revokedBy = findObject(iri, API.isRevokedBy, model);
        var error = findObject(iri, API.hasError, model).map(errorIri -> errorHandler.fromModel(errorIri, model));
        var accessDelegation = accessDelegationHandler.fromModel(accessDelegationIri, model);
        return new AccessDelegationRequest(iri, error, requestStatus, requestedAt, requestedBy, revokedAt, revokedBy,
            accessDelegation);
    }

    @Override
    public Model fromJava(AccessDelegationRequest entity) {
        var builder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, API.AccessDelegationRequest)
            .add(API.hasAccessDelegation, entity.accessDelegation().iri())
            .add(API.hasRequestStatus, entity.requestStatus().iri())
            .add(API.isRequestedAt, Values.literal(entity.requestedAt().toString(), CoreDatatype.XSD.DATETIME))
            .add(API.isRequestedBy, entity.requestedBy());
        entity.revokedAt().ifPresent(instant ->
            builder.add(API.isRevokedAt, Values.literal(instant.toString(), CoreDatatype.XSD.DATETIME)));
        entity.revokedBy().ifPresent(iri -> builder.add(API.isRevokedBy, iri));
        entity.error().ifPresent(e -> builder.add(API.hasError, e.iri()));

        var rqModel = builder.build();
        var accessDelegationModel = accessDelegationHandler.fromJava(entity.accessDelegation());

        return merge(rqModel, accessDelegationModel);
    }
}
