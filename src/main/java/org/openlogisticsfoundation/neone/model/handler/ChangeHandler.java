// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.Operation;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ChangeHandler extends ModelHandler<Change> {

    private final OperationHandler operationHandler;

    @Inject
    public ChangeHandler(OperationHandler operationHandler) {
        this.operationHandler = operationHandler;
    }

    @Override
    public Change fromModel(IRI subject, Model model) {
        var revision = getObjectLiteral(subject, API.hasRevision, model).intValue();
        var logisticsObject = getObject(subject, API.hasLogisticsObject, model);
        var description = findObjectString(subject, API.hasDescription, model);
        Set<IRI> operationSubjects = getObjects(subject, API.hasOperation, model);
        Set<Operation> operations = operationSubjects.stream()
            .map(iri -> operationHandler.fromModel(iri, model))
            .collect(Collectors.toSet());
        var notifyRequestStatusChange = findObjectLiteral(subject, API.notifyRequestStatusChange, model)
            .map(Literal::booleanValue);

        return new Change(subject, description, operations, revision, logisticsObject, notifyRequestStatusChange);
    }

    @Override
    public Model fromJava(Change entity) {
        ModelBuilder modelBuilder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, API.Change)
            .add(API.hasRevision, entity.revision())
            .add(API.hasLogisticsObject, entity.hasLogisticsObject())
            .add(API.notifyRequestStatusChange, entity.notifyRequestStatusChange().orElse(false));
        entity.operations().forEach(operation -> modelBuilder.add(API.hasOperation, operation.iri()));
        entity.description().ifPresent(description -> modelBuilder.add(API.hasDescription, description));
        Model[] operationModels = entity.operations().stream()
            .map(operationHandler::fromJava)
            .toArray(Model[]::new);

        return merge(modelBuilder.build(), operationModels);
    }
}
