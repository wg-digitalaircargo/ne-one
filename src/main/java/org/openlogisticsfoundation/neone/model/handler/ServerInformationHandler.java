//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.openlogisticsfoundation.neone.model.ServerInformation;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ServerInformationHandler extends ModelHandler<ServerInformation> {

    @Override
    public ServerInformation fromModel(IRI subject, Model model) {
        return new ServerInformation(subject, model);
    }

    @Override
    public Model fromJava(ServerInformation entity) {
        return entity.model();
    }
}
