//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.AuditTrail;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class AuditTrailHandler extends ModelHandler<AuditTrail> {

    private final ChangeRequestHandler crHandler;

    @Inject
    public AuditTrailHandler(ChangeRequestHandler crHandler) {
        this.crHandler = crHandler;
    }

    @Override
    public AuditTrail fromModel(IRI iri, Model model) {
        var latestRevision = getObjectLiteral(iri, API.hasLatestRevision, model).intValue();
        var crIris = findObjects(iri, API.hasChangeRequest, model);
        var changeRequests = crIris.stream().map(crIri -> crHandler.fromModel(crIri, model)).toList();
        return new AuditTrail(iri, changeRequests, latestRevision);
    }

    @Override
    public Model fromJava(AuditTrail entity) {
        ModelBuilder builder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, API.AuditTrail)
            .add(API.hasLatestRevision, entity.latestRevision());

        var crs = entity.changeRequests();
        crs.forEach(cr -> builder.add(API.hasChangeRequest, cr.iri()));

        Model[] crModels = crs.stream().map(crHandler::fromJava).toArray(Model[]::new);
        return merge(builder.build(), crModels);
    }
}
