//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class NeoneEventHandler extends ModelHandler<NeoneEvent> {

    @Override
    public NeoneEvent fromModel(IRI iri, Model model) {
        var loId = getObject(iri, NEONE.referencesLogisticsObject, model);
        var type = getObject(iri, API.hasEventType, model);
        var state = getObject(iri, NEONE.hasState, model);
        var triggerdBy = findObject(iri, API.isTriggeredBy, model);
        var changedProperties = findObjects(iri, API.hasChangedProperty, model);
        var loTypes = getObjects(iri, NEONE.loType, model);

        return new NeoneEvent(
            iri,
            loId,
            type,
            NeoneEvent.State.from(state),
            triggerdBy,
            changedProperties,
            loTypes);
    }

    @Override
    public Model fromJava(NeoneEvent entity) {
        var builder = new ModelBuilder();
        builder.subject(entity.iri())
            .add(RDF.TYPE, NEONE.NeoneEvent)
            .add(API.hasEventType, entity.notificationEventType())
            .add(NEONE.hasState, entity.state().getValue())
            .add(NEONE.referencesLogisticsObject, entity.loId());

        entity.loTypes().forEach(t -> builder.add(NEONE.loType, t));
        entity.triggeredBy().map(iri -> builder.add(API.isTriggeredBy, iri));
        entity.changedProperties().forEach(p -> builder.add(API.hasChangedProperty, p));

        return builder.build();
    }
}
