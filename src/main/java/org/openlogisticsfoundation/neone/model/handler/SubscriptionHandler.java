//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model.handler;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.Subscription;

import jakarta.enterprise.context.ApplicationScoped;
import java.time.Instant;

@ApplicationScoped
public class SubscriptionHandler extends ModelHandler<Subscription> {

    @Override
    public Subscription fromModel(IRI iri, Model model) {
        var subscriber = getObject(null, API.hasSubscriber, model);
        var topic = getObjectUri(null, API.hasTopic, model);
        var typeIri = getObject(null, API.hasTopicType, model);
        var topicType = Subscription.TopicType.from(typeIri);
        var description = findObjectString(null, API.hasDescription, model);
        var expiresAt = findObjectString(null, API.expiresAt, model)
            .map(Instant::parse);
        var includeSubscriptionEventType = findObjectString(null, API.includeSubscriptionEventType, model);
        var sendLoBody = findObjectString(null, API.sendLogisticsObjectBody, model)
            .map(Boolean::valueOf);
        var notifyRequestStatusChange = findObjectString(null, API.notifyRequestStatusChange, model)
            .map(Boolean::valueOf);

        return new Subscription(iri, subscriber, topicType, topic, description, expiresAt, sendLoBody,
            includeSubscriptionEventType, notifyRequestStatusChange);
    }

    @Override
    public Model fromJava(Subscription entity) {
        var builder = new ModelBuilder()
            .subject(entity.iri())
            .add(RDF.TYPE, API.Subscription)
            .add(API.hasSubscriber, entity.subscriber())
            .add(API.hasTopic, Values.literal(entity.topic(), CoreDatatype.XSD.ANYURI))
            .add(API.hasTopicType, entity.topicType().getValue());
        entity.description().ifPresent(description -> builder.add(API.hasDescription, description));
        entity.includeSubscriptionEventType()
            .ifPresent(e -> builder.add(API.includeSubscriptionEventType, Values.literal(e)));
        entity.expiresAt()
            .map(Instant::toString)
            .ifPresent(e -> builder.add(API.expiresAt, Values.literal(e, CoreDatatype.XSD.DATETIME)));
        entity.sendLoBody().ifPresent(e -> builder.add(API.sendLogisticsObjectBody, Values.literal(e)));
        Subscription.contentTypes().forEach(ct -> builder.add(API.hasSupportedContentType, ct));
        return builder.build();
    }
}
