// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;

import jakarta.ws.rs.core.Response;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

public record Error(IRI iri, String title, Set<ErrorDetail> errorDetail) implements Referencable {

    public static Error createError(IRI iri, String title) {
        return new Error(iri, title, Collections.emptySet());
    }

    public static Error createError(IRI iri, String title, ErrorDetail detail) {
        return new Error(iri, title, Set.of(detail));
    }

    public static Error createError(IRI iri, String title,
                                    IRI detailIri, String code, String detailMessage) {
        return createError(iri,
            title,
            new ErrorDetail(detailIri,
                code,
                Optional.ofNullable(detailMessage),
                Optional.empty(),
                Optional.empty()));
    }
}
