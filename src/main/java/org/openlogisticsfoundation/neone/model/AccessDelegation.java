//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.model;

import org.eclipse.rdf4j.model.IRI;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.vocab.ACL;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

public record AccessDelegation(
    IRI iri,
    Optional<String> description,
    Set<Permission> permissions,
    Set<IRI> isRequestedFor,
    Set<IRI> hasLogisticsObject,
    Optional<Boolean> notifyRequestStatusChange
) implements Referencable {

    public enum Permission {
        GET_LOGISTICS_EVENT(API.GET_LOGISTICS_EVENT, ACL.Read),
        GET_LOGISTICS_OBJECT(API.GET_LOGISTICS_OBJECT, ACL.Read),
        PATCH_LOGISTICS_OBJECT(API.PATCH_LOGISTICS_OBJECT, ACL.Read), // "Write" not required.
        POST_LOGISTICS_EVENT(API.POST_LOGISTICS_EVENT, ACL.Write);

        private static final Map<IRI, AccessDelegation.Permission> reverseLookup = new HashMap<>();

        private final IRI iri;

        private final IRI mode;

        Permission(IRI iri, IRI mode) {
            this.iri = iri;
            this.mode = mode;
        }

        public IRI iri() {
            return iri;
        }

        public IRI mode() {
            return mode;
        }

        public static AccessDelegation.Permission from(IRI value) {
            AccessDelegation.Permission state = reverseLookup.get(value);
            if (state == null) {
                throw new NoSuchElementException(value.stringValue());
            }

            return state;
        }

        static {
            for (AccessDelegation.Permission p : AccessDelegation.Permission.values()) {
                reverseLookup.put(p.iri(), p);
            }
        }
    }
}
