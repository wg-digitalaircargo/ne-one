//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.exception.InvalidNotificationException;
import org.openlogisticsfoundation.neone.model.handler.LogisticsObjectHandler;
import org.openlogisticsfoundation.neone.model.handler.NotificationHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.NoSuchElementException;
import java.util.Optional;

@Provider
@Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class NotificationMessageBodyReader extends BodyHandlerBase implements MessageBodyReader<NotificationMessage> {

    private final NotificationHandler notificationHandler;

    private final LogisticsObjectHandler logisticsObjectHandler;

    private final IdProvider idProvider;

    @Inject
    public NotificationMessageBodyReader(NotificationHandler notificationHandler,
                                         LogisticsObjectHandler logisticsObjectHandler,
                                         IdProvider idProvider, WriterConfig writerConfig,
                                         CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.notificationHandler = notificationHandler;
        this.logisticsObjectHandler = logisticsObjectHandler;
        this.idProvider = idProvider;
    }

    @Override
    protected String rootNamespace() {
        return API.NAMESPACE;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == NotificationMessage.class;
    }

    @Override
    public NotificationMessage readFrom(Class<NotificationMessage> type,
                                        Type genericType,
                                        Annotation[] annotations,
                                        MediaType mediaType,
                                        MultivaluedMap<String, String> httpHeaders,
                                        InputStream entityStream) throws WebApplicationException {

        try {
            var model = Rio.parse(entityStream, getRdfFormat(mediaType));

            // Even the IRI of the notification may be empty:
            Optional<IRI> notificationIri = Models.subjectIRI(model.filter(null, RDF.TYPE, API.Notification));
            Optional<IRI> logisticsObjectIri = Models.objectIRI(model.filter(null, API.hasLogisticsObject, null));

            var notification = notificationHandler.fromModel(notificationIri.orElse(null), model);
            if (notificationIri.isEmpty() && logisticsObjectIri.isEmpty()) {
                throw new InvalidNotificationException("Neither IRI of notification nor IRI or logistics object specified.");
            }

            NotificationMessage notificationMessage = new NotificationMessage().withNotification(notification);
            if (logisticsObjectIri.isPresent()) {
                var lo = logisticsObjectHandler.fromModel(logisticsObjectIri.get(), model);
                notificationMessage.withLogisticsObject(lo);
            }

            return notificationMessage;
        } catch (IOException | NoSuchElementException e) {
            throw new InvalidApiRequestException("Invalid Notification Message structure.");
        }
    }
}
