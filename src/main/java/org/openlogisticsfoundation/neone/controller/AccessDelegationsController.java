//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.openlogisticsfoundation.neone.model.AccessDelegation;
import org.openlogisticsfoundation.neone.service.AccessDelegationService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.net.URI;

@Path("access-delegations")
@Consumes({"application/ld+json", "application/x-turtle", "text/turtle"})
@Produces({"application/ld+json", "application/x-turtle", "text/turtle"})
public class AccessDelegationsController {

    private final AccessDelegationService accessDelegationService;

    @Inject
    public AccessDelegationsController(AccessDelegationService accessDelegationService) {
        this.accessDelegationService = accessDelegationService;
    }

    @POST
    @Authenticated
    public Response createAccessDelegationRequest(AccessDelegation accessDelegation) {
        var request = accessDelegationService.handleAccessDelegation(accessDelegation);
        var uri = URI.create(request.iri().stringValue());
        return Response.created(uri).entity("Request for access delegation was successful").build();
    }
}
