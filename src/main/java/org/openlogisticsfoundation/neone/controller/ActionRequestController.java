// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.eclipse.rdf4j.model.Model;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.security.AccessMode;
import org.openlogisticsfoundation.neone.security.AccessObject;
import org.openlogisticsfoundation.neone.security.AclSecured;
import org.openlogisticsfoundation.neone.service.ActionRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

@Path("/" + ActionRequestController.ACTION_REQUEST_RESOURCE_NAME)
@Produces(BodyHandlerBase.APPLICATION_LD_JSON)
public class ActionRequestController {

    private static final Logger log = LoggerFactory.getLogger(ActionRequestController.class);

    public static final String ACTION_REQUEST_RESOURCE_NAME = "action-requests";

    private final ActionRequestService actionRequestService;

    private final IdProvider idProvider;

    @Inject
    public ActionRequestController(ActionRequestService actionRequestService, IdProvider idProvider) {
        this.actionRequestService = actionRequestService;
        this.idProvider = idProvider;
    }

    @GET
    @Path("/{id}")
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public Response getActionRequest(
        @AccessObject(AccessObject.Type.ACTION_REQUEST) @PathParam("id") String internalId) {

        NeoneId requestId = idProvider.getActionRequestId(internalId);

        Model actionRequest = actionRequestService.getActionRequest(requestId.getIri());

        return Response.ok(actionRequest).build();
    }

    @PATCH
    @Path("{id}")
    public Response updateActionRequestStatus(@PathParam("id") String id,
                                              @QueryParam("status") String newStatus) {
        log.debug("Handling update on action request for LO [{}]. Status: [{}]", id, newStatus);
        actionRequestService.updateActionRequestStatus(idProvider.getActionRequestId(id).getIri(),
            RequestStatus.valueOf(newStatus));
        return Response.noContent().build();
    }

    @DELETE
    @Path("{id}")
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.WRITE)
    )
    public Response revokeActionRequest(@AccessObject(AccessObject.Type.ACTION_REQUEST) @PathParam("id") String id) {
        var actionRequestId = idProvider.getActionRequestId(id);
        log.debug("Revoking action request for LO [{}]", actionRequestId);
        actionRequestService.revokeActionRequest(actionRequestId.getIri());
        return Response.noContent().build();
    }
}
