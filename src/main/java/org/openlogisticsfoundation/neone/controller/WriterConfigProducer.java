// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.eclipse.rdf4j.rio.helpers.JSONLDMode;
import org.eclipse.rdf4j.rio.helpers.JSONLDSettings;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;

public class WriterConfigProducer {

    @ConfigProperty(name = "jsonld.mode")
    JSONLDMode mode;

    @Produces
    @ApplicationScoped
    public WriterConfig writerConfig() {
        WriterConfig writerConfig = new WriterConfig()
            .set(JSONLDSettings.OPTIMIZE, true)
            .set(JSONLDSettings.COMPACT_ARRAYS, true)
            .set(JSONLDSettings.JSONLD_MODE, mode);

        return writerConfig;
    }
}
