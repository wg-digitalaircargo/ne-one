// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.security.AccessMode;
import org.openlogisticsfoundation.neone.security.AccessObject;
import org.openlogisticsfoundation.neone.security.AclSecured;
import org.openlogisticsfoundation.neone.service.AuditTrailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

@Path("/logistics-objects/{id}/" + AuditTrailController.AUDIT_TRAIL_RESOURCE_NAME)
@Produces({"application/ld+json", "application/x-turtle", "text/turtle"})
@Authenticated
public class AuditTrailController {

    public static final String AUDIT_TRAIL_RESOURCE_NAME = "audit-trail";

    private static final Logger log = LoggerFactory.getLogger(AuditTrailController.class);

    private final AuditTrailService auditTrailService;

    @Inject
    public AuditTrailController(AuditTrailService auditTrailService) {
        this.auditTrailService = auditTrailService;
    }

    @GET
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public AuditTrail getAuditTrail(@AccessObject @PathParam("id") String loId,
                                    @QueryParam("updated-from") String updatedFrom,
                                    @QueryParam("updated-to") String updatedTo) {

        log.info("Audit trail requested for LO [{}] and time range from [{}] until [{}]", loId, updatedFrom, updatedTo);

        var from = DateTimeConverter.convert(updatedFrom);
        var until = DateTimeConverter.convert(updatedTo);
        if (from != null && until != null && from.isAfter(until)) {
            throw new InvalidApiRequestException("invalid time range");
        }

        return auditTrailService.getAuditTrailByLoId(loId, from, until);
    }
}
