//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.Statements;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.model.LogisticsEvent;
import org.openlogisticsfoundation.neone.model.handler.LogisticsEventHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

public class LogisticsEventBodyReaderWriter {


    @Provider
    @Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
    public static class LogisticsEventReaderWriter
        extends BodyHandlerBase
        implements MessageBodyWriter<LogisticsEvent>, MessageBodyReader<LogisticsEvent> {

        private final LogisticsEventHandler handler;

        private final IdProvider idProvider;

        @Inject
        public LogisticsEventReaderWriter(LogisticsEventHandler handler, WriterConfig writerConfig,
                                          IdProvider idProvider,
                                          CargoOntologyService cargoOntologyService) {
            super(writerConfig, cargoOntologyService);
            this.handler = handler;
            this.idProvider = idProvider;
        }

        @Override
        public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
            return type.isAssignableFrom(LogisticsEvent.class);
        }

        @Override
        public void writeTo(LogisticsEvent logisticsEvent,
                            Class<?> type,
                            Type genericType,
                            Annotation[] annotations,
                            MediaType mediaType,
                            MultivaluedMap<String, Object> httpHeaders,
                            OutputStream entityStream) throws WebApplicationException {

            Model model = handler.fromJava(logisticsEvent);

            writeRdfResponse(model, mediaType, httpHeaders, entityStream);
        }

        @Override
        protected String rootNamespace() {
            return CARGO.NAMESPACE;
        }

        @Override
        public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
            return type == LogisticsEvent.class;
        }

        @Override
        public LogisticsEvent readFrom(Class<LogisticsEvent> type,
                                       Type genericType,
                                       Annotation[] annotations,
                                       MediaType mediaType,
                                       MultivaluedMap<String, String> httpHeaders,
                                       InputStream entityStream) throws IOException, WebApplicationException {

            Model model = processModel(entityStream, mediaType, idProvider);
            IRI rootIri = getRootIri(model);

            return handler.fromModel(rootIri, model);
        }
    }

    @Provider
    @Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
    public static class LogisticsEventCollectionBodyWriter
        extends BodyHandlerBase
        implements MessageBodyWriter<Collection<LogisticsEvent>> {


        private final LogisticsEventHandler handler;

        private final LogisticsEventController.Context logisticsEventContext;


        @Inject
        public LogisticsEventCollectionBodyWriter(LogisticsEventHandler handler, WriterConfig writerConfig,
                                                  CargoOntologyService cargoOntologyService, IdProvider idProvider,
                                                  LogisticsEventController.Context logisticsEventContext) {
            super(writerConfig, cargoOntologyService);
            this.handler = handler;
            this.logisticsEventContext = logisticsEventContext;
        }

        @Override
        protected String rootNamespace() {
            return CARGO.NAMESPACE;
        }

        @Override
        public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
            if (Collection.class.isAssignableFrom(type) && genericType instanceof ParameterizedType) {
                return ((ParameterizedType) genericType).getActualTypeArguments()[0].equals(LogisticsEvent.class);
            } else {
                return false;
            }
        }

        @Override
        public void writeTo(Collection<LogisticsEvent> logisticsEvents, Class<?> type, Type genericType,
                            Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders,
                            OutputStream entityStream) throws WebApplicationException {

            // Create collection container
            IRI collectionIri = Values.iri(logisticsEventContext.getLoId().getIri().stringValue() + "/"
                + LogisticsEventController.LOGISTICS_EVENT_RESOURCE_NAME);
            Model container = new DynamicModelFactory().createEmptyModel();
            container.add(collectionIri, RDF.TYPE, API.Collection);

            // set total items
            container.add(collectionIri, API.hasTotalItems, Values.literal(logisticsEvents.size()));

            // add all logistics events to the container
            logisticsEvents.stream()
                .<Statement>mapMulti((logisticsEvent, statementConsumer) -> {
                    // add the LO event statements
                    handler.fromJava(logisticsEvent).forEach(statementConsumer::accept);
                    // add the hasItem reference
                    statementConsumer.accept(
                        Statements.statement(collectionIri, API.hasItem, logisticsEvent.iri(), null)
                    );
                })
                .forEach(container::add);

            writeRdfResponse(container, mediaType, httpHeaders, entityStream);
        }
    }

}
