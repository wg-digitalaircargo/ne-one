// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.exception.MissingMetadataException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.security.AccessMode;
import org.openlogisticsfoundation.neone.security.AccessObject;
import org.openlogisticsfoundation.neone.security.AclSecured;
import org.openlogisticsfoundation.neone.service.LogisticsObjectService;
import org.openlogisticsfoundation.neone.service.SnapshotService;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectMetadataService;
import org.openlogisticsfoundation.neone.vocab.NEONE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HEAD;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import java.time.Instant;
import java.util.Optional;

@Path("/logistics-objects")
@Consumes({"application/ld+json", "application/x-turtle", "text/turtle"})
@Produces({"application/ld+json", "application/x-turtle", "text/turtle"})
public class LogisticsObjectController {

    public static final String LATEST_REVISION_HEADER = "Latest-Revision";
    public static final String REVISION_HEADER = "Revision";
    public static final String LAST_MODIFIED_HEADER = "Last-Modified";
    public static final String CONTENT_LANGUAGE = "Content-Language";
    public static final String LOCATION_HEADER = "Location";
    public static final String TYPE_HEADER = "Type";

    @ConfigProperty(name = "default-language")
    String defaultLanguage;

    private static final Logger log = LoggerFactory.getLogger(LogisticsObjectController.class);

    private final LogisticsObjectService loService;

    private final SnapshotService snapshotService;

    private final LogisticsObjectMetadataService metadataService;

    private final IdProvider idProvider;

    public static class GetLogisticsObjectResponse {
        LogisticsObject logisticsObject;
        final MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();

        public GetLogisticsObjectResponse withLogisticsObject(LogisticsObject lo) {
            this.logisticsObject = lo;
            return this;
        }

        public GetLogisticsObjectResponse withHeader(String key, Object value) {
            this.headers.putSingle(key, value);
            return this;
        }
    }

    @Inject
    public LogisticsObjectController(LogisticsObjectService loService,
                                     SnapshotService snapshotService,
                                     LogisticsObjectMetadataService metadataService,
                                     IdProvider idProvider) {
        this.loService = loService;
        this.snapshotService = snapshotService;
        this.metadataService = metadataService;
        this.idProvider = idProvider;
    }

    @POST
    public Response createLogisticsObject(LogisticsObjectList logisticsObjects,
                                          @QueryParam("public") boolean publicAccess) {

        loService.createLogisticsObjects(logisticsObjects, publicAccess);

        Response.ResponseBuilder created = Response.created(URI.create(logisticsObjects.rootIRI().stringValue()));
        logisticsObjects.rootLoTypes().forEach(iri -> created.header(TYPE_HEADER, iri.stringValue()));

        return created.build();
    }

    @GET
    @Path("/internal/_all")
    public Response getAllLogisticsObjects(@QueryParam("limit") int limit,
                                           @QueryParam("offset") int offset,
                                           @QueryParam("t") String loType) {

        Model allLogisticsObjects = loService.getAllLogisticsObjects(limit, offset, loType);
        long totalCount = Models.objectLiteral(allLogisticsObjects.filter(null, NEONE.hasTotalCount, null))
            .map(Literal::longValue)
            .orElse(0L);

        allLogisticsObjects.remove(null, NEONE.hasTotalCount, null);

        return Response.ok()
            .entity(allLogisticsObjects)
            .header("NEONE-Total-Count", totalCount)
            .build();
    }

    @GET
    @Path("{id}")
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public GetLogisticsObjectResponse getLogisticsObject(@AccessObject @PathParam("id") String id,
                                                         @QueryParam("at") String at,
                                                         @QueryParam("embedded") boolean embedded) {

        log.debug("Handling GET request with id [{}] at timestamp [{}]", id, at);

        NeoneId loId = idProvider.getUriForLoId(id);
        Instant ts = at != null ? DateTimeConverter.convert(at) : null;
        LogisticsObject logisticsObject;
        Optional<Snapshot> snapshot;

        if (ts == null) {
            // Retrieve latest version of the Lo.
            logisticsObject = loService.getLogisticsObject(loId.getUri(), embedded);
            snapshot = Optional.empty();
        } else {
            snapshot = snapshotService.getSnapshot(loId.getIri(), ts);
            logisticsObject = snapshot.map(LogisticsObjectService::toLogisticsObject)
                .orElseThrow(() -> new SubjectNotFoundException(loId.getIri().stringValue()));
        }
        var latestSnapshot = snapshotService.getLatestSnapshot(loId.getIri());
        var meta = metadataService.getMetadataOf(logisticsObject.iri())
            .orElseThrow(() ->
                new MissingMetadataException("Missing metadata for [" + logisticsObject.iri().stringValue() + "]"));
        var latestRevision = meta.revision();
        var revision = snapshot.map(Snapshot::revision).orElse(latestRevision);
        var lastModified = latestSnapshot.map(Snapshot::at).orElse(meta.createdAt());

        return new GetLogisticsObjectResponse()
            .withLogisticsObject(logisticsObject)
            .withHeader(LOCATION_HEADER, logisticsObject.iri())
            .withHeader(REVISION_HEADER, revision)
            .withHeader(LATEST_REVISION_HEADER, latestRevision)
            .withHeader(LAST_MODIFIED_HEADER, lastModified)
            // For the time being, default language used:
            .withHeader(CONTENT_LANGUAGE, defaultLanguage);
    }

    @PATCH
    @Path("{id}")
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public Response updateLogisticsObject(Change change,
                                          @AccessObject @PathParam("id") String id) {
        log.debug("Handling PATCH request with id [{}]", id);

        var changeRequest = loService.createChangeRequest(idProvider.getUriForLoId(id), change);
        var uri = URI.create(changeRequest.iri().stringValue());
        return Response.created(uri).build();
    }

    @HEAD
    @Path("{id}")
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public Response getLogisticsObjectHeaders(@AccessObject @PathParam("id") String id,
                                            @QueryParam("at") String at,
                                            @QueryParam("embedded") boolean embedded) {

        log.debug("Handling HEAD request with id [{}] at timestamp [{}]", id, at);

        NeoneId loId = idProvider.getUriForLoId(id);
        Instant ts = at != null ? DateTimeConverter.convert(at) : null;        
        
        LogisticsObjectMetadata meta = null;

        try {
            meta = metadataService.getMetadataOf(loId.getIri())
            .orElseThrow(() ->
                new MissingMetadataException("Missing metadata for [" + loId.getIri().stringValue() + "]"));
        } catch (MissingMetadataException e) {
            throw new SubjectNotFoundException(loId.getIri().stringValue());
        }
        
        var latestSnapshot = snapshotService.getLatestSnapshot(loId.getIri());                
        var latestRevision = meta.revision();

        Optional<Snapshot> snapshot = Optional.empty();
        if (ts != null) {            
            snapshot = snapshotService.getSnapshot(loId.getIri(), ts);            
        }        
        var revision = snapshot.map(Snapshot::revision).orElse(latestRevision);
        var lastModified = latestSnapshot.map(Snapshot::at).orElse(meta.createdAt());

        return Response.ok()
            .header(LOCATION_HEADER, loId.getIri().stringValue())
            .header(REVISION_HEADER, revision)
            .header(LATEST_REVISION_HEADER, latestRevision)
            .header(LAST_MODIFIED_HEADER, lastModified)
            .header(CONTENT_LANGUAGE, defaultLanguage)
            .build();
    }

}