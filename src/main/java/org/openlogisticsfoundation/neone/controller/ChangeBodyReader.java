// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.handler.ChangeHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.NoSuchElementException;

@Provider
@Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class ChangeBodyReader extends BodyHandlerBase implements MessageBodyReader<Change> {

    private final IdProvider idProvider;

    private final ChangeHandler changeHandler;

    @Inject
    public ChangeBodyReader(IdProvider idProvider, ChangeHandler changeHandler, WriterConfig writerConfig,
                            CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.idProvider = idProvider;
        this.changeHandler = changeHandler;
    }

    @Override
    protected String rootNamespace() {
        return API.NAMESPACE;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == Change.class;
    }

    @Override
    public Change readFrom(Class<Change> type,
                           Type genericType,
                           Annotation[] annotations,
                           MediaType mediaType,
                           MultivaluedMap<String, String> httpHeaders,
                           InputStream entityStream) throws WebApplicationException {

        try {
            Model model = processModel(entityStream, mediaType, idProvider);
            IRI rootIri = getRootIri(model);
            return changeHandler.fromModel(rootIri, model);
        } catch (IOException | NoSuchElementException e) {
            throw new InvalidApiRequestException("Invalid Change Request structure.");
        }
    }
}
