// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;


import io.quarkus.security.Authenticated;
import org.openlogisticsfoundation.neone.model.ServerInformation;
import org.openlogisticsfoundation.neone.service.ServerInformationService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

@Path("/")
@Consumes({"application/ld+json", "application/x-turtle", "text/turtle"})
@Produces({"application/ld+json", "application/x-turtle", "text/turtle"})
public class ServerInformationController {

    private final ServerInformationService service;

    @Inject
    public ServerInformationController(ServerInformationService service) {
        this.service = service;
    }

    @GET
    @Authenticated
    public ServerInformation getServerInformation() {
        return service.getServerInformation();
    }
}
