// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import org.openlogisticsfoundation.neone.controller.RandomIdStrategy;

import java.util.Optional;

@ConfigMapping(prefix = "lo-id-config")
public interface LogisticsObjectIdConfig {

    @WithDefault("{server.canonical-hostname}")
    String host();

    Optional<String> port();

    @WithDefault("https")
    String scheme();

    @WithDefault("neone")
    String internalIriScheme();

    @WithDefault("/")
    String rootPath();

    @WithDefault("UUID")
    RandomIdStrategy randomIdStrategy();
}
