// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.NeoneServerConfig;
import org.openlogisticsfoundation.neone.controller.config.LogisticsObjectIdConfig;
import org.openlogisticsfoundation.neone.controller.internal.AclAuthorizationController;
import org.openlogisticsfoundation.neone.exception.IllegalIriFormatException;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;

@ApplicationScoped
public class IdProvider {

    private final LogisticsObjectIdConfig loIdConfig;

    private final NeoneServerConfig neoneServerConfig;

    @Inject
    public IdProvider(LogisticsObjectIdConfig loIdConfig,
                      NeoneServerConfig neoneServerConfig) {
        this.loIdConfig = loIdConfig;
        this.neoneServerConfig = neoneServerConfig;
    }

    public NeoneId createUniqueLoUri() {
        return NeoneId.fromUri(getLoUriBase().path(loIdConfig.randomIdStrategy().randomId()).build());
    }

    public NeoneId getUriForLoId(String uuid) {
        return NeoneId.fromUri(getLoUriBase().path(uuid).build());
    }

    public NeoneId getDataHolderId() {
        return getUriForLoId(neoneServerConfig.dataHolder().id());
    }

    private UriBuilder getLoUriBase() {
        return baseUri().path(LogisticsObjectController.class);
    }

    public NeoneId createLoEventUri(NeoneId loUri) {
        return createLoEventUri(loUri.getIri());
    }

    public NeoneId createLoEventUri(IRI loIri) {
        return NeoneId.fromUri(UriBuilder.fromUri(URI.create(loIri.stringValue()))
            .path(LogisticsEventController.LOGISTICS_EVENT_RESOURCE_NAME)
            .path(loIdConfig.randomIdStrategy().randomId())
            .build());
    }

    public NeoneId createNotificationId(IRI loIri) {
        return NeoneId.fromUri(UriBuilder.fromUri(URI.create(loIri.stringValue()))
            .path("notifications")
            .path(loIdConfig.randomIdStrategy().randomId())
            .build());
    }

    public NeoneId getUriForEventId(String loUuid, String eventId) {
        return NeoneId.fromUri(UriBuilder.fromUri(getUriForLoId(loUuid).getUri())
            .path(LogisticsEventController.LOGISTICS_EVENT_RESOURCE_NAME)
            .path(eventId)
            .build());
    }

    public NeoneId getAuditTrailIriFromLoId(String uuid) {
        return NeoneId.fromUri(UriBuilder.fromUri(getUriForLoId(uuid).getUri())
            .path(AuditTrailController.AUDIT_TRAIL_RESOURCE_NAME)
            .build());
    }

    public NeoneId getActionRequestId(String uuid) {
        return NeoneId.fromUri(baseUri().path(ActionRequestController.class).path(uuid).build());
    }

    public NeoneId createAclAuthorizationId() {
        return NeoneId.fromUri(baseUri().path(AclAuthorizationController.class).path(loIdConfig.randomIdStrategy().randomId())
            .build());
    }

    public NeoneId getAclAuthorizationId(String id) {
        return NeoneId.fromUri(baseUri().path(AclAuthorizationController.class).path(id)
            .build());
    }

    public NeoneId createActionRequestId() {
        return getActionRequestId(loIdConfig.randomIdStrategy().randomId());
    }

    public NeoneId getAuditTrailIriFromLoIri(IRI loIRI) {
        return NeoneId.fromIri(Values.iri(loIRI.stringValue() + "/" + AuditTrailController.AUDIT_TRAIL_RESOURCE_NAME));
    }

    public NeoneId createUniqueSubscriptionRequestUri() {
        return NeoneId.fromUri(getSubscriptionRequestUriBase().path(loIdConfig.randomIdStrategy().randomId()).build());
    }

    public NeoneId createUniqueAccessDelegationRequestUri() {
        return NeoneId.fromUri(baseUri().path(ActionRequestController.class).path(loIdConfig.randomIdStrategy().randomId()).build());
    }

    private UriBuilder getSubscriptionRequestUriBase() {
        return baseUri().path(ActionRequestController.class);
    }

    public NeoneId createUniqueSubscriptionUri() {
        return NeoneId.fromUri(getSubscriptionUriBase().path(loIdConfig.randomIdStrategy().randomId()).build());
    }
    private UriBuilder getSubscriptionUriBase() {
        return baseUri().path(SubscriptionController.class);
    }

    public NeoneId createInternalIri() {
        var schema = loIdConfig.internalIriScheme();
        return NeoneId.fromIri(
            Values.iri(schema + ":" + loIdConfig.randomIdStrategy().randomId()), true, true
        );
    }

    public NeoneId getBaseId() {
        return NeoneId.fromUri(baseUri().build());
    }

    private UriBuilder baseUri() {
        return UriBuilder.fromUri(loIdConfig.scheme() + "://" + loIdConfig.host()
            + loIdConfig.port().map(port -> ":" + port).orElse("")
            + loIdConfig.rootPath());
    }

    public IRI getLogisticsObjectBaseIri() {
        return Values.iri(this.getLoUriBase().build().toString());
    }

    public NeoneId parse(String iriString) {
        try {
            IRI iri = Values.iri(iriString);
            return NeoneId.fromString(iriString, isInternalId(iri), isLocal(iri));
        } catch(IllegalArgumentException ex) {
            throw new IllegalIriFormatException(iriString);
        }
    }

    public NeoneId parse(IRI iri) {
        try {
            return NeoneId.fromString(iri.stringValue(), isInternalId(iri), isLocal(iri));
        } catch(IllegalArgumentException ex) {
            throw new IllegalIriFormatException(iri.stringValue());
        }
    }

    private boolean isLocal(IRI iri) {
        return iri.stringValue().startsWith(baseUri().build().toString());
    }

    private boolean isInternalId(IRI iri) {
        return iri.stringValue().startsWith(loIdConfig.internalIriScheme() + ":");
    }
}
