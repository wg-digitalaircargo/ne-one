// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.LogisticsEvent;
import org.openlogisticsfoundation.neone.security.AccessMode;
import org.openlogisticsfoundation.neone.security.AccessObject;
import org.openlogisticsfoundation.neone.security.AclSecured;
import org.openlogisticsfoundation.neone.service.LogisticsEventService;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/logistics-objects/{loId}/" + LogisticsEventController.LOGISTICS_EVENT_RESOURCE_NAME)
@Consumes({
    ModelBodyHandler.APPLICATION_LD_JSON,
    ModelBodyHandler.APPLICATION_X_TURTLE,
    ModelBodyHandler.TEXT_TURTLE
})
@Produces({
    ModelBodyHandler.APPLICATION_LD_JSON,
    ModelBodyHandler.APPLICATION_X_TURTLE,
    ModelBodyHandler.TEXT_TURTLE}
)
public class LogisticsEventController {

    public static final String LOGISTICS_EVENT_RESOURCE_NAME = "logistics-events";

    private final IdProvider idProvider;

    private final LogisticsEventService logisticsEventService;

    private final Context context;


    @Inject
    public LogisticsEventController(IdProvider idProvider,
                                    LogisticsEventService logisticsEventService,
                                    Context context) {
        this.idProvider = idProvider;
        this.logisticsEventService = logisticsEventService;
        this.context = context;
    }

    @POST
    @Authenticated
    public Response addEvent(@PathParam("loId") String id, LogisticsEvent event) {
        NeoneId loId = idProvider.getUriForLoId(id);

        LogisticsEvent createdEvent = logisticsEventService.addLogisticsEvent(loId, event);

        return Response.created(URI.create(createdEvent.iri().stringValue())).build();
    }

    @GET
    @Path("/{eventId}")
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public LogisticsEvent getEvent(@AccessObject @PathParam("loId") String loId,
                             @AccessObject(AccessObject.Type.LOGISTICS_EVENT_ID) @PathParam("eventId") String eventId,
                             @QueryParam("embedded") boolean embedded,
                             @QueryParam("at") String loAt) {

        NeoneId eventUri = idProvider.getUriForEventId(loId, eventId);
        var at = DateTimeConverter.convert(loAt);

        return logisticsEventService.getLogisticsEvent(eventUri, embedded, at);
    }

    @GET
    @Authenticated
    public List<LogisticsEvent> findEventsOfLogisticsObject(@PathParam("loId") String loId,
                                                            @QueryParam("event-type") String eventType,
                                                            @QueryParam("created-after") String createdAfter,
                                                            @QueryParam("created-before") String createdBefore,
                                                            @QueryParam("occurred-after") String occurredAfter,
                                                            @QueryParam("occurred-before") String occurredBefore,
                                                            @QueryParam("limit") Integer limit) {

        NeoneId logisticsObjectId = idProvider.getUriForLoId(loId);

        context.setLoId(logisticsObjectId);

        var createdFrom = DateTimeConverter.convert(createdAfter);
        var createdUntil = DateTimeConverter.convert(createdBefore);
        if (createdFrom != null && createdUntil != null && createdFrom.isAfter(createdUntil)) {
            throw new InvalidApiRequestException("invalid range creation date.");
        }

        var occurredFrom = DateTimeConverter.convert(occurredAfter);
        var occurredUntil = DateTimeConverter.convert(occurredBefore);
        if (occurredFrom != null && occurredUntil != null && occurredFrom.isAfter(occurredUntil)) {
            throw new InvalidApiRequestException("invalid range of occurred date (event date).");
        }

        List<LogisticsEvent> logisticsEvents = logisticsEventService.findLogisticsEvents(
            logisticsObjectId, eventType, createdFrom, createdUntil, occurredFrom, occurredUntil, limit);

        return logisticsEvents;
    }

    @RequestScoped
    public static class Context {

        private NeoneId loId;

        public NeoneId getLoId() {
            return loId;
        }

        public void setLoId(NeoneId loId) {
            this.loId = loId;
        }
    }
}
