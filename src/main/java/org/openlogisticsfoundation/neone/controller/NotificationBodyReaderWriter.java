//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.handler.NotificationHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.NoSuchElementException;
import java.util.Optional;

@Provider
@Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
@Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class NotificationBodyReaderWriter extends BodyHandlerBase
    implements MessageBodyReader<Notification>, MessageBodyWriter<NotificationMessage> {

    private final IdProvider idProvider;

    private final NotificationHandler notificationHandler;

    @Inject
    public NotificationBodyReaderWriter(NotificationHandler notificationHandler,
                                        IdProvider idProvider, WriterConfig writerConfig,
                                        CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.notificationHandler = notificationHandler;
        this.idProvider = idProvider;
    }

    @Override
    protected String rootNamespace() {
        return API.NAMESPACE;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == Notification.class;
    }

    @Override
    public Notification readFrom(Class<Notification> type,
                                 Type genericType,
                                 Annotation[] annotations,
                                 MediaType mediaType,
                                 MultivaluedMap<String, String> httpHeaders,
                                 InputStream entityStream) throws WebApplicationException {
        try {
            Model model = Rio.parse(entityStream, getRdfFormat(mediaType));
            Optional<IRI> notificationIri = Models.subjectIRI(model.filter(null, RDF.TYPE, API.Notification));
            if (notificationIri.isEmpty()) {
                notificationIri = Models.objectIRI(model.filter(null, API.hasLogisticsObject, null))
                    .map(iri -> idProvider.createNotificationId(iri).getIri());
            }
            // Notification iri may be null.
            return notificationHandler.fromModel(notificationIri.orElse(null), model);
        } catch (IOException | NoSuchElementException e) {
            throw new InvalidApiRequestException("Invalid Notification structure.");
        }
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return NotificationMessage.class.isAssignableFrom(type);
    }

    @Override
    public void writeTo(NotificationMessage notificationMessage,
                        Class<?> type,
                        Type genericType,
                        Annotation[] annotations,
                        MediaType mediaType,
                        MultivaluedMap<String, Object> httpHeaders,
                        OutputStream entityStream) throws WebApplicationException {

        Model notificationModel = notificationHandler.fromJava(notificationMessage.notification);
        Model model = notificationMessage.logisticsObject
            .map(LogisticsObject::model)
            .map(newLoModel -> notificationHandler.merge(notificationModel, newLoModel))
            .orElse(notificationModel);

        writeRdfResponse(model, mediaType, httpHeaders, entityStream);
    }
}
