//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.NoSuchElementException;

@Provider
@Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
@Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class SubscriptionBodyReaderWriter extends BodyHandlerBase
    implements MessageBodyReader<Subscription>, MessageBodyWriter<Subscription> {

    private final SubscriptionHandler subscriptionHandler;

    private final IdProvider idProvider;

    @Inject
    SubscriptionBodyReaderWriter(SubscriptionHandler subscriptionHandler, IdProvider idProvider,
                                 WriterConfig writerConfig, CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.subscriptionHandler = subscriptionHandler;
        this.idProvider = idProvider;
    }

    @Override
    protected String rootNamespace() {
        return API.NAMESPACE;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == Subscription.class;
    }

    @Override
    public Subscription readFrom(Class<Subscription> type, Type genericType, Annotation[] annotations,
                                 MediaType mediaType, MultivaluedMap<String, String> httpHeaders,
                                 InputStream entityStream) throws WebApplicationException {
        try {
            Model model = Rio.parse(entityStream, getRdfFormat(mediaType));
            var subscriptionIri = idProvider.createUniqueSubscriptionUri().getIri();
            return subscriptionHandler.fromModel(subscriptionIri, model);
        } catch (IOException | NoSuchElementException e) {
            throw new InvalidApiRequestException("Invalid Subscription structure.");
        }
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type.isAssignableFrom(Subscription.class);
    }

    @Override
    public void writeTo(Subscription subscription, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
        throws WebApplicationException {

        Model model = subscriptionHandler.fromJava(subscription);

        writeRdfResponse(model, mediaType, httpHeaders, entityStream);
    }
}
