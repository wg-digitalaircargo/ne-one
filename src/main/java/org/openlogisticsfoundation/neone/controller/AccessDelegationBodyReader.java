// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.AccessDelegation;
import org.openlogisticsfoundation.neone.model.handler.AccessDelegationHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.NoSuchElementException;

@Provider
@Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class AccessDelegationBodyReader extends BodyHandlerBase implements MessageBodyReader<AccessDelegation> {

    private final IdProvider idProvider;

    private final AccessDelegationHandler requestHandler;

    @Inject
    public AccessDelegationBodyReader(IdProvider idProvider, AccessDelegationHandler requestHandler,
                                      WriterConfig writerConfig, CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.idProvider = idProvider;
        this.requestHandler = requestHandler;
    }


    @Override
    protected String rootNamespace() {
        return API.NAMESPACE;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == AccessDelegation.class;
    }

    @Override
    public AccessDelegation readFrom(Class<AccessDelegation> type,
                                     Type genericType,
                                     Annotation[] annotations,
                                     MediaType mediaType,
                                     MultivaluedMap<String, String> httpHeaders,
                                     InputStream entityStream) throws WebApplicationException {

        try {
            Model model = processModel(entityStream, mediaType, idProvider);
            IRI rootIri = getRootIri(model);
            return requestHandler.fromModel(rootIri, model);
        } catch (IOException | NoSuchElementException e) {
            throw new InvalidApiRequestException("Invalid Access Delegation structure.");
        }
    }
}
