// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.service.SubscriptionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.net.URI;

/**
 * Public endpoint to ...
 * 1) ... receive subscriptions ("Pub/Sub initiated by the Subscriber")
 * 2) ... answer subscription proposal requests sent by another 1R server (Pub/Sub initiated by the Publisher).
 */
@Path("/subscriptions")
@Consumes({"application/ld+json", "application/x-turtle", "text/turtle"})
@Produces({"application/ld+json"})
public class SubscriptionController {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionController.class);

    private final SubscriptionService subscriptionService;

    @Inject
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    /**
     * Subscriber initiated Pub/Sub, i.e. we receive a subscription. It will be wrapped in a SubscriptionRequest object
     * and enqueued for asynchronous processing where the decision is made to accept or to deny the request.
     */
    @POST
    @Authenticated
    public Response handleSubscription(Subscription subscription) {
        log.info("Receiving subscription request [{}]...", subscription);
        var subscriptionRequest = subscriptionService.handleSubscription(subscription);
        // TODO: Spec states to return HTTP code "no content"?!
        return Response.created(URI.create(subscriptionRequest.iri().stringValue())).build();
    }

    /**
     * Publisher initiated Pub/Sub, i.e. we are invited to subscribe and have to reply with a
     * corresponding Subscription object.
     *
     * @param topicType either LOGISTICS_OBJECT_TYPE or LOGISTICS_OBJECT_URI
     * @param topic either a specific LO type or LO iri, depending on topicType
     * @return Subscription
     */
    @GET
    @Authenticated
    public Subscription handleSubscriptionProposal(@QueryParam("topicType") String topicType,
                                                   @QueryParam("topic") String topic) {
        log.info("Receiving subscription proposal for topicType [{}] and topic [{}]...",
            topicType, topic);

        if (topicType == null || topicType.isBlank() ||
            topic == null || topic.isBlank()) {
            throw new InvalidApiRequestException("Missing query parameter 'topicType' or 'topic'.");
        }

        var tt = Subscription.TopicType.from(Values.iri(topicType));
        var subscription = subscriptionService.handleSubscriptionProposal(tt, topic);
        log.info("Proposal response: [{}]", subscription);

        return subscription;
    }
}
