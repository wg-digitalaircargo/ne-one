// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import io.smallrye.mutiny.Uni;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.client.OneRecordClientException;
import org.openlogisticsfoundation.neone.controller.BodyHandlerBase;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectProxyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.util.Collections;

@Path("/internal/proxy")
@Produces(BodyHandlerBase.APPLICATION_LD_JSON)
public class LogisticsObjectProxyController {

    private final IdProvider idProvider;

    private final LogisticsObjectProxyService logisticsObjectProxyService;


    @Inject
    public LogisticsObjectProxyController(IdProvider idProvider,
                                          LogisticsObjectProxyService logisticsObjectProxyService) {
        this.idProvider = idProvider;
        this.logisticsObjectProxyService = logisticsObjectProxyService;
    }

    @GET
    public Uni<Response> fetchLogisticsObject(@QueryParam("iri") String iri) {

        NeoneId id = idProvider.parse(iri);

        return logisticsObjectProxyService.fetchLogisticsObject(id)
                .invoke(statements -> statements.setNamespace("", CARGO.NAMESPACE)) // TODO: move setting the namespace to specific body writer
                .map(model -> Response.ok().entity(model).build())
                .onFailure(IllegalArgumentException.class).recoverWithItem(iriNotValid(iri))
                .onFailure(OneRecordClientException.class).recoverWithItem(throwable -> {
                    OneRecordClientException clientException = (OneRecordClientException) throwable;
                    return clientException.asErrorResponse("Upstream failed for IRI [" + iri + "]");
                });
    }

    private Response iriNotValid(String iri) {
        Error error = new Error(idProvider.createInternalIri().getIri(),
                "Not a valid LogisticsObject iri [" + iri + "]", Collections.emptySet());

        return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
    }
}
