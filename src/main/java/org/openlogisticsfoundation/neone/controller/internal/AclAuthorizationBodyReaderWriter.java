//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.controller.BodyHandlerBase;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.AclAuthorization;
import org.openlogisticsfoundation.neone.model.handler.AclAuthorizationHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;
import org.openlogisticsfoundation.neone.vocab.ACL;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

public class AclAuthorizationBodyReaderWriter {

    @Provider
    @Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
    @Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
    public static class SingleAclAuthorizationBodyReaderWriter
        extends BodyHandlerBase implements MessageBodyReader<AclAuthorization>, MessageBodyWriter<AclAuthorization> {

        private final IdProvider idProvider;

        private final AclAuthorizationHandler aclHandler;

        @Inject
        public SingleAclAuthorizationBodyReaderWriter(IdProvider idProvider, AclAuthorizationHandler aclHandler,
                                                      WriterConfig writerConfig,
                                                      CargoOntologyService cargoOntologyService) {
            super(writerConfig, cargoOntologyService);
            this.idProvider = idProvider;
            this.aclHandler = aclHandler;
        }

        @Override
        protected String rootNamespace() {
            return API.NAMESPACE;
        }

        @Override
        protected List<Namespace> additionalNamespaces() {
            return List.of(new SimpleNamespace(ACL.PREFIX, ACL.NAMESPACE));
        }

        @Override
        public boolean isReadable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
            return aClass == AclAuthorization.class;
        }

        @Override
        public AclAuthorization readFrom(Class<AclAuthorization> aClass, Type type, Annotation[] annotations,
                                         MediaType mediaType, MultivaluedMap<String, String> multivaluedMap,
                                         InputStream inputStream) throws WebApplicationException {
            try {
                Model model = processModel(inputStream, mediaType, idProvider);
                IRI rootIri = getRootIri(model);
                return aclHandler.fromModel(rootIri, model).withIri(idProvider.createAclAuthorizationId().getIri());
            } catch (IOException | NoSuchElementException e) {
                throw new InvalidApiRequestException("Invalid ACL authorization structure.");
            }
        }

        @Override
        public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
            return type.isAssignableFrom(AclAuthorization.class);
        }

        @Override
        public void writeTo(AclAuthorization aclAuthorization, Class<?> type,
                            Type genericType, Annotation[] annotations,
                            MediaType mediaType, MultivaluedMap<String, Object> httpHeaders,
                            OutputStream entityStream) throws WebApplicationException {

            writeRdfResponse(aclHandler.fromJava(aclAuthorization), mediaType, httpHeaders, entityStream);
        }
    }

    @Provider
    @Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
    public static class MultiAclAuthorizationBodyWriter
        extends BodyHandlerBase implements MessageBodyWriter<Collection<AclAuthorization>> {

        private final AclAuthorizationHandler aclHandler;

        @Inject
        public MultiAclAuthorizationBodyWriter(AclAuthorizationHandler aclHandler, WriterConfig writerConfig,
                                               CargoOntologyService cargoOntologyService) {
            super(writerConfig, cargoOntologyService);
            this.aclHandler = aclHandler;
        }

        @Override
        protected String rootNamespace() {
            return API.NAMESPACE;
        }

        @Override
        protected List<Namespace> additionalNamespaces() {
            return List.of(new SimpleNamespace(ACL.PREFIX, ACL.NAMESPACE));
        }

        @Override
        public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
            if (Collection.class.isAssignableFrom(type) && genericType instanceof ParameterizedType) {
                return ((ParameterizedType) genericType).getActualTypeArguments()[0].equals(AclAuthorization.class);
            } else {
                return false;
            }
        }

        @Override
        public void writeTo(Collection<AclAuthorization> aclAuthorizations, Class<?> type, Type genericType,
                            Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders,
                            OutputStream entityStream) throws WebApplicationException {

            Model[] aclModels = aclAuthorizations.stream().map(aclHandler::fromJava).toArray(Model[]::new);
            Model acls = aclHandler.merge(new DynamicModelFactory().createEmptyModel(), aclModels);

            writeRdfResponse(acls, mediaType, httpHeaders, entityStream);
        }
    }
}
