//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;


@Provider
@Consumes({SubscriptionProposalReader.APPLICATION_JSON})
public class SubscriptionProposalReader implements MessageBodyReader<SubscriptionProposal> {

    public static final String APPLICATION_JSON = "application/json";

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == SubscriptionProposal.class;
    }

    @Override
    public SubscriptionProposal readFrom(Class<SubscriptionProposal> type,
                                         Type genericType,
                                         Annotation[] annotations,
                                         MediaType mediaType,
                                         MultivaluedMap<String, String> httpHeaders,
                                         InputStream entityStream) throws WebApplicationException {
        try {
            return new ObjectMapper().readValue(entityStream, SubscriptionProposal.class);
        } catch (IOException e) {
            // In fact, exception mapping is the main purpose of the SubscriptionProposalReader.
            throw new InvalidApiRequestException("Invalid SubscriptionProposal structure.");
        }
    }
}
