//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.AclAuthorization;
import org.openlogisticsfoundation.neone.service.internal.AclAuthorizationService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Endpoint for internal users to administer ACLs.
 */
@Path("internal/acls")
@Consumes({"application/ld+json", "application/x-turtle", "text/turtle"})
@Produces({"application/ld+json", "application/x-turtle", "text/turtle"})
public class AclAuthorizationController {

    private final AclAuthorizationService aclService;

    private final IdProvider idProvider;

    @Inject
    public AclAuthorizationController(AclAuthorizationService aclService, IdProvider idProvider) {
        this.aclService = aclService;
        this.idProvider = idProvider;
    }

    @GET
    public List<AclAuthorization> findAclAuthorization(@QueryParam("agent") String agent,
                                                       @QueryParam("accessTo") String accessTo) {
        var agentIri = Optional.ofNullable(agent).filter(s -> !s.isBlank()).map(Values::iri);
        var accessToIri = Optional.ofNullable(accessTo).filter(s -> !s.isBlank()).map(Values::iri);

        if (agentIri.isEmpty() && accessToIri.isEmpty()) {
            throw new InvalidApiRequestException("Incomplete parameters. agent: " + agent + ", accessTo: " + accessTo);
        }

        return aclService.findAclAuthorizations(agentIri, accessToIri);
    }

    @GET
    @Path("{id}")
    public AclAuthorization getAclAuthorization(@PathParam("id") String id) {
        return aclService.getAclAuthorization(idProvider.getAclAuthorizationId(id));
    }

    @POST
    public Response createAclAuthorization(AclAuthorization aclAuthorization) {
        aclService.createAclAuthorization(aclAuthorization);
        var uri = URI.create(aclAuthorization.iri().stringValue());
        return Response.created(uri).build();
    }

    @POST
    @Path("grant")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response grantAccess(@FormParam("agent") String agent,
                                 @FormParam("accessTo") String accessTo,
                                 @FormParam("modes") Set<String> modes) {
        IRI aclIri = aclService.authorize(
            Values.iri(agent),
            Values.iri(accessTo),
            modes.stream().map(Values::iri).collect(Collectors.toSet())
        );

        return  Response.created(URI.create(aclIri.stringValue())).build();
    }

    @PATCH
    @Path("{id}")
    public Response updateAclAuthorization(@PathParam("id") String id, AclAuthorization aclAuthorization) {
        if (id.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        aclAuthorization = aclAuthorization.withIri(idProvider.getAclAuthorizationId(id).getIri());
        aclService.updateAclAuthorization(aclAuthorization);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteAclAuthorization(@PathParam("id") String id) {
        if (id.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        aclService.deleteAclAuthorization(idProvider.getAclAuthorizationId(id).getIri());
        return Response.ok().build();
    }
}
