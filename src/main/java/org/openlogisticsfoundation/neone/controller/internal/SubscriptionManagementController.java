// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import org.openlogisticsfoundation.neone.service.internal.SubscriptionManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Endpoint for internal users to trigger a subscription proposal for a defined topic and a certain partner.
 * ("Publisher/Subscriber initiated by the Publisher")
 */
@Path("internal/subscription/proposal")
@Consumes(MediaType.APPLICATION_JSON)
public class SubscriptionManagementController {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionManagementController.class);

    private final SubscriptionManagementService subService;

    @Inject
    public SubscriptionManagementController(SubscriptionManagementService subService) {
        this.subService = subService;
    }

    @POST
    public Response sendProposal(SubscriptionProposal proposal) {
        log.info("Request for sending subscription proposal to partner [{}] for type [{}] and topic [{}] received. Processing...",
            proposal.partnerUri(), proposal.topicType(), proposal.topic());
        var proposalResponse = subService.proposeSubscription(proposal.partnerUri(),
            proposal.topic(), proposal.topicType());
        log.info("Partner [{}] replied to proposal with the following response: [{}]", proposal.partnerUri(),
            proposalResponse.toString());
        return Response.ok().build();
    }
}
