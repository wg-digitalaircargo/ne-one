// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import io.quarkus.security.Authenticated;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.BlobContent;
import org.openlogisticsfoundation.neone.security.AccessMode;
import org.openlogisticsfoundation.neone.security.AccessObject;
import org.openlogisticsfoundation.neone.security.AclSecured;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectBlobStoreService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;

@Path("/files")
public class LogisticsObjectBlobStoreController {

    private final IdProvider idProvider;

    private final LogisticsObjectBlobStoreService blobStoreService;

    @Inject
    public LogisticsObjectBlobStoreController(IdProvider idProvider,
                                              LogisticsObjectBlobStoreService blobStoreService) {
        this.idProvider = idProvider;
        this.blobStoreService = blobStoreService;
    }

    @POST
    @Path("/upload/{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Authenticated
    public Response upload(@RestForm FileUpload data,
                           @RestForm String mimetype,
                           @RestForm String filename,
                           @PathParam("id") String shortId,
                           @Context UriInfo uriInfo) {

        NeoneId loId = idProvider.getUriForLoId(shortId);

        blobStoreService.saveBlob(shortId, data.uploadedFile(), filename, mimetype);

        URI uri = uriInfo.getBaseUriBuilder()
            .path(LogisticsObjectBlobStoreController.class)
            .path(LogisticsObjectBlobStoreController.class, "download")
            .build(shortId, filename);

        return Response.created(uri).build();
    }

    @GET
    @Path("/download/{id}/{filename}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Authenticated
    @AclSecured(
        @AccessMode(AccessMode.READ)
    )
    public Response download(@AccessObject @PathParam("id") String shortId,
                             @PathParam("filename") String filename) {

        BlobContent file = blobStoreService.downloadBlob(shortId, filename);

        return Response.ok(file.data())
            .header("Content-Type", file.contentType())
            .build();
    }
}
