//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.model.ServerInformation;
import org.openlogisticsfoundation.neone.model.handler.ServerInformationHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
@Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class ServerInformationWriter extends BodyHandlerBase implements MessageBodyWriter<ServerInformation> {

    private final ServerInformationHandler handler;

    @Inject
    public ServerInformationWriter(ServerInformationHandler handler, WriterConfig writerConfig,
                                   CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.handler = handler;
    }

    @Override
    protected String rootNamespace() {
        return "";
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type.isAssignableFrom(ServerInformation.class);
    }

    @Override
    public void writeTo(ServerInformation serverInformation, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
        throws WebApplicationException {

        Model model = handler.fromJava(serverInformation);
        model.setNamespace("", API.NAMESPACE);

        writeRdfResponse(model, mediaType, httpHeaders, entityStream);
    }
}
