// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.openlogisticsfoundation.neone.model.LogisticsObject;

import java.util.List;
import java.util.Set;

public record LogisticsObjectList(List<LogisticsObject> logisticsObjects, IRI rootIRI, Set<IRI> rootLoTypes) {

    public static LogisticsObjectList fromLogisticsObject(LogisticsObject logisticsObject) {
        Set<IRI> types = Models.objectIRIs(logisticsObject.model().filter(logisticsObject.iri(), RDF.TYPE, null));
        return new LogisticsObjectList(List.of(logisticsObject), logisticsObject.iri(), types);
    }
}
