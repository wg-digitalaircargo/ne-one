// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/notifications")
@Consumes({"application/ld+json", "application/x-turtle", "text/turtle"})
@Authenticated
public class NotificationController {

    private static final Logger log = LoggerFactory.getLogger(NotificationController.class);

    @ConfigProperty(name = "forward-notifications")
    boolean forwardNotifications;

    private final NotificationService notificationService;

    @Inject
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @POST
    public Response handleNotification(NotificationMessage notification) {
        log.info("Received notification [{}]", notification);
        if (forwardNotifications) {
            log.info("Forwarding notification [{}]", notification);
            notificationService.forward(notification);
        }
        notificationService.invalidateCachedLogisticsObject(notification);
        return Response.noContent().build();
    }
}
