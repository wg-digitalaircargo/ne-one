// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.exception.InvalidApiRequestException;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.handler.LogisticsObjectHandler;
import org.openlogisticsfoundation.neone.service.internal.CargoOntologyService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

@Provider
@Consumes({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
@Produces({BodyHandlerBase.APPLICATION_LD_JSON, BodyHandlerBase.APPLICATION_X_TURTLE, BodyHandlerBase.TEXT_TURTLE})
public class LogisticsObjectBodyReaderWriter extends BodyHandlerBase
    implements MessageBodyReader<LogisticsObjectList>, MessageBodyWriter<LogisticsObjectController.GetLogisticsObjectResponse> {

    private final IdProvider idProvider;

    private final LogisticsObjectHandler logisticsObjectHandler;

    @Inject
    public LogisticsObjectBodyReaderWriter(IdProvider idProvider, LogisticsObjectHandler logisticsObjectHandler,
                                           WriterConfig writerConfig, CargoOntologyService cargoOntologyService) {
        super(writerConfig, cargoOntologyService);
        this.idProvider = idProvider;
        this.logisticsObjectHandler = logisticsObjectHandler;
    }

    @Override
    protected String rootNamespace() {
        return CARGO.NAMESPACE;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == LogisticsObjectList.class;
    }

    @Override
    public LogisticsObjectList readFrom(Class<LogisticsObjectList> type,
                                    Type genericType,
                                    Annotation[] annotations,
                                    MediaType mediaType,
                                    MultivaluedMap<String, String> httpHeaders,
                                    InputStream entityStream) throws WebApplicationException {

        try {
            final ModelAndMetadata modelAndMetadata = processModelAndMetadata(entityStream, mediaType, idProvider);

            if (!rootTypeIsLo(modelAndMetadata.rootIriTypes())) {
                throw new InvalidApiRequestException("Root type is not a LogisticsObject");
            }

            final Model model = modelAndMetadata.model();
            Set<IRI> iris = Models.subjectIRIs(model);
            Map<IRI, Model> modelMapping = new HashMap<>();
            iris.forEach(iri -> {
                if (!idProvider.parse(iri).isInternal()) {
                    Model localModel = new DynamicModelFactory().createEmptyModel();
                    localModel.addAll(model.filter(iri, null, null));
                    embedEmbeddedObjects(localModel, model, new HashSet<>());
                    modelMapping.put(iri, localModel);
                }
            });

            List<LogisticsObject> logisticsObjects = modelMapping.entrySet().stream()
                .map(iriModelEntry ->
                    logisticsObjectHandler.fromModel(iriModelEntry.getKey(), iriModelEntry.getValue())
                )
                .toList();

            return new LogisticsObjectList(logisticsObjects, modelAndMetadata.rootIri(),
                modelAndMetadata.rootIriTypes());
        } catch (IOException | NoSuchElementException e) {
            throw new InvalidApiRequestException("Invalid Logistics Object structure.");
        }
    }

    private boolean rootTypeIsLo(Set<IRI> rootTypes) {
        return getCargoOntologyService().isLogisticsObject(
            rootTypes.stream().map(IRI::stringValue).toArray(String[]::new)
        );
    }

    private void embedEmbeddedObjects(final Model model, final Model baseModel, Set<IRI> visited) {
        Set<IRI> iris = Models.objectIRIs(model).stream()
            .filter(iri -> idProvider.parse(iri).isInternal() && !visited.contains(iri))
            .collect(Collectors.toSet());

        iris.forEach(iri -> {
            model.addAll(baseModel.filter(iri, null, null));
            visited.add(iri);
            embedEmbeddedObjects(model, baseModel, visited);
        });
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type.isAssignableFrom(org.openlogisticsfoundation.neone.controller.LogisticsObjectController.GetLogisticsObjectResponse.class);
    }

    @Override
    public void writeTo(org.openlogisticsfoundation.neone.controller.LogisticsObjectController.GetLogisticsObjectResponse loResponse, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
        throws WebApplicationException {

        Model model = logisticsObjectHandler.fromJava(loResponse.logisticsObject);
        loResponse.headers.forEach(httpHeaders::addAll);

        writeRdfResponse(model, mediaType, httpHeaders, entityStream);
    }
}
