// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelCollector;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.LogisticsEvent;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.model.handler.LogisticsEventHandler;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsEventRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.security.AccessSubject;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import java.net.URI;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@ApplicationScoped
public class LogisticsEventService {

    @ConfigProperty(name = "query-limit")
    Integer queryLimit;

    @ConfigProperty(name = "authorize-creator")
    boolean authorizeCreator;

    private final LogisticsEventRepository loEventRepository;

    private final RepositoryTransaction transaction;

    private final LogisticsObjectService loService;

    private final LogisticsEventHandler logisticsEventHandler;

    private final NeoneEventRepository neoneEventRepository;

    private final LogisticsObjectRepository loRepository;

    private final IdProvider idProvider;

    private final AclAuthorizationRepository aclRepository;

    private final NeoneEventService neoneEventService;

    private final Instance<AccessSubject> accessSubject;

    @Inject
    public LogisticsEventService(LogisticsEventRepository loEventRepository,
                                 LogisticsObjectService loService,
                                 RepositoryTransaction transaction,
                                 LogisticsEventHandler logisticsEventHandler,
                                 NeoneEventRepository neoneEventRepository,
                                 LogisticsObjectRepository loRepository,
                                 IdProvider idProvider,
                                 AclAuthorizationRepository aclRepository,
                                 NeoneEventService neoneEventService,
                                 Instance<AccessSubject> accessSubject) {

        this.loEventRepository = loEventRepository;
        this.loService = loService;
        this.logisticsEventHandler = logisticsEventHandler;
        this.transaction = transaction;
        this.neoneEventRepository = neoneEventRepository;
        this.loRepository = loRepository;
        this.idProvider = idProvider;
        this.aclRepository = aclRepository;
        this.neoneEventService = neoneEventService;
        this.accessSubject = accessSubject;
    }

    public LogisticsEvent getLogisticsEvent(NeoneId loEventId, boolean embedLo, Instant loAt) {
        var event = fetchEvent(loEventId);
        Optional<LogisticsObject> lo = embedLo
            ? fetchLogisticsObjectLinkedToEvent(event, loAt)
            : Optional.empty();

        return lo.map(LogisticsObject::model)
            .map(event::withEmbeddedLoModel)
            .orElse(event);
    }

    public List<LogisticsEvent> findLogisticsEvents(NeoneId loId,
                                                    String eventType,
                                                    Instant createdFrom,
                                                    Instant createdUntil,
                                                    Instant occurredFrom,
                                                    Instant occurredUntil,
                                                    Integer limit) {

        var max = Optional.ofNullable(limit).filter(l -> l > 0 && l <= queryLimit).orElse(queryLimit);
        return transaction.transactionallyGet(connection -> {
                List<LogisticsEvent> events;
                if (loRepository.exists(loId.getIri(), connection)) {
                    events = loEventRepository.findEventsOfLogisticsObject(
                        loId.getIri(),
                        Optional.ofNullable(eventType),
                        Optional.ofNullable(createdFrom),
                        Optional.ofNullable(createdUntil),
                        Optional.ofNullable(occurredFrom),
                        Optional.ofNullable(occurredUntil),
                        max,
                        connection);
                } else {
                    throw new SubjectNotFoundException(loId.getIri().stringValue());
                }
                return events;
            }
        );
    }

    private LogisticsEvent fetchEvent(NeoneId loEventId) {
        return transaction.transactionallyGet(connection ->
            loEventRepository.findByIri(loEventId.getIri(), connection)
        ).orElseThrow(() -> new SubjectNotFoundException(loEventId.getIri().stringValue()));
    }

    private Optional<LogisticsObject> fetchLogisticsObjectLinkedToEvent(LogisticsEvent event, Instant loAt) {
        var at = Optional.ofNullable(loAt);
        return event.eventFor().flatMap(loIri ->
            at.flatMap(instant -> loService.getLogisticsObjectSnapshot(loIri, instant))
                .or(() -> Optional.of(loService.getLogisticsObject(URI.create(loIri.stringValue()), false)))
        );
    }

    public LogisticsEvent addLogisticsEvent(NeoneId loId, LogisticsEvent event) {
        // assign event id
        LogisticsEvent eventWithId = event.withId(idProvider.createLoEventUri(loId).getIri());

        transaction.transactionallyDo((connection, hooks) -> {
            if (!loRepository.exists(loId.getIri(), connection)) {
                throw new SubjectNotFoundException(loId.getIri().stringValue());
            }
            var loTypes = loRepository.getLogisticsObjectTypes(loId.getIri(), connection);
            var completedEvent = eventWithId
                .withLinkedObjectAndCreatedAt(loId.getIri(), Instant.now());
            loEventRepository.persist(completedEvent.iri(), completedEvent, connection);

            NeoneEvent loEventReceivedEvent = neoneEventRepository.addEvent(
                loId.getIri(), API.LOGISTICS_EVENT_RECEIVED, Optional.empty(),
                Collections.emptySet(), loTypes, connection
            );
            hooks.registerPostCommitHook(() -> neoneEventService.publishEvent(loEventReceivedEvent));

            IRI creatorIri = authorizeCreator ? accessSubject.get().iri() : null;
            aclRepository.grantDefaultAccess(eventWithId.iri(), creatorIri, false, connection);
        });

        return eventWithId;
    }

    private Model setRootIRI(NeoneId loEventId, Model model) {
        Model root = findRoot(model);

        return model.stream()
            .map(st ->
                root.stream().toList().contains(st)
                    ? SimpleValueFactory.getInstance().createStatement(loEventId.getIri(), st.getPredicate(), st.getObject())
                    : st
            )
            .collect(ModelCollector.toModel());
    }

    private static Model findRoot(Model model) {
        Predicate<Value> statementWithObjectValueExists =
            object -> model.getStatements(null, null, object).iterator().hasNext();

        Map<Resource, List<Statement>> statementsBySubject =
            model.stream().collect(Collectors.groupingBy(Statement::getSubject));

        return statementsBySubject.keySet().stream()
            .filter(Predicate.not(statementWithObjectValueExists)).findAny()
            .map(statementsBySubject::get).orElse(Collections.emptyList())
            .stream().collect(ModelCollector.toModel());
    }
}
