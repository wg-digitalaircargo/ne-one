//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.common.annotation.Blocking;
import org.apache.commons.math3.util.Pair;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.InvalidAccessDelegationObjectException;
import org.openlogisticsfoundation.neone.model.AccessDelegation;
import org.openlogisticsfoundation.neone.model.AccessDelegationRequest;
import org.openlogisticsfoundation.neone.model.AclAuthorization;
import org.openlogisticsfoundation.neone.model.ActionRequestType;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.NotificationEventType;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.handler.AccessDelegationRequestHandler;
import org.openlogisticsfoundation.neone.repository.AccessDelegationRequestRepository;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.security.AccessSubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class AccessDelegationService {

    private static final Logger log = LoggerFactory.getLogger(AccessDelegationService.class);

    @ConfigProperty(name = "authorize-creator")
    boolean authorizeCreator;

    private final AccessDelegationRequestHandler accessDelegationRequestHandler;
    private final AccessDelegationRequestRepository accessDelegationRequestRepository;
    private final AclAuthorizationRepository aclAuthorizationRepository;
    private final LogisticsObjectRepository logisticsObjectRepository;
    private final RepositoryTransaction transaction;
    private final ActionRequestService actionRequestService;
    private final IdProvider idProvider;
    private final Instance<AccessSubject> accessSubject;
    private final ActionRequestNotificationService notificationService;

    @Inject
    public AccessDelegationService(AccessDelegationRequestHandler accessDelegationRequestHandler,
                                   AccessDelegationRequestRepository accessDelegationRequestRepository,
                                   LogisticsObjectRepository logisticsObjectRepository,
                                   AclAuthorizationRepository aclAuthorizationRepository,
                                   RepositoryTransaction transaction,
                                   ActionRequestService actionRequestService,
                                   IdProvider idProvider,
                                   Instance<AccessSubject> accessSubject,
                                   ActionRequestNotificationService notificationService) {
        this.accessDelegationRequestHandler = accessDelegationRequestHandler;
        this.accessDelegationRequestRepository = accessDelegationRequestRepository;
        this.logisticsObjectRepository = logisticsObjectRepository;
        this.aclAuthorizationRepository = aclAuthorizationRepository;
        this.transaction = transaction;
        this.actionRequestService = actionRequestService;
        this.idProvider = idProvider;
        this.accessSubject = accessSubject;
        this.notificationService = notificationService;
    }

    public AccessDelegationRequest handleAccessDelegation(AccessDelegation accessDelegation) {
        checkAccessDelegation(accessDelegation);
        var requestIri = idProvider.createUniqueAccessDelegationRequestUri().getIri();
        var requestedBy = accessSubject.get().iri();
        var request = new AccessDelegationRequest(requestIri, Optional.empty(),
            RequestStatus.REQUEST_PENDING, Instant.now(),
            requestedBy, Optional.empty(), Optional.empty(), accessDelegation);

        var accessDelegationRequest = accessDelegationRequestHandler.fromJava(request);
        transaction.transactionallyDo(connection -> {
            accessDelegationRequestRepository.persist(accessDelegationRequest, connection);
            IRI creatorIri = authorizeCreator ? requestedBy : null;
            aclAuthorizationRepository.grantDefaultAccess(requestIri, creatorIri, false, connection);
        });
        return request;
    }

    private void checkAccessDelegation(AccessDelegation accessDelegation) {
        // Check if referenced LOs exist.
        var logisticsObjects = new HashSet<>(accessDelegation.hasLogisticsObject());
        transaction.transactionallyDo(conn -> {
            var existing = logisticsObjects.stream().filter(iri ->
                logisticsObjectRepository.exists(iri, conn)).toList();
            logisticsObjects.removeAll(existing);
            if (!logisticsObjects.isEmpty()) {
                throw new InvalidAccessDelegationObjectException(logisticsObjects);
            }
        });
    }

    @ConsumeEvent(ActionRequestType.ACCESS_DELEGATION_REQUEST)
    @Blocking
    public void onAccessDelegationRequestChanged(AccessDelegationRequest accessDelegationRequest) {
        var status = accessDelegationRequest.requestStatus();
        log.info("Status of accessDelegationRequest [{}] has changed to [{}].",
            accessDelegationRequest.iri(), status);
        transaction.transactionallyDo(connection -> {
            actionRequestService.overwriteActionRequestStatus(accessDelegationRequest.iri(), status);
            if (status.equals(RequestStatus.REQUEST_ACCEPTED)) {
                try {
                    addToAcl(accessDelegationRequest.accessDelegation(), connection);
                    sendNotification(accessDelegationRequest, NotificationEventType.ACCESS_DELEGATION_REQUEST_ACCEPTED);
                } catch (RuntimeException ex) {
                    actionRequestService.overwriteActionRequestStatus(accessDelegationRequest.iri(),
                        RequestStatus.REQUEST_FAILED);
                    sendNotification(accessDelegationRequest, NotificationEventType.ACCESS_DELEGATION_REQUEST_FAILED);
                    throw ex;
                }
            } else if (status.equals(RequestStatus.REQUEST_REJECTED)) {
                sendNotification(accessDelegationRequest, NotificationEventType.ACCESS_DELEGATION_REQUEST_REJECTED);
            } else if (status.equals(RequestStatus.REQUEST_REVOKED)) {
                revokeAccessDelegationRequest(accessDelegationRequest, connection);
                sendNotification(accessDelegationRequest, NotificationEventType.ACCESS_DELEGATION_REQUEST_REVOKED);
            }
        });
    }

    private void addToAcl(AccessDelegation delegation, RepositoryConnection connection) {
        var targets = delegation.hasLogisticsObject();
        var agents = delegation.isRequestedFor();
        var modes = delegation.permissions().stream().map(AccessDelegation.Permission::mode)
            .collect(Collectors.toSet());

        cartesianProductOf(targets, agents).forEach(pair -> {
            var aclIri = idProvider.createAclAuthorizationId().getIri();
            var aclAuth = new AclAuthorization(aclIri, pair.getFirst(), pair.getSecond(), modes);
            aclAuthorizationRepository.persist(aclAuth, connection);
        });
    }

    private List<Pair<IRI, IRI>> cartesianProductOf(Set<IRI> targets, Set<IRI> agents) {
        // create set of any combination of (target, agent)
        return targets.stream()
            .flatMap(targetIri -> agents.stream()
                .map(agentIri -> new Pair<>(targetIri, agentIri)))
            .toList();
    }

    private void revokeAccessDelegationRequest(AccessDelegationRequest r, RepositoryConnection connection) {
        var agents = r.accessDelegation().isRequestedFor();
        var targets = r.accessDelegation().hasLogisticsObject();
        cartesianProductOf(agents, targets).forEach(pair ->
            revokeDelegatedAccess(pair.getFirst(), pair.getSecond(), connection));
    }

    private void revokeDelegatedAccess(IRI agent, IRI accessTo, RepositoryConnection connection) {
        var acls = aclAuthorizationRepository.findAclAuthorizations(Optional.of(agent),
            Optional.of(accessTo), connection);
        // Look for transitively delegated access:
        var requestedFor = accessDelegationRequestRepository.findRequestedFor(agent, accessTo, connection);

        acls.forEach(acl -> aclAuthorizationRepository.deleteAll(acl.iri(), connection));

        requestedFor.ifPresent(iri -> revokeDelegatedAccess(iri, accessTo, connection));
    }

    private void sendNotification(AccessDelegationRequest actionRequest, NotificationEventType eventType) {
        if (!actionRequest.accessDelegation().notifyRequestStatusChange().orElse(false)) {
            return;
        }

        Notification notification = new Notification(
            idProvider.createInternalIri().getIri(),
            eventType.iri(),
            Optional.of(Subscription.TopicType.LOGISTICS_OBJECT_TYPE.getValue().stringValue()),
            Optional.empty(),
            Optional.of(actionRequest.requestedBy()),
            Collections.emptySet());
        notificationService.notifyRequestor(actionRequest, notification);
    }
}
