// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.common.annotation.Blocking;
import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.controller.LogisticsObjectList;
import org.openlogisticsfoundation.neone.exception.AlreadyExistsException;
import org.openlogisticsfoundation.neone.exception.BadRevisionNumberException;
import org.openlogisticsfoundation.neone.exception.InvalidAddressException;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.exception.RevisionMissingException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.ActionRequestType;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.ChangeRequest;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.model.Metadata;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.NotificationEventType;
import org.openlogisticsfoundation.neone.model.Operation;
import org.openlogisticsfoundation.neone.model.PatchOperation;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.ChangeRequestRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectMetadataRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.security.InternalAccessSubject;
import org.openlogisticsfoundation.neone.vocab.ACL;
import org.openlogisticsfoundation.neone.vocab.NEONE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class LogisticsObjectService {

    private static final Logger log = LoggerFactory.getLogger(LogisticsObjectService.class);

    @ConfigProperty(name = "authorize-creator")
    boolean authorizeCreator;

    private final LogisticsObjectRepository loRepository;

    private final LogisticsObjectMetadataRepository metadataRepository;

    private final NeoneEventRepository neoneEventRepository;

    private final RepositoryTransaction transaction;

    private final AuditTrailService auditTrailService;

    private final SnapshotService snapshotService;

    private final ActionRequestService actionRequestService;

    private final IdProvider idProvider;

    private final ChangeRequestRepository changeRequestRepository;

    private final InternalAccessSubject accessSubject;

    private final ActionRequestNotificationService notificationService;

    private final AclAuthorizationRepository aclRepository;

    private final NeoneEventService neoneEventService;

    @Inject
    public LogisticsObjectService(LogisticsObjectRepository loRepository,
                                  LogisticsObjectMetadataRepository metadataRepository,
                                  NeoneEventRepository neoneEventRepository,
                                  RepositoryTransaction transaction,
                                  AuditTrailService auditTrailService,
                                  SnapshotService snapshotService,
                                  ActionRequestService actionRequestService,
                                  ChangeRequestRepository changeRequestRepository,
                                  IdProvider idProvider,
                                  InternalAccessSubject accessSubject,
                                  ActionRequestNotificationService notificationService,
                                  AclAuthorizationRepository aclRepository,
                                  NeoneEventService neoneEventService) {
        this.loRepository = loRepository;
        this.metadataRepository = metadataRepository;
        this.neoneEventRepository = neoneEventRepository;
        this.transaction = transaction;
        this.auditTrailService = auditTrailService;
        this.actionRequestService = actionRequestService;
        this.snapshotService = snapshotService;
        this.idProvider = idProvider;
        this.changeRequestRepository = changeRequestRepository;
        this.accessSubject = accessSubject;
        this.notificationService = notificationService;
        this.aclRepository = aclRepository;
        this.neoneEventService = neoneEventService;
    }

    public void createLogisticsObject(LogisticsObject logisticsObject) {
        createLogisticsObjects(LogisticsObjectList.fromLogisticsObject(logisticsObject));
    }

    public void createLogisticsObject(LogisticsObject logisticsObject, boolean publicAccess) {
        createLogisticsObjects(LogisticsObjectList.fromLogisticsObject(logisticsObject), publicAccess);
    }

    public void createLogisticsObject(LogisticsObject logisticsObject, boolean authorizeCreator, boolean publicAccess) {
        createLogisticsObject(LogisticsObjectList.fromLogisticsObject(logisticsObject), authorizeCreator, publicAccess);
    }

    public void createLogisticsObjects(LogisticsObjectList logisticsObjects) {
        createLogisticsObjects(logisticsObjects, false);
    }

    public void createLogisticsObjects(LogisticsObjectList logisticsObjects, boolean publicAccess) {
        createLogisticsObject(logisticsObjects, authorizeCreator, publicAccess);
    }

    public void createLogisticsObject(LogisticsObjectList logisticsObjects, boolean authorizeCreator,
                                      boolean publicAccess) {

        transaction.transactionallyDo((connection, hooks) -> {

            for (LogisticsObject logisticsObject : logisticsObjects.logisticsObjects()) {
                var loTypes = getLogisticsObjectTypes(logisticsObject.iri(), logisticsObject.model());

                log.info("Persisting model with root IRI [{}] of type(s) [{}]",
                    logisticsObject.iri().stringValue(),
                    loTypes.stream().map(IRI::stringValue).collect(Collectors.joining(",")));
                boolean hasPredefIri = hasPredefinedIri(logisticsObject);
                if (hasPredefIri) {
                    checkIri(logisticsObject, connection);
                }
                removeHasPredefinedIriPredicate(logisticsObject);
                loRepository.persist(logisticsObject, connection);
                createAndPersistLoMetadata(logisticsObject.iri(), hasPredefIri, connection);
                snapshotService.createSnapshot(logisticsObject, AuditTrailService.INITIAL_REVISION, connection);
                auditTrailService.createAuditTrail(logisticsObject.iri(), connection);
                NeoneEvent loCreatedEvent = createAndPersistLoCreatedEvent(logisticsObject.iri(), loTypes, connection);
                hooks.registerPostCommitHook(() -> neoneEventService.publishEvent(loCreatedEvent));

                IRI creatorIri = null;
                if (authorizeCreator) {
                    if (accessSubject.isResolvable()) {
                        creatorIri = accessSubject.iri();
                    } else {
                        // Creating a logistics object does not necessarily require authentication.
                        log.warn("Creator not authenticated. Access permission to logistics object [{}] not established!",
                            logisticsObject.iri());
                    }
                }
                aclRepository.grantDefaultAccess(logisticsObject.iri(), creatorIri, publicAccess, connection);
            }
        });
    }

    private boolean hasPredefinedIri(LogisticsObject logisticsObject) {
        // Remove predicate temporarily introduced by LogisticsObjectBodyReader. Information is still part
        // of metadata.
        var hasPredefIri = Models.objectLiteral(logisticsObject.model().filter(logisticsObject.iri(), NEONE.hasPredefinedIri, null));
        return hasPredefIri.map(Literal::booleanValue).orElse(false);
    }

    private void checkIri(LogisticsObject logisticsObject, RepositoryConnection connection) {
        var iri = logisticsObject.iri();
        // Check for proper address path.
        if (!iri.toString().contains(idProvider.getLogisticsObjectBaseIri().toString())) {
            throw new InvalidAddressException(iri, idProvider.getLogisticsObjectBaseIri());
        }

        // Check if IRI is already taken.
        Optional<LogisticsObject> entity = loRepository.findByIri(iri, connection);
        entity.ifPresent(e -> {
            throw new AlreadyExistsException(e.iri().stringValue());
        });
    }

    private void removeHasPredefinedIriPredicate(LogisticsObject logisticsObject) {
        IRI iri = logisticsObject.iri();
        logisticsObject.model().remove(iri, NEONE.hasPredefinedIri, null);
    }

    public Model getAllLogisticsObjects(int limit, int offset, String loType) {
        IRI loTypeIri;
        if (loType == null) {
            loTypeIri = CARGO.LogisticsObject;
        } else {
            loTypeIri = Values.iri(loType);
        }
        return transaction.transactionallyGet(connection ->
            loRepository.getLogisticsObjects(limit, offset, loTypeIri, connection)
        );
    }

    public LogisticsObject getLogisticsObject(URI uri, boolean embedded) {
        log.debug("Fetching LO for uri [{}]", uri);
        IRI iri = Values.iri(uri.toString());
        log.debug("Generated IRI to fetch logistic object: [{}]", iri.toString());

        Optional<LogisticsObject> lo = transaction.transactionallyGet(connection ->
            embedded ?
                loRepository.findGraphByIri(iri, accessSubject.iri(), connection) :
                loRepository.findByIri(iri, connection)
        );

        return lo.orElseThrow(() -> new SubjectNotFoundException(iri.stringValue()));
    }

    public Optional<Snapshot> getSnapshot(IRI loIri, Instant at) {
        log.debug("Fetching snapshot of Logistics Object [{}] at [{}]", loIri, at);
        return snapshotService.getSnapshot(loIri, at);
    }

    public Optional<LogisticsObject> getLogisticsObjectSnapshot(IRI loIri, Instant at) {
        return getSnapshot(loIri, at).map(LogisticsObjectService::toLogisticsObject);
    }

    public static LogisticsObject toLogisticsObject(Snapshot snapshot) {
        try (InputStream is = IOUtils.toInputStream(snapshot.payload(), "UTF-8")) {
            Model model = Rio.parse(is, "", RDFFormat.JSONLD);
            return new LogisticsObject(snapshot.loIri(), model);
        } catch (IOException e) {
            throw new NeoneException("Cannot retrieve Logistics Object from snapshot", e);
        }
    }

    public ChangeRequest createChangeRequest(NeoneId loId, Change change) {
        log.debug("Creating Change Request for Logistics Object [{}]", loId.getIri());

        IRI loIri = loId.getIri();

        return transaction.transactionallyGet((connection, hooks) -> {
            LogisticsObject logisticsObject = loRepository.findByIri(loIri, connection)
                .orElseThrow(() -> new SubjectNotFoundException(loIri.stringValue()));
            checkRevision(logisticsObject.iri(), change, connection);
            var changeRequest = createAndPersistChangeRequest(change, connection);
            auditTrailService.updateAuditTrail(loIri, changeRequest, connection);
            var loTypes = getLogisticsObjectTypes(loIri, logisticsObject.model());
            NeoneEvent changeRequestPendingEvent = createChangeRequestPendingEvent(loIri, loTypes, connection);
            hooks.registerPostCommitHook(() -> neoneEventService.publishEvent(changeRequestPendingEvent));
            aclRepository.grantAccess(
                idProvider.getDataHolderId().getIri(), changeRequest.iri(), Set.of(ACL.Read, ACL.Write), connection
            );
            if (authorizeCreator && !idProvider.getDataHolderId().getIri().equals(accessSubject.iri())) {
                aclRepository.grantAccess(
                    accessSubject.iri(), changeRequest.iri(), Set.of(ACL.Read, ACL.Write), connection
                );
            }

            return changeRequest;
        });
    }

    private Set<IRI> getLogisticsObjectTypes(IRI loIri, Model model) {
        var types = Models.objectIRIs(model.filter(loIri, RDF.TYPE, null));
        if(types.isEmpty()) {
            throw new NeoneException("LogisticsObject [" + loIri.stringValue() + "] not found");
        }
        return types;
    }

    @ConsumeEvent(value = ActionRequestType.CHANGE_REQUEST, ordered = true)
    @Blocking
    public void onChangeRequestUpdated(ChangeRequest changeRequest) {
        RequestStatus status = changeRequest.requestStatus();
        IRI loIri = changeRequest.change().hasLogisticsObject();

        transaction.transactionallyDo((connection, hooks) -> {
            var loTypes = loRepository.getLogisticsObjectTypes(loIri, connection);
            actionRequestService.overwriteActionRequestStatus(changeRequest.iri(), status);
            if (status.equals(RequestStatus.REQUEST_REJECTED)) {
                rejectChangeRequest(changeRequest, loIri, loTypes, connection, hooks);
            } else if (status.equals(RequestStatus.REQUEST_ACCEPTED)) {
                acceptChangeRequest(changeRequest, loIri, loTypes, connection, hooks);
            } else if (status.equals(RequestStatus.REQUEST_REVOKED)) {
                revokeChangeRequest(changeRequest);
            }
        });
    }

    private void rejectChangeRequest(ChangeRequest cr, IRI loIri, Set<IRI> loTypes, RepositoryConnection connection,
                                     RepositoryTransaction.RepositoryTransactionHooks hooks) {

        auditTrailService.updateAuditTrail(loIri, cr, connection);
        NeoneEvent changeRequestRejectedEvent = createChangeRequestRejectedEvent(loIri, loTypes, connection);
        hooks.registerPostCommitHook(() -> neoneEventService.publishEvent(changeRequestRejectedEvent));
        sendNotification(cr, NotificationEventType.CHANGE_REQUEST_REJECTED);
    }

    private void acceptChangeRequest(ChangeRequest cr, IRI loIri, Set<IRI> loTypes, RepositoryConnection connection,
                                     RepositoryTransaction.RepositoryTransactionHooks hooks) {
        try {
            Change change = cr.change();
            // check revision
            int currentRevision = metadataRepository.getRevision(loIri, connection);
            if (change.revision() != currentRevision) {
                throw new BadRevisionNumberException(change.revision());
            }
            var changedProperties = applyOperations(change, connection);
            var revision = increaseRevision(loIri, change.revision(), connection);
            createSnapshot(loIri, revision, connection);
            auditTrailService.updateAuditTrail(loIri, cr, connection);
            NeoneEvent changeRequestAcceptedEvent = createChangeRequestAcceptedEvent(loIri, loTypes, connection);
            hooks.registerPostCommitHook(() -> neoneEventService.publishEvent(changeRequestAcceptedEvent));
            NeoneEvent loUpdatedEvent =
                createAndPersistLoUpdatedEvent(loIri, changedProperties, cr.iri(), loTypes, connection);
            hooks.registerPostCommitHook(() -> neoneEventService.publishEvent(loUpdatedEvent));
            sendNotification(cr, NotificationEventType.CHANGE_REQUEST_ACCEPTED);
        } catch (RuntimeException ex) {
            hooks.registerPostRollbackHook(() -> {
                actionRequestService.overwriteActionRequestStatus(cr.iri(), RequestStatus.REQUEST_FAILED);
                sendNotification(cr, NotificationEventType.CHANGE_REQUEST_FAILED);
            });
            throw ex;
        }
    }

    private void revokeChangeRequest(ChangeRequest cr) {
        sendNotification(cr, NotificationEventType.CHANGE_REQUEST_REVOKED);
    }

    private Set<IRI> applyOperations(Change change, RepositoryConnection connection) {
        log.debug("Applying operations...");

        Set<IRI> changedProperties = new HashSet<>();
        Set<Resource> affectedSubjects = new HashSet<>();

        Supplier<Stream<Operation>> deletes = () -> change.operations().stream()
            .filter(operation -> operation.op() == PatchOperation.DELETE);

        // assemble triples to delete
        deletes.get().forEach(operation -> {
            changedProperties.add(operation.p());

            // get the subject
            Resource subject = Values.iri(operation.s());
            affectedSubjects.add(subject);

            // either the datatype id a core XSD datatype or an IRI
            CoreDatatype literalDatatype = CoreDatatype.from(Values.iri(operation.o().datatype()));
            Value object;
            if (literalDatatype.isXSDDatatype()) {
                object = Values.literal(operation.o().value(), literalDatatype);
            } else {
                object = Values.iri(operation.o().value());
            }

            // delete the triple
            int deletedTriples = loRepository.delete(subject, operation.p(), object, connection);

            // if not exactly one triple is deleted throw an exception, this cancels the transaction and
            // nothing is changed
            if (deletedTriples != 1) {
                throw new NeoneException("Number of affected triples should be 1 but was " + deletedTriples);
            }
        });

        // now to the add operations
        Supplier<Stream<Operation>> adds = () -> change.operations().stream()
            .filter(operation -> operation.op() == PatchOperation.ADD);

        // assign internal IDs for non literal values (embedded objects)
        Map<String, IRI> bnodeToIri = adds.get()
            .filter(operation -> !CoreDatatype.from(Values.iri(operation.o().datatype())).isXSDDatatype())
            .collect(Collectors.toMap(
                operation -> operation.o().value(),
                s -> idProvider.createInternalIri().getIri(),
                (iri, iri2) -> iri
            ));

        adds.get().forEach(operation -> {
            changedProperties.add(operation.p());
            // either the subject is an IRI or a blank node that is mapped to an internal IRI
            IRI subject = bnodeToIri.computeIfAbsent(operation.s(), s -> Values.iri(operation.s()));

            CoreDatatype literalDatatype = CoreDatatype.from(Values.iri(operation.o().datatype()));
            Value object;
            if (literalDatatype.isXSDDatatype()) {
                // a literal is added
                object = Values.literal(operation.o().value(), literalDatatype);
            } else {
                IRI objectIRI;
                //check if the object is an embedded object by finding another add operation with the object as subject
                if (adds.get().anyMatch(op -> op.s().equals(operation.o().value()))) {
                    // calculate the internal IRI of the "embedded object"
                    objectIRI = bnodeToIri.computeIfAbsent(
                        operation.o().value(), s -> Values.iri(operation.o().value())
                    );
                    // if an "embedded" object is to be added, add type statement using operations datatype value
                    loRepository.add(objectIRI, RDF.TYPE, Values.iri(operation.o().datatype()), connection);
                } else {
                    // an IRI, i.e. a reference to another resource is to be added
                    objectIRI = Values.iri(operation.o().value());
                }
                object = objectIRI;
            }

            loRepository.add(subject, operation.p(), object, connection);
        });

        return changedProperties;
    }

    private void checkRevision(IRI loIri, Change change, RepositoryConnection connection) {
        log.debug("Checking revision...");
        int revisionFromRequest = change.revision();
        int revision = metadataRepository.getMetadataOfSubject(loIri, connection)
            .map(LogisticsObjectMetadata::revision)
            .orElseThrow(() -> new RevisionMissingException(loIri));
        if (revision != revisionFromRequest) {
            throw new BadRevisionNumberException(revisionFromRequest);
        }
    }

    private ChangeRequest createAndPersistChangeRequest(Change change,
                                                        RepositoryConnection connection) {
        log.debug("Creating and persisting ChangeRequest...");

        NeoneId changeRequestId = idProvider.createActionRequestId();

        ChangeRequest changeRequest = new ChangeRequest(
            changeRequestId.getIri(),
            Optional.empty(),
            RequestStatus.REQUEST_PENDING,
            Instant.now(),
            accessSubject.iri(),
            Optional.empty(),
            Optional.empty(),
            change);

        changeRequestRepository.persist(changeRequest, connection);

        return changeRequest;
    }

    private void createAndPersistLoMetadata(IRI loIri, boolean hasPredefinedIri, RepositoryConnection connection) {
        LogisticsObjectMetadata metadata = new LogisticsObjectMetadata(
            idProvider.createInternalIri().getIri(),
            loIri,
            1,
            Instant.now(),
            Optional.of(hasPredefinedIri)
        );
        metadataRepository.persist(metadata, connection);
    }

    private int increaseRevision(IRI loIri, int previousRevision, RepositoryConnection connection) {
        var newRevision = previousRevision + 1;
        log.debug("Increasing revision for LO [{}] to [{}])...", loIri, newRevision);
        Metadata metadata = metadataRepository.getMetadataOfSubject(loIri, connection)
            .orElseThrow(() -> new SubjectNotFoundException(loIri.stringValue()));
        metadataRepository.delete(metadata.iri(), NEONE.hasRevision, connection);
        metadataRepository.add(metadata.iri(), NEONE.hasRevision, Values.literal(newRevision), connection);
        return newRevision;
    }

    private void createSnapshot(IRI loIri, int revision, RepositoryConnection connection) {
        log.debug("Creating snapshot of LO [{}]...", loIri);
        // Ensure to have the newest LO at hand, in full detail!
        var lo = loRepository.findByIri(loIri, connection)
            .orElseThrow(() -> new NeoneException("Cannot find logistics object " + loIri));
        snapshotService.createSnapshot(lo, revision, connection);
    }

    private NeoneEvent createAndPersistLoCreatedEvent(IRI loIri, Set<IRI> loTypes, RepositoryConnection connection) {
        log.debug("Creating and persisting LogisticsObjectEvent (CREATED)...");
        return neoneEventRepository.addEvent(loIri, API.LOGISTICS_OBJECT_CREATED,
            Optional.empty(), Collections.emptySet(), loTypes, connection);
    }

    private NeoneEvent createAndPersistLoUpdatedEvent(IRI loIri, Set<IRI> changedProperties,
                                                      IRI changeRequestIri, Set<IRI> loTypes,
                                                      RepositoryConnection connection) {
        log.debug("Creating and persisting LogisticsObjectEvent (UPDATED)...");
        return neoneEventRepository.addEvent(loIri, API.LOGISTICS_OBJECT_UPDATED,
            Optional.of(changeRequestIri), changedProperties, loTypes, connection
        );
    }

    private NeoneEvent createChangeRequestPendingEvent(IRI loIri, Set<IRI> loTypes, RepositoryConnection connection) {
        log.debug("Creating and persisting event Change Request Pending...");
        return neoneEventRepository.addEvent(loIri, API.CHANGE_REQUEST_PENDING,
            Optional.empty(), Collections.emptySet(), loTypes, connection);
    }

    private NeoneEvent createChangeRequestAcceptedEvent(IRI loIri, Set<IRI> loTypes, RepositoryConnection connection) {
        log.debug("Creating and persisting event Change Request accepted...");
        return neoneEventRepository.addEvent(loIri, API.CHANGE_REQUEST_ACCEPTED,
            Optional.empty(), Collections.emptySet(), loTypes, connection);
    }

    private NeoneEvent createChangeRequestRejectedEvent(IRI loIri, Set<IRI> loTypes, RepositoryConnection connection) {
        log.debug("Creating and persisting event Change Request rejected...");
        return neoneEventRepository.addEvent(loIri, API.CHANGE_REQUEST_REJECTED,
            Optional.empty(), Collections.emptySet(), loTypes, connection);
    }

    private void sendNotification(ChangeRequest cr, NotificationEventType eventType) {
        Notification notification = new Notification(
            idProvider.createInternalIri().getIri(),
            eventType.iri(),
            Optional.of(Subscription.TopicType.LOGISTICS_OBJECT_TYPE.getValue().stringValue()),
            Optional.ofNullable(cr.change().hasLogisticsObject()),
            Optional.ofNullable(cr.requestedBy()),
            Collections.emptySet());
        if (cr.change().notifyRequestStatusChange().orElse(false)) {
            notificationService.notifyRequestor(cr, notification);
        }
    }
}
