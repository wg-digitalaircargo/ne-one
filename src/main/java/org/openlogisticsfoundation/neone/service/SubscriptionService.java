// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.common.annotation.Blocking;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.client.ProposalResponse;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.client.SubscriptionClient;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.model.ActionRequestType;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.NotificationEventType;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.SubscriptionMetadata;
import org.openlogisticsfoundation.neone.model.SubscriptionRequest;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionRequestHandler;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SubscriptionMetadataRepository;
import org.openlogisticsfoundation.neone.repository.SubscriptionRepository;
import org.openlogisticsfoundation.neone.repository.SubscriptionRequestRepository;
import org.openlogisticsfoundation.neone.security.AccessSubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class SubscriptionService {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionService.class);

    private final SubscriptionRepository subscriptionRepository;
    private final SubscriptionRequestRepository subscriptionRequestRepository;
    private final SubscriptionRequestHandler subscriptionRequestHandler;
    private final SubscriptionMetadataRepository subscriptionMetadataRepository;

    private final RepositoryTransaction transaction;

    private final IdProvider idProvider;
    private final RestClientBuilderProducer restClientBuilderProducer;
    private final ActionRequestService actionRequestService;
    private final ActionRequestNotificationService notificationService;
    private final AclAuthorizationRepository aclAuthorizationRepository;
    private final Instance<AccessSubject> accessSubject;

    @ConfigProperty(name = "authorize-creator")
    boolean authorizeCreator;

    @ConfigProperty(name = "quarkus.rest-client.subscription-client.url")
    String baseUrl;

    @ConfigProperty(name = "quarkus.rest-client.subscription-client.connect-timeout")
    int connectTimeout;

    @ConfigProperty(name = "quarkus.rest-client.subscription-client.read-timeout")
    int readTimeout;


    @ConfigProperty(name = "auto-accept-subscription-proposals", defaultValue = "true")
    boolean autoAcceptSubscriptionProposals;

    @ConfigProperty(name = "default-subscription-lifespan", defaultValue = "1h")
    Duration defaultSubscriptionLifespan;

    @Inject
    public SubscriptionService(SubscriptionRepository subscriptionRepository,
                               SubscriptionRequestRepository subscriptionRequestRepository,
                               SubscriptionMetadataRepository subscriptionMetadataRepository,
                               AclAuthorizationRepository aclAuthorizationRepository,
                               RepositoryTransaction transaction,
                               SubscriptionRequestHandler subscriptionRequestHandler,
                               IdProvider idProvider,
                               RestClientBuilderProducer restClientBuilderProducer,
                               ActionRequestNotificationService notificationService,
                               ActionRequestService actionRequestService,
                               Instance<AccessSubject> accessSubject) {
        this.subscriptionRepository = subscriptionRepository;
        this.subscriptionRequestRepository = subscriptionRequestRepository;
        this.subscriptionMetadataRepository = subscriptionMetadataRepository;
        this.aclAuthorizationRepository = aclAuthorizationRepository;
        this.transaction = transaction;
        this.subscriptionRequestHandler = subscriptionRequestHandler;
        this.idProvider = idProvider;
        this.restClientBuilderProducer = restClientBuilderProducer;
        this.notificationService = notificationService;
        this.actionRequestService = actionRequestService;
        this.accessSubject = accessSubject;
    }

    public SubscriptionRequest handleSubscription(Subscription subscription) {
        try {
            // get the base url of the server where the notifications are sent to which is the subscriber url
            // with stripped off path
            URL callbackUrl = new URL(new URL(subscription.subscriber().stringValue()), "/");
            var metadata = new SubscriptionMetadata(idProvider.createInternalIri().getIri(),
                subscription.iri(), callbackUrl);

            var requestIri = idProvider.createUniqueSubscriptionRequestUri().getIri();
            var request = new SubscriptionRequest(requestIri, RequestStatus.REQUEST_PENDING, Instant.now(),
                subscription.subscriber(), Optional.empty(), Optional.empty(),
                Optional.empty(), subscription);

            var subscriptionRequest = subscriptionRequestHandler.fromJava(request);
            transaction.transactionallyDo(connection -> {
                subscriptionRequestRepository.persist(subscriptionRequest, connection);
                subscriptionMetadataRepository.persist(metadata, connection);
                IRI creatorIri = authorizeCreator ? accessSubject.get().iri() : null;
                aclAuthorizationRepository.grantDefaultAccess(requestIri, creatorIri, false, connection);
            });

            return request;
        } catch (MalformedURLException e) {
            throw new NeoneException("Malformed URL in Subscription service: "
                + subscription.subscriber().stringValue(), e);
        }
    }

    @ConsumeEvent(ActionRequestType.SUBSCRIPTION_REQUEST)
    @Blocking
    public void onSubscriptionRequestChanged(SubscriptionRequest subscriptionRequest) {
        var status = subscriptionRequest.requestStatus();
        log.info("Status of subscriptionRequest [{}] has changed to [{}].",
            subscriptionRequest.iri(), status);
        transaction.transactionallyDo(connection -> {
            // Persist not yet persisted new status:
            actionRequestService.overwriteActionRequestStatus(subscriptionRequest.iri(), status);

            if (status == RequestStatus.REQUEST_ACCEPTED) {
                sendNotification(subscriptionRequest, NotificationEventType.SUBSCRIPTION_REQUEST_ACCEPTED);
            } else if (status == RequestStatus.REQUEST_REJECTED) {
                sendNotification(subscriptionRequest, NotificationEventType.SUBSCRIPTION_REQUEST_REJECTED);
            } else if (status == RequestStatus.REQUEST_REVOKED) {
                var subscription = subscriptionRequest.subscription();
                subscriptionRepository.delete(subscription.iri(), null, connection);
                sendNotification(subscriptionRequest, NotificationEventType.SUBSCRIPTION_REQUEST_REVOKED);
            }
        });
    }

    public Subscription handleSubscriptionProposal(Subscription.TopicType topicType, String topic) {
        ProposalResponse subscriptionRes;
        if (autoAcceptSubscriptionProposals) {
            subscriptionRes = new ProposalResponse(true, false, null,
                Instant.now().plus(defaultSubscriptionLifespan));
        } else {
            log.info("Forwarding Subscription Proposal topicType={} topic={}", topicType, topic);
            subscriptionRes = forwardProposal(topicType, topic);
        }
        return createProposalResponse(topicType, topic, subscriptionRes);
    }

    /*
     * Ask some external system if it wants to subscribe a given topicType/topic.
     */
    private ProposalResponse forwardProposal(Subscription.TopicType topicType, String topic) {
        var uri = URI.create(baseUrl);
        var client = restClientBuilderProducer.restClientBuilder()
            .baseUri(uri)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(SubscriptionClient.class);
        return client.handleSubscription(topicType.name(), topic);
    }

    private Subscription createProposalResponse(Subscription.TopicType topicType,
                                                String topic,
                                                ProposalResponse proposalResponse) {
        var subscriber = idProvider.getDataHolderId();
        var iri = idProvider.createInternalIri().getIri();
        return new Subscription(iri, subscriber.getIri(), topicType, topic,
            Optional.of("some description"),
            Optional.ofNullable(proposalResponse.expiresAt()),
            Optional.of(proposalResponse.sendLoBody()),
            Optional.ofNullable(proposalResponse.includeEventType()),
            Optional.empty());
    }

    private void sendNotification(SubscriptionRequest actionRequest, NotificationEventType eventType) {
        if (!actionRequest.subscription().notifyRequestStatusChange().orElse(false)) {
            return;
        }

        var subscription = actionRequest.subscription();
        var lo = subscription.topic().equals(Subscription.TopicType.LOGISTICS_OBJECT_TYPE.toString()) ?
            Values.iri(actionRequest.subscription().topic()) : null;
        Notification notification = new Notification(
            idProvider.createInternalIri().getIri(),
            eventType.iri(),
            Optional.ofNullable(subscription.topic()),
            Optional.ofNullable(lo),
            Optional.ofNullable(actionRequest.requestedBy()),
            Collections.emptySet());
        notificationService.notifyRequestor(actionRequest, notification);
    }
}
