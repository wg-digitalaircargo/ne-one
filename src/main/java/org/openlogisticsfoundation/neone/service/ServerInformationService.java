// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.ServerInformation;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.ServerInformationRepository;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class ServerInformationService {

    private final ServerInformationRepository repository;

    private final RepositoryTransaction transaction;

    private final IdProvider idProvider;

    @Inject
    public ServerInformationService(ServerInformationRepository repository,
                                    RepositoryTransaction transaction,
                                    IdProvider idProvider) {
        this.repository = repository;
        this.transaction = transaction;
        this.idProvider = idProvider;
    }

    public ServerInformation getServerInformation() {
        IRI serverInformationIri = idProvider.getBaseId().getIri();
        return transaction.transactionallyGet(connection ->
            repository.findByIri(serverInformationIri, connection)
        ).orElseThrow(() -> new SubjectNotFoundException(serverInformationIri.stringValue()));
    }
}
