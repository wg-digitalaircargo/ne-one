// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.RevisionMissingException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.model.ChangeRequest;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.handler.AuditTrailHandler;
import org.openlogisticsfoundation.neone.repository.AuditTrailRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectMetadataService;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;
import java.util.List;

@ApplicationScoped
public class AuditTrailService {

    public static final int INITIAL_REVISION = 1;
    private final AuditTrailRepository repository;
    private final RepositoryTransaction transaction;
    private final AuditTrailHandler auditTrailHandler;
    private final IdProvider idProvider;
    private final LogisticsObjectMetadataService loMetaService;

    @Inject
    public AuditTrailService(AuditTrailRepository auditTrailRepository,
                             RepositoryTransaction transaction,
                             AuditTrailHandler auditTrailHandler,
                             LogisticsObjectMetadataService loMetaService,
                             IdProvider idProvider) {
        this.repository = auditTrailRepository;
        this.transaction = transaction;
        this.auditTrailHandler = auditTrailHandler;
        this.idProvider = idProvider;
        this.loMetaService = loMetaService;
    }

    public AuditTrail getAuditTrailByLoId(String loId,
                                          Instant from, Instant until) {
        var auditTrailIri = idProvider.getAuditTrailIriFromLoId(loId);
        return transaction.transactionallyGet(connection ->
            repository.getAuditTrail(auditTrailIri.getIri(), from, until, connection)
                .orElseThrow(() -> new SubjectNotFoundException(auditTrailIri.getIri().stringValue()))
        );
    }

    public void createAuditTrail(IRI loIri) {
        transaction.transactionallyDo(connection -> createAuditTrail(loIri, connection));
    }

    public void createAuditTrail(IRI loIri,
                                 RepositoryConnection connection) {
        var auditTrailIri = idProvider.getAuditTrailIriFromLoIri(loIri).getIri();
        var auditTrail = new AuditTrail(auditTrailIri, List.of(), INITIAL_REVISION);
        repository.persist(auditTrail, connection);
    }

    public void updateAuditTrail(IRI loIri,
                                 ChangeRequest changeRequest,
                                 RepositoryConnection connection) {
        var auditTrailIri = idProvider.getAuditTrailIriFromLoIri(loIri).getIri();
        var auditTrail = repository.findGraphByIri(auditTrailIri, connection)
            .orElseThrow(() -> new SubjectNotFoundException(auditTrailIri.stringValue()));

        // It's assumed that the change request itself has already been persisted,
        // so that only the link between audit trail and change request must be established.
        var builder = new ModelBuilder().add(auditTrail.iri(), API.hasChangeRequest, changeRequest.iri());
        repository.persist(builder.build(), connection);

        if (changeRequest.requestStatus().equals(RequestStatus.REQUEST_ACCEPTED)) {
            increaseRevisionNumber(auditTrail, connection);
        }
    }

    private void increaseRevisionNumber(AuditTrail auditTrail, RepositoryConnection connection) {
        var model = auditTrailHandler.fromJava(auditTrail);
        var latestRevision = Models.objectString(model.filter(null, API.hasLatestRevision, null))
            .orElseThrow(() -> new RevisionMissingException(auditTrail.iri()));
        repository.delete(auditTrail.iri(), API.hasLatestRevision, connection);
        repository.add(auditTrail.iri(), API.hasLatestRevision,
            Values.literal(Integer.parseInt(latestRevision) + 1),
            connection
        );
    }
}
