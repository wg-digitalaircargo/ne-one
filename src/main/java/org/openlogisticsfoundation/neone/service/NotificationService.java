// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.cache.CacheInvalidate;
import io.quarkus.cache.CacheKey;
import io.quarkus.scheduler.Scheduled;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.openlogisticsfoundation.neone.client.NotificationClient;
import org.openlogisticsfoundation.neone.client.OneRecordClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.controller.NotificationMessage;
import org.openlogisticsfoundation.neone.exception.MissingMetadataException;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.NotificationMetadata;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.SubscriptionMetadata;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.NotificationMetadataRepository;
import org.openlogisticsfoundation.neone.repository.NotificationRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SubscriptionMetadataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import java.net.URL;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class NotificationService {

    private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

    @ConfigProperty(name = "quarkus.rest-client.notification-client.url")
    String baseUrl;

    @ConfigProperty(name = "quarkus.rest-client.notification-client.connect-timeout")
    int connectTimeout;

    @ConfigProperty(name = "quarkus.rest-client.notification-client.read-timeout")
    int readTimeout;

    private final NeoneEventRepository neoneEventRepository;
    private final NotificationRepository notificationRepository;
    private final SubscriptionMetadataRepository subscriptionMetadataRepository;
    private final LogisticsObjectRepository logisticsObjectRepository;
    private final RepositoryTransaction transaction;
    private final RestClientBuilderProducer restClientBuilderProducer;
    private final NotificationMetadataRepository notificationMetadataRepository;
    private final IdProvider idProvider;

    @Inject
    public NotificationService(NeoneEventRepository neoneEventRepository,
                               NotificationRepository notificationRepository,
                               LogisticsObjectRepository logisticsObjectRepository,
                               RepositoryTransaction transaction,
                               IdProvider idProvider,
                               NotificationMetadataRepository notificationMetadataRepository,
                               SubscriptionMetadataRepository subscriptionMetadataRepository,
                               RestClientBuilderProducer restClientBuilderProducer) {

        this.neoneEventRepository = neoneEventRepository;
        this.notificationRepository = notificationRepository;
        this.logisticsObjectRepository = logisticsObjectRepository;
        this.transaction = transaction;
        this.notificationMetadataRepository = notificationMetadataRepository;
        this.subscriptionMetadataRepository = subscriptionMetadataRepository;
        this.restClientBuilderProducer = restClientBuilderProducer;
        this.idProvider = idProvider;
    }

    public void enqueue(NeoneEvent event, Collection<Subscription> subscriptions) {
        // in a transaction do
        transaction.transactionallyDo(connection -> {
            var loTypes = logisticsObjectRepository.getLogisticsObjectTypes(event.loId(), connection);

            // 2. for each callback callbackUrl persist a notification and link the event and a callback
            subscriptions.forEach(subscription -> {
                // First check if subscription has expired:
                var subscriptionMetadata = subscriptionMetadataRepository.getMetadataOfSubject(subscription.iri(), connection);
                var hasExpired = subscription.expiresAt().map(Instant.now()::isAfter).orElse(false);
                if (hasExpired) {
                    subscriptionMetadata.ifPresent(sm -> deleteSubscription(sm, connection));
                    return;
                }

                var loType = subscription.topicType() == Subscription.TopicType.LOGISTICS_OBJECT_TYPE ?
                    Optional.of(subscription.topic()) :
                    loTypes.stream().map(IRI::stringValue).findFirst();
                Notification notification = new Notification(
                    idProvider.createNotificationId(event.loId()).getIri(),
                    event.notificationEventType(),
                    loType,
                    Optional.of(event.loId()),
                    event.triggeredBy(),
                    event.changedProperties()
                );
                notificationRepository.persist(notification, connection);
                neoneEventRepository.addNotification(event.iri(), notification.iri(), connection);

                var notificationMetadata = new NotificationMetadata(
                    idProvider.createInternalIri().getIri(),
                    notification.iri(),
                    subscriptionMetadata
                        .orElseThrow(() -> new MissingMetadataException("Missing metadata for " + subscription.iri()))
                        .callbackUrl(),
                    subscription.sendLoBody().orElse(false),
                    event.iri());

                notificationMetadataRepository.persist(notificationMetadata, connection);
            });
            // 3. set event to pending state
            neoneEventRepository.setEventState(event.iri(), NeoneEvent.State.PENDING, connection);
        });
    }

    @Scheduled(
        every = "{notification.send.interval}",
        concurrentExecution = Scheduled.ConcurrentExecution.SKIP,
        delayed = "{notification.send.interval}"
    )
    public void sendNotifications() {
        log.info("Fetching all outstanding notifications");

        // get all notifications to send
        List<Notification> notifications = transaction.transactionallyGet(
            notificationRepository::getPendingNotifications
        );

        notifications.forEach(notification -> transaction.transactionallyDo(connection -> {
            var meta = notificationMetadataRepository.getMetadataOfSubject(notification.iri(), connection)
                .orElseThrow(() -> new MissingMetadataException("Missing metadata for " + notification.iri()));
            Optional<LogisticsObject> lo = notification.hasLogisticsObject()
                .flatMap(iri -> logisticsObjectRepository.findByIri(iri, connection));

            log.info("Sending [{}] to [{}] for event [{}]",
                notification,
                meta.callbackUrl(),
                meta.loEventIri());

            try {
                var response = sendNotification(notification, meta.callbackUrl(), lo);
                log.debug("Received notification response [{}]: ", response);

                // Afterward, delete notification and callback if successful
                neoneEventRepository.deleteNotification(
                    meta.loEventIri(),
                    notification.iri(),
                    connection
                );
                notificationRepository.delete(notification.iri(), null, connection);
                notificationMetadataRepository.delete(meta.iri(), null, connection);
            } catch (Exception exc) {
                log.warn("Unable to send notification [{}] to [{}]",
                    notification.iri().stringValue(), meta.callbackUrl());
                log.debug("Exception while sending notification [{}] to [{}]", notification.iri().stringValue(), meta.callbackUrl(), exc);
            }

        }));

        cleanUp();
    }

    private Response sendNotification(Notification notification, URL callbackUrl, Optional<LogisticsObject> lo) {
        var restClient = restClientBuilderProducer.restClientBuilder()
            .baseUrl(callbackUrl)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(OneRecordClient.class);

        return restClient.notifySubscriber(
            new NotificationMessage().withNotification(notification).withLogisticsObject(lo.orElse(null)));
    }

    private void cleanUp() {
        transaction.transactionallyDo(connection -> {
            List<IRI> events = neoneEventRepository.getPendingEventsWithoutNotifications(connection);
            events.forEach(eventId -> {
                log.info(
                    "Event [{}] is in [PENDING] state but has no active notifications, setting it to [PROCESSED]",
                    eventId.stringValue()
                );

                neoneEventRepository.setEventState(eventId, NeoneEvent.State.PROCESSED, connection);
            });
        });
    }

    public List<String> getCompanyIdsToNotify(IRI logisticsObjectId) {
        var uri = URI.create(baseUrl);
        var client = restClientBuilderProducer.restClientBuilder()
            .baseUri(uri)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(NotificationClient.class);
        return client.shouldNotify(logisticsObjectId.stringValue()).companyIds();
    }

    private void deleteSubscription(SubscriptionMetadata subscriptionMeta, RepositoryConnection connection) {
        // Removes the subscription as well:
        subscriptionMetadataRepository.deleteAll(subscriptionMeta.iri(), connection);
    }

    public void forward(NotificationMessage notification) {
        var uri = URI.create(baseUrl);
        var client = restClientBuilderProducer.restClientBuilder()
            .baseUri(uri)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(NotificationClient.class);
        client.notify(notification);
    }

    public void invalidateCachedLogisticsObject(NotificationMessage notificationMsg) {
        var loId = notificationMsg.getNotification().hasLogisticsObject();
        loId.ifPresent(this::invalidateCache);
    }

    // NOTE: Cache enabled function must not be private.
    @CacheInvalidate(cacheName = "remote-lo-cache")
    void invalidateCache(@CacheKey IRI iri) {
        // No implementation required.
    }
}
