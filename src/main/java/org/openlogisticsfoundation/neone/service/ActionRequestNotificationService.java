//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.openlogisticsfoundation.neone.client.OneRecordClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.controller.NotificationMessage;
import org.openlogisticsfoundation.neone.model.ActionRequest;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.handler.NotificationHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.net.URI;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class ActionRequestNotificationService {

    private static final Logger log = LoggerFactory.getLogger(ActionRequestNotificationService.class);

    @ConfigProperty(name = "quarkus.rest-client.action-request-client.connect-timeout")
    int connectTimeout;

    @ConfigProperty(name = "quarkus.rest-client.action-request-client.read-timeout")
    int readTimeout;

    private final RestClientBuilderProducer restClientBuilderProducer;

    @Inject
    public ActionRequestNotificationService(
        RestClientBuilderProducer restClientBuilderProducer,
        NotificationHandler notificationHandler) {
        this.restClientBuilderProducer = restClientBuilderProducer;
    }

    public void notifyRequestor(ActionRequest actionRequest, Notification notification) {
        String requestedBy = actionRequest.requestedBy().stringValue();
        int idx = requestedBy.lastIndexOf("/logistics-objects");
        if (idx >= 0) {
            String base = requestedBy.substring(0, idx);
            URI address = URI.create(base);
            sendNotification(address, notification);
        }
    }

    private void sendNotification(URI address, Notification notification) {
        var client = restClientBuilderProducer.restClientBuilder()
            .baseUri(address)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(OneRecordClient.class);
        var response = client.notifySubscriber(new NotificationMessage().withNotification(notification));
        response.close();
    }
}
