// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.vertx.mutiny.core.eventbus.EventBus;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class EventService {

    private static final Logger log = LoggerFactory.getLogger(EventService.class);

    private final NeoneEventRepository neoneEventRepository;

    private final RepositoryTransaction transaction;
    private final EventBus eventBus;

    @Inject
    public EventService(NeoneEventRepository neoneEventRepository,
                        RepositoryTransaction transaction,
                        EventBus eventBus) {
        this.neoneEventRepository = neoneEventRepository;
        this.transaction = transaction;
        this.eventBus = eventBus;
    }

    public void triggerMissedEvents() {
        log.info("Checking for missed events");
        transaction.transactionallyDo(connection -> neoneEventRepository.getNewEvents(connection)
            .forEach(event -> eventBus.publish(NeoneEvent.ADDRESS, event)));
    }
}
