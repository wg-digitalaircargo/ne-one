// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.arc.All;
import io.quarkus.scheduler.Scheduled;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.client.ActionRequestClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.exception.StatusChangeDeniedException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.ActionRequest;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.handler.ModelHandler;
import org.openlogisticsfoundation.neone.repository.ActionRequestRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.security.AccessSubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class ActionRequestService {
    private static final Logger log = LoggerFactory.getLogger(ActionRequestService.class);

    @ConfigProperty(name = "quarkus.rest-client.action-request-client.url")
    String baseUrl;

    @ConfigProperty(name = "quarkus.rest-client.action-request-client.connect-timeout")
    int connectTimeout;

    @ConfigProperty(name = "quarkus.rest-client.action-request-client.read-timeout")
    int readTimeout;

    @ConfigProperty(name = "auto-accept-action-requests")
    boolean autoAccept;

    private final Instance<AccessSubject> accessSubject;

    private final EventBus eventBus;

    private final RepositoryTransaction transaction;

    private final List<ActionRequestRepository<?>> actionRequestRepositories;

    private final RestClientBuilderProducer restClientBuilderProducer;

    private final IdProvider idProvider;

    @Inject
    public ActionRequestService(RepositoryTransaction transaction,
                                RestClientBuilderProducer restClientBuilderProducer,
                                EventBus eventBus,
                                Instance<AccessSubject> accessSubject,
                                @All List<ActionRequestRepository<?>> actionRequestRepositories,
                                IdProvider idProvider) {
        this.transaction = transaction;
        this.restClientBuilderProducer = restClientBuilderProducer;
        this.eventBus = eventBus;
        this.accessSubject = accessSubject;
        this.actionRequestRepositories = actionRequestRepositories;
        this.idProvider = idProvider;
    }

    public Model getActionRequest(IRI actionRequestIri) {
        return transaction.transactionallyGet(connection -> {
            ActionRequestRepository<?> requestRepository = determineRepository(actionRequestIri, connection);
            ActionRequest actionRequest = requestRepository.findGraphByIri(actionRequestIri, connection)
                .orElseThrow(() -> new SubjectNotFoundException(actionRequestIri.stringValue()));
            ModelHandler<ActionRequest> modelHandler = requestRepository.getActionRequestModelHandler();
            return modelHandler.fromJava(actionRequest);
        });
    }

    public void revokeActionRequest(IRI requestIri) {
        updateActionRequestStatus(requestIri, RequestStatus.REQUEST_REVOKED);
    }

    public void updateActionRequestStatus(IRI requestIri, RequestStatus newStatus) {
        log.debug("Updating action request for iri [{}] to [{}]", requestIri, newStatus);
        transaction.transactionallyDo((connection, hooks) -> {
            var type = getActionRequestType(requestIri, connection);
            var repo = determineRepository(requestIri, connection);
            ActionRequest actionRequest = repo.findGraphByIri(requestIri, connection)
                .orElseThrow(() -> new SubjectNotFoundException(requestIri.stringValue()));
            if (actionRequest.requestStatus() != RequestStatus.REQUEST_PENDING && type.equals(API.ChangeRequest)) {
                throw new StatusChangeDeniedException(requestIri);
            }
            var updatedActionRequest = (newStatus == RequestStatus.REQUEST_REVOKED) ?
                actionRequest.withRevocation(accessSubject.get().iri(), Instant.now()) :
                actionRequest.withStatus(newStatus);
            hooks.registerPostCommitHook(() -> eventBus.publish(type.stringValue(), updatedActionRequest));
        });
    }

    public void overwriteActionRequestStatus(IRI requestIri, RequestStatus newStatus) {
        // Just change the status value without all the fuss around usual status updates.
        log.debug("Updating action request status for iri [{}]: [{}]", requestIri, newStatus);
        transaction.transactionallyDo(connection -> {
            ActionRequestRepository<?> repo = determineRepository(requestIri, connection);
            repo.delete(requestIri, API.hasRequestStatus, connection);
            repo.add(requestIri, API.hasRequestStatus, newStatus.iri(), connection);
        });
    }

    private ActionRequestRepository<?> determineRepository(IRI actionRequestIri, RepositoryConnection connection) {
        IRI actionRequestType = getActionRequestType(actionRequestIri, connection);
        return actionRequestRepositories.stream()
            .filter(actionRequestRepository -> actionRequestRepository.getRepositoryType().equals(actionRequestType))
            .findFirst()
            .orElseThrow(() -> new NeoneException("Unknown ActionRequest type [" + actionRequestType + "]"));
    }

    private IRI getActionRequestType(IRI actionRequestIri, RepositoryConnection connection) {
        Model model = QueryResults.asModel(connection.getStatements(actionRequestIri, RDF.TYPE, null));
        if (model.isEmpty()) {
            throw new SubjectNotFoundException(actionRequestIri.stringValue());
        }

        return Models.objectIRI(model)
            .orElseThrow(() -> new NeoneException("ActionRequest [" + actionRequestIri.stringValue() + "] not found"));
    }

    private IRI getActionRequestType(IRI actionRequestIri, Model model) {
        return Models.objectIRI(model.filter(actionRequestIri, RDF.TYPE, null))
            .orElseThrow(() -> new NeoneException("ActionRequest [" + actionRequestIri.stringValue() + "] not found"));
    }

    @Scheduled(
        every = "{action-request.evaluation.interval}",
        concurrentExecution = Scheduled.ConcurrentExecution.SKIP,
        delayed = "{action-request.evaluation.interval}"
    )
    public void evaluatePendingActionRequests() {
        log.info("Fetching all pending action requests.");
        List<IRI> pending = fetchPendingActionRequests();
        pending.forEach(iri ->
            transaction.transactionallyDo((connection, hooks) -> {
                var repo = determineRepository(iri, connection);
                var actionRequest = repo.findGraphByIri(iri, connection)
                    .orElseThrow(() -> new NeoneException("Cannot find action request " + iri.stringValue()));
                log.info("Querying for action request status.");
                var handler = repo.getActionRequestModelHandler();
                hooks.registerPostCommitHook(() -> {
                    RequestStatus status = queryRequestStatus(handler, actionRequest);
                    if (status != RequestStatus.REQUEST_PENDING) {
                        // Status has changed!
                        var actionRequestModel = handler.fromJava(actionRequest);
                        IRI type = getActionRequestType(iri, actionRequestModel);
                        eventBus.publish(type.stringValue(), actionRequest.withStatus(status));
                    }
                });
            })
        );
    }

    private List<IRI> fetchPendingActionRequests() {
        return transaction.transactionallyGet(connection -> {
                List<IRI> iriList = new ArrayList<>();
                actionRequestRepositories.forEach(r -> {
                    var type = r.getRepositoryType();
                    List<IRI> iris = r.getAllPending(connection, type);
                    iriList.addAll(iris);
                });
                return iriList;
            }
        );
    }

    public RequestStatus queryRequestStatus(ModelHandler handler, ActionRequest actionRequest) {
        var actionRequestModel = handler.fromJava(actionRequest);
        var requestedBy = actionRequest.requestedBy();
        if (autoAccept && requestedBy.equals(idProvider.getDataHolderId().getIri())) {
            log.info("Action request [{}] automatically accepted.", actionRequest.iri());
            return RequestStatus.REQUEST_ACCEPTED;
        } else {
            return requestStatusExternally(actionRequestModel);
        }
    }

    public RequestStatus requestStatusExternally(Model actionRequest) {
        var uri = URI.create(baseUrl);
        var client = restClientBuilderProducer.restClientBuilder()
            .baseUri(uri)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(ActionRequestClient.class);
        var response = client.evaluate(actionRequest);
        return RequestStatus.valueOf(response.actionRequestStatus());
    }
}
