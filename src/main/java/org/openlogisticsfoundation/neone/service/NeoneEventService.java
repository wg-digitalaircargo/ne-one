// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.common.annotation.Blocking;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SubscriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class NeoneEventService {

    private static final Logger log = LoggerFactory.getLogger(NeoneEventService.class);

    private final NotificationService notificationService;

    private final SubscriptionRepository subscriptionRepository;

    private final RepositoryTransaction transaction;

    private final boolean validateSubscribers;

    private final EventBus eventBus;

    @Inject
    public NeoneEventService(NotificationService notificationService,
                             SubscriptionRepository subscriptionRepository,
                             RepositoryTransaction transaction,
                             @ConfigProperty(name = "validate-subscribers") boolean validateSubscribers,
                             EventBus eventBus) {
        this.notificationService = notificationService;
        this.subscriptionRepository = subscriptionRepository;
        this.transaction = transaction;
        this.validateSubscribers = validateSubscribers;
        this.eventBus = eventBus;
    }

    @ConsumeEvent(NeoneEvent.ADDRESS)
    @Blocking
    public void onLogisticsObjectChanged(NeoneEvent event) {
        log.debug("Handling [{}] for LogisticsObject [{}]", event.notificationEventType(), event.loId().stringValue());

        List<String> companyIds;
        if (validateSubscribers) {
            companyIds = notificationService.getCompanyIdsToNotify(event.loId());
        } else {
            companyIds = null;
        }

        Set<Subscription> subscriptions = retrieveSubscriptions(companyIds, event.loTypes(), event.loId());

        log.info("Enqueuing notification for subscriptions [{}]", subscriptions);
        notificationService.enqueue(event, subscriptions);
    }

    private Set<Subscription> retrieveSubscriptions(List<String> companyIds, Collection<IRI> loTypes, IRI loIri) {
        var types = loTypes.stream().map(IRI::stringValue).toList();
        return transaction.transactionallyGet(connection -> {
            List<Subscription> s1 = subscriptionRepository.getAcceptedSubscriptions(companyIds,
                Subscription.TopicType.LOGISTICS_OBJECT_TYPE, types, connection);
            List<Subscription> s2 = subscriptionRepository.getAcceptedSubscriptions(companyIds,
                Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER, List.of(loIri.stringValue()), connection);
            Set<Subscription> subscriptions = new HashSet<>(s1);
            subscriptions.addAll(s2);
            return subscriptions;
        });
    }

    public void publishEvent(NeoneEvent event) {
        log.debug("Publishing event [{}]", event);
        eventBus.publish(NeoneEvent.ADDRESS, event);
    }
}
