// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SnapshotRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Optional;

@ApplicationScoped
public class SnapshotService {

    private static final Logger log = LoggerFactory.getLogger(SnapshotService.class);

    private final SnapshotRepository repository;

    private final RepositoryTransaction transaction;

    private final IdProvider idProvider;

    @Inject
    public SnapshotService(SnapshotRepository repository,
                           RepositoryTransaction transaction,
                           IdProvider idProvider) {
        this.repository = repository;
        this.transaction = transaction;
        this.idProvider = idProvider;
    }

    public Snapshot createSnapshot(LogisticsObject logisticsObject,
                                   Integer revision,
                                   RepositoryConnection connection) {
        log.debug("Creating snapshot for LO [{}]", logisticsObject.iri());
        var payload = LogisticsObjectUtil.convertToJsonLd(logisticsObject.model());
        var snapshot = new Snapshot(
            idProvider.createInternalIri().getIri(),
            logisticsObject.iri(),
            payload,
            Instant.now(),
            revision);
        repository.persist(snapshot, connection);
        return snapshot;
    }

    public Optional<Snapshot> getSnapshot(IRI loIri, Instant at) {
        log.debug("Fetching snapshot for LO [{}] at timestamp [{}]", loIri, at);
        return transaction.transactionallyGet(connection -> repository.getSnapshot(loIri, at, connection));
    }

    public Optional<Snapshot> getLatestSnapshot(IRI loIri) {
        log.debug("Fetching latest snapshot for LO [{}]", loIri);
        // No snapshot can be younger than "now".
        return transaction.transactionallyGet(connection -> repository.getSnapshot(loIri, Instant.now(), connection));
    }
}
