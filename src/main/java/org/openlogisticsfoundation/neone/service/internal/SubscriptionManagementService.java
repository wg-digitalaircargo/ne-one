// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.openlogisticsfoundation.neone.client.OneRecordClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.SubscriptionMetadata;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SubscriptionMetadataRepository;
import org.openlogisticsfoundation.neone.repository.SubscriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class SubscriptionManagementService {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionManagementService.class);

    @ConfigProperty(name = "quarkus.rest-client.one-record-client.connect-timeout")
    int connectTimeout;

    @ConfigProperty(name = "quarkus.rest-client.one-record-client.read-timeout")
    int readTimeout;

    private final SubscriptionRepository subscriptionRepository;

    private final SubscriptionMetadataRepository subscriptionMetadataRepository;

    private final RepositoryTransaction transaction;

    private final IdProvider idProvider;

    private final RestClientBuilderProducer restClientBuilderProducer;

    @Inject
    public SubscriptionManagementService(SubscriptionRepository subscriptionRepository,
                                         SubscriptionMetadataRepository subscriptionMetadataRepository,
                                         RepositoryTransaction transaction,
                                         IdProvider idProvider,
                                         RestClientBuilderProducer restClientBuilderProducer) {
        this.subscriptionRepository = subscriptionRepository;
        this.subscriptionMetadataRepository = subscriptionMetadataRepository;
        this.transaction = transaction;
        this.idProvider = idProvider;
        this.restClientBuilderProducer = restClientBuilderProducer;
    }

    /**
     * Trigger a subscription proposal, i.e. ask an external partner if they would like to subscribe to some topic.
     */
    public Subscription proposeSubscription(String partnerUri,
                                            String topic, String topicType) {
        try {
            var callbackUrl = URI.create(partnerUri).toURL();
            var s = sendSubscriptionProposal(URI.create(partnerUri), topic, topicType);
            var subscription = new Subscription(s.iri(), s.subscriber(), s.topicType(), s.topic(),
                s.description(), s.expiresAt(), s.sendLoBody(), s.includeSubscriptionEventType(),
                Optional.empty());
            var metadata = new SubscriptionMetadata(idProvider.createInternalIri().getIri(), s.iri(), callbackUrl);

            persistProposalResponse(subscription, metadata);

            return subscription;
        } catch (MalformedURLException e) {
            throw new NeoneException("Malformed URL in subscription management service: " + partnerUri, e);
        }
    }

    private Subscription sendSubscriptionProposal(URI partnerUri, String topic, String topicType) {
        log.debug("Sending request to [{}] to propose subscription...", partnerUri.toString());
        var requestor = restClientBuilderProducer.restClientBuilder()
            .baseUri(partnerUri)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .build(OneRecordClient.class);
        var s = requestor.proposeSubscription(topic, topicType);
        return new Subscription(s.iri(), s.subscriber(), s.topicType(), topic,
            s.description(), s.expiresAt(), s.sendLoBody(), s.includeSubscriptionEventType(),
            Optional.empty());
    }

    private void persistProposalResponse(Subscription subscription, SubscriptionMetadata metadata) {
        log.info("Persisting proposal response for partner [{}]...", subscription.subscriber());
        transaction.transactionallyDo(connection -> {
            subscriptionRepository.persist(subscription, connection);
            subscriptionMetadataRepository.persist(metadata, connection);
        });
    }
}
