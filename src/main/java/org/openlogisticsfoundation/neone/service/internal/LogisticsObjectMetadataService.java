// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectMetadataRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Optional;

@ApplicationScoped
public class LogisticsObjectMetadataService {

    private final LogisticsObjectMetadataRepository metadataRepository;

    private final RepositoryTransaction transaction;

    @Inject
    public LogisticsObjectMetadataService(LogisticsObjectMetadataRepository metadataRepository,
                                          RepositoryTransaction transaction) {
        this.metadataRepository = metadataRepository;
        this.transaction = transaction;
    }

    public Optional<LogisticsObjectMetadata> getMetadataOf(IRI subject) {
        return transaction.transactionallyGet(connection ->
            metadataRepository.getMetadataOfSubject(subject, connection)
        );
    }
}
