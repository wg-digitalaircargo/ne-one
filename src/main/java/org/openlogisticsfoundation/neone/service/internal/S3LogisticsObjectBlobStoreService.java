// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.exception.AlreadyExistsException;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.BlobContent;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.exception.InvalidLogisticsObjectTypeException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.nio.file.Path;
import java.util.stream.Collectors;

@ApplicationScoped
public class S3LogisticsObjectBlobStoreService implements LogisticsObjectBlobStoreService {

    private static final Logger log = LoggerFactory.getLogger(S3LogisticsObjectBlobStoreService.class);

    private final LogisticsObjectRepository loRepository;

    private final IdProvider idProvider;

    private final S3Client s3Client;

    private final BlobStoreConfig config;

    private final RepositoryTransaction tx;

    @Inject
    public S3LogisticsObjectBlobStoreService(LogisticsObjectRepository loRepository,
                                             IdProvider idProvider,
                                             S3Client s3Client,
                                             BlobStoreConfig config,
                                             RepositoryTransaction tx) {

        this.loRepository = loRepository;
        this.idProvider = idProvider;
        this.s3Client = s3Client;
        this.config = config;
        this.tx = tx;
    }

    @Override
    public void saveBlob(String shortId, Path file, String filename, String mimeType) {
        NeoneId loId = idProvider.getUriForLoId(shortId);
        String s3Key = createS3Key(shortId, filename);
        if (objectExists(s3Key)) {
            throw new AlreadyExistsException(s3Key);
        }

        tx.transactionallyDo(connection -> validateExternalReference(loId, connection));

        s3Client.putObject(builder -> builder
            .bucket(config.bucketName())
            .key(s3Key)
            .contentType(mimeType), file
        );
    }

    @Override
    public BlobContent downloadBlob(String shortId, String filename) {
        ResponseBytes<GetObjectResponse> file;
        String s3Key = createS3Key(shortId, filename);

        try {
            file = s3Client.getObjectAsBytes(
                builder -> builder.bucket(config.bucketName()).key(s3Key)
            );
        } catch (NoSuchKeyException noSuchKeyException) {
            throw new SubjectNotFoundException(s3Key);
        }

        return new BlobContent(file.asByteArray(), file.response().contentType());
    }

    @Override
    public void createBucket(String bucket) {
        try {
            log.info("Checking if bucket [{}] exists", bucket);
            s3Client.headBucket(builder -> builder.bucket(bucket));
            log.warn("Bucket already exists [{}], not creating it", bucket);
        } catch (NoSuchBucketException noSuchBucketException) {
            log.info("Creating bucket [{}]", bucket);
            s3Client.createBucket(builder -> builder.bucket(bucket));
        }
    }

    private boolean objectExists(String s3Key) {
        log.info("Checking if [{}] already exists", s3Key);
        try {
            // to check if the file already exists in the bucket make a head rq
            s3Client.headObject(builder -> builder.bucket(config.bucketName()).key(s3Key));
            log.debug("Object [{}] already exists", s3Key);
            return true;
        } catch (NoSuchKeyException noSuchKeyException) {
            // NoSuchKeyException -> does not exist
            log.debug("Object [{}] does not exist", s3Key);
            return false;
        }
    }

    @Override
    public void validateExternalReference(NeoneId loId, RepositoryConnection connection) {
        log.debug("Checking if Lo exists");
        if (!loRepository.exists(loId.getIri(), connection)) {
            throw new SubjectNotFoundException(loId.getIri().stringValue());
        }
        var logisticsObjectTypes = loRepository.getLogisticsObjectTypes(loId.getIri(), connection);
        if(!logisticsObjectTypes.contains(CARGO.ExternalReference)) {
            throw new InvalidLogisticsObjectTypeException(
                loId.getIri().stringValue(),
                logisticsObjectTypes.stream().map(IRI::stringValue).collect(Collectors.joining(",")),
                CARGO.ExternalReference.stringValue()
            );
        }
    }

    private String createS3Key(String shortId, String filename) {
        return shortId + "/" + filename;
    }
}
