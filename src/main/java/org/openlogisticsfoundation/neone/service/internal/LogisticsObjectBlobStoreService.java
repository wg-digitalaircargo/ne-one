// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.model.BlobContent;

import java.nio.file.Path;

public interface LogisticsObjectBlobStoreService {
    void saveBlob(String shortId, Path file, String filename, String mimeType);

    BlobContent downloadBlob(String shortId, String filename);

    void createBucket(String bucket);

    void validateExternalReference(NeoneId loId, RepositoryConnection connection);
}
