// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.NeoneServerConfig;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectMetadataRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.service.LogisticsObjectService;
import org.openlogisticsfoundation.neone.vocab.NEONE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@ApplicationScoped
public class InitializationService {

    private static final Logger log = LoggerFactory.getLogger(InitializationService.class);

    private final NeoneServerConfig config;

    private final IdProvider idProvider;

    private final Repository rdfRepository;

    private final CargoOntologyService cargoOntologyService;

    private final LogisticsObjectMetadataRepository metadataRepository;

    private final LogisticsObjectService loService;

    private final RepositoryTransaction transaction;

    @Inject
    public InitializationService(NeoneServerConfig config, IdProvider idProvider,
                                 Repository rdfRepository, CargoOntologyService cargoOntologyService,
                                 LogisticsObjectMetadataRepository metadataRepository,
                                 LogisticsObjectService loService,
                                 RepositoryTransaction transaction) {
        this.config = config;
        this.idProvider = idProvider;
        this.rdfRepository = rdfRepository;
        this.cargoOntologyService = cargoOntologyService;
        this.metadataRepository = metadataRepository;
        this.loService = loService;
        this.transaction = transaction;
    }

    public void initializeServerInformation() {
        if (isServerInitialized()) {
            log.info("Server already initialized.");
            return;
        }

        log.info("Initializing server");
        IRI dataHolderIri = idProvider.getUriForLoId(config.dataHolder().id()).getIri();
        Model dataHolderModel = createDataHolderModel(dataHolderIri);
        LogisticsObject dataHolder = new LogisticsObject(dataHolderIri, dataHolderModel);
        Model serverInformationModel = config.toServerInformationModel(idProvider.getBaseId().getIri(), dataHolderIri);
        LogisticsObjectMetadata loMetaData = new LogisticsObjectMetadata(
            idProvider.createInternalIri().getIri(),
            dataHolderIri, 1, Instant.now(), Optional.of(false)
        );
        log.info("Persisting data holder object");
        loService.createLogisticsObject(dataHolder, false, true);
        transaction.transactionallyDo(connection -> {
            log.info("Persisting server information object");
            connection.add(serverInformationModel);
        });

    }

    private Model createDataHolderModel(IRI iri) {
        Model model = new DynamicModelFactory().createEmptyModel();
        model.add(iri, RDF.TYPE, CARGO.Organization);
        model.add(iri, CARGO.name, Values.literal(config.dataHolder().name()));

        return  model;
    }

    private void addMetadata(Model model, RepositoryConnection connection) {
        Models.subjectIRIs(model.filter(null, null, null)).stream()
            .filter(subject -> {
                Set<String> types = Models.objectStrings(model.filter(subject, RDF.TYPE, null));
                return cargoOntologyService.isLogisticsObject(types);
            })
            .map(loIri -> {
                LogisticsObjectMetadata meta = null;
                if (model.filter(loIri, NEONE.hasRevision, null).isEmpty() &&
                    model.filter(loIri, NEONE.isCreatedAt, null).isEmpty()) {
                    // Default revision = 1, default creation date = current time stamp.
                    meta = new LogisticsObjectMetadata(idProvider.createInternalIri().getIri(), loIri,
                        1, Instant.now(), Optional.of(Boolean.FALSE));
                }
                return meta;
            })
            .filter(Objects::nonNull)
            .forEach(meta -> metadataRepository.persist(meta, connection));
    }

    private boolean isServerInitialized() {
        try (RepositoryConnection conn = rdfRepository.getConnection()) {
            return conn.hasStatement(
                idProvider.getBaseId().getIri(),
                RDF.TYPE,
                API.ServerInformation,
                false
            );
        }
    }
}
