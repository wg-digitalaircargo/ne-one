// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import org.openlogisticsfoundation.neone.NeoneStartupException;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyLoaderConfiguration;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public abstract class OntologyService {

    private static final Logger log = LoggerFactory.getLogger(OntologyService.class);

    private final OWLOntology ontology;

    private final OWLDataFactory dataFactory;

    private final OWLReasoner reasoner;

    public OntologyService(String ontologyFile) {
        log.info("Loading ontology [{}]", ontologyFile);
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntologyLoaderConfiguration cfg = new OWLOntologyLoaderConfiguration()
            .addIgnoredImport(IRI.create("https://onerecord.iata.org/ns/coreCodeLists/1.0.0"));
        manager.setOntologyLoaderConfiguration(cfg);
        try {
            this.ontology = manager.loadOntologyFromOntologyDocument(
                Objects.requireNonNull(OntologyService.class.getClassLoader().getResourceAsStream(ontologyFile))
            );
            this.dataFactory = manager.getOWLDataFactory();
            OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
            this.reasoner = reasonerFactory.createReasoner(ontology);
            reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);

        } catch (OWLOntologyCreationException e) {
            throw new NeoneStartupException(e);
        }
    }

    protected OWLOntology getOntology() {
        return ontology;
    }

    protected OWLDataFactory getDataFactory() {
        return dataFactory;
    }

    protected OWLReasoner getReasoner() {
        return reasoner;
    }
}
