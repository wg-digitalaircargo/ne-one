// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.iata.onerecord.cargo.CARGO;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.client.OneRecordClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SubscriptionRepository;
import org.openlogisticsfoundation.neone.service.LogisticsObjectService;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
public class LogisticsObjectProxyService {

    private static final Pattern loIdPattern =
        Pattern.compile("(?<basePath>\\S+)logistics-objects/(?<loId>[^/]+)/?(?<rest>.*)$");

    private final LogisticsObjectService logisticsObjectService;

    private final SubscriptionRepository subscriptionRepository;

    private final LogisticsObjectRepository logisticsObjectRepository;

    private final RepositoryTransaction repositoryTransaction;

    private final RestClientBuilderProducer restClientBuilderProducer;

    private final boolean alwaysResolveExternal;

    private final Cache cache;

    @Inject
    public LogisticsObjectProxyService(LogisticsObjectService logisticsObjectService,
                                       SubscriptionRepository subscriptionRepository,
                                       LogisticsObjectRepository logisticsObjectRepository,
                                       RepositoryTransaction repositoryTransaction,
                                       RestClientBuilderProducer restClientBuilderProducer,
                                       @ConfigProperty(name = "logistics-object-proxy.always-resolve-external")
                                       boolean alwaysResolveExternal,
                                       @CacheName("remote-lo-cache") Cache cache) {
        this.logisticsObjectService = logisticsObjectService;
        this.restClientBuilderProducer = restClientBuilderProducer;
        this.alwaysResolveExternal = alwaysResolveExternal;
        this.subscriptionRepository = subscriptionRepository;
        this.repositoryTransaction = repositoryTransaction;
        this.logisticsObjectRepository = logisticsObjectRepository;
        this.cache = cache;
    }

    public Uni<Model> fetchLogisticsObject(NeoneId id) {
        var loModel = (id.isLocal() && !alwaysResolveExternal) ?
            fetchLocal(id) : fetchRemote(id);
        return loModel.invoke(model -> model.setNamespace("", CARGO.NAMESPACE));
    }

    Uni<Model> fetchLocal(NeoneId id) {
        return Uni.createFrom()
            .item(logisticsObjectService.getLogisticsObject(id.getUri(), false))
            .map(LogisticsObject::model);
    }

    Uni<Model> fetchRemote(NeoneId loIri) {
        Uni<Model> result;
        if (subscriptionExists(loIri)) {
            result = cache.getAsync(loIri, this::resolveExternalReference);
        } else {
            result = resolveExternalReference(loIri);
        }

        return result;
    }

    boolean subscriptionExists(NeoneId loIri) {
        return repositoryTransaction.transactionallyGet(conn -> {
            var loTypes = logisticsObjectRepository.getLogisticsObjectTypes(loIri.getIri(), conn);
            return subscriptionRepository.subscriptionForLogisticsObjectsExists(loIri.getIri(), loTypes, conn);
        });
    }

    Uni<Model> resolveExternalReference(NeoneId iri) {
        ExternalLogisticsObjectReference reference = createReference(iri.getIri());
        OneRecordClient client = createOneRecordClient(reference.basePath());
        return client.getLogisticsObject(reference.id());
    }

    private ExternalLogisticsObjectReference createReference(IRI iri) {
        Matcher matcher = loIdPattern.matcher(iri.stringValue());
        if (matcher.find()) {
            String basePath = matcher.group("basePath");
            String loId = matcher.group("loId");
            String rest = matcher.group("rest");

            // check validity, basePath must not be empty, loId must not be empty and rest must be empty
            if (basePath.isEmpty() || loId.isEmpty() || !rest.isEmpty()) {
                throw new IllegalArgumentException("Could not find a valid logistics object id");
            }

            return new ExternalLogisticsObjectReference(basePath, loId);
        } else {
            throw new IllegalArgumentException("Could not find a valid logistics object id");
        }
    }

    private OneRecordClient createOneRecordClient(String basePath) {
        return restClientBuilderProducer.restClientBuilder()
            .baseUri(URI.create(basePath))
            .build(OneRecordClient.class);
    }

    private record ExternalLogisticsObjectReference(String basePath, String id) {
    }
}
