// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.redis.datasource.codecs.Codec;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.openlogisticsfoundation.neone.exception.NeoneException;

import jakarta.enterprise.context.ApplicationScoped;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * Redis codec for de/serializing RDF4J {@link Model}s.
 */
@ApplicationScoped
public class ModelCodec implements Codec {

    @Override
    public boolean canHandle(Type clazz) {
        return clazz instanceof Class && Model.class.isAssignableFrom((Class<?>) clazz);
    }

    @Override
    public byte[] encode(Object item) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Rio.write((Model) item, out, RDFFormat.JSONLD);
            return out.toByteArray();
        } catch (IOException e) {
            throw new NeoneException("Failed to encode cache value.", e);
        }
    }

    @Override
    public Object decode(byte[] item) {
        try (ByteArrayInputStream in = new ByteArrayInputStream(item)) {
            return Rio.parse(in, RDFFormat.JSONLD);
        } catch (IOException e) {
            throw new NeoneException("Failed to decode cache value.", e);
        }
    }
}
