// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.redis.datasource.codecs.Codec;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.NeoneId;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

public class NeoneIdCodec {

    private NeoneIdCodec() {
    }

    /**
     * Create inner class for programmatically registering more than one {@link Codec} as workaround for
     * <a href="https://github.com/quarkusio/quarkus/issues/35661">codec registration issue</a>
     *
     * @return Redis Codec
     */
    public static Codec getNeoneIdCodec() {
        return new Codec() {
            @Override
            public boolean canHandle(Type clazz) {
                return clazz.equals(NeoneId.class);
            }

            @Override
            public byte[] encode(Object item) {
                return ((NeoneId) item).getIri().stringValue().getBytes(StandardCharsets.UTF_8);
            }

            @Override
            public Object decode(byte[] item) {
                return NeoneId.fromIri(Values.iri(new String(item, StandardCharsets.UTF_8)));
            }
        };
    }
}
