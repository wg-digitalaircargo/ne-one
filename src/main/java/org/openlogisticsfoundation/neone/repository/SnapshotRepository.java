// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.model.handler.SnapshotHandler;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Optional;

@ApplicationScoped
public class SnapshotRepository extends ModelRepository<Snapshot> {

    @Inject
    public SnapshotRepository(Repository repository, SnapshotHandler handler, IdProvider idProvider) {
        super(repository, handler, idProvider);
    }

    public Optional<Snapshot> getSnapshot(IRI loIri, Instant ts, RepositoryConnection connection) {
        Variable snapshot = SparqlBuilder.var("snapshot");
        Variable logisticsObjectIri = SparqlBuilder.var("loIri");
        Variable at = SparqlBuilder.var("at");

        String queryStr = createSnapshotQuery(snapshot, logisticsObjectIri, at);
        TupleQuery query = connection.prepareTupleQuery(queryStr);
        query.setBinding(logisticsObjectIri.getVarName(), loIri);
        query.setBinding(at.getVarName(), Values.literal(ts.toString(), CoreDatatype.XSD.DATETIME));

        try (TupleQueryResult result = query.evaluate()) {
            return result.stream()
                .map(bindingSet -> {
                    IRI snaphostIri = (IRI) bindingSet.getValue(snapshot.getVarName());
                    return findByIri(snaphostIri, connection);
                })
                .flatMap(Optional::stream)
                .findFirst();
        }
    }

    private String createSnapshotQuery(Variable snap, Variable loIri, Variable at) {
        Variable createdAt = SparqlBuilder.var("createdAt");
        return org.eclipse.rdf4j.sparqlbuilder.core.query.Queries.SELECT()
            .select(snap)
            .where(snap
                .isA(NEONE.Snapshot)
                .andHas(NEONE.referencesLogisticsObject, loIri)
                .andHas(NEONE.isCreatedAt, createdAt)
                .filter(Expressions.lte(createdAt, at))
            )
            .orderBy(createdAt.desc())
            .limit(1)
            .getQueryString();
    }
}
