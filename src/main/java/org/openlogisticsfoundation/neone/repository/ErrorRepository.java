// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.repository.Repository;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Error;
import org.openlogisticsfoundation.neone.model.handler.ErrorHandler;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ErrorRepository extends ModelRepository<Error> {

    public ErrorRepository(Repository repository, ErrorHandler modelHandler, IdProvider idProvider) {
        super(repository, modelHandler, idProvider);
    }
}
