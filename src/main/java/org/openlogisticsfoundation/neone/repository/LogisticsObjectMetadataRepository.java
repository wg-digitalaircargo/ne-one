//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.MissingMetadataException;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.model.handler.LogisticsObjectMetadataHandler;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class LogisticsObjectMetadataRepository extends MetadataRepository<LogisticsObjectMetadata> {

    @Inject
    public LogisticsObjectMetadataRepository(Repository repository,
                                             LogisticsObjectMetadataHandler modelHandler,
                                             IdProvider idProvider) {
        super(repository, modelHandler, idProvider);
    }

    public int getRevision(IRI loIri, RepositoryConnection connection) {
        Variable revisionVar = SparqlBuilder.var("revision");
        try (TupleQueryResult result = connection.prepareTupleQuery(Q.getRevisionOfLo(revisionVar, loIri)).evaluate()) {
            if (result.hasNext()) {
                BindingSet bindingSet = result.next();
                int revision = Literals.getIntValue(bindingSet.getValue(revisionVar.getVarName()), -1);
                if (revision == -1) {
                    throw new MissingMetadataException("Missing revision for " + loIri.stringValue() + ".");
                }
                return revision;
            } else  {
                throw new MissingMetadataException("Missing revision for " + loIri.stringValue() + ".");
            }
        }
    }

    private interface Q {
        static String getRevisionOfLo(Variable revision, IRI loIri) {
            Variable metadataSubject = SparqlBuilder.var("loMetadata");

            return Queries.SELECT()
                .select(revision)
                .where(metadataSubject
                    .isA(NEONE.LogisticsObjectMetadata)
                    .andHas(NEONE.describes, Rdf.iri(loIri))
                    .andHas(NEONE.hasRevision, revision)
                )
                .getQueryString();
        }
    }
}
