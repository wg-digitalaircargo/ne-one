//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatternNotTriples;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.AclAuthorization;
import org.openlogisticsfoundation.neone.model.handler.AclAuthorizationHandler;
import org.openlogisticsfoundation.neone.vocab.ACL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class AclAuthorizationRepository extends ModelRepository<AclAuthorization> {

    private static final Logger log = LoggerFactory.getLogger(AclAuthorizationRepository.class);

    @Inject
    public AclAuthorizationRepository(Repository repository, AclAuthorizationHandler handler, IdProvider idProvider) {
        super(repository, handler, idProvider);
    }

    public AclAuthorization getAclAuthorization(IRI agent, IRI accessTo, RepositoryConnection connection) {
        List<AclAuthorization> aclAuthorizations =
            findAclAuthorizations(Optional.of(agent), Optional.of(accessTo), connection);

        if (aclAuthorizations.size() > 1) {
            log.warn("Ambiguous ACl entries found [{}]", aclAuthorizations.stream()
                .map(AclAuthorization::iri)
                .map(IRI::stringValue)
                .collect(Collectors.joining(",")));
        }

        return aclAuthorizations.stream().findFirst().orElse(null);
    }

    public List<AclAuthorization> findAclAuthorizations(Optional<IRI> agent,
                                                        Optional<IRI> accessTo,
                                                        RepositoryConnection connection) {
        log.info("Looking for ACLs for agent [{}] and accessTo [{}]",
            agent, accessTo);

        Variable agentVar = SparqlBuilder.var("agent");
        Variable accessToVar = SparqlBuilder.var("accessTo");
        var queryString = createAclQuery(agentVar, accessToVar);

        GraphQuery query = connection.prepareGraphQuery(queryString);
        agent.ifPresent(a -> query.setBinding(agentVar.getVarName(), a));
        accessTo.ifPresent(a -> query.setBinding(accessToVar.getVarName(), a));

        try (GraphQueryResult queryResult = query.evaluate()) {
            var model = QueryResults.asModel(queryResult);

            return Models.subjectIRIs(model).stream()
                .map(iri -> modelHandler.fromModel(Values.iri(iri.stringValue()), model))
                .toList();
        }
    }

    public boolean aclExists(IRI agent, IRI accessTo, IRI mode, RepositoryConnection connection) {
        Variable agentVar = SparqlBuilder.var("agent");
        Variable accessToVar = SparqlBuilder.var("accessTo");
        Variable modeVar = SparqlBuilder.var("mode");

        TupleQuery query = connection.prepareTupleQuery(aclExistsQuery(agentVar, accessToVar, modeVar));
        query.setBinding(agentVar.getVarName(), agent);
        query.setBinding(accessToVar.getVarName(), accessTo);
        query.setBinding(modeVar.getVarName(), mode);

        try (TupleQueryResult queryResult = query.evaluate()) {
            return queryResult.hasNext();
        }
    }

    public IRI grantAccess(IRI agent, IRI accessTo, Set<IRI> modes, RepositoryConnection connection) {
        log.info("Granting access to [{}] for [{}] with modes [{}]", accessTo, agent, modes);

        AclAuthorization acl = getAclAuthorization(agent, accessTo, connection);
        if (acl != null) {
            IRI aclIri = acl.iri();
            Set<IRI> currentModes = acl.modes();
            modes.forEach(iri -> {
                if (!currentModes.contains(iri)) {
                    log.debug("ACL already available for [{}] and access to [{}], adding mode [{}]",
                        agent, accessTo, iri);
                    connection.add(aclIri, ACL.mode, iri);
                }
            });
        } else {
            acl = new AclAuthorization(
                idProvider.createAclAuthorizationId().getIri(),
                accessTo,
                agent,
                modes
            );
            log.debug("No ACL entry present, creating a new one");
            persist(acl, connection);
        }

        return acl.iri();
    }

    private String createAclQuery(Variable agentVar,
                                  Variable accessToVar) {

        Variable aclVar = SparqlBuilder.var("acl");
        Variable modeVar = SparqlBuilder.var("mode");

        return Queries.CONSTRUCT()
            .construct(
                aclVar.isA(ACL.Authorization),
                aclVar.has(ACL.agent, agentVar),
                aclVar.has(ACL.accessTo, accessToVar),
                aclVar.has(ACL.mode, modeVar))
            .where(aclVar
                .isA(ACL.Authorization)
                .andHas(ACL.agent, agentVar)
                .andHas(ACL.accessTo, accessToVar)
                .andHas(ACL.mode, modeVar))
            .getQueryString();
    }

    private String aclExistsQuery(Variable agentVar, Variable accessToVar, Variable modeVar) {
        Variable aclVar = SparqlBuilder.var("acl");

        GraphPatternNotTriples union =
            GraphPatterns.union(aclVar.has(ACL.agent, agentVar), aclVar.has(ACL.agent, Rdf.iri(ACL.AuthenticatedAgent)));

        return Queries.SELECT()
            .select(aclVar)
            .where(
                aclVar.isA(ACL.Authorization)
                    .andHas(ACL.mode, modeVar)
                    .andHas(ACL.accessTo, accessToVar)
                    .and(union)
            ).limit(1)
            .getQueryString();
    }

    /**
     * 1) Grant dataholder Read/Write access to specified target object.
     * 2) If desired, grant creator Read access to specified target object.
     * 3) If desired, grant AuthenticatedAgent (public user) read access to target object.
     *
     * @param targetIri iri of target object (Lo, Event, ActionRequest, ...)
     * @param creatorIri if set, the creator gets read access to target object.
     * @param publicAccess if true, public gets read access to target object.
     * @param connection database connection
     */
    public void grantDefaultAccess(IRI targetIri,
                                   IRI creatorIri,
                                   boolean publicAccess,
                                   RepositoryConnection connection) {
        // add ACL(Read,Write) for data holder
        grantAccess(idProvider.getDataHolderId().getIri(), targetIri,
            Set.of(ACL.Read, ACL.Write), connection);
        // add ACL(Read) for public access
        if (publicAccess) {
            grantAccess(ACL.AuthenticatedAgent, targetIri, Set.of(ACL.Read), connection);
        }
        // add ACL(Read) for creator if desired
        if(creatorIri != null && !creatorIri.equals(idProvider.getDataHolderId().getIri())) {
            grantAccess(creatorIri, targetIri, Set.of(ACL.Read), connection);
        }
    }
}
