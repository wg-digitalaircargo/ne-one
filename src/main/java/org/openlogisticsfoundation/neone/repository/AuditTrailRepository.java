// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expression;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.core.Orderable;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.model.handler.AuditTrailHandler;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Optional;

@ApplicationScoped
public class AuditTrailRepository extends ModelRepository<AuditTrail> {

    @Inject
    public AuditTrailRepository(Repository repository, AuditTrailHandler handler, IdProvider idProvider) {
        super(repository, handler, idProvider);
    }

    public Optional<AuditTrail> getAuditTrail(IRI auditTrailIri,
                                              Instant changesFrom,
                                              Instant changesUntil,
                                              RepositoryConnection connection) {

        Variable auditTrail = SparqlBuilder.var("auditTrail");
        Variable from = SparqlBuilder.var("from");
        Variable until = SparqlBuilder.var("until");
        var queryString = constructAuditTrailQuery(auditTrail, from, until,
            Optional.ofNullable(changesFrom), Optional.ofNullable(changesUntil),
            Optional.empty(), Optional.empty());

        GraphQuery query = connection.prepareGraphQuery(queryString);
        query.setBinding("auditTrail", auditTrailIri);
        if (changesFrom != null) {
            query.setBinding("from", Values.literal(changesFrom.toString(), CoreDatatype.XSD.DATETIME));
        }
        if (changesUntil != null) {
            query.setBinding("until", Values.literal(changesUntil.toString(), CoreDatatype.XSD.DATETIME));
        }

        try (GraphQueryResult result = query.evaluate()) {
            if (result.hasNext()) {
                var model = QueryResults.asModel(result);
                var at = modelHandler.fromModel(auditTrailIri, model);
                return Optional.of(at);
            } else {
                return Optional.empty();
            }
        }
    }

    private String constructAuditTrailQuery(Variable varAuditTrail,
                                            Variable varFrom,
                                            Variable varUntil,
                                            Optional<Instant> from,
                                            Optional<Instant> until,
                                            Optional<Orderable> orderBy,
                                            Optional<Integer> limit) {
        Variable latestRevision = SparqlBuilder.var("latestRevision");
        Variable changeRequest = SparqlBuilder.var("changeRequest");
        Variable change = SparqlBuilder.var("change");
        Variable operation = SparqlBuilder.var("operation");
        Variable o = SparqlBuilder.var("o");
        Variable op = SparqlBuilder.var("op");
        Variable p = SparqlBuilder.var("p");
        Variable s = SparqlBuilder.var("s");
        Variable error = SparqlBuilder.var("error");
        Variable requestStatus = SparqlBuilder.var("requestStatus");
        Variable requestedAt = SparqlBuilder.var("requestedAt");
        Variable requestedBy = SparqlBuilder.var("requestedBy");
        Variable revokedAt = SparqlBuilder.var("revokedAt");
        Variable revokedBy = SparqlBuilder.var("revokedBy");
        Variable description = SparqlBuilder.var("description");
        Variable revision = SparqlBuilder.var("revision");
        Variable logisticsObject = SparqlBuilder.var("logisticsObject");
        Variable datatype = SparqlBuilder.var("datatype");
        Variable value = SparqlBuilder.var("value");

        var xsd = SparqlBuilder.prefix("xsd", Values.iri("http://www.w3.org/2001/XMLSchema#"));
        var construct = Queries.CONSTRUCT()
            .prefix(xsd)
            .construct(
                varAuditTrail.has(API.hasLatestRevision, latestRevision),
                varAuditTrail.has(API.hasChangeRequest, changeRequest),
                changeRequest.has(API.isRequestedAt, requestedAt),
                changeRequest.has(API.isRequestedBy, requestedBy),
                changeRequest.has(API.hasRequestStatus, requestStatus),
                changeRequest.has(API.isRevokedAt, revokedAt),
                changeRequest.has(API.isRevokedBy, revokedBy),
                changeRequest.has(API.hasError, error),
                changeRequest.has(API.hasChange, change),
                change.has(API.hasDescription, description),
                change.has(API.hasRevision, revision),
                change.has(API.hasLogisticsObject, logisticsObject),
                change.has(API.hasOperation, operation),
                operation.has(API.op, op),
                operation.has(API.o, o),
                operation.has(API.p, p),
                operation.has(API.s, s),
                o.has(API.hasDatatype, datatype),
                o.has(API.hasValue, value));


        var c1 = varAuditTrail.isA(API.AuditTrail)
            .andHas(API.hasLatestRevision, latestRevision);
        var c11 = varAuditTrail.has(API.hasChangeRequest, changeRequest);
        var c2temp = changeRequest.isA(API.ChangeRequest)
            .andHas(API.isRequestedAt, requestedAt)
            .andHas(API.isRequestedBy, requestedBy)
            .andHas(API.hasRequestStatus, requestStatus)
            .andHas(API.hasChange, change)
            .and(GraphPatterns.optional(changeRequest.has(API.isRevokedAt, revokedAt)))
            .and(GraphPatterns.optional(changeRequest.has(API.isRevokedBy, revokedBy)))
            .and(GraphPatterns.optional(changeRequest.has(API.hasError, error)));
        var expression = createFilter(requestedAt, varFrom, varUntil, from, until);
        var c2 = expression.map(c2temp::filter).orElse(c2temp);
        var c3 = GraphPatterns.optional(change.has(API.hasDescription, description));
        var c31 = change.has(API.hasLogisticsObject, logisticsObject)
            .andHas(API.hasRevision, revision)
            .andHas(API.hasOperation, operation);
        var c4 = operation.has(API.op, op)
            .andHas(API.s, s)
            .andHas(API.p, p)
            .andHas(API.o, o);
        var c5 = o.has(API.hasDatatype, datatype)
            .andHas(API.hasValue, value);

        construct = construct.where(c1, GraphPatterns.optional(c11, c2, c3, c31, c4, c5));


        var constructWithOrderBy = orderBy.map(construct::orderBy).orElse(construct);
        var query = limit.map(constructWithOrderBy::limit).orElse(constructWithOrderBy);

        return query.getQueryString();
    }

    private Optional<Expression> createFilter(Variable requestedAt, Variable varFrom, Variable varUntil,
                                              Optional<Instant> from, Optional<Instant> until) {
        Optional<Expression> filterExpression = Optional.empty();
        if (from.isPresent() && until.isPresent()) {
            filterExpression = Optional.of(Expressions.and(Expressions.gte(requestedAt, varFrom), Expressions.lte(requestedAt, varUntil)));
        } else if (from.isPresent()) {
            filterExpression = Optional.of(Expressions.gte(requestedAt, varFrom));
        } else if (until.isPresent()) {
            filterExpression = Optional.of(Expressions.lte(requestedAt, varUntil));
        }
        return filterExpression;
    }
}
/*
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
CONSTRUCT {
    ?auditTrail <https://onerecord.iata.org/ns/api#hasLatestRevision> ?latestRevision .
    ?auditTrail <https://onerecord.iata.org/ns/api#hasChangeRequest> ?changeRequest .
    ?changeRequest <https://onerecord.iata.org/ns/api#isRequestedAt> ?requestedAt .
    ?changeRequest <https://onerecord.iata.org/ns/api#isRequestedBy> ?requestedBy .
    ?changeRequest <https://onerecord.iata.org/ns/api#hasRequestStatus> ?requestStatus .
    ?changeRequest <https://onerecord.iata.org/ns/api#isRevokedAt> ?revokedAt .
    ?changeRequest <https://onerecord.iata.org/ns/api#isRevokedBy> ?revokedBy .
    ?changeRequest <https://onerecord.iata.org/ns/api#hasError> ?error .
    ?changeRequest <https://onerecord.iata.org/ns/api#hasChange> ?change .
    ?change <https://onerecord.iata.org/ns/api#hasDescription> ?description .
    ?change <https://onerecord.iata.org/ns/api#hasRevision> ?revision .
    ?change <https://onerecord.iata.org/ns/api#hasLogisticsObject> ?logisticsObject .
    ?change <https://onerecord.iata.org/ns/api#hasOperation> ?operation .
    ?operation <https://onerecord.iata.org/ns/api#op> ?op .
    ?operation <https://onerecord.iata.org/ns/api#o> ?o .
    ?operation <https://onerecord.iata.org/ns/api#p> ?p .
    ?operation <https://onerecord.iata.org/ns/api#s> ?s .
    ?o <https://onerecord.iata.org/ns/api#hasDatatype> ?datatype .
    ?o <https://onerecord.iata.org/ns/api#hasValue> ?value .
}
WHERE {
    ?auditTrail a <https://onerecord.iata.org/ns/api#AuditTrail> ;
        <https://onerecord.iata.org/ns/api#hasLatestRevision> ?latestRevision .
    OPTIONAL {
        ?auditTrail <https://onerecord.iata.org/ns/api#hasChangeRequest> ?changeRequest .
        {
            ?changeRequest a <https://onerecord.iata.org/ns/api#ChangeRequest> ;
                <https://onerecord.iata.org/ns/api#isRequestedAt> ?requestedAt ;
                <https://onerecord.iata.org/ns/api#isRequestedBy> ?requestedBy ;
                <https://onerecord.iata.org/ns/api#hasRequestStatus> ?requestStatus ;
                <https://onerecord.iata.org/ns/api#hasChange> ?change .
                OPTIONAL { ?changeRequest <https://onerecord.iata.org/ns/api#isRevokedAt> ?revokedAt . }
                OPTIONAL { ?changeRequest <https://onerecord.iata.org/ns/api#isRevokedBy> ?revokedBy . }
                OPTIONAL { ?changeRequest <https://onerecord.iata.org/ns/api#hasError> ?error . }
        }
        OPTIONAL { ?change <https://onerecord.iata.org/ns/api#hasDescription> ?description . }
        ?change <https://onerecord.iata.org/ns/api#hasLogisticsObject> ?logisticsObject ;
            <https://onerecord.iata.org/ns/api#hasRevision> ?revision ;
            <https://onerecord.iata.org/ns/api#hasOperation> ?operation .
        ?operation <https://onerecord.iata.org/ns/api#op> ?op ;
            <https://onerecord.iata.org/ns/api#s> ?s ;
            <https://onerecord.iata.org/ns/api#p> ?p ;
            <https://onerecord.iata.org/ns/api#o> ?o .
        ?o <https://onerecord.iata.org/ns/api#hasDatatype> ?datatype ;
            <https://onerecord.iata.org/ns/api#hasValue> ?value .
    }
}
*/
