//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.AccessDelegationRequest;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.handler.AccessDelegationRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Optional;

@ApplicationScoped
public class AccessDelegationRequestRepository extends ActionRequestRepository<AccessDelegationRequest> {

    private static final Logger log = LoggerFactory.getLogger(AccessDelegationRequestRepository.class);

    @Inject
    public AccessDelegationRequestRepository(Repository repository,
                                             AccessDelegationRequestHandler modelHandler,
                                             IdProvider idProvider) {
        super(repository, modelHandler, idProvider);
    }

    @Override
    public IRI getRepositoryType() {
        return API.AccessDelegationRequest;
    }

    public Optional<IRI> findRequestedFor(IRI requestedBy, IRI hasLogisticsObject, RepositoryConnection connection) {
        log.info("Looking for access delegation requested by [{}] for Lo [{}]", requestedBy, hasLogisticsObject);

        Variable requestedForVar = SparqlBuilder.var("requestedFor");
        Variable requestedByVar = SparqlBuilder.var("requestedBy");
        Variable hasLogisticsObjectVar = SparqlBuilder.var("hasLogisticsObject");
        Variable hasRequestStatusVar = SparqlBuilder.var("hasRequestStatus");

        var queryString = createDelegationRequestQuery(requestedForVar, requestedByVar,
            hasLogisticsObjectVar, hasRequestStatusVar);

        TupleQuery query = connection.prepareTupleQuery(queryString);
        query.setBinding(requestedByVar.getVarName(), requestedBy);
        query.setBinding(hasLogisticsObjectVar.getVarName(), hasLogisticsObject);
        query.setBinding(hasRequestStatusVar.getVarName(), RequestStatus.REQUEST_ACCEPTED.iri());

        try (TupleQueryResult result = query.evaluate()) {
            return result.stream()
                .map(bindingSet -> (IRI) bindingSet.getValue(requestedForVar.getVarName()))
                .findFirst();
        }
    }

    private String createDelegationRequestQuery(Variable requestedForVar,
                                                Variable requestedByVar,
                                                Variable logisticsObjectVar,
                                                Variable requestStatus) {
        Variable delegationRequestVar = SparqlBuilder.var("accessDelegationRequest");
        Variable delegationVar = SparqlBuilder.var("accessDelegation");
        var c1 = delegationRequestVar
            .isA(API.AccessDelegationRequest)
            .andHas(API.isRequestedBy, requestedByVar)
            .andHas(API.hasRequestStatus, requestStatus)
            .andHas(API.hasAccessDelegation, delegationVar);
        var c2 = delegationVar
            .isA(API.AccessDelegation)
            .andHas(API.hasLogisticsObject, logisticsObjectVar)
            .andHas(API.isRequestedFor, requestedForVar);
        return org.eclipse.rdf4j.sparqlbuilder.core.query.Queries.SELECT()
            .select(requestedForVar)
            .where(c1, c2)
            .limit(1)
            .getQueryString();
    }
}
