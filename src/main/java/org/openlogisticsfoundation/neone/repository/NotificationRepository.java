// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.Notification;
import org.openlogisticsfoundation.neone.model.handler.NotificationHandler;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@ApplicationScoped
public class NotificationRepository extends ModelRepository<Notification> {

    @Inject
    public NotificationRepository(Repository repository, NotificationHandler handler, IdProvider idProvider) {
        super(repository, handler, idProvider);
    }

    public List<Notification> getPendingNotifications(RepositoryConnection connection) {

        Variable eventVar = SparqlBuilder.var("event");
        Variable notificationVar = SparqlBuilder.var("notification");
        Variable stateVar = SparqlBuilder.var("state");

        TupleQuery notificationsQuery = connection.prepareTupleQuery(
            Q.getNotificationsForEventsInState(eventVar, notificationVar, stateVar)
        );
        notificationsQuery.setBinding(stateVar.getVarName(), NEONE.PENDING);

        final List<Notification> notifications = new ArrayList<>();
        try (TupleQueryResult result = notificationsQuery.evaluate()) {
            result.forEach(bindingSet -> {
                IRI notificationId = (IRI) bindingSet.getValue(notificationVar.getVarName());
                Notification notification = findByIri(notificationId, connection)
                    .orElseThrow(() -> new NeoneException("Missing notification " + notificationId));
                notifications.add(notification);
            });
        }
        return notifications;
    }

    public interface Q {
        static String getNotificationsForEventsInState(Variable event, Variable notification, Variable state) {
            return org.eclipse.rdf4j.sparqlbuilder.core.query.Queries.SELECT()
                .select(notification)
                .where(
                    event
                        .isA(NEONE.NeoneEvent)
                        .andHas(NEONE.hasState, state)
                        .andHas(NEONE.hasNotification, notification)
                ).getQueryString();
        }
    }
}
