//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.Repository;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.SubscriptionRequest;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionRequestHandler;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class SubscriptionRequestRepository extends ActionRequestRepository<SubscriptionRequest> {

    @Inject
    public SubscriptionRequestRepository(Repository repository,
                                         SubscriptionRequestHandler modelHandler,
                                         IdProvider idProvider) {
        super(repository, modelHandler, idProvider);
    }

    @Override
    public IRI getRepositoryType() {
        return API.SubscriptionRequest;
    }
}
