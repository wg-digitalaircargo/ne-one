// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatternNotTriples;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.TriplePattern;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfLiteral;
import org.iata.onerecord.api.API;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class SubscriptionRepository extends ModelRepository<Subscription> {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionRepository.class);

    @Inject
    public SubscriptionRepository(Repository repository, SubscriptionHandler modelHandler, IdProvider idProvider) {
        super(repository, modelHandler, idProvider);
    }

    public boolean subscriptionForLogisticsObjectsExists(IRI loIri, Set<IRI> loTypes, RepositoryConnection connection) {

        var types = loTypes.stream().map(IRI::stringValue).collect(Collectors.toSet());

        log.info("Finding out if subscripion for LO [{}] with type(s) [{}] exists", loIri,
            String.join(",", types));

        Variable iriVar = SparqlBuilder.var("loIri");

        var queryString = createSubscriptionExistsQuery(iriVar, types);
        TupleQuery query = connection.prepareTupleQuery(queryString);
        query.setBinding(iriVar.getVarName(), Values.literal(loIri.stringValue()));

        try (TupleQueryResult queryResult = query.evaluate()) {
            return queryResult.hasNext();
        }
    }

    private String createSubscriptionExistsQuery(Variable loIriVar, Set<String> loTypes) {
        var subscriptionVar = SparqlBuilder.var("subscrIri");
        var loTypeVar = SparqlBuilder.var("loType");

        var topicLiterals = loTypes.stream()
            .map(Rdf::literalOf)
            .toArray(RdfLiteral.StringLiteral[]::new);

        GraphPatternNotTriples union =
            GraphPatterns.union(
                subscriptionVar
                    .has(API.hasTopicType, API.LOGISTICS_OBJECT_IDENTIFIER)
                    .andHas(API.hasTopic, loIriVar),
                subscriptionVar
                    .has(API.hasTopicType, API.LOGISTICS_OBJECT_TYPE)
                    .andHas(API.hasTopic, loTypeVar)
                    .filter(Expressions.in(loTypeVar, topicLiterals)));

        return Queries.SELECT()
            .select(subscriptionVar)
            .where(subscriptionVar.isA(API.Subscription)
                .and(union))
            .getQueryString();
    }

    public List<Subscription> getAcceptedSubscriptions(List<String> companyIds, Subscription.TopicType topicType,
                                                       Collection<String> topics, RepositoryConnection connection) {
        log.info("Get accepted subscriptions for companies [{}] with topicType [{}] and topic(s) [{}]",
            companyIds, topicType, String.join(",", topics));

        Variable subscription = SparqlBuilder.var("subscription");
        var query = createSubscriptionsQuery(subscription, companyIds, topicType, topics, Set.of(RequestStatus.REQUEST_ACCEPTED));
        TupleQuery tupleQuery = connection.prepareTupleQuery(query);
        try (TupleQueryResult queryResult = tupleQuery.evaluate()) {
            return queryResult.stream().map(bindingSet -> {
                IRI subscriptionIri = (IRI) bindingSet.getValue(subscription.getVarName());
                return findByIri(subscriptionIri, connection);
            }).filter(Optional::isPresent).map(Optional::get).toList();
        }
    }

    private String createSubscriptionsQuery(Variable subscription, List<String> companyIds,
                                            Subscription.TopicType topicType, Collection<String> topics,
                                            Collection<RequestStatus> requestStatuses) {

        Variable topicVar = SparqlBuilder.var("topic");

        TriplePattern whereClause = subscription
            .isA(API.Subscription)
            .andHas(API.hasTopicType, Rdf.iri(topicType.getValue()))
            .andHas(API.hasTopic, topicVar);

        GraphPattern pattern;

        if (requestStatuses != null && !requestStatuses.isEmpty()) {
            Variable subscriptionRequest = SparqlBuilder.var("subscriptionRequest");
            Variable requestStatus = SparqlBuilder.var("requestStatus");
            Iri[] requestStatusIris = requestStatuses.stream().map(rs -> Rdf.iri(rs.iri())).toArray(Iri[]::new);
            pattern = whereClause
                .and(subscriptionRequest.has(API.hasSubscription, subscription))
                .and(
                    subscriptionRequest
                        .has(API.hasRequestStatus, requestStatus)
                        .filter(Expressions.in(requestStatus, requestStatusIris)));
        } else {
            pattern = whereClause;
        }

        if (companyIds != null) {
            Iri[] companyIris = companyIds.stream().map(Rdf::iri).toList().toArray(new Iri[companyIds.size()]);
            Variable companyId = SparqlBuilder.var("companyId");
            pattern = whereClause.andHas(API.hasSubscriber, companyId).filter(Expressions.in(companyId, companyIris));
        }

        var topicLiterals = topics.stream()
            .map(s -> Rdf.literalOfType(s, CoreDatatype.XSD.ANYURI.getIri()))
            .toArray(size -> new RdfLiteral.StringLiteral[size]);
        pattern.filter(Expressions.in(topicVar, topicLiterals));

        return Queries.SELECT().select(subscription).where(pattern).getQueryString();
    }
}
