//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import org.eclipse.rdf4j.repository.Repository;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.SubscriptionMetadata;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionMetadataHandler;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class SubscriptionMetadataRepository extends MetadataRepository<SubscriptionMetadata> {

    @Inject
    public SubscriptionMetadataRepository(Repository repository,
                                          SubscriptionMetadataHandler modelHandler,
                                          IdProvider idProvider) {
        super(repository, modelHandler, idProvider);
    }
}
