// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;


import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.model.handler.NeoneEventHandler;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class NeoneEventRepository extends ModelRepository<NeoneEvent> {

    @Inject
    public NeoneEventRepository(Repository repository, NeoneEventHandler handler,  IdProvider idProvider) {
        super(repository, handler, idProvider);
    }

    public void setEventState(IRI eventId, NeoneEvent.State state, RepositoryConnection connection) {
        connection.remove(eventId, NEONE.hasState, null);
        connection.add(eventId, NEONE.hasState, state.getValue());
    }

    public void addNotification(IRI eventId, IRI notificationId, RepositoryConnection connection) {
        connection.add(eventId, NEONE.hasNotification, notificationId);
    }

    public void deleteNotification(IRI eventId, IRI notificationId, RepositoryConnection connection) {
        connection.remove(eventId, NEONE.hasNotification, notificationId);
    }

    public List<IRI> getPendingEventsWithoutNotifications(RepositoryConnection connection) {
        Variable eventVar = SparqlBuilder.var("event");
        try (TupleQueryResult result = connection.prepareTupleQuery(
            Q.getPendingEventsWithoutNotifications(eventVar)).evaluate()) {

            return result.stream()
                .map(bindingSet -> (IRI) bindingSet.getValue(eventVar.getVarName()))
                .collect(Collectors.toList());
        }
    }

    public NeoneEvent addEvent(IRI loId, IRI eventType, Optional<IRI> triggeredBy,
                               Set<IRI> changedProperties, Set<IRI> loTypes, RepositoryConnection connection) {

        NeoneEvent event = new NeoneEvent(idProvider.createLoEventUri(loId).getIri(), loId,
            eventType, NeoneEvent.State.NEW, triggeredBy, changedProperties, loTypes);

        persist(event, connection);

        return event;
    }

    public List<NeoneEvent> getNewEvents(RepositoryConnection connection) {
        Variable eventVar = SparqlBuilder.var("event");
        Variable stateVar = SparqlBuilder.var("state");

        TupleQuery query = connection.prepareTupleQuery(Q.getEventsInState(eventVar, stateVar));
        query.setBinding(stateVar.getVarName(), NeoneEvent.State.NEW.getValue());
        try (TupleQueryResult result = query.evaluate()) {
            return result.stream().map(bindingSet -> {
                    IRI eventIri = (IRI) bindingSet.getValue(eventVar.getVarName());
                    return findByIri(eventIri, connection);
                })
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        }
    }

    public interface Q {
        static String getPendingEventsWithoutNotifications(Variable eventVar) {
            Variable notificationVar = SparqlBuilder.var("x");

            return org.eclipse.rdf4j.sparqlbuilder.core.query.Queries.SELECT()
                .select(eventVar)
                .where(
                    eventVar
                        .isA(NEONE.NeoneEvent)
                        .andHas(NEONE.hasState, Rdf.iri(NEONE.PENDING))
                        .filterNotExists(eventVar.has(NEONE.hasNotification, notificationVar))
                ).getQueryString();
        }

        static String getEventsInState(Variable eventVar, Variable stateVar) {
            return org.eclipse.rdf4j.sparqlbuilder.core.query.Queries.SELECT()
                .select(eventVar)
                .where(eventVar
                    .isA(NEONE.NeoneEvent)
                    .andHas(NEONE.hasState, stateVar)
                ).getQueryString();
        }
    }
}
