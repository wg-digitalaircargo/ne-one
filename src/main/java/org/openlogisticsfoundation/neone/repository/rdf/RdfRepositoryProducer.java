// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository.rdf;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.sail.lmdb.LmdbStore;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;

import java.io.File;
import java.util.Optional;

public class RdfRepositoryProducer {

    private static final Logger log = LoggerFactory.getLogger(RdfRepositoryProducer.class);

    @ConfigProperty(name = "repository-type", defaultValue = "in-memory")
    String repoType;

    @ConfigProperty(name = "http-repository-url")
    Optional<String> repositoryUrl;

    @ConfigProperty(name = "repository-data-dir")
    Optional<String> repositoryDataDir;

    @ConfigProperty(name = "sparql-query-endpoint")
    Optional<String> sparqlQueryEndpoint;

    @ConfigProperty(name = "sparql-update-endpoint")
    Optional<String> sparqlUpdateEndpoint;

    @ConfigProperty(name = "repository-username")
    Optional<String> repositoryUsername;

    @ConfigProperty(name = "repository-password")
    Optional<String> repositoryPassword;

    @ApplicationScoped
    @Produces
    Repository repository() {
        switch (repoType) {
            case "http" -> {
                if (repositoryUrl.isEmpty()) {
                    throw new IllegalArgumentException("Missing [http-repository-url] configuration");
                }
                log.info("Using http repository located at [{}]", repositoryUrl.get());

                HTTPRepository repository = new HTTPRepository(repositoryUrl.get());
                if (repositoryUsername.isPresent() && repositoryPassword.isPresent()) {
                    repository.setUsernameAndPassword(repositoryUsername.get(), repositoryPassword.get());
                }
                return repository;
            }
            case "lmdb" -> {
                if (repositoryDataDir.isEmpty()) {
                    throw new IllegalArgumentException("Missing [repository-data-dir] configuration");
                }
                log.info("Using LMDB store with data located at [{}]", repositoryDataDir.get());

                File dataDir = new File(repositoryDataDir.get());

                return new SailRepository(new LmdbStore(dataDir));
            }
            case "sparql" -> {
                if (sparqlQueryEndpoint.isEmpty() || sparqlUpdateEndpoint.isEmpty()) {
                    throw new IllegalArgumentException("Missing [sparql-query-endpoint] and/or [sparql-update-endpoint] configuration");
                }
                log.info("Using SPARQL repository with SPARQL query endpoint located at [{}] and SPARQL update endpoint located at [{}]", sparqlQueryEndpoint.get(), sparqlUpdateEndpoint.get());
                SPARQLRepository repository = new SPARQLRepository(sparqlQueryEndpoint.get(), sparqlUpdateEndpoint.get());
                if (repositoryUsername.isPresent() && repositoryPassword.isPresent()) {
                    repository.setUsernameAndPassword(repositoryUsername.get(), repositoryPassword.get());
                }
                return repository;
            }
            case "native" -> {
                if (repositoryDataDir.isEmpty()) {
                    throw new IllegalArgumentException("Missing [repository-data-dir] configuration");
                }

                log.info("Using native RDF repository with data located at [{}]", repositoryDataDir.get());
                File dataDir = new File(repositoryDataDir.get());
                return new SailRepository(new NativeStore(dataDir));
            }
            default -> {
                log.warn("Using in memory RDF store, rdf store will not be persistent");
                return new SailRepository(new MemoryStore());
            }
        }
    }
}
