//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.AccessDeniedException;
import org.openlogisticsfoundation.neone.exception.NeoneException;
import org.openlogisticsfoundation.neone.service.LogisticsObjectService;
import org.openlogisticsfoundation.neone.service.internal.AclAuthorizationService;

import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@AclSecured
@Interceptor
public class AclInterceptor {

    final LogisticsObjectService loService;

    final AclAuthorizationService aclService;

    final Instance<AccessSubject> accessSubject;

    final IdProvider idProvider;

    @Inject
    public AclInterceptor(LogisticsObjectService loService,
                          AclAuthorizationService aclService,
                          Instance<AccessSubject> accessSubject,
                          IdProvider idProvider) {
        this.loService = loService;
        this.aclService = aclService;
        this.accessSubject = accessSubject;
        this.idProvider = idProvider;
    }

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {

        Map<AccessObject.Type, String> typeValueMapping = getMethodParameterValueMapping(context);
        validateAccessObjectDeclaration(typeValueMapping);
        IRI accessObject = getAccessObjectId(typeValueMapping).getIri();
        IRI requiredMode = determineRequiredPermission(context.getMethod());
        IRI agent = accessSubject.get().iri();

        checkPermission(agent, accessObject, requiredMode);

        return context.proceed();
    }

    private NeoneId getAccessObjectId(Map<AccessObject.Type, String> typeValueMapping) {
        NeoneId accessObjectId;
        switch (typeValueMapping.size()) {
            case 1 -> {
                if (typeValueMapping.containsKey(AccessObject.Type.ACTION_REQUEST)) {
                    accessObjectId = idProvider.getActionRequestId(
                        typeValueMapping.get(AccessObject.Type.ACTION_REQUEST)
                    );
                } else {
                    accessObjectId = idProvider.getUriForLoId(
                        typeValueMapping.get(AccessObject.Type.LOGISTICS_OBJECT_ID)
                    );
                }
            }
            case 2 -> accessObjectId = idProvider.getUriForEventId(
                typeValueMapping.get(AccessObject.Type.LOGISTICS_OBJECT_ID),
                typeValueMapping.get(AccessObject.Type.LOGISTICS_EVENT_ID)
            );
            default -> throw new NeoneException("Invalid AccessObject definition");
        }

        return accessObjectId;
    }

    private static void validateAccessObjectDeclaration(Map<AccessObject.Type, String> typeValueMapping) {
        boolean valid = false;

        switch (typeValueMapping.size()) {
            case 1 ->
                // if only one AccessObject.Type is present it must be either LOGISTICS_OBJECT_ID
                // or ACTION_REQUEST
                valid = typeValueMapping.containsKey(AccessObject.Type.LOGISTICS_OBJECT_ID) ||
                    typeValueMapping.containsKey(AccessObject.Type.ACTION_REQUEST);
            case 2 ->
                // if two AccessObject.Types are present it must be LOGISTICS_OBJECT_ID and
                // LOGISTICS_EVENT_ID
                valid = typeValueMapping.keySet().containsAll(
                    EnumSet.of(AccessObject.Type.LOGISTICS_OBJECT_ID, AccessObject.Type.LOGISTICS_EVENT_ID)
                );
            default -> {
            }
        }

        if (!valid) {
            throw new NeoneException(
                """
                Ambiguous access objects found, either exactly one AccessObject has to be
                defined and pointing to a LogisticsObject or an ActionRequest internal id,
                or two AccessObjects have to be defined, one pointing to a LogisticsObject internal id
                and the other pointing to a LogisticsEvent internal id.
                """);
        }
    }

    private static Map<AccessObject.Type, String> getMethodParameterValueMapping(InvocationContext context) {
        Parameter[] methodParams = context.getMethod().getParameters();
        Map<AccessObject.Type, String> typeValueMapping = new HashMap<>();
        for (int i = 0; i < methodParams.length; i++) {
            AccessObject accessObject = methodParams[i].getAnnotation(AccessObject.class);
            if (accessObject != null) {
                if (!(context.getParameters()[i] instanceof String)) {
                    throw new NeoneException(
                        """
                        Invalid declaration of AccessObject on parameter [%s],
                        has to be of type String, was [%s]
                        """.formatted(methodParams[i].getName(), methodParams[i].getType().getName()));
                }
                typeValueMapping.put(
                    accessObject.value(),
                    (String) context.getParameters()[i]
                );
            }
        }
        return typeValueMapping;
    }

    private IRI determineRequiredPermission(Method method) {
        AclSecured aclSecured = Optional.ofNullable(method.getAnnotation(AclSecured.class))
            .orElseThrow(() -> new NeoneException(
                """
                No access mode declared on [%s] in class [%s]
                """.formatted(method.getName(), method.getDeclaringClass().getName())));

        if (aclSecured.value().length != 1) {
            throw new NeoneException(
                 """
                 Invalid access mode declaration [%s] in class [%s].
                 There needs to be exactly one declared access mode.
                 """.formatted(method.getName(), method.getDeclaringClass().getName() ));
        }

        return Values.iri(aclSecured.value()[0].value());
    }

    private void checkPermission(IRI agent, IRI accessTo, IRI mode) {
        if (!aclService.isAuthorized(agent, accessTo, mode)) {
            throw new AccessDeniedException(agent, accessTo);
        }
    }
}
