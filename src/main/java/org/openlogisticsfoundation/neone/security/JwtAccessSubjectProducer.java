// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.quarkus.arc.lookup.LookupIfProperty;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.auth.principal.JWTCallerPrincipal;
import org.eclipse.rdf4j.model.util.Values;
import org.openlogisticsfoundation.neone.exception.AccessDeniedException;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;

public class JwtAccessSubjectProducer {

    public static final String ONE_RECORD_ID_CLAIM = "logistics_agent_uri";

    @Inject
    SecurityIdentity identity;

    @LookupIfProperty(name = "disable.access.subject", stringValue = "false")
    @Produces
    @RequestScoped
    AccessSubject accessSubject() {
        JWTCallerPrincipal principal = (JWTCallerPrincipal) identity.getPrincipal();
        String logisticsAgentUri = principal.getClaim(ONE_RECORD_ID_CLAIM);
        if (logisticsAgentUri != null) {
            return new AccessSubject(Values.iri(logisticsAgentUri));
        }
        throw new AccessDeniedException();
    }
}
