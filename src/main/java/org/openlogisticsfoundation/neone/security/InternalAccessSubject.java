// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import org.eclipse.rdf4j.model.IRI;
import org.openlogisticsfoundation.neone.exception.AccessDeniedException;

public class InternalAccessSubject {

    private final AccessSubject accessSubject;

    public InternalAccessSubject(AccessSubject accessSubject) {
        this.accessSubject = accessSubject;
    }

    public IRI iri() {
        if(accessSubject == null) {
            throw new AccessDeniedException();
        }
        return accessSubject.iri();
    }

    public boolean isResolvable() {
        return accessSubject != null;
    }
}
