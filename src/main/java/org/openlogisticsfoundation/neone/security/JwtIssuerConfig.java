// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

import java.util.Map;

@ConfigMapping(prefix = "auth")
public interface JwtIssuerConfig {

    Map<String, String> validIssuers();
    Map<String, IssuerConfig> issuers();

    interface IssuerConfig {
        @WithName("publickey.location")
        String publicKeyLocation();
    }
}
