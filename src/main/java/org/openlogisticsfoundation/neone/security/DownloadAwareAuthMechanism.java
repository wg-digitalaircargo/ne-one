// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.quarkus.oidc.runtime.OidcAuthenticationMechanism;
import io.quarkus.security.identity.IdentityProviderManager;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.identity.request.AuthenticationRequest;
import io.quarkus.smallrye.jwt.runtime.auth.JWTAuthMechanism;
import io.quarkus.vertx.http.runtime.security.ChallengeData;
import io.quarkus.vertx.http.runtime.security.HttpAuthenticationMechanism;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Inject;
import java.util.HashSet;
import java.util.Set;

@Alternative
@Priority(1)
@ApplicationScoped
public class DownloadAwareAuthMechanism implements HttpAuthenticationMechanism {

    @Inject
    JWTAuthMechanism jwt;

    @Inject
    OidcAuthenticationMechanism oidc;

    @ConfigProperty(name = "quarkus.http.root-path")
    String rootPath;

    @Override
    public Uni<SecurityIdentity> authenticate(RoutingContext context, IdentityProviderManager identityProviderManager) {
        return conditionallyEnableOidc(context).authenticate(context, identityProviderManager);
    }

    @Override
    public Uni<ChallengeData> getChallenge(RoutingContext context) {
        return conditionallyEnableOidc(context).getChallenge(context);
    }

    @Override
    public Set<Class<? extends AuthenticationRequest>> getCredentialTypes() {
        Set<Class<? extends AuthenticationRequest>> credentialTypes = new HashSet<>();
        credentialTypes.addAll(jwt.getCredentialTypes());
        credentialTypes.addAll(oidc.getCredentialTypes());
        return credentialTypes;
    }

    private HttpAuthenticationMechanism conditionallyEnableOidc(RoutingContext context) {
        if (context.request().path().startsWith(rootPath + "files/download")
            && context.request().method().equals(HttpMethod.GET)) {

            if (context.request().headers().contains("Authorization")) {
                return jwt;
            }
            return oidc;
        }
        return jwt;
    }
}
