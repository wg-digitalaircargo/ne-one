// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import io.smallrye.jwt.auth.principal.DefaultJWTTokenParser;
import io.smallrye.jwt.auth.principal.JWTAuthContextInfo;
import io.smallrye.jwt.auth.principal.JWTCallerPrincipal;
import io.smallrye.jwt.auth.principal.JWTCallerPrincipalFactory;
import io.smallrye.jwt.auth.principal.ParseException;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;
import org.openlogisticsfoundation.neone.exception.AccessDeniedException;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Inject;

import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
@Alternative
@Priority(1)
public class MultiIssuerJwtCallerPrincipalFactory extends JWTCallerPrincipalFactory {

    private final Map<String, DefaultJWTTokenParser> parsers;

    @Inject
    JwtIssuerConfig config;


    public MultiIssuerJwtCallerPrincipalFactory() {
        this.parsers = new HashMap<>();
    }

    @PostConstruct
    public void onPostConstruct() {
        config.validIssuers().forEach((k, v) -> parsers.put(v, new DefaultJWTTokenParser()));
    }

    @Override
    public JWTCallerPrincipal parse(String token, JWTAuthContextInfo authContextInfo) throws ParseException {
        // only parse the token to get the issuer
        JwtClaims claims = parseUnverified(token);
        // check if the issuer is a trusted issuer
        String issuer = getValidatedIssuer(claims);
        // create a custom auth context configuration for the specific issuer
        JWTAuthContextInfo authContextInfoForIssuer = createAuthContextInfoForIssuer(issuer, authContextInfo);

        // validate the token
        JwtContext jwtContext = parsers.get(issuer).parse(token, authContextInfoForIssuer);
        String type = jwtContext.getJoseObjects().get(0).getHeader("typ");
        return new DefaultJWTCallerPrincipal(type, jwtContext.getJwtClaims());

    }

    private JwtClaims parseUnverified(String token) throws ParseException {
        JwtConsumer consumer = new JwtConsumerBuilder()
            .setSkipAllValidators()
            .setDisableRequireSignature()
            .setSkipSignatureVerification()
            .build();

        try {
            return consumer.processToClaims(token);
        } catch (InvalidJwtException e) {
            throw new ParseException("Invalid Json Web Token", e);
        }
    }

    private String getValidatedIssuer(JwtClaims claims) throws ParseException {
        try {
            if (config.validIssuers().containsValue(claims.getIssuer())) {
                return claims.getIssuer();
            } else {
                throw new ParseException("Invalid issuer [" + claims.getIssuer() + "]");
            }
        } catch (MalformedClaimException e) {
            throw new ParseException("Unable to get [iss] claim from JWT", e);
        }
    }

    private JWTAuthContextInfo createAuthContextInfoForIssuer(String issuer, JWTAuthContextInfo defaultContext)
        throws ParseException {

        String issuerKey = config.validIssuers().entrySet().stream()
            .filter(entry -> entry.getValue().equals(issuer))
            .map(Map.Entry::getKey)
            .findAny()
            .orElseThrow(AccessDeniedException::new);

        String publicKeyLocation = config.issuers().get(issuerKey).publicKeyLocation();

        if (publicKeyLocation == null) {
            throw new ParseException("No public key location set for [" + issuer +"]");
        }

        JWTAuthContextInfo authContextInfo = new JWTAuthContextInfo(defaultContext);
        authContextInfo.setPublicKeyLocation(publicKeyLocation);
        authContextInfo.setIssuedBy(issuer);

        return authContextInfo;
    }
}
