//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.quarkus.security.identity.SecurityIdentity;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;

public class InternalAccessSubjectProducer {
    private static final Logger log = LoggerFactory.getLogger(InternalAccessSubjectProducer.class);

    @ConfigProperty(name = "disable.access.subject")
    boolean disableAccessSubject;

    @Produces
    @RequestScoped
    public InternalAccessSubject internalAccessSubject(SecurityIdentity identity, Instance<AccessSubject> accessSubject) {
        if (disableAccessSubject) {
            return new InternalAccessSubject(accessSubject.get());
        } else {
            if (identity.getPrincipal() == null) {
                // NullJsonWebToken
                log.warn("No JWT provided");
                return new InternalAccessSubject(null);
            } else {
                return new InternalAccessSubject(accessSubject.get());
            }
        }
    }
}
