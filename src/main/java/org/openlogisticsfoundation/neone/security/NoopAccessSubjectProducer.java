// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.quarkus.arc.lookup.LookupIfProperty;
import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import org.openlogisticsfoundation.neone.NeoneServerConfig;
import org.openlogisticsfoundation.neone.controller.IdProvider;

public class NoopAccessSubjectProducer {

    @Inject
    NeoneServerConfig config;

    @Inject
    IdProvider idProvider;

    @LookupIfProperty(name = "disable.access.subject", stringValue = "true")
    @Produces
    @RequestScoped
    AccessSubject accessSubject() {
        return new AccessSubject(idProvider.getUriForLoId(config.dataHolder().id()).getIri());
    }
}
