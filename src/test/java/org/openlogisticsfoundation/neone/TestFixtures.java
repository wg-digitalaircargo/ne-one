// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.ChangeRequest;
import org.openlogisticsfoundation.neone.model.Operation;
import org.openlogisticsfoundation.neone.model.OperationObject;
import org.openlogisticsfoundation.neone.model.PatchOperation;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.SubscriptionRequest;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class TestFixtures {

    static String loadResource(String fileName) {
        return new BufferedReader(new InputStreamReader(Objects.requireNonNull(
            TestFixtures.class.getClassLoader().getResourceAsStream(fileName)),
            StandardCharsets.UTF_8
        )).lines().collect(Collectors.joining("\n"));
    }

    public static Model loadResourceAsModel(String fileName, RDFFormat format) throws IOException {
        return Rio.parse
            (TestFixtures.class.getClassLoader().getResourceAsStream(fileName),
                format);
    }

    public static Model asModel(String content, RDFFormat format) throws IOException {
        return Rio.parse(new StringReader(content), format);
    }

    public static final String PIECE_WITH_ONE_HI_JSON_LD = loadResource("pieceWithOneHandlingInstruction.json");

    public static final String PIECE_WITH_TWO_TYPES_JSON_LD = loadResource("pieceWithTwoTypes.json");

    public static final String PIECE_WITH_ONE_HI_TURTLE = loadResource("pieceWithOneHandlingInstruction.ttl");

    public static final String PIECE_WITH_ID_JSONLD = loadResource("pieceWithId.json");

    public static final String PIECE_WITHOUT_REVISION_NUMBER = loadResource("pieceWithoutRevisionNumber.json");

    public static final String PIECE_WITHOUT_CREATED_AT = loadResource("pieceWithoutCreatedAt.json");

    public static final String DELETE_PATCH_RQ = loadResource("delete-patch-rq.json");

    public static final String INVALID_PATCH_RQ = loadResource("invalid-patch-rq.json");

    public static final String SUBSCRIPTION_PROPOSAL_RQ = loadResource("subscription-proposal-rq.json");

    public static final String SUBSCRIPTION_PROPOSAL_INVALID_RQ = loadResource("subscription-proposal-invalid-rq.json");

    public static final String MINIMAL_SUBSCRIPTION = loadResource("minimal-subscription.json");

    public static final String SIMPLE_EVENT = loadResource("simple-event.json");

    public static final String SIMPLE_EVENT_1 = loadResource("simple-event-1.json");

    public static final String SIMPLE_EVENT_2 = loadResource("simple-event-2.json");

    public static final String ACL = loadResource("acl.json");

    public static final String ACL2 = loadResource("acl2.json");

    public static final String ACCESS_DELEGATION = loadResource("access-delegation.json");

    public static final String ACCESS_DELEGATION_INVALID = loadResource("access-delegation-invalid-lo.json");

    public static final String WAYBILL_WITH_SHIPMENT = loadResource("waybillWithShipment.json");

    public static AuditTrail getAuditTrailTestModel(IRI auditTrailIri, int latestRevision, Instant first, Instant second) {
        Operation o = new Operation(Values.iri("some:iri"),
            new OperationObject(Values.iri("some:iri"), "dt", "value"),
            PatchOperation.ADD,
            Values.iri("some:iri"),
            "some string");

        ChangeRequest cr1 = new ChangeRequest(Values.iri("http://some-iri/1"),
            Optional.empty(),
            RequestStatus.REQUEST_ACCEPTED,
            first,
            Values.iri("http://some-iri/1-1"),
            Optional.empty(),
            Optional.empty(),
            new Change(Values.iri("http://some-iri/1-1-1"),
                Optional.of("change1"), Set.of(o), 1, Values.iri("http://some-iri/1-1-2"), Optional.empty()));

        ChangeRequest cr2 = new ChangeRequest(Values.iri("http://some-iri/2"), Optional.empty(),
            RequestStatus.REQUEST_ACCEPTED, second, Values.iri("http://some-iri/2-2"), Optional.empty(),
            Optional.empty(), new Change(Values.iri("http://some-iri/2-2-2"),
            Optional.of("change2"), Set.of(o), 2, Values.iri("http://some-iri/1-1-2"), Optional.empty()));

        return new AuditTrail(auditTrailIri, List.of(cr1, cr2), latestRevision);
    }

    public static AuditTrail getAuditTrailWithNotAcceptedChangeRequestsModel(IRI auditTrailIri, Instant first, Instant second) {
        Operation o = new Operation(Values.iri("some:iri"),
            new OperationObject(Values.iri("some:iri"), "dt", "value"),
            PatchOperation.ADD,
            Values.iri("some:iri"),
            "some string");

        ChangeRequest cr1 = new ChangeRequest(Values.iri("http://some-iri/1"),
            Optional.empty(),
            RequestStatus.REQUEST_PENDING,
            first,
            Values.iri("http://some-iri/1-1"),
            Optional.empty(),
            Optional.empty(),
            new Change(Values.iri("http://some-iri/1-1-1"),
                Optional.of("change1"), Set.of(o), 1, Values.iri("http://some-iri/1-1-2"), Optional.empty()));

        ChangeRequest cr2 = new ChangeRequest(Values.iri("http://some-iri/2"), Optional.empty(),
            RequestStatus.REQUEST_REJECTED, second, Values.iri("http://some-iri/2-2"), Optional.empty(),
            Optional.empty(), new Change(Values.iri("http://some-iri/2-2-2"),
            Optional.of("change2"), Set.of(o), 1, Values.iri("http://some-iri/1-1-2"), Optional.empty()));

        return new AuditTrail(auditTrailIri, List.of(cr1, cr2), 1);
    }

    public static Subscription getSubscription() {
        return new Subscription(
            Values.iri("neon:some_subscription_iri"),
            Values.iri("neone:some_subscriber_iri"),
            Subscription.TopicType.LOGISTICS_OBJECT_TYPE,
            "some topic",
            Optional.of("some description"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty()
        );
    }

    public static SubscriptionRequest getPendingSubscriptionRequest() {
        return new SubscriptionRequest(
            Values.iri("neone:some_subscription_iri"),
            RequestStatus.REQUEST_PENDING,
            Instant.now(),
            Values.iri("neone:some_requested_by_iri"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            getSubscription()
        );
    }

    public static void mockDoInTransactionCall(RepositoryTransaction transaction, RepositoryConnection connection) {
        doAnswer(invocationOnMock -> {
            Function<RepositoryConnection, Void> unitOfWork = invocationOnMock.getArgument(0);
            return unitOfWork.apply(connection);
        }).when(transaction).transactionallyGet(any(Function.class));

        ManagedExecutor executor = mock(ManagedExecutor.class);
        doAnswer(invocationOnMock -> {
            invocationOnMock.getArgument(0, Runnable.class).run();
            return null;
        }).when(executor).execute(any());

        doAnswer(invocationOnMock -> {
            BiFunction<RepositoryConnection, RepositoryTransaction.RepositoryTransactionHooks, Void> unitOfWork = invocationOnMock.getArgument(0);
            RepositoryTransaction.RepositoryTransactionHooks hooks = new RepositoryTransaction.RepositoryTransactionHooks(executor);
            Void apply = unitOfWork.apply(connection, hooks);
            hooks.runPostCommitHooks();
            return apply;
        }).when(transaction).transactionallyGet(any(BiFunction.class));

        doAnswer(invocationOnMock -> {
            Consumer<RepositoryConnection> unitOfWork = invocationOnMock.getArgument(0);
            unitOfWork.accept(connection);
            return null;
        }).when(transaction).transactionallyDo(any(Consumer.class));

        doAnswer(invocationOnMock -> {
            BiConsumer<RepositoryConnection, RepositoryTransaction.RepositoryTransactionHooks> unitOfWork = invocationOnMock.getArgument(0);
            RepositoryTransaction.RepositoryTransactionHooks hooks = new RepositoryTransaction.RepositoryTransactionHooks(executor);
            unitOfWork.accept(connection, hooks);
            hooks.runPostCommitHooks();
            return null;
        }).when(transaction).transactionallyDo(any(BiConsumer.class));
    }
}
