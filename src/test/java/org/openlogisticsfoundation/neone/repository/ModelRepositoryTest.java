// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.model.LogisticsObject;

import jakarta.inject.Inject;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@QuarkusTest
class ModelRepositoryTest {

    @Inject
    LogisticsObjectRepository repository;

    @Inject
    RepositoryTransaction transaction;

    @Test
    public void should_not_modify_the_number_of_statements() throws Exception {
        // Given.
        var model = Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ONE_HI_JSON_LD), RDFFormat.JSONLD);

        // When it comes to setting the root IRI...
        var newRootId = Values.iri("https://some/new/root/id");
        var newModel = repository.addIdToRootStatements(model, newRootId);

        // ... then the number of statements in the model must be unchanged.
        var originalSpliterator = model.getStatements(null, null, null).spliterator();
        long originalNumberOfStatements = StreamSupport.stream(originalSpliterator, false).count();
        var newSpliterator = newModel.getStatements(null, null, null).spliterator();
        long newNumberOfStatements = StreamSupport.stream(newSpliterator, false).count();
        assertThat(originalNumberOfStatements, is(equalTo(newNumberOfStatements)));
    }

    @Test
    public void should_replace_the_iri_of_the_root_element() throws Exception {
        // Given some model with 1 root element referencing some child.
        var model = Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ONE_HI_JSON_LD), RDFFormat.JSONLD);

        // When it comes to setting the root IRI...
        var newRootId = Values.iri("https://some/new/root/id");
        var newModel = repository.addIdToRootStatements(model, newRootId);

        // ... then the original root IRI must be replaced:
        boolean rootHasNewId = newModel.getStatements(newRootId, null, null).iterator().hasNext();
        assertThat(rootHasNewId, is(true));
    }

    @Test
    public void should_keep_the_references_to_the_children_unchanged() throws Exception {
        // Given some model with a root element that references some child.
        var model = Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ONE_HI_JSON_LD), RDFFormat.JSONLD);

        // When it comes to setting the root IRI...
        var newRootId = Values.iri("https://some/new/root/id");
        var newModel = repository.addIdToRootStatements(model, newRootId);

        // ...then the child references must be kept unchanged:
        var allOriginalIds = model.subjects().stream().map(Object::toString).collect(Collectors.toSet());
        var childIds = newModel.stream().filter(Predicate.not(statement -> statement.getSubject().equals(newRootId)))
            .map(statement -> statement.getSubject().toString()).collect(Collectors.toSet());
        assertThat(allOriginalIds.containsAll(childIds), is(true));

        var childReferences = new ArrayList<String>();
        newModel.getStatements(newRootId, null, null).iterator()
            .forEachRemaining(statement -> childReferences.add(statement.getObject().toString()));
        assertThat(childReferences.containsAll(childIds), is(true));
    }

    @Test
    public void should_persist_a_model_using_provided_iri() {
        // given

        String id = UUID.randomUUID().toString();
        IRI subject = Values.iri("http://test/" + id);
        IRI predicate = RDF.TYPE;
        IRI object = CARGO.LogisticsObject;

        Model model = new ModelBuilder()
            .subject(Values.bnode())
            .add(predicate, object)
            .build();

        LogisticsObject entity = new LogisticsObject(subject, model);

        // when
        transaction.transactionallyDo(connection -> repository.persist(subject, entity, connection));

        // then
        try (RepositoryConnection conn = repository.getRepository().getConnection()) {
            RepositoryResult<Statement> result = conn.getStatements(subject, null, null);
            List<Statement> statements = result.stream().toList();

            assertThat(statements.size(), is(equalTo(1)));

            Statement statement = statements.get(0);
            assertThat(statement.getSubject(), is(equalTo(subject)));
            assertThat(statement.getPredicate(), is(equalTo(predicate)));
            assertThat(statement.getObject(), is(equalTo(object)));
        }
    }

    @Test
    public void should_throw_IllegalStateException() {
        // given
        Model model = new TreeModel();

        // when
        Exception thrown = assertThrows(RuntimeException.class,
            () -> transaction.transactionallyGet(connection -> {
                connection.close();
                repository.persist(new LogisticsObject(Values.iri("https://some/iri"), model), connection);
                return null;
            }));

        // then
        assertThat(thrown, instanceOf(IllegalStateException.class));
    }

    @Test
    public void should_find_a_persisted_LO() {
        // given
        IRI subject = Values.iri("http://test/s1");
        IRI predicate = RDF.TYPE;
        IRI object = Values.iri("http://test/t#type");

        Model model = new ModelBuilder()
            .subject(subject)
            .add(predicate, object)
            .build();

        LogisticsObject entity = new LogisticsObject(subject, model);

        transaction.transactionallyDo(connection -> repository.persist(entity, connection));

        // when
        var result = transaction.transactionallyGet(connection -> repository.findByIri(subject, connection)).get();

        // then
        assertThat(result.model().subjects().size(), is(equalTo(1)));
        assertThat(result.model().subjects().toArray()[0], is(equalTo(subject)));
    }

    @Test
    public void should_return_null_if_LO_is_not_in_the_repo() {
        // given
        IRI subject = Values.iri("some:iri-that-does-not-exist");

        // when
        var result = transaction.transactionallyGet(connection -> repository.findByIri(subject, connection));

        // then
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void should_traverse_a_subjects_objects_downwards() {
        // given
        IRI typePredicate = RDF.TYPE;

        IRI rootSubject = Values.iri("http://test/rs1");
        IRI rootType = Values.iri("http://test/t#type");
        IRI containsChild = Values.iri("http://test/t#contains");

        IRI child = Values.iri("neone:child");
        IRI childType = Values.iri("http://test/c#type");
        IRI childProp = Values.iri("http://test/c#prop");
        Literal childPropValue = Values.literal("cprop");
        IRI childContainsChild = Values.iri("http://test/c#contains");

        IRI grandChild = Values.iri("neone:grandchild");
        IRI grandChildType = Values.iri("http://test/gc#type");
        IRI grandChildProp = Values.iri("http://test/gc#prop");
        Literal grandChildPropValue = Values.literal("gcprop");

        Model model = new ModelBuilder()
            // root
            .subject(rootSubject)
            .add(RDF.TYPE, rootType)
            .add(containsChild, child)
            // 1st level
            .subject(child)
            .add(typePredicate, childType)
            .add(childProp, childPropValue)
            .add(childContainsChild, grandChild)
            // 2nd level
            .subject(grandChild)
            .add(RDF.TYPE, grandChildType)
            .add(grandChildProp, grandChildPropValue)
            .build();

        LogisticsObject entity = new LogisticsObject(rootSubject, model);

        transaction.transactionallyDo(connection -> repository.persist(entity, connection));

        // when
        var result = transaction.transactionallyGet(connection -> repository.findGraphByIri(rootSubject, connection)).get();

        // then
        // the result should contain all 3 subjects
        assertThat(result.model().subjects().size(), is(3));
        assertThat(result.model().contains(null, RDF.TYPE, rootType), is(true));
        assertThat(result.model().contains(null, RDF.TYPE, childType), is(true));
        assertThat(result.model().contains(null, RDF.TYPE, grandChildType), is(true));

        // also the nested props should be retrieved
        assertThat(result.model().contains(null, childProp, childPropValue), is(true));
        assertThat(result.model().contains(null, grandChildProp, grandChildPropValue), is(true));
    }

    @Test
    public void should_add_given_predicate_to_LO() {
        // given
        IRI subject = Values.iri("http://test/s1");
        IRI predicate = RDF.TYPE;
        IRI object = Values.iri("http://test/t#type");

        IRI predicateToAdd = Values.iri("http://test/t#a");
        String value = "value";

        Model model = new ModelBuilder()
            .subject(subject)
            .add(predicate, object)
            .build();

        LogisticsObject entity = new LogisticsObject(subject, model);

        transaction.transactionallyDo(connection -> repository.persist(entity, connection));

        // when
        transaction.transactionallyGet(connection -> {
            repository.add(subject, predicateToAdd, value, connection);
            return null;
        });

        // then
        LogisticsObject result = transaction.transactionallyGet(connection -> repository.findByIri(subject, connection)).get();
        String addedValue = Models.objectString(result.model().filter(subject, predicateToAdd, null)).get();

        assertThat(addedValue, is(equalTo(value)));
    }

    @Test
    public void should_delete_predicate() {
        // given
        IRI subject = Values.iri("http://test/s1");
        IRI predicate = RDF.TYPE;
        IRI object = Values.iri("http://test/t#type");

        IRI predicateToDelete = Values.iri("http://test/t#a");
        String value = "value";

        Model model = new ModelBuilder()
            .subject(subject)
            .add(predicate, object)
            .add(predicateToDelete, Values.literal(value))
            .build();

        LogisticsObject entity = new LogisticsObject(subject, model);

        transaction.transactionallyDo(connection -> repository.persist(entity, connection));

        // when
        transaction.transactionallyGet(connection -> {
            repository.delete(subject, predicateToDelete, connection);
            return null;
        });

        // then
        LogisticsObject result = transaction.transactionallyGet(connection -> repository.findByIri(subject, connection)).get();
        Optional<String> addedValue = Models.objectString(result.model().filter(subject, predicateToDelete, null));
        assertThat(addedValue.isEmpty(), is(true));
    }

    @Test
    public void should_return_true_if_queried_subject_is_present() {
        // given
        IRI subject = Values.iri("http://test/s1");
        IRI predicate = RDF.TYPE;
        IRI object = Values.iri("http://test/t#type");

        Model model = new ModelBuilder()
            .subject(subject)
            .add(predicate, object)
            .build();

        LogisticsObject entity = new LogisticsObject(subject, model);

        transaction.transactionallyDo(connection -> repository.persist(entity, connection));

        // when
        boolean exists = transaction.transactionallyGet(connection -> repository.exists(subject, connection));

        // then
        assertThat(exists, is(true));
    }

    @Test
    public void should_return_false_if_queried_subject_is_not_present() {
        // given
        IRI subject = Values.iri("http://some-object-that-does-not-exist");

        // when
        boolean exists = transaction.transactionallyGet(connection -> repository.exists(subject, connection));

        // then
        assertThat(exists, is(false));
    }
}
