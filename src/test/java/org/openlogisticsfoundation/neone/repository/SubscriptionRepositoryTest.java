// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.SubscriptionRequest;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

@QuarkusTest
class SubscriptionRepositoryTest {

    @Inject
    SubscriptionRepository subscriptionRepository;

    @Inject
    SubscriptionRequestRepository subscriptionRequestRepository;

    @Inject
    Repository repository;

    @BeforeEach
    public void setup() {
        try (RepositoryConnection connection = repository.getConnection()) {
            connection.clear();
        }
    }

    @Test
    @DisplayName("Should return only accepted subscriptions when also pending subscriptions are present")
    public void should_return_only_accepted_subscriptions() {
        Subscription pendingSubscription = new Subscription(
            Values.iri("neon:some_subscription_iri"),
            Values.iri("neone:some_subscriber_iri"),
            Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER,
            "http://some_topic",
            Optional.of("some description"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty()
        );
        SubscriptionRequest pendingSubscriptionRequest = new SubscriptionRequest(
            Values.iri("neone:some_subscription_iri"),
            RequestStatus.REQUEST_PENDING,
            Instant.now(),
            Values.iri("neone:some_requested_by_iri"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            pendingSubscription
        );

        Subscription acceptedSubscription = new Subscription(
            Values.iri("neon:some_subscription_iri2"),
            Values.iri("neone:some_subscriber_iri2"),
            Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER,
            "http://some_topic",
            Optional.of("some description"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty()
        );
        SubscriptionRequest acceptedSubscriptionRequest = new SubscriptionRequest(
            Values.iri("neone:some_subscription_iri2"),
            RequestStatus.REQUEST_ACCEPTED,
            Instant.now(),
            Values.iri("neone:some_requested_by_iri2"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            pendingSubscription
        );

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.begin();
            subscriptionRepository.persist(pendingSubscription, connection);
            subscriptionRequestRepository.persist(pendingSubscriptionRequest, connection);
            subscriptionRepository.persist(acceptedSubscription, connection);
            subscriptionRequestRepository.persist(acceptedSubscriptionRequest, connection);
            connection.commit();
        }

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.begin();
            MatcherAssert.assertThat(subscriptionRepository.getAcceptedSubscriptions(
                null,
                Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER,
                Set.of("http://some_topic"), connection).size(), Matchers.is(1));
            connection.commit();
        }
    }

    @Test
    @DisplayName("Should not return not accepted subscriptions")
    public void should_not_return_not_accepted_subscriptions() {
        Subscription pendingSubscription = new Subscription(
            Values.iri("neon:some_subscription_iri"),
            Values.iri("neone:some_subscriber_iri"),
            Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER,
            "http://some_topic",
            Optional.of("some description"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty()
        );
        SubscriptionRequest pendingSubscriptionRequest = new SubscriptionRequest(
            Values.iri("neone:some_subscription_iri"),
            RequestStatus.REQUEST_PENDING,
            Instant.now(),
            Values.iri("neone:some_requested_by_iri"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            pendingSubscription
        );

        Subscription rejectedSubscription = new Subscription(
            Values.iri("neon:some_subscription_iri2"),
            Values.iri("neone:some_subscriber_iri2"),
            Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER,
            "http://some_topic",
            Optional.of("some description"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty()
        );
        SubscriptionRequest rejectedSubscriptionRequest = new SubscriptionRequest(
            Values.iri("neone:some_subscription_iri2"),
            RequestStatus.REQUEST_REJECTED,
            Instant.now(),
            Values.iri("neone:some_requested_by_iri2"),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            pendingSubscription
        );

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.begin();
            subscriptionRepository.persist(pendingSubscription, connection);
            subscriptionRequestRepository.persist(pendingSubscriptionRequest, connection);
            subscriptionRepository.persist(rejectedSubscription, connection);
            subscriptionRequestRepository.persist(rejectedSubscriptionRequest, connection);
            connection.commit();
        }

        try (RepositoryConnection connection = repository.getConnection()) {
            connection.begin();
            MatcherAssert.assertThat(subscriptionRepository.getAcceptedSubscriptions(
                null,
                Subscription.TopicType.LOGISTICS_OBJECT_IDENTIFIER,
                Set.of("http://some_topic"), connection).size(), Matchers.is(0));
            connection.commit();
        }
    }




/*
    @Test
    public void should_return_subscribers_according_to_list_of_partners_and_specified_topic() throws IOException {
        // given
        Model subscriptionWithCorrectTopicAndCompanyId_1 = TestFixtures.loadResourceAsModel("minimal-subscription1.json", RDFFormat.JSONLD);
        Model subscriptionWithCorrectTopicButDifferentCompanyId = TestFixtures.loadResourceAsModel("minimal-subscription2.json", RDFFormat.JSONLD);
        Model subscriptionWithCorrectTopicAndCompanyId_2 = TestFixtures.loadResourceAsModel("minimal-subscription3.json", RDFFormat.JSONLD);
        Model subscriptionWithDifferentTopicButCorrectCompanyId = TestFixtures.loadResourceAsModel("minimal-subscription4.json", RDFFormat.JSONLD);
        Model subscriptionWithDifferentTopicAndCompanyId = TestFixtures.loadResourceAsModel("minimal-subscription5.json", RDFFormat.JSONLD);

        transaction.transactionallyDo(connection -> {
            subscriptionRepository.persist(
                handler.fromModel(Values.iri("neone:subscription_" + UUID.randomUUID()), subscriptionWithCorrectTopicAndCompanyId_1),
                connection);
            subscriptionRepository.persist(
                handler.fromModel(Values.iri("neone:subscription_" + UUID.randomUUID()), subscriptionWithCorrectTopicButDifferentCompanyId),
                connection);
            subscriptionRepository.persist(
                handler.fromModel(Values.iri("neone:subscription_" + UUID.randomUUID()), subscriptionWithCorrectTopicAndCompanyId_2),
                connection);
            subscriptionRepository.persist(
                handler.fromModel(Values.iri("neone:subscription_" + UUID.randomUUID()), subscriptionWithDifferentTopicButCorrectCompanyId),
                connection);
            subscriptionRepository.persist(
                handler.fromModel(Values.iri("neone:subscription_" + UUID.randomUUID()), subscriptionWithDifferentTopicAndCompanyId),
                connection);
        });

        List<String> partnersToBeNotified = Arrays.asList("http://subscriber-server1", "http://subscriber-server3");
        List<String> callbackUrlsTobeFetched = Arrays.asList("http://subscriber-server1/callback", "http://subscriber-server3/callback");

        // when
        List<String> fetchedCallbackUrls = transaction.transactionallyGet(connection ->
            subscriptionRepository.getSubscriptionCallbackUrls(partnersToBeNotified, Subscription.TopicType.LOGISTICS_OBJECT_TYPE, CARGO.waybill, connection));

        // then
        MatcherAssert.assertThat(fetchedCallbackUrls.size(), Matchers.not(0));
        MatcherAssert.assertThat(callbackUrlsTobeFetched.containsAll(fetchedCallbackUrls), CoreMatchers.is(true));
    }

 */
}
