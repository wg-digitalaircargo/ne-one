// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.util.Values;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.model.Notification;

import jakarta.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
class NotificationRepositoryTest {

    @Inject
    RepositoryTransaction transaction;

    @Inject
    NotificationRepository notificationRepository;

    @Inject
    NeoneEventRepository neoneEventRepository;

    @Test
    public void should_get_pending_notification() {
        // given

        NeoneEvent event = new NeoneEvent(
            Values.iri("http://test"),
            Values.iri("http://loid"),
            API.LOGISTICS_OBJECT_CREATED,
            NeoneEvent.State.PENDING,
            Optional.empty(),
            Collections.emptySet(),
            Set.of(CARGO.Piece));
        var notificationIri = Values.iri("some:iri");
        Notification notification = new Notification(notificationIri, Values.iri("some:iri2"),
            Optional.of("topic"), Optional.empty(), Optional.empty(), Collections.emptySet());
        transaction.transactionallyDo(connection -> {
            notificationRepository.persist(notification, connection);
            neoneEventRepository.persist(event, connection);
            neoneEventRepository.setEventState(event.iri(), NeoneEvent.State.PENDING, connection);
            neoneEventRepository.addNotification(event.iri(), notification.iri(), connection);
        });

        // when
        List<Notification> returnedNotificationData = transaction.transactionallyGet(
            notificationRepository::getPendingNotifications);

        // then
        assertThat(returnedNotificationData.size(), is(1));
        assertThat(returnedNotificationData.get(0).iri(), is(notificationIri));
    }
}
