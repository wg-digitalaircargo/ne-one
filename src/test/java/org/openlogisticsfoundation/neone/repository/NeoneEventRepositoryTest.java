// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.NeoneEvent;

import jakarta.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.openlogisticsfoundation.neone.NeoneId.fromString;

@QuarkusTest
class NeoneEventRepositoryTest {

    @Inject
    NeoneEventRepository neoneEventRepository;

    @Inject
    RepositoryTransaction transaction;

    @InjectMock
    IdProvider idProvider;

    @BeforeEach
    public void setup() {
        when(idProvider.parse(any(IRI.class))).thenReturn(fromString("some:id", false, true));
    }

    @Test
    public void should_return_pending_events() {
        // given
        NeoneEvent event = new NeoneEvent(
            Values.iri("http://test"),
            Values.iri("http://lo"),
            API.LOGISTICS_OBJECT_CREATED,
            NeoneEvent.State.NEW,
            Optional.empty(),
            Set.of(),
            Set.of(CARGO.Piece));

        transaction.transactionallyDo(connection -> {
            neoneEventRepository.persist(event, connection);
            neoneEventRepository.setEventState(event.iri(), NeoneEvent.State.PENDING, connection);
        });

        // when
        List<IRI> returnedPendingEvents = transaction.transactionallyGet(neoneEventRepository::getPendingEventsWithoutNotifications);

        // then
        assertThat(returnedPendingEvents.isEmpty(), is(false));
        returnedPendingEvents.forEach(eventIri -> assertThat(eventIri, is(event.iri())));
    }

    @Test
    public void should_return_new_events() {
        // given
        NeoneEvent event = new NeoneEvent(
            Values.iri("http://event-iri"),
            Values.iri("http://lo-iri"),
            API.LOGISTICS_OBJECT_CREATED,
            NeoneEvent.State.NEW,
            Optional.empty(),
            Set.of(),
            Set.of(CARGO.Piece));

        transaction.transactionallyDo(connection -> neoneEventRepository.persist(event, connection));

        // when
        List<NeoneEvent> returnedPendingEvents = transaction.transactionallyGet(neoneEventRepository::getNewEvents);

        // then
        assertThat(returnedPendingEvents.isEmpty(), is(false));
        returnedPendingEvents.forEach(returnedEvent -> assertThat(returnedEvent.iri(), is(event.iri())));
    }

}
