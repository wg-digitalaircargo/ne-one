// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.micrometer.core.instrument.Counter;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.model.handler.SnapshotHandler;

import java.time.Instant;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.openlogisticsfoundation.neone.NeoneId.fromString;

public class SnapshotRepositoryTest {

    private final static IRI loIri = Values.iri("http://some/lo/iri");

    private final static Instant firstTs = Instant.now();

    private final static Instant secondTs = firstTs.plusSeconds(60);

    private final static String firstPayload = "payload1";

    private final static String secondPayload = "payload2";

    private final static Integer revision = 1;

    private static SnapshotRepository repository;

    private static RepositoryTransaction transaction;

    @BeforeAll
    public static void setupTestObjects() {
        IdProvider idProvider = mock(IdProvider.class);
        when(idProvider.parse(any(IRI.class))).thenReturn(fromString("some:id", false, true));

        repository = new SnapshotRepository(
            new SailRepository(new MemoryStore()), new SnapshotHandler(), idProvider
        );

        ManagedExecutor executor = mock(ManagedExecutor.class);
        doAnswer(invocationOnMock -> {
            invocationOnMock.getArgument(0, Runnable.class).run();
            return null;
        }).when(executor).execute(any());
        RepositoryTransactionMetrics repositoryTransactionMetrics = mock(RepositoryTransactionMetrics.class);
        when(repositoryTransactionMetrics.txBegin()).thenReturn(mock(Counter.class));
        when(repositoryTransactionMetrics.txCommit()).thenReturn(mock(Counter.class));
        when(repositoryTransactionMetrics.txRollback()).thenReturn(mock(Counter.class));
        transaction = new RepositoryTransaction(repository.getRepository(),repositoryTransactionMetrics, executor);

        // given: 2 Snapshots of the same LO at two timestamps.
        var sn1 = new Snapshot(Values.iri("http://some/sn/iri/1"), loIri, firstPayload, firstTs, revision);
        var sn2 = new Snapshot(Values.iri("http://some/sn/iri/2"), loIri, secondPayload, secondTs, revision);
        transaction.transactionallyDo(connection -> {
            repository.persist(sn1, connection);
            repository.persist(sn2, connection);
        });
    }

    @Test
    public void should_find_snapshot_with_perfect_match_of_timestamp() {
        // when timestamp perfectly matches the creation time of a snapshot...
        var sn = transaction.transactionallyGet(connection ->
            repository.getSnapshot(loIri, firstTs, connection));

        // ... then this snapshot should be returned.
        assertThat(sn.isPresent(), is(true));
        assertThat(sn.get().payload(), is(firstPayload));
    }

    @Test
    public void should_find_latest_snapshot_according_to_timestamp() {
        // when the timestamp is greater than the actual creation time of a snapshot, but smaller
        // than the next snapshot...
        var sn = transaction.transactionallyGet(connection ->
            repository.getSnapshot(loIri, firstTs.plusSeconds(30), connection));

        // ... then the first snapshot should be returned.
        assertThat(sn.isPresent(), is(true));
        assertThat(sn.get().payload(), is(firstPayload));
    }

    @Test
    public void should_find_newest_snapshot() {
        // when the timestamp is greater than the creation time of the newest snapshot...
        var sn = transaction.transactionallyGet(connection ->
            repository.getSnapshot(loIri, secondTs.plusSeconds(30), connection));

        // ... then this newest snapshot is to be returned.
        assertThat(sn.isPresent(), is(true));
        assertThat(sn.get().payload(), is(secondPayload));
    }

    @Test
    public void should_not_return_any_snapshot_if_timestamps_too_early() {
        // when the timestamp is smaller than the creation time of the oldest snapshot...
        var sn = transaction.transactionallyGet(connection ->
            repository.getSnapshot(loIri, firstTs.minusSeconds(30), connection));

        // ... then nothing should be returned.
        assertThat(sn.isEmpty(), is(true));
    }
}
