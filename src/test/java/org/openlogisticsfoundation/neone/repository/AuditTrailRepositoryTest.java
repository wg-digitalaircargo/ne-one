// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.micrometer.core.instrument.Counter;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.model.handler.AuditTrailHandler;
import org.openlogisticsfoundation.neone.model.handler.ChangeHandler;
import org.openlogisticsfoundation.neone.model.handler.ChangeRequestHandler;
import org.openlogisticsfoundation.neone.model.handler.ErrorDetailHandler;
import org.openlogisticsfoundation.neone.model.handler.ErrorHandler;
import org.openlogisticsfoundation.neone.model.handler.OperationHandler;
import org.openlogisticsfoundation.neone.model.handler.OperationObjectHandler;

import java.time.Instant;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuditTrailRepositoryTest {

    final Instant first = Instant.now();

    final Instant second = first.plusSeconds(60);

    final IRI auditTrailIri = Values.iri("http://some/audittrail/id");

    final int latestRevision = 2;

    final AuditTrailRepository repository = new AuditTrailRepository(
        new SailRepository(new MemoryStore()),
        new AuditTrailHandler(
            new ChangeRequestHandler(new ChangeHandler(new OperationHandler(new OperationObjectHandler())),
            new ErrorHandler(new ErrorDetailHandler()))
        ), null);

    RepositoryTransaction transaction;

    @BeforeEach
    public void setup() {
        ManagedExecutor executor = mock(ManagedExecutor.class);
        doAnswer(invocationOnMock -> {
            invocationOnMock.getArgument(0, Runnable.class).run();
            return null;
        }).when(executor).execute(any());
        RepositoryTransactionMetrics repositoryTransactionMetrics = mock(RepositoryTransactionMetrics.class);
        when(repositoryTransactionMetrics.txBegin()).thenReturn(mock(Counter.class));
        when(repositoryTransactionMetrics.txCommit()).thenReturn(mock(Counter.class));
        when(repositoryTransactionMetrics.txRollback()).thenReturn(mock(Counter.class));
        transaction = new RepositoryTransaction(repository.getRepository(), repositoryTransactionMetrics, executor);
    }

    @AfterEach
    public void tearDown() {
        repository.getRepository().shutDown();
    }

    @Test
    public void should_find_audit_trail_by_iri_alone() {
        // given
        final AuditTrail auditTrail = TestFixtures.getAuditTrailTestModel(auditTrailIri, latestRevision, first, second);
        transaction.transactionallyDo(connection -> repository.persist(auditTrail, connection));

        // when
        var at = transaction.transactionallyGet(connection -> repository.getAuditTrail(auditTrailIri, null, null, connection));

        // then
        assertThat(at.isPresent(), is(true));
    }

    @Test
    public void should_return_audit_trail_with_all_changeResponses_when_time_range_matches() {
        // given
        final AuditTrail auditTrail = TestFixtures.getAuditTrailTestModel(auditTrailIri, latestRevision, first, second);
        transaction.transactionallyDo(connection -> repository.persist(auditTrail, connection));

        // when
        var at = transaction.transactionallyGet(connection ->
            repository.getAuditTrail(auditTrailIri, first.minusSeconds(10), second.plusSeconds(1), connection));

        // then
        assertThat(at.isPresent(), is(true));
        assertThat(at.get().changeRequests().size(), is(2));
    }

    @Test
    public void should_miss_if_time_range_does_not_match() {
        // given
        final AuditTrail auditTrail = TestFixtures.getAuditTrailTestModel(auditTrailIri, latestRevision, first, second);
        transaction.transactionallyDo(connection -> repository.persist(auditTrail, connection));

        // when lower bound too late then miss
        var at1 = transaction.transactionallyGet(connection ->
            repository.getAuditTrail(auditTrailIri, second.plusSeconds(10), null, connection));
        assertThat(at1.get().changeRequests().size(), is(0));

        // when upper bound too early then miss
        var at2 = transaction.transactionallyGet(connection ->
            repository.getAuditTrail(auditTrailIri, null, first.minusSeconds(10), connection));
        assertThat(at2.get().changeRequests().size(), is(0));
    }

    @Test
    public void should_return_not_accepted_change_requests() {
        // given
        final AuditTrail auditTrail = TestFixtures.getAuditTrailWithNotAcceptedChangeRequestsModel(auditTrailIri, first, second);
        transaction.transactionallyDo(connection -> repository.persist(auditTrail, connection));

        var at2 = transaction.transactionallyGet(connection ->
            repository.getAuditTrail(auditTrailIri, null, null, connection));
        assertThat(at2.get().changeRequests().size(), is(2));
    }

    @Test
    public void should_ony_return_changeResponses_for_timestamp_in_range() {
        // given
        final AuditTrail auditTrail = TestFixtures.getAuditTrailTestModel(auditTrailIri, latestRevision, first, second);
        transaction.transactionallyDo(connection -> repository.persist(auditTrail, connection));

        // when
        var at = transaction.transactionallyGet(connection ->
            repository.getAuditTrail(auditTrailIri, first.plusSeconds(10), second.plusSeconds(10), connection));

        // then
        assertThat(at.isPresent(), is(true));
        assertThat(at.get().changeRequests().size(), is(1));
    }
}
