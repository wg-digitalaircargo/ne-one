// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.micrometer.core.instrument.Counter;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.handler.LogisticsObjectHandler;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.openlogisticsfoundation.neone.vocab.ACL.Read;

class LogisticsObjectRepositoryTest {

    LogisticsObjectRepository repository;

    RepositoryTransaction transaction;

    IdProvider idProvider;

    AclAuthorizationRepository authorizationRepository;

    @BeforeEach
    public void setup() {
        idProvider = mock(IdProvider.class);
        authorizationRepository = mock(AclAuthorizationRepository.class);
        repository =  new LogisticsObjectRepository(
            new SailRepository(new MemoryStore()), new LogisticsObjectHandler(), idProvider, authorizationRepository
        );
        ManagedExecutor executor = mock(ManagedExecutor.class);
        doAnswer(invocationOnMock -> {
            invocationOnMock.getArgument(0, Runnable.class).run();
            return null;
        }).when(executor).execute(any());
        RepositoryTransactionMetrics repositoryTransactionMetrics = mock(RepositoryTransactionMetrics.class);
        when(repositoryTransactionMetrics.txBegin()).thenReturn(mock(Counter.class));
        when(repositoryTransactionMetrics.txCommit()).thenReturn(mock(Counter.class));
        when(repositoryTransactionMetrics.txRollback()).thenReturn(mock(Counter.class));
        transaction = new RepositoryTransaction(repository.getRepository(), repositoryTransactionMetrics, executor);
    }

    @Test
    public void should_return_type_of_logistics_object() {
        // given
        when(authorizationRepository.aclExists(any(), any(), any(), any())).thenReturn(true);

        IRI subject = Values.iri("http://test/s1");
        IRI predicate = RDF.TYPE;
        IRI object = Values.iri("http://test/t#type");

        Model model = new ModelBuilder()
            .subject(subject)
            .add(predicate, object)
            .build();
        LogisticsObject entity = new LogisticsObject(subject, model);
        transaction.transactionallyDo(connection -> repository.persist(entity, connection));

        // when
        Set<IRI> returnedTypes = transaction.transactionallyGet(connection ->  repository.getLogisticsObjectTypes(subject, connection));

        // then
        assertThat(returnedTypes.contains(object), is(Boolean.TRUE));
    }

    @Test
    public void should_only_return_embedded_los_if_acl_exists() throws Exception {
        // given
        IRI subject = Values.iri("test:s1");
        IRI embeddedSubject = Values.iri("test:s2");
        IRI agent = Values.iri("test:agent");
        Model model = new DynamicModelFactory().createEmptyModel();
        model.add(subject, Values.iri("test:p1"), embeddedSubject);
        model.add(embeddedSubject, Values.iri("test:p2"), Values.literal("literal"));

        LogisticsObject lo = new LogisticsObject(subject, model);

        transaction.transactionallyDo(connection -> repository.persist(lo, connection));

        when(idProvider.parse(eq(embeddedSubject))).thenReturn(NeoneId.fromString(embeddedSubject.stringValue(), false, true));
        when(idProvider.parse(eq(CARGO.LogisticsObject))).thenReturn(NeoneId.fromString(CARGO.LogisticsObject.stringValue(), false, false));
        when(authorizationRepository.aclExists(eq(agent), eq(embeddedSubject), eq(Read), any())).thenReturn(false);

        // when
        LogisticsObject result = transaction.transactionallyGet(c -> repository.findGraphByIri(subject, agent, c)).get();

        // then
        assertThat(result.model().contains(embeddedSubject, null, null), is(false));
    }
}
