// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.LogisticsEvent;
import org.openlogisticsfoundation.neone.vocab.ACL;

import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
class LogisticsEventRepositoryTest {

    @Inject
    LogisticsEventRepository logisticsEventRepository;

    @Inject
    RepositoryTransaction transaction;

    @Inject
    AclAuthorizationRepository aclAuthorizationRepository;

    @Inject
    Repository repository;

    @BeforeEach
    public void setup() {
        try (RepositoryConnection connection = repository.getConnection()) {
            connection.clear();
        }
    }

    static LogisticsEvent createLoEventTestFixture(IRI eventIri, IRI loIri) {
        return new LogisticsEvent(eventIri, Optional.of(Instant.now()), Optional.empty(),
            Optional.empty(), Optional.empty(), Optional.empty(),
            Collections.emptySet(), Optional.of(loIri), Optional.empty(),
            Optional.empty(), Optional.empty(),
            Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void should_find_persisted_events_for_lo() {
        // given
        IRI eventIri = Values.iri("http://some/event/uri");
        IRI loIri = Values.iri("http://linkedlo");

        var event = createLoEventTestFixture(eventIri, loIri);

        transaction.transactionallyDo(connection -> {
            logisticsEventRepository.persist(eventIri, event, connection);

            aclAuthorizationRepository.add(Values.iri("neone:acl"), RDF.TYPE, ACL.Authorization, connection);
            aclAuthorizationRepository.add(Values.iri("neone:acl"), ACL.agent, Values.iri("http://localhost:8081/logistics-objects/_data-holder"), connection);
            aclAuthorizationRepository.add(Values.iri("neone:acl"), ACL.accessTo, eventIri, connection);
        });

        // when
        List<LogisticsEvent> logisticsEvents = transaction.transactionallyGet(connection ->
            logisticsEventRepository.findEventsOfLogisticsObject(loIri, Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), 1,
                connection)
        );

        // then
        assertThat(logisticsEvents.size(), is(equalTo(1)));
        assertThat(logisticsEvents.get(0), is(equalTo(event)));
    }
}
