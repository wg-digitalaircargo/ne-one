// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.repository;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.iata.onerecord.api.API;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.ChangeRequest;
import org.openlogisticsfoundation.neone.model.Error;
import org.openlogisticsfoundation.neone.model.ErrorDetail;
import org.openlogisticsfoundation.neone.model.Operation;
import org.openlogisticsfoundation.neone.model.OperationObject;
import org.openlogisticsfoundation.neone.model.PatchOperation;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.handler.ChangeRequestHandler;
import org.openlogisticsfoundation.neone.model.handler.ErrorHandler;

import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;

@QuarkusTest
public class ChangeRequestRepositoryTest {

    @Inject
    ChangeRequestRepository repository;

    @Inject
    ChangeRequestHandler changeRequestHandler;

    @Inject
    ErrorRepository errorRepository;

    @Inject
    RepositoryTransaction transaction;

    @Inject
    ErrorHandler errorHandler;

    @Test
    void name() {
        OperationObject oo = new OperationObject(Values.iri("neone:oo1"), CoreDatatype.XSD.STRING.toString(), "value1");
        Operation o = new Operation(Values.iri("neone:o1"), oo, PatchOperation.ADD, API.hasTopic, "http://localhost:8080/logistics-objects/123");
        Change change = new Change(Values.iri("neone:c1"), Optional.of("Change description"), Collections.singleton(o),
            0, Values.iri("http://localhost:8080/logistics-objects/123"), Optional.empty());

        ErrorDetail ed = new ErrorDetail(Values.iri("neone:ed1"), "CODE", Optional.of("message"), Optional.of(Values.iri("http://cg#123")), Optional.of(Values.iri("http://cg#321")));
        Error e = new Error(Values.iri("neone:e1"), "title", Collections.singleton(ed));

        ChangeRequest changeRequest = new ChangeRequest(
            Values.iri("neone:cr1"),
            Optional.of(e),
            RequestStatus.REQUEST_PENDING,
            Instant.now(),
            Values.iri("http://someserver/owner"),
            Optional.of(Instant.now()),
            Optional.of(Values.iri("http://someserver/owner")),
            change);

        transaction.transactionallyDo(connection -> repository.persist(changeRequest, connection));

        transaction.transactionallyDo(connection -> {
            // Model model = QueryResults.asModel(connection.getStatements(null, null, null));
            Model model = changeRequestHandler.fromJava(repository.findByIri(Values.iri("neone:cr1"), connection).orElseThrow());
            Rio.write(model, System.out, RDFFormat.TURTLE);
            System.out.println();
            System.out.println("-------------");
            Model errorModel = errorHandler.fromJava(errorRepository.findByIri(Values.iri("neone:e1"), connection).orElseThrow());
            Rio.write(errorModel, System.out, RDFFormat.TURTLE);
        });
    }
}
