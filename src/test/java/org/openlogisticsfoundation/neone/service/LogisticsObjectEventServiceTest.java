// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.service.internal.SubscriptionManagementService;

import jakarta.inject.Inject;

@QuarkusTest
class LogisticsObjectEventServiceTest {

    @Inject
    NeoneEventService neoneEventService;

    @InjectMock
    NotificationService notificationService;

    @InjectMock
    SubscriptionManagementService subscriptionManagementService;

    @InjectMock
    LogisticsObjectRepository logisticsObjectRepository;
/*
    @Test
    public void should_notify_subscribed_partners_on_logistics_object_changed() {
        // given
        LogisticsObjectEvent event = new LogisticsObjectEvent(
            Values.iri("http://test"),
            LogisticsObjectEvent.Type.CREATED);
        String loType = "http://type";
        List<String> companyIds = List.of("company1", "company2");
        List<String> callbackUrls = List.of("http://callback1", "http://callback2");

        when(notificationService.getCompanyIdsToNotify(event.iri())).thenReturn(companyIds);
        when(subscriptionManagementService.getSubscriptionCallbackUrls(companyIds, loType)).thenReturn(callbackUrls);
        when(logisticsObjectRepository.getLogisticsObjectType(any(), any())).thenReturn(loType);
        TestFixtures.mockDoInTransactionCall(logisticsObjectRepository, mock(RepositoryConnection.class));

        // when
        logisticsObjectEventService.onLogisticsObjectChanged(event);

        // then
        verify(notificationService, times(1)).getCompanyIdsToNotify(event.iri());
        verify(subscriptionManagementService, times(1)).getSubscriptionCallbackUrls(companyIds, loType);
        verify(notificationService, times(1)).enqueue(event, callbackUrls);
    }

 */
}
