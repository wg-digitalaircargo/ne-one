// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.client.ProposalResponse;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.client.SubscriptionClient;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.Subscription;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@QuarkusTest
public class SubscriptionServiceTest {

    @InjectMock
    IdProvider idProvider;

    @InjectMock
    RestClientBuilderProducer producer;

    @Inject
    SubscriptionService subService;

    @Test
    public void should_return_proper_proposal_response() {
        SubscriptionClient client = mock(SubscriptionClient.class);
        when(client.handleSubscription(anyString(), anyString())).thenReturn(new ProposalResponse(
            true, true, null, Instant.now()));
        RestClientBuilder builder = mock(RestClientBuilder.class, Mockito.RETURNS_DEEP_STUBS);
        when(builder
            .baseUri(any())
            .connectTimeout(anyLong(), any(TimeUnit.class))
            .readTimeout(anyLong(), any(TimeUnit.class))
            .build(any()))
            .thenReturn(client);
        when(producer.restClientBuilder()).thenReturn(builder);
        when(idProvider.getDataHolderId()).thenReturn(NeoneId.fromString("http://localhost:8080/logistics-objects/_me"));

        UriInfo uriInfo = mock(UriInfo.class);
        when(uriInfo.getAbsolutePath()).thenReturn(URI.create("http://some_uri"));
        when(uriInfo.getBaseUri()).thenReturn(URI.create("http://some_base_uri"));
        when(idProvider.createUniqueLoUri()).thenReturn(NeoneId.fromUri(URI.create("http://some.iri")));
        when(idProvider.createInternalIri()).thenReturn(NeoneId.fromUri(URI.create("http://some.iri")));

        var subscription = subService.handleSubscriptionProposal(Subscription.TopicType.LOGISTICS_OBJECT_TYPE, "some topic");
        assertThat(subscription, is(notNullValue()));
    }
}
