// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.repository.SnapshotRepository;

import jakarta.inject.Inject;
import java.io.IOException;
import java.io.StringReader;
import java.time.Instant;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@QuarkusTest
public class SnapshotServiceTest {

    @InjectMock
    SnapshotRepository repository;

    @Inject
    SnapshotService service;

    @Test
    public void should_call_repository_on_get_with_correct_params() {
        // given
        RepositoryConnection connection = mock(RepositoryConnection.class);

        IRI iri = Values.iri("https://some/iri");
        Instant at = Instant.now();

        // when
        service.getSnapshot(iri, at);

        // then
        verify(repository, times(1)).getSnapshot(eq(iri), eq(at), any());
    }

    @Test
    public void should_call_repository_on_create_with_correct_params() throws IOException {
        // given
        RepositoryConnection connection = mock(RepositoryConnection.class);
        var loModel = Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ID_JSONLD), RDFFormat.JSONLD);
        var loIri = Models.subjectIRI(loModel.filter(null, RDF.TYPE, null)).orElse(null);
        var lo = new LogisticsObject(loIri, loModel);

        // when
        var snapshot = service.createSnapshot(lo, 1, connection);

        // then
        verify(repository, times(1)).persist(eq(snapshot), any());
    }
}
