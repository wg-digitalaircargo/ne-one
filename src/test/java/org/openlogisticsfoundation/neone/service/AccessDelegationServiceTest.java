//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.AccessDelegation;
import org.openlogisticsfoundation.neone.model.AccessDelegationRequest;
import org.openlogisticsfoundation.neone.model.AclAuthorization;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.repository.AccessDelegationRequestRepository;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;

import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class AccessDelegationServiceTest {

    @Inject
    AccessDelegationService accessDelegationService;

    @Inject
    RepositoryTransaction transaction;

    @Inject
    AccessDelegationRequestRepository accessDelegationRequestRepository;

    @Inject
    AclAuthorizationRepository aclAuthorizationRepository;

    // Test fixture.
    final IRI accessTo = Values.iri("http://some/lo/iri");
    final IRI owner = Values.iri("http://owner/iri");
    final IRI requestedBy1 = owner;
    final IRI requestedFor1 = Values.iri("http://requestedFor1/iri");
    final IRI requestedBy2 = requestedFor1;
    final IRI requestedFor2 = Values.iri("http://requestedFor2/iri");
    final Set<AccessDelegation.Permission> modes =
        Collections.singleton(AccessDelegation.Permission.GET_LOGISTICS_OBJECT);
    final AccessDelegationRequest adr1 =
        new AccessDelegationRequest(
            Values.iri("http://some/adr1/iri"),
            Optional.empty(),
            RequestStatus.REQUEST_ACCEPTED,
            Instant.now(),
            requestedBy1,
            Optional.empty(),
            Optional.empty(),
            new AccessDelegation(Values.iri("http://some/ad1/iri"), Optional.empty(), modes,
                Set.of(requestedFor1), Set.of(accessTo), Optional.empty()));
    final AccessDelegationRequest adr2 =
        new AccessDelegationRequest(
            Values.iri("http://some/adr2/iri"),
            Optional.empty(),
            RequestStatus.REQUEST_ACCEPTED,
            Instant.now(),
            requestedBy2,
            Optional.empty(),
            Optional.empty(),
            new AccessDelegation(Values.iri("http://some/ad2/iri"), Optional.empty(), modes,
                Set.of(requestedFor2), Set.of(accessTo), Optional.empty()));
    final AclAuthorization acl1 = new AclAuthorization(Values.iri("http://acl1/iri"), accessTo, requestedFor1,
        modes.stream().map(AccessDelegation.Permission::iri).collect(Collectors.toSet()));
    final AclAuthorization acl2 = new AclAuthorization(Values.iri("http://acl2/iri"), accessTo, requestedFor2,
        modes.stream().map(AccessDelegation.Permission::iri).collect(Collectors.toSet()));

    @Test
    public void shouldRemoveAclRecursively() {
        // given
        transaction.transactionallyDo(connection -> {
            accessDelegationRequestRepository.persist(adr1, connection);
            accessDelegationRequestRepository.persist(adr2, connection);
            aclAuthorizationRepository.persist(acl1, connection);
            aclAuthorizationRepository.persist(acl2, connection);
        });

        // when
        var revokedAdr1 = new AccessDelegationRequest(adr1.iri(), adr1.error(), RequestStatus.REQUEST_REVOKED,
            adr1.requestedAt(), adr1.requestedBy(), Optional.of(Instant.now()), adr1.revokedBy(),
            adr1.accessDelegation());
        accessDelegationService.onAccessDelegationRequestChanged(revokedAdr1);

        // then
        var acl = transaction.transactionallyGet(connection ->
            aclAuthorizationRepository.findAclAuthorizations(Optional.empty(), Optional.of(accessTo), connection));
        assertThat(acl.isEmpty(), is(equalTo(true)));
    }
}
