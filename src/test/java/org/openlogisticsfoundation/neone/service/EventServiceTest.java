// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.mockito.MockitoConfig;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;

import jakarta.inject.Inject;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class EventServiceTest {

    @InjectMock
    NeoneEventRepository repository;

    @InjectMock
    @MockitoConfig(convertScopes = true)
    EventBus bus;

    @InjectMock
    RepositoryTransaction transaction;

    @Inject
    EventService eventService;

    NeoneEvent createLoEvent(IRI iri) {
        return new NeoneEvent(iri,
            Values.iri("some:iri-1-1"), API.LOGISTICS_OBJECT_CREATED, NeoneEvent.State.NEW,
            Optional.empty(), Collections.emptySet(), Set.of(CARGO.Piece));
    }

    @Test
    public void should_publish_events_for_missed_events() {

        // given
        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        NeoneEvent e1 = createLoEvent(Values.iri("some:id1"));
        NeoneEvent e2 = createLoEvent(Values.iri("some:id2"));
        List<NeoneEvent> events = List.of(e1, e2);

        when(repository.getNewEvents(connection)).thenReturn(events);

        // when
        eventService.triggerMissedEvents();

        // then
        verify(bus, times(1)).publish(NeoneEvent.ADDRESS, e1);
        verify(bus, times(1)).publish(NeoneEvent.ADDRESS, e2);
    }
}
