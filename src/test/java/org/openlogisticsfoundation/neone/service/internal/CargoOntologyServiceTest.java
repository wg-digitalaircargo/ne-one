// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.test.junit.QuarkusTest;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
class CargoOntologyServiceTest {

    @Inject
    CargoOntologyService cargoOntologyService;

    @Test
    public void should_return_true_if_queried_with_an_lo_type() {
        boolean isLo = cargoOntologyService.isLogisticsObject(CARGO.Piece.stringValue());

        assertThat(isLo, is(true));
    }

    @Test
    public void should_return_false_if_queried_with_not_a_lo_type() {
        boolean isLo = cargoOntologyService.isLogisticsObject(CARGO.Geolocation.stringValue());

        assertThat(isLo, is(false));
    }

    @Test
    public void should_return_true_if_queried_with_at_least_one_lo_type() {
        boolean isLo = cargoOntologyService.isLogisticsObject(CARGO.Piece.stringValue(), CARGO.Geolocation.stringValue());

        assertThat(isLo, is(true));
    }

    @Test
    public void should_return_false_if_queried_no_lo_type() {
        boolean isLo = cargoOntologyService.isLogisticsObject(CARGO.Geolocation.stringValue());

        assertThat(isLo, is(false));
    }

    @Test
    public void should_return_true_if_queried_with_direct_lo_type() {
        boolean isLo = cargoOntologyService.isLogisticsObject(CARGO.LogisticsObject.stringValue());

        assertThat(isLo, is(true));
    }
}
