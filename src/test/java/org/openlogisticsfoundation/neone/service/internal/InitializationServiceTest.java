// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.api.API;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.service.LogisticsObjectService;

import jakarta.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class InitializationServiceTest {

    @InjectMock
    LogisticsObjectService loService;

    @InjectMock
    RepositoryTransaction transaction;

    @Inject
    InitializationService initService;

    @Inject
    IdProvider idProvider;

    @Test
    public void should_initialize_serverinformation_when_not_present() {
        // given
        RepositoryConnection repositoryConnection = Mockito.mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, repositoryConnection);
        when(repositoryConnection.hasStatement(
            Values.iri("neone:ServerInformation"),
            RDF.TYPE,
            API.ServerInformation,
            false
        )).thenReturn(false);

        // when
        initService.initializeServerInformation();

        // then
        // the data owner should be created
        verify(loService).createLogisticsObject(any(LogisticsObject.class), eq(false), eq(true));
        // the server information should get persisted
        verify(repositoryConnection).add(any(Model.class));
    }

    @Test
    public void should_not_initialize_serverinformation_when_already_present() {
        RepositoryConnection repositoryConnection = Mockito.mock(RepositoryConnection.class);
        when(repositoryConnection.hasStatement(
            idProvider.getBaseId().getIri(),
            RDF.TYPE,
            API.ServerInformation,
            false
        )).thenReturn(true);

        // when
        initService.initializeServerInformation();

        // then
        verify(repositoryConnection, never()).begin();
        verify(repositoryConnection, never()).add(any(Model.class));
        verify(repositoryConnection, never()).commit();
    }
}
