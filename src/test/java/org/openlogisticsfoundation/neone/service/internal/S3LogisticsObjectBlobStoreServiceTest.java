// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.exception.AlreadyExistsException;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.exception.InvalidLogisticsObjectTypeException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import jakarta.inject.Inject;
import java.nio.file.Path;
import java.util.Set;
import java.util.function.Consumer;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.openlogisticsfoundation.neone.NeoneId.fromIri;

@QuarkusTest
class S3LogisticsObjectBlobStoreServiceTest {

    @InjectMock
    IdProvider idProvider;

    @InjectMock
    S3Client s3Client;

    @InjectMock
    LogisticsObjectRepository loRepository;

    @Inject
    S3LogisticsObjectBlobStoreService blobStoreService;

    @Test
    public void should_save_blob() {
        when(idProvider.getUriForLoId(any())).thenReturn(fromIri(iri("some:iri")));
        when(s3Client.headObject(any(Consumer.class))).thenThrow(NoSuchKeyException.builder().build());
        when(loRepository.exists(any(), any())).thenReturn(true);
        when(loRepository.getLogisticsObjectTypes(any(), any())).thenReturn(Set.of(CARGO.ExternalReference));

        Path path = Path.of("");

        blobStoreService.saveBlob("123", path, "filename", "text/plain");

        verify(s3Client).putObject(any(Consumer.class), eq(path));

    }

    @Test
    public void should_throw_AlreadyExistsException_if_file_already_exists() {
        when(idProvider.getUriForLoId(any())).thenReturn(fromIri(iri("some:iri")));

        Path path = Path.of("");

        assertThrows(AlreadyExistsException.class, () -> blobStoreService.saveBlob("123", path, "filename", "text/plain"));
    }

    @Test
    public void should_throw_SubjectNotFoundException_when_lo_not_existent() {
        when(idProvider.getUriForLoId(any())).thenReturn(fromIri(iri("some:iri")));
        when(s3Client.headObject(any(Consumer.class))).thenThrow(NoSuchKeyException.builder().build());
        when(loRepository.exists(any(), any())).thenReturn(false);

        Path path = Path.of("");

        assertThrows(SubjectNotFoundException.class, () -> blobStoreService.saveBlob("123", path, "filename", "text/plain"));
    }

    @Test
    public void should_throw_InvalidLogisticsObjectTypeException_if_lo_has_wrong_type() {
        when(idProvider.getUriForLoId(any())).thenReturn(fromIri(iri("some:iri")));
        when(s3Client.headObject(any(Consumer.class))).thenThrow(NoSuchKeyException.builder().build());
        when(loRepository.exists(any(), any())).thenReturn(true);
        when(loRepository.getLogisticsObjectTypes(any(), any())).thenReturn(Set.of(CARGO.Piece));

        Path path = Path.of("");

        assertThrows(InvalidLogisticsObjectTypeException.class, () -> blobStoreService.saveBlob("123", path, "filename", "text/plain"));
    }

    @Test
    public void should_download_blob() {
        ResponseBytes<GetObjectResponse> response = mock(ResponseBytes.class);
        GetObjectResponse objectResponse  = mock(GetObjectResponse.class);
        when(response.response()).thenReturn(objectResponse);
        when(s3Client.getObjectAsBytes(any(Consumer.class))).thenReturn(response);

        blobStoreService.downloadBlob("123", "filename");

        verify(s3Client).getObjectAsBytes(any(Consumer.class));
    }

    @Test
    public void should_throw_SubjectNotFoundException_if_file_does_not_exist() {
        when(s3Client.getObjectAsBytes(any(Consumer.class))).thenThrow(NoSuchKeyException.builder().build());

        assertThrows(SubjectNotFoundException.class, () -> blobStoreService.downloadBlob("123", "filename"));
    }

}
