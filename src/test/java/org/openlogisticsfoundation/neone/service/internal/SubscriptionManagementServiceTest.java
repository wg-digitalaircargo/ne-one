// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service.internal;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.client.OneRecordClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionHandler;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.SubscriptionRepository;

import jakarta.inject.Inject;
import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
public class SubscriptionManagementServiceTest {

    @InjectMock
    SubscriptionRepository repository;

    @InjectMock
    RepositoryTransaction transaction;

    @InjectMock
    RestClientBuilderProducer producer;

    @Inject
    SubscriptionManagementService subService;

    @Test
    public void should_orchestrate_properly() throws IOException {
        // given
        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);
        doNothing().when(repository).persist(any(Subscription.class), any());

        Model m = Rio.parse(new StringReader(TestFixtures.MINIMAL_SUBSCRIPTION), RDFFormat.JSONLD);
        var testResponse = new SubscriptionHandler().fromModel(Values.iri("some:iri"), m);
        OneRecordClient requestor = mock(OneRecordClient.class);
        when(requestor.proposeSubscription(any(), any())).thenReturn(testResponse);
        RestClientBuilder builder = mock(RestClientBuilder.class, Mockito.RETURNS_DEEP_STUBS);
        when(builder
            .baseUri(any())
            .connectTimeout(anyLong(), any(TimeUnit.class))
            .readTimeout(anyLong(), any(TimeUnit.class))
            .build(any()))
            .thenReturn(requestor);
        when(producer.restClientBuilder()).thenReturn(builder);

        // when
        subService.proposeSubscription("http://some-partner-id",
            "some topic", "some type");

        // then
        verify(requestor, times(1)).proposeSubscription(any(), any());
        verify(repository, times(1)).persist(any(Subscription.class), any());
    }
}
