// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.NotificationRepository;

import jakarta.inject.Inject;

@QuarkusTest
class NotificationServiceTest {
    @Inject
    NotificationService notificationService;

    @InjectMock
    NeoneEventRepository neoneEventRepository;

    @InjectMock
    NotificationRepository notificationRepository;

    @InjectMock
    LogisticsObjectRepository logisticsObjectRepository;

    @InjectMock
    RestClientBuilderProducer producer;
/*
    @Test
    public void should_enqueue() {
        // given
        LogisticsObjectEvent event = new LogisticsObjectEvent(
            Values.iri("http://test"),
            LogisticsObjectEvent.Type.CREATED);
        List<String> callbackUrls = List.of("http://callback1", "http://callback2");

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(notificationRepository, connection);

        // when
        notificationService.enqueue(event, callbackUrls);

        // then
        verify(notificationRepository, times(1)).transactionallyDo(any());
        verify(logisticsObjectRepository, times(callbackUrls.size())).getLogisticsObjectType(event.getLoId(), connection);
        verify(notificationRepository, times(callbackUrls.size())).persist(any(Notification.class), eq(connection));
        verify(eventRepository, times(callbackUrls.size())).addNotification(eq(event.getId()), any(), eq(connection));
        verify(callbackRepository, times(callbackUrls.size())).persist(any(Callback.class), eq(connection));
        verify(eventRepository, times(1)).setEventState(event.getId(), LogisticsObjectEvent.State.PENDING, connection);
    }

    @Test
    public void should_send_notifications_including_LO_when_isSendLo_is_true() {
        //given
        Notification notification = new Notification(
            LogisticsObjectEvent.Type.CREATED,
            Values.iri("http://lo_1")
            , "topic");
        Callback callback = new Callback(
            "http://localhost",
            true,
            notification.getId());
        NotificationData notificationData = new NotificationData(
            Values.iri("http://event_1"),
            callback.getCallbackUrl(),
            callback.getId(),
            notification
        );
        List<NotificationData> data = List.of(
            notificationData
        );
        LogisticsObjectModel logisticsObjectModel = new LogisticsObjectModel(
            notification.getLoId(),
            new ModelBuilder()
                .subject(notification.getLoId())
                .add(RDF.TYPE, Values.iri("http://type"))
                .add(NEONE.hasRevision, 1)
                .add(NEONE.isCreatedAt, Instant.now().toString())
                .build());

        TestFixtures.mockDoInTransactionCall(notificationRepository, mock(RepositoryConnection.class));

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(eventRepository, connection);

        when(notificationRepository.getPendingNotifications(any(RepositoryConnection.class))).thenReturn(data);
        when(callbackRepository.findByIri(notificationData.callbackId(), false, connection)).thenReturn(callback);
        when(logisticsObjectRepository.findByIri(notificationData.notification().getLoId(), true, connection)).thenReturn(logisticsObjectModel);

        RestClientBuilder builder = mock(RestClientBuilder.class, Mockito.RETURNS_DEEP_STUBS);
        OneRecordClient client = mock(OneRecordClient.class);
        when(builder
            .baseUri(any())
            .connectTimeout(anyLong(), any(TimeUnit.class))
            .readTimeout(anyLong(), any(TimeUnit.class))
            .build(any()))
            .thenReturn(client);
        when(producer.restClientBuilder()).thenReturn(builder);

        // when
        notificationService.sendNotifications();

        // then
        verify(notificationRepository, times(1)).transactionallyGet(any());
        verify(notificationRepository, times(1)).getPendingNotifications(any());

        verify(callbackRepository, times(data.size())).findByIri(notificationData.callbackId(), false, connection);
        verify(logisticsObjectRepository, times(data.size())).findByIri(notificationData.notification().getLoId(), true, connection);
        verify(eventRepository, times(data.size())).deleteNotification(any(), any(), eq(connection));
        verify(notificationRepository, times(data.size())).deleteAll(any(), eq(false), eq(connection));
        verify(callbackRepository, times(data.size())).deleteAll(any(), eq(false), eq(connection));
    }

    @Test
    public void should_send_notifications_without_LO_when_isSendLo_is_false() {
        //given
        Notification notification = new Notification(
            LogisticsObjectEvent.Type.CREATED,
            Values.iri("http://lo_1")
            , "topic");
        Callback callback = new Callback(
            "http://localhost",
            false,
            notification.getId());
        NotificationData notificationData = new NotificationData(
            Values.iri("http://event_1"),
            callback.getCallbackUrl(),
            callback.getId(),
            notification
        );
        List<NotificationData> data = List.of(
            notificationData
        );
        LogisticsObjectModel logisticsObjectModel = new LogisticsObjectModel(
            notification.getLoId(),
            new ModelBuilder()
                .subject(notification.getLoId())
                .add(RDF.TYPE, Values.iri("http://type"))
                .add(NEONE.hasRevision, 1)
                .add(NEONE.isCreatedAt, Instant.now().toString())
                .build());

        TestFixtures.mockDoInTransactionCall(notificationRepository, mock(RepositoryConnection.class));

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(eventRepository, connection);

        when(notificationRepository.getPendingNotifications(any(RepositoryConnection.class))).thenReturn(data);
        when(callbackRepository.findByIri(notificationData.callbackId(), false, connection)).thenReturn(callback);
        when(logisticsObjectRepository.findByIri(notificationData.notification().getLoId(), true, connection)).thenReturn(logisticsObjectModel);

        RestClientBuilder builder = mock(RestClientBuilder.class, Mockito.RETURNS_DEEP_STUBS);
        OneRecordClient client = mock(OneRecordClient.class);
        when(builder
            .baseUri(any())
            .connectTimeout(anyLong(), any(TimeUnit.class))
            .readTimeout(anyLong(), any(TimeUnit.class))
            .build(any()))
            .thenReturn(client);
        when(producer.restClientBuilder()).thenReturn(builder);

        // when
        notificationService.sendNotifications();

        // then
        verify(notificationRepository, times(1)).transactionallyGet(any());
        verify(notificationRepository, times(1)).getPendingNotifications(any());

        verify(callbackRepository, times(data.size())).findByIri(notificationData.callbackId(), false, connection);
        verifyNoInteractions(logisticsObjectRepository);
        verify(logisticsObjectRepository, never()).findByIri(notificationData.notification().getLoId(), true, connection);
        verify(eventRepository, times(data.size())).deleteNotification(any(), any(), eq(connection));
        verify(notificationRepository, times(data.size())).deleteAll(any(), eq(false), eq(connection));
        verify(callbackRepository, times(data.size())).deleteAll(any(), eq(false), eq(connection));
    }

 */
}
