// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.controller.IdProvider;

import jakarta.inject.Inject;

@QuarkusTest
public class AuditTrailServiceCreateOrUpdateTest {

    @Inject
    AuditTrailService service;

    @Inject
    IdProvider idProvider;

    static String loId = "1";

    @Test
    @Disabled
    public void should_create_audit_trail() {
        // when
        var id = idProvider.getUriForLoId(loId);
        service.createAuditTrail(id.getIri());

        // then
        var auditTrail = service.getAuditTrailByLoId(loId, null, null);
        MatcherAssert.assertThat(auditTrail.latestRevision(), CoreMatchers.is(1));
    }
}
