// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.repository.AuditTrailRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectMetadataService;

import jakarta.inject.Inject;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@QuarkusTest
public class AuditTrailServiceGetTest {
    @InjectMock
    AuditTrailRepository repository;

    @InjectMock
    RepositoryTransaction transaction;

    @InjectMock
    LogisticsObjectMetadataService loMetadataService;

    @Inject
    IdProvider idProvider;

    @Inject
    AuditTrailService service;

    @Test
    public void should_find_audit_trail_via_loId() {
        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        var loId = "some_lo_id";
        var auditTrailId = idProvider.getAuditTrailIriFromLoId(loId);
        when(loMetadataService.getMetadataOf(any())).thenReturn(Optional.empty());
        when(repository.getAuditTrail(eq(auditTrailId.getIri()), any(), any(), any()))
            .thenReturn(Optional.of(new AuditTrail(Values.iri("some:iri"), Collections.emptyList(), 1)));
        var auditTrail = service.getAuditTrailByLoId(loId, null, null);

        assertThat(auditTrail, is(notNullValue()));
    }
}
