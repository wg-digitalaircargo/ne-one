// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.model.ServerInformation;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.repository.ServerInformationRepository;

import jakarta.inject.Inject;

import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class ServerInformationServiceTest {

    @InjectMock
    ServerInformationRepository repository;

    @InjectMock
    RepositoryTransaction transaction;

    @InjectMock
    IdProvider idProvider;

    @Inject
    ServerInformationService service;

    @Test
    public void should_query_the_repository_for_the_server_information() {
        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);
        NeoneId baseId = NeoneId.fromIri(Values.iri("neone:some"));
        when(idProvider.getBaseId()).thenReturn(baseId);
        when(repository.findByIri(baseId.getIri(), connection))
            .thenReturn(Optional.of(new ServerInformation(baseId.getIri(), new DynamicModelFactory().createEmptyModel())));

        service.getServerInformation();

        verify(repository).findByIri(baseId.getIri(), connection);
    }
}
