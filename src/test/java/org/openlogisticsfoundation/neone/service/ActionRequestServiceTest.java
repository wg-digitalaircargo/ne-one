// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.base.CoreDatatype;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.Change;
import org.openlogisticsfoundation.neone.model.ChangeRequest;
import org.openlogisticsfoundation.neone.model.Operation;
import org.openlogisticsfoundation.neone.model.OperationObject;
import org.openlogisticsfoundation.neone.model.PatchOperation;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.model.Subscription;
import org.openlogisticsfoundation.neone.model.SubscriptionRequest;
import org.openlogisticsfoundation.neone.model.handler.ChangeRequestHandler;
import org.openlogisticsfoundation.neone.model.handler.SubscriptionRequestHandler;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;

import jakarta.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
class ActionRequestServiceTest {

    @Inject
    ActionRequestService actionRequestService;

    @Inject
    RepositoryTransaction transaction;

    @Inject
    ChangeRequestHandler changeRequestHandler;

    @Inject
    SubscriptionRequestHandler subscriptionRequestHandler;

    @Test
    public void should_return_a_requested_ChangeRequest() {

        IRI crIri = Values.iri("neone:cr");
        IRI changeIri = Values.iri("neone:c");
        IRI requestor = Values.iri("some://requestor");
        IRI lo = Values.iri("some://lo");

        OperationObject operationObject = new OperationObject(Values.iri("neone:opObject"), CoreDatatype.XSD.STRING.toString(), "value");
        Operation op = new Operation(Values.iri("neone:op"), operationObject, PatchOperation.ADD, Values.iri("some://predicate"), "some://subject");
        Change change = new Change(changeIri, Optional.of("description"), Collections.singleton(op), 1, lo,
            Optional.empty());
        ChangeRequest cr = new ChangeRequest(crIri, Optional.empty(), RequestStatus.REQUEST_ACCEPTED,
            Instant.now(), requestor, Optional.empty(),
            Optional.empty(), change);

        transaction.transactionallyDo(connection -> connection.add(changeRequestHandler.fromJava(cr)));

        Model actionRequest = actionRequestService.getActionRequest(crIri);

        assertThat(actionRequest.contains(crIri, RDF.TYPE, API.ChangeRequest), is(true));

    }

    @Test
    public void should_return_a_requested_SubscriptionRequest() {

        IRI subscriptionIri = Values.iri("neone:subscription");
        IRI subscriptionRequestIri = Values.iri("neone:subRQ");
        IRI requestor = Values.iri("some://requestor");

        Subscription subscription = new Subscription(subscriptionIri, Values.iri("some://subscriber"),
            Subscription.TopicType.LOGISTICS_OBJECT_TYPE, CARGO.Piece.stringValue(), Optional.empty(),
            Optional.empty(), Optional.empty(), Optional.empty(),
            Optional.empty());
        SubscriptionRequest subscriptionRequest = new SubscriptionRequest(subscriptionRequestIri,
            RequestStatus.REQUEST_ACCEPTED, Instant.now(), requestor, Optional.empty(),
            Optional.empty(), Optional.empty(), subscription);

        transaction.transactionallyDo(connection -> connection.add(subscriptionRequestHandler.fromJava(subscriptionRequest)));

        Model actionRequest = actionRequestService.getActionRequest(subscriptionRequestIri);

        assertThat(actionRequest.contains(subscriptionRequestIri, RDF.TYPE, API.SubscriptionRequest), is(true));

    }

}
