// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.iata.onerecord.api.API;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.NeoneEvent;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.NeoneEventRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.vocab.ACL;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.inject.Inject;
import java.net.URI;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
class LogisticsObjectServiceTest {

    @Inject
    LogisticsObjectService service;

    @InjectMock
    LogisticsObjectUtil logisticsObjectUtil;

    @InjectMock
    LogisticsObjectRepository repository;

    @InjectMock
    NeoneEventRepository neoneEventRepository;

    @InjectMock
    RepositoryTransaction transaction;

    @InjectMock
    SnapshotService snapshotService;

    @io.quarkus.test.InjectMock
    NeoneEventService neoneEventService;

    @io.quarkus.test.InjectMock
    AclAuthorizationRepository aclRepository;

    @Inject
    IdProvider idProvider;

    @Test
    public void should_persist_a_LO_using_generated_id() throws Exception {
        // given
        URI uri = new URI("http://testuri");
        IRI iri = Values.iri(uri.toString());

        Model model = new ModelBuilder()
            .subject(iri)
            .add(RDF.TYPE, CARGO.Piece)
            .add(NEONE.hasRevision, Values.literal(1))
            .add(NEONE.isCreatedAt, Values.literal(Instant.now().toString()))
            .build();

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(logisticsObjectUtil.generateResourceIri(uri)).thenReturn(iri);
        when(repository.addIdToRootStatements(model, iri)).thenReturn(model);
        when(logisticsObjectUtil.addMetadata(model, iri)).thenReturn(model);
        when(snapshotService.createSnapshot(any(), any(), any())).thenReturn(null);
        NeoneEvent neoneEvent = new NeoneEvent(Values.iri("some:event"), iri, API.LOGISTICS_OBJECT_CREATED, NeoneEvent.State.NEW,
            Optional.empty(), Collections.emptySet(), Set.of(CARGO.Piece));
        when(neoneEventRepository.addEvent(
            eq(iri), eq(API.LOGISTICS_OBJECT_CREATED), eq(Optional.empty()),
            eq(Collections.emptySet()), anySet(), eq(connection))
        ).thenReturn(neoneEvent);

        // when
        service.createLogisticsObject(new LogisticsObject(iri, model), false, false);

        // then
        verify(repository, times(1)).persist(any(LogisticsObject.class), eq(connection));
        verify(neoneEventRepository, times(1)).addEvent(
            eq(iri), eq(API.LOGISTICS_OBJECT_CREATED), eq(Optional.empty()),
            eq(Collections.emptySet()), eq(Set.of(CARGO.Piece)), eq(connection));
        verify(snapshotService, times(1)).createSnapshot(any(), any(), any());
        verify(neoneEventService, times(1)).publishEvent(eq(neoneEvent));
        verify(aclRepository).grantDefaultAccess(eq(iri), eq(null), eq(false), any());
    }

    @Test
    public void should_persist_a_LO_using_generated_id_and_acl_for_creator() throws Exception {
        // given
        URI uri = new URI("http://testuri");
        IRI iri = Values.iri(uri.toString());

        Model model = new ModelBuilder()
            .subject(iri)
            .add(RDF.TYPE, CARGO.Piece)
            .add(NEONE.hasRevision, Values.literal(1))
            .add(NEONE.isCreatedAt, Values.literal(Instant.now().toString()))
            .build();

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(logisticsObjectUtil.generateResourceIri(uri)).thenReturn(iri);
        when(repository.addIdToRootStatements(model, iri)).thenReturn(model);
        when(logisticsObjectUtil.addMetadata(model, iri)).thenReturn(model);
        when(snapshotService.createSnapshot(any(), any(), any())).thenReturn(null);
        NeoneEvent neoneEvent = new NeoneEvent(Values.iri("some:event"), iri, API.LOGISTICS_OBJECT_CREATED, NeoneEvent.State.NEW,
            Optional.empty(), Collections.emptySet(), Set.of(CARGO.Piece));
        when(neoneEventRepository.addEvent(
            eq(iri), eq(API.LOGISTICS_OBJECT_CREATED), eq(Optional.empty()),
            eq(Collections.emptySet()), anySet(), eq(connection))
        ).thenReturn(neoneEvent);

        // when
        service.createLogisticsObject(new LogisticsObject(iri, model), true, false);

        // then
        verify(repository, times(1)).persist(any(LogisticsObject.class), eq(connection));
        verify(neoneEventRepository, times(1)).addEvent(
            eq(iri), eq(API.LOGISTICS_OBJECT_CREATED), eq(Optional.empty()),
            eq(Collections.emptySet()), eq(Set.of(CARGO.Piece)), eq(connection));
        verify(snapshotService, times(1)).createSnapshot(any(), any(), any());
        verify(neoneEventService, times(1)).publishEvent(eq(neoneEvent));
        verify(aclRepository).grantDefaultAccess(eq(iri), eq(idProvider.getDataHolderId().getIri()), eq(false), any());
    }

    @Test
    public void should_persist_a_LO_using_generated_id_and_acl_for_creator_and_public_access() throws Exception {
        // given
        URI uri = new URI("http://testuri");
        IRI iri = Values.iri(uri.toString());

        Model model = new ModelBuilder()
            .subject(iri)
            .add(RDF.TYPE, CARGO.Piece)
            .add(NEONE.hasRevision, Values.literal(1))
            .add(NEONE.isCreatedAt, Values.literal(Instant.now().toString()))
            .build();

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(logisticsObjectUtil.generateResourceIri(uri)).thenReturn(iri);
        when(repository.addIdToRootStatements(model, iri)).thenReturn(model);
        when(logisticsObjectUtil.addMetadata(model, iri)).thenReturn(model);
        when(snapshotService.createSnapshot(any(), any(), any())).thenReturn(null);
        NeoneEvent neoneEvent = new NeoneEvent(Values.iri("some:event"), iri, API.LOGISTICS_OBJECT_CREATED, NeoneEvent.State.NEW,
            Optional.empty(), Collections.emptySet(), Set.of(CARGO.Piece));
        when(neoneEventRepository.addEvent(
            eq(iri), eq(API.LOGISTICS_OBJECT_CREATED), eq(Optional.empty()),
            eq(Collections.emptySet()), anySet(), eq(connection))
        ).thenReturn(neoneEvent);

        // when
        service.createLogisticsObject(new LogisticsObject(iri, model), true, true);

        // then
        verify(repository, times(1)).persist(any(LogisticsObject.class), eq(connection));
        verify(neoneEventRepository, times(1)).addEvent(
            eq(iri), eq(API.LOGISTICS_OBJECT_CREATED), eq(Optional.empty()),
            eq(Collections.emptySet()), eq(Set.of(CARGO.Piece)), eq(connection));
        verify(snapshotService, times(1)).createSnapshot(any(), any(), any());
        verify(neoneEventService, times(1)).publishEvent(eq(neoneEvent));
        verify(aclRepository).grantDefaultAccess(eq(iri), eq(idProvider.getDataHolderId().getIri()), eq(true), any());
    }

    @Test
    public void should_fetch_a_LO_by_Iri() throws Exception {
        // given
        URI uri = new URI("http://testuri");
        IRI iri = SimpleValueFactory.getInstance().createIRI(uri.toString());

        Model model = new ModelBuilder()
            .subject(iri)
            .add(NEONE.hasRevision, Values.literal(1))
            .add(NEONE.isCreatedAt, Values.literal(Instant.now().toString()))
            .build();

        LogisticsObject lom = new LogisticsObject(iri, model);

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(repository.findByIri(any(), any())).thenReturn(Optional.of(lom));

        // when
        service.getLogisticsObject(uri, false);

        // then
        verify(transaction, times(1)).transactionallyGet(any(Function.class));
        verify(repository, times(1)).findByIri(iri, connection);
    }

    @Test
    public void should_throw_LogisticsObjectNotFoundException_when_LO_not_found_in_repo() {

        assertThrows(SubjectNotFoundException.class, () -> {

            // given
            URI uri = new URI("http://testuri");

            RepositoryConnection connection = mock(RepositoryConnection.class);
            TestFixtures.mockDoInTransactionCall(transaction, connection);

            when(repository.findByIri(any(), any())).thenReturn(Optional.empty());

            // when
            service.getLogisticsObject(uri, false);
        });
    }
/*
    @Test
    @Disabled
    public void should_apply_delete_and_add_operation() throws Exception {
        // given
        URI uri = new URI("http://testuri");
        IRI subject = Values.iri(uri.toString());
        IRI predicate = Values.iri("http://testuri/Test#p1");
        String value = "value";

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(repository, connection);

        NewLo model = mock(NewLo.class);
        // TODO: revision
        when(model.iri()).thenReturn(subject);
        //when(repository.findByIri(any(), any())).thenReturn(model);
        doNothing().when(auditTrailService).createOrUpdateAuditTrail(any(), any(), any());
        when(snapshotService.createSnapshot(any(), any())).thenReturn(null);

        PatchRequest.Operation delete = new PatchRequest.Operation(
            PatchRequest.Operation.Op.DEL,
            predicate,
            XSD.Datatype.STRING,
            value
        );

        PatchRequest.Operation add = new PatchRequest.Operation(
            PatchRequest.Operation.Op.ADD,
            predicate,
            XSD.Datatype.STRING,
            value
        );

        var patchRequest = new PatchRequest.PatchRequestBuilder()
            .withDescription("description")
            .withOperations(List.of(delete))
            .withOperations(List.of(add))
            .withRevision("1")
            .build();

        // when
        service.newupdateLogisticsObject(NeoneId.fromUri(uri), patchRequest);

        // then
        verify(repository, times(1)).findByIri(subject, connection);
        verify(repository, times(1)).delete(subject, predicate, Values.literal(value), connection);
        verify(repository, times(1)).add(subject, predicate, Values.literal(value), connection);

        verify(eventRepository, times(1)).persist(any(LogisticsObjectEvent.class), eq(connection));

        verify(bus, times(1)).publish(eq(LogisticsObjectEvent.ADDRESS), any(LogisticsObjectEvent.class));

        verify(snapshotService, times(1)).createSnapshot(any(), any());
    }

    @Test
    @Disabled
    public void should_apply_multiple_delete_and_add_operations() throws Exception {
        // given
        URI uri = new URI("http://testuri");
        IRI subject = Values.iri(uri.toString());
        IRI predicate = Values.iri("http://testuri/Test#p1");
        String value = "value";

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(repository, connection);

        NewLo model = mock(NewLo.class);
        // TODO: revision
        when(repository.findByIri(any(), any())).thenReturn(Optional.of(model));
        doNothing().when(auditTrailService).createOrUpdateAuditTrail(any(), any(), any());

        PatchRequest.Operation delete = new PatchRequest.Operation(
            PatchRequest.Operation.Op.DEL,
            predicate,
            XSD.Datatype.STRING,
            value
        );

        PatchRequest.Operation add = new PatchRequest.Operation(
            PatchRequest.Operation.Op.ADD,
            predicate,
            XSD.Datatype.STRING,
            value
        );

        var patchRequest = new PatchRequest.PatchRequestBuilder()
            .withDescription("description")
            .withOperations(List.of(delete, delete))
            .withOperations(List.of(add, add))
            .withRevision("1")
            .build();

        // when
        service.newupdateLogisticsObject(NeoneId.fromUri(uri), patchRequest);

        // then
        verify(repository, times(1)).findByIri(subject, connection);
        verify(repository, times(2)).delete(subject, predicate, Values.literal(value), connection);
        verify(repository, times(2)).add(subject, predicate, Values.literal(value), connection);

        verify(eventRepository, times(1)).persist(any(LogisticsObjectEvent.class), eq(connection));

        verify(bus, times(1)).publish(eq(LogisticsObjectEvent.ADDRESS), any(LogisticsObjectEvent.class));
    }
 */
}
