// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.LogisticsEvent;
import org.openlogisticsfoundation.neone.model.handler.LogisticsEventHandler;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsEventRepository;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;

import jakarta.inject.Inject;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.openlogisticsfoundation.neone.NeoneId.fromString;

@QuarkusTest
class LogisticsEventServiceTest {

    @InjectMock
    RepositoryTransaction transaction;

    @InjectMock
    LogisticsEventRepository logisticsEventRepository;

    @InjectMock
    LogisticsObjectRepository logisticsObjectRepository;

    @Inject
    LogisticsEventService logisticsEventService;

    @Inject
    LogisticsEventHandler eventHandler;

    @InjectMock
    AclAuthorizationRepository aclRepository;

    @InjectMock
    IdProvider idProvider;

    @Test
    public void should_persist_event() throws Exception {
        // given
        NeoneId loId = NeoneId.fromUri(new URI("http://some/lo"));
        NeoneId eventId = NeoneId.fromUri(new URI("http://some/lo/event"));
        NeoneId dataHolder = fromString("http://dataholder");

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        Model model = TestFixtures.asModel(TestFixtures.SIMPLE_EVENT, RDFFormat.JSONLD);
        LogisticsEvent event = eventHandler.fromModel(eventId.getIri(), model);

        when(logisticsObjectRepository.exists(any(), any())).thenReturn(true);
        when(logisticsObjectRepository.getLogisticsObjectTypes(any(), any())).thenReturn(Set.of(CARGO.Piece));
        when(aclRepository.findAclAuthorizations(any(), any(), any())).thenReturn(List.of());

        when(idProvider.createLoEventUri(any(NeoneId.class))).thenReturn(eventId);
        when(idProvider.createLoEventUri(any(IRI.class))).thenReturn(eventId);
        when(idProvider.getDataHolderId()).thenReturn(dataHolder);
        when(idProvider.getUriForLoId(any())).thenReturn(dataHolder);

        // when
        logisticsEventService.addLogisticsEvent(loId, event);

        // then
        verify(logisticsEventRepository, times(1))
            .persist(eq(eventId.getIri()), any(LogisticsEvent.class), eq(connection));
    }

    @Test
    public void should_not_persist_event_if_lo_does_not_exist() throws Exception {
        // given
        NeoneId loId = NeoneId.fromUri(new URI("http://some/lo"));
        NeoneId eventId = NeoneId.fromUri(new URI("http://some/lo/event"));

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        Model model = TestFixtures.asModel(TestFixtures.SIMPLE_EVENT, RDFFormat.JSONLD);
        LogisticsEvent event = eventHandler.fromModel(eventId.getIri(), model);

        when(logisticsObjectRepository.exists(loId.getIri(), connection)).thenReturn(false);
        when(logisticsObjectRepository.getLogisticsObjectTypes(any(), any())).thenReturn(Set.of(CARGO.Piece));

        when(idProvider.createLoEventUri(any(NeoneId.class))).thenReturn(eventId);

        // when
        assertThrows(SubjectNotFoundException.class,
            () -> logisticsEventService.addLogisticsEvent(loId, event)
        );

        // then
        verify(logisticsEventRepository, never()).persist(any(), any(), any());
    }

    @Test
    public void should_return_events_for_lo() throws Exception {
        // given
        URI loUri = new URI("http://some/lo");
        IRI loIri = Values.iri(loUri.toString());

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(logisticsObjectRepository.exists(loIri, connection)).thenReturn(true);

        // when
        logisticsEventService.findLogisticsEvents(NeoneId.fromUri(loUri), any(),
            null, null, null, null, null);

        // then
        verify(logisticsEventRepository, times(1))
            .findEventsOfLogisticsObject(loIri, Optional.empty(),Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(),
                100, connection);
    }

    @Test
    public void should_throw_not_found_exception_when_lo_does_not_exist() throws Exception {
        // given
        URI loUri = new URI("http://some/lo");
        IRI loIri = Values.iri(loUri.toString());

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(logisticsObjectRepository.exists(loIri, connection)).thenReturn(false);

        // then
        verify(logisticsEventRepository, never())
            .findEventsOfLogisticsObject(loIri, Optional.empty(),
                Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(),
                1, connection);
    }

    @Test
    public void should_query_for_specific_event() throws Exception {
        // given
        URI eventUri = new URI("http://some/lo/event");
        IRI eventIri = Values.iri(eventUri.toString());

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        Model model = TestFixtures.asModel(TestFixtures.SIMPLE_EVENT, RDFFormat.JSONLD);
        LogisticsEvent event = eventHandler.fromModel(eventIri, model);

        when(logisticsEventRepository.findByIri(eventIri, connection))
            .thenReturn(Optional.of(event));

        // when
        LogisticsEvent logisticsEvent = logisticsEventService
            .getLogisticsEvent(NeoneId.fromUri(eventUri), false, null);

        // then
        assertThat(logisticsEvent, is(equalTo(event)));
    }

    @Test
    public void should_throw_not_found_exception_when_specific_event_does_not_exist() throws Exception {

        URI eventUri = new URI("http://some/lo/event");
        IRI eventIri = Values.iri(eventUri.toString());

        RepositoryConnection connection = mock(RepositoryConnection.class);
        TestFixtures.mockDoInTransactionCall(transaction, connection);

        when(logisticsEventRepository.findByIri(eventIri, connection))
            .thenReturn(Optional.empty());

        assertThrows(SubjectNotFoundException.class, () -> logisticsEventService
            .getLogisticsEvent(NeoneId.fromUri(eventUri), false, null));
    }
}
