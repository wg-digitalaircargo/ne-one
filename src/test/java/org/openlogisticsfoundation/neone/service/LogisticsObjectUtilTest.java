// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.service;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.repository.LogisticsObjectRepository;
import org.openlogisticsfoundation.neone.vocab.NEONE;

import jakarta.inject.Inject;
import java.io.StringReader;
import java.net.URI;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
public class LogisticsObjectUtilTest {

    @Inject
    LogisticsObjectUtil util;

    @Inject
    LogisticsObjectRepository repository;

    @Test
    public void should_generate_an_IRI_from_a_given_URI() throws Exception {
        // given
        URI uri = new URI("http://test/uri");

        // when
        IRI iri = util.generateResourceIri(uri);

        //then
        assertThat(iri.stringValue(), is(equalTo(uri.toString())));
    }

    @Test
    public void should_add_initial_revision_number_to_new_LO() throws Exception {
        // Given some model that has a proper id (IRI).
        var id = Values.iri("https://some/id");
        var model = repository.addIdToRootStatements(
            Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ONE_HI_JSON_LD), RDFFormat.JSONLD),
            id);

        // When the initial revision number is set...
        Model versionedModel = util.addMetadata(model, id);

        // ... then it should occur in the model with value 1.
        Value revisionNumber = versionedModel.getStatements(id, NEONE.hasRevision, null).iterator().next().getObject();
        assertThat(revisionNumber.stringValue(), is(equalTo(LogisticsObjectUtil.INITIAL_REVISION.toString())));
    }
}
