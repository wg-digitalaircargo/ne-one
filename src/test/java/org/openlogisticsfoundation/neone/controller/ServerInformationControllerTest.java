// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.model.ServerInformation;
import org.openlogisticsfoundation.neone.service.ServerInformationService;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(ServerInformationController.class)
class ServerInformationControllerTest {

    @InjectMock
    ServerInformationService serverInformationService;

    @Test
    @TestSecurity(user = "test")
    public void should_return_the_server_information() {
        when(serverInformationService.getServerInformation()).thenReturn(
            new ServerInformation(Values.iri("some://iri"), new ModelBuilder().build()));

        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get()
            .then()
            .statusCode(200);
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_404_if_no_server_information_present() {
        when(serverInformationService.getServerInformation()).thenThrow(new SubjectNotFoundException(""));

        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get()
            .then()
            .statusCode(404);
    }
}
