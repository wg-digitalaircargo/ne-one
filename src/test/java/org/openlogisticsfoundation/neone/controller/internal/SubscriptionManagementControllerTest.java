// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.service.internal.SubscriptionManagementService;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(SubscriptionManagementController.class)
public class SubscriptionManagementControllerTest {

    @InjectMock
    SubscriptionManagementService subscriptionManagementService;

    @Test
    public void should_return_200_on_proper_proposal_request() {
        when(subscriptionManagementService.proposeSubscription(any(), any(), any()))
            .thenReturn(TestFixtures.getSubscription());

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(TestFixtures.SUBSCRIPTION_PROPOSAL_RQ)
            .when().post()
            .then().statusCode(200);
    }

    @Test
    public void should_return_415_on_invalid_content_type() {
        when(subscriptionManagementService.proposeSubscription(any(), any(), any()))
            .thenReturn(null);

        given()
            .config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig()
                .encodeContentTypeAs("application/some_illegal_type", ContentType.TEXT)))
            .contentType("application/some_illegal_type")
            .body(TestFixtures.SUBSCRIPTION_PROPOSAL_RQ)
            .when().post()
            .then().statusCode(Response.Status.UNSUPPORTED_MEDIA_TYPE.getStatusCode());
    }

    @Test
    public void should_return_bad_request_on_invalid_proposal_request() {
        when(subscriptionManagementService.proposeSubscription(any(), any(), any()))
            .thenReturn(null);

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(TestFixtures.SUBSCRIPTION_PROPOSAL_INVALID_RQ)
            .when().post()
            .then().statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }
}
