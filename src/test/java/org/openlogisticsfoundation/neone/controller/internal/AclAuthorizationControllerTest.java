// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller.internal;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.controller.BodyHandlerBase;
import org.openlogisticsfoundation.neone.controller.IdProvider;
import org.openlogisticsfoundation.neone.controller.config.LogisticsObjectIdConfig;
import org.openlogisticsfoundation.neone.model.AclAuthorization;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.vocab.ACL;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

@QuarkusTest
@TestHTTPEndpoint(AclAuthorizationController.class)
class AclAuthorizationControllerTest {

    @Inject
    RepositoryTransaction tx;

    @Inject
    LogisticsObjectIdConfig idConfig;

    @Inject
    AclAuthorizationRepository aclRepository;

    @Inject
    IdProvider idProvider;

    @Inject
    Repository repository;

    @BeforeEach
    public void setup() {
        try (RepositoryConnection connection = repository.getConnection()) {
            connection.clear();
        }
    }

    @Test
    public void should_add_acl() {
        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .header("Location", is(notNullValue()))
            .header("Location",
                startsWith(idConfig.scheme() + "://" + idConfig.host() + ":" + idConfig.port().get() + "/internal/acls/")
            );
    }

    @Test
    public void should_get_acl() throws Exception {
        String internalAclId = "some-acl";
        IRI accessTo = iri("some:object");
        IRI agent = iri("some:subject");

        NeoneId aclId = idProvider.getAclAuthorizationId(internalAclId);
        tx.transactionallyDo(connection -> aclRepository.persist(new AclAuthorization(
            aclId.getIri(),
            accessTo,
            agent,
            Set.of(ACL.Read)
        ), connection));

        InputStream aclStream = given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .pathParam("id", internalAclId)
            .when().get("/{id}")
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        Model acl = Rio.parse(aclStream, RDFFormat.JSONLD);
        assertThat(Models.subjectIRI(acl).get(), is(equalTo(aclId.getIri())));
        assertThat(Models.objectIRI(acl.filter(aclId.getIri(), ACL.accessTo, null)).get(), is(equalTo(accessTo)));
        assertThat(Models.objectIRI(acl.filter(aclId.getIri(), ACL.agent, null)).get(), is(equalTo(agent)));
        assertThat(Models.objectIRI(acl.filter(aclId.getIri(), ACL.mode, null)).get(), is(equalTo(ACL.Read)));
    }

    @Test
    public void should_return_404_if_acl_not_present() {
        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .pathParam("id", "does-not-exist")
            .when().get("/{id}")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void should_update_acl() throws Exception {
        String location = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL2)
            .when().patch(location)
            .then()
            .statusCode(Response.Status.OK.getStatusCode());

        InputStream aclStream = given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get(location)
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        Model acl = Rio.parse(aclStream, RDFFormat.JSONLD);
        assertThat(Models.subjectIRI(acl).get(), is(equalTo(Values.iri(location))));
        assertThat(Models.objectIRI(acl.filter(Values.iri(location), ACL.mode, null)).get(), is(equalTo(ACL.Write)));

    }

    @Test
    public void should_respond_with_404_when_updating_non_existent_acl() {
        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL)
            .when().patch("/{id}", "does-not-exist")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void should_respons_with_404_when_fetching_non_existent_acl() {
        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get("/{id}", "does-not-exist")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void should_delete_acl() {
        String location = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().delete(location)
            .then()
            .statusCode(Response.Status.OK.getStatusCode());

        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get("/{id}", "does-not-exist")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void should_respond_with_404_when_deleting_non_existent_acl() {
        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().delete("/{id}", "does-not-exist")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }


    @Test
    public void should_grant_read_and_write_access() throws IOException {
        String acl = given()
            .contentType(ContentType.URLENC)
            .formParam("accessTo", "http://localhost:8080/logistics-objects/some")
            .formParam("agent", "http://external/logistics-objects/_data-holder")
            .formParam("modes", List.of(ACL.Read.stringValue(), ACL.Write.stringValue()))
            .when().post("/grant")
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        InputStream aclStream = given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .queryParam("accessTo", "http://localhost:8080/logistics-objects/some")
            .queryParam("agent", "http://external/logistics-objects/_data-holder")
            .when().get()
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        Model acls = Rio.parse(aclStream, RDFFormat.JSONLD);
        assertThat(Models.objectIRIs(acls.filter(null, ACL.mode, null)), containsInAnyOrder(ACL.Read, ACL.Write));
    }

    @Test
    public void should_grant_read_and_write_access_in_seperate_rq() throws IOException {
        String acl = given()
            .contentType(ContentType.URLENC)
            .formParam("accessTo", "http://localhost:8080/logistics-objects/some")
            .formParam("agent", "http://external/logistics-objects/_data-holder")
            .formParam("modes", ACL.Read.stringValue())
            .when().post("/grant")
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        given()
            .contentType(ContentType.URLENC)
            .formParam("accessTo", "http://localhost:8080/logistics-objects/some")
            .formParam("agent", "http://external/logistics-objects/_data-holder")
            .formParam("modes", ACL.Write.stringValue())
            .when().post("/grant")
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        InputStream aclStream = given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .queryParam("accessTo", "http://localhost:8080/logistics-objects/some")
            .queryParam("agent", "http://external/logistics-objects/_data-holder")
            .when().get()
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        Model acls = Rio.parse(aclStream, RDFFormat.JSONLD);
        assertThat(Models.objectIRIs(acls.filter(null, ACL.mode, null)), containsInAnyOrder(ACL.Read, ACL.Write));
    }

    @Test
    public void should_find_acls_with_agent_set() throws IOException {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        String location1 = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        String location2 = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL2)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        InputStream aclStream = given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .queryParam("agent", "http://external/logistics-objects/_data-holder")
            .when().get()
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        Model acls = Rio.parse(aclStream, RDFFormat.JSONLD);
        assertThat(Models.subjectIRIs(acls), containsInAnyOrder(Values.iri(location1), Values.iri(location2)));
    }

    @Test
    public void should_respond_with_BAD_REQUEST_when_no_query_params_given_for_finding_acls() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        String location1 = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        String location2 = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACL2)
            .when().post()
            .then()
            .statusCode(Response.Status.CREATED.getStatusCode())
            .extract().header("Location");

        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get()
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

}
