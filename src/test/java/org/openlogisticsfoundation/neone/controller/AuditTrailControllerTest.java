// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.eclipse.rdf4j.model.impl.DynamicModelFactory;
import org.eclipse.rdf4j.model.util.Values;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.openlogisticsfoundation.neone.model.AuditTrail;
import org.openlogisticsfoundation.neone.model.handler.AuditTrailHandler;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.service.AuditTrailService;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;

import jakarta.ws.rs.core.Response;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(AuditTrailController.class)
public class AuditTrailControllerTest {

    @InjectMock
    AuditTrailService auditTrailService;

    @InjectMock
    AuditTrailHandler handler;

    @io.quarkus.test.InjectMock
    AclAuthorizationRepository aclAuthorizationRepository;

    MockedConstruction<AuditTrail> auditTrailMockedConstruction;

    @BeforeEach
    public void beforeEach() {
        auditTrailMockedConstruction = mockConstruction(AuditTrail.class);

        // mock access control
        when(aclAuthorizationRepository.exists(any(), any())).thenReturn(true);
        when(aclAuthorizationRepository.aclExists(any(), any(), any(), any())).thenReturn(true);
    }

    @AfterEach
    public void afterEach() {
        auditTrailMockedConstruction.close();
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_OK_on_the_success_path() {


        // Every object creation is returning a mock from now on.
        var auditTrail = new AuditTrail(Values.iri("http://some/iri"), Collections.emptyList(), 1);

        when(auditTrailService.getAuditTrailByLoId(any(), any(), any())).thenReturn(auditTrail);
        when(handler.fromJava(any())).thenReturn(new DynamicModelFactory().createEmptyModel());

        given()
            .pathParam("id", "4711")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().get()
            .then()
            .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_NOT_FOUND_when_audit_trail_id_does_not_match() {
        when(auditTrailService.getAuditTrailByLoId(any(), any(), any())).thenThrow(new SubjectNotFoundException(""));
        given()
            .pathParam("id", "4711")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().get()
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_BAD_REQUEST_if_timestamp_until_is_before_from() {
        when(auditTrailService.getAuditTrailByLoId(any(), any(), any())).thenReturn(null);
        given()
            .pathParam("id", "4711")
            .queryParam("updated-from", "2023-03-20T00:00:00Z")
            .queryParam("updated-to", "2023-03-01T00:00:00Z")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().get()
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }
}
