// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.iata.onerecord.api.API;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.LogisticsEvent;
import org.openlogisticsfoundation.neone.model.handler.LogisticsEventHandler;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.service.LogisticsEventService;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(LogisticsEventController.class)
class LogisticsEventControllerTest {

    @InjectMock
    IdProvider idProvider;

    @InjectMock
    LogisticsEventService logisticsEventService;

    @io.quarkus.test.InjectMock
    AclAuthorizationRepository aclAuthorizationRepository;

    @Inject
    LogisticsEventHandler handler;

    @BeforeEach
    public void beforeEach() {
        // mock acls
        when(aclAuthorizationRepository.exists(any(), any())).thenReturn(true);
        when(aclAuthorizationRepository.aclExists(any(), any(), any(), any())).thenReturn(true);
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_the_location_of_a_posted_logistics_event() throws Exception {

        NeoneId loId = NeoneId.fromUri(new URI("http://someserver/logistics-objects/123"));
        NeoneId eventId = NeoneId.fromUri(new URI("http://someserver/logistics-objects/123/events/1"));

        LogisticsEvent logisticsEvent = mock(LogisticsEvent.class);
        when(logisticsEvent.iri()).thenReturn(eventId.getIri());

        when(idProvider.getUriForLoId(any())).thenReturn(loId);
        when(idProvider.createLoEventUri(any(NeoneId.class))).thenReturn(eventId);
        when(idProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(logisticsEventService.addLogisticsEvent(any(), any())).thenReturn(logisticsEvent);

        given()
            .pathParam("loId", "123")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.SIMPLE_EVENT)
            .when().post()
            .then()
            .statusCode(201)
            .header("Location", is(equalTo(eventId.getUri().toString())));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_404_if_lo_does_not_exist() {

        when(idProvider.getUriForLoId(anyString())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080/_data-holder")));
        when(idProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));

        doThrow(new SubjectNotFoundException(""))
            .when(logisticsEventService).addLogisticsEvent(any(), any());

        given()
            .pathParam("loId", "123")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.SIMPLE_EVENT)
            .when().post()
            .then()
            .statusCode(404);
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_events_for_lo() throws Exception {
        // given
        String loId = "1";
        String event1Id = "1";
        String event2Id = "2";
        NeoneId loUri = NeoneId.fromUri(new URI("http://someserver/logistics-objects/" + loId));
        NeoneId event1Uri = NeoneId.fromUri(new URI("http://someserver/logistics-objects/"
            + loId + "/logistics-events/" + event1Id));
        NeoneId event2Uri = NeoneId.fromUri(new URI("http://someserver/logistics-objects/"
            + loId + "/logistics-events/" + event2Id));

        LogisticsEvent event1 = handler.fromModel(
            event1Uri.getIri(),
            TestFixtures.asModel(TestFixtures.SIMPLE_EVENT_1, RDFFormat.JSONLD));
        LogisticsEvent event2 = handler.fromModel(
            event2Uri.getIri(),
            TestFixtures.asModel(TestFixtures.SIMPLE_EVENT_2, RDFFormat.JSONLD));

        when(idProvider.getUriForLoId(loId)).thenReturn(loUri);
        when(idProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(logisticsEventService.findLogisticsEvents(any(), any(), any(), any(), any(), any(), any()))
            .thenReturn(List.of(event1, event2));

        // when
        InputStream response = given()
            .pathParam("loId", loId)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get()
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        // then
        Model model = Rio.parse(response, RDFFormat.JSONLD);
        Model event1Model = model.filter(event1Uri.getIri(), null, null);
        Model event2Model = model.filter(event2Uri.getIri(), null, null);
        assertThat(event1Model, containsInAnyOrder(handler.fromJava(event1).toArray()));
        assertThat(event2Model, containsInAnyOrder(handler.fromJava(event2).toArray()));
        assertThat(model.contains(null, RDF.TYPE, API.Collection), is(true));
        assertThat(model.contains(null, API.hasTotalItems, Values.literal(2)), is(true));
        assertThat(model.contains(null, API.hasItem, event1Uri.getIri()), is(true));
        assertThat(model.contains(null, API.hasItem, event2Uri.getIri()), is(true));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_empty_collection_if_no_events_for_lo_are_found() throws Exception {
        // given
        String loId = "1";
        String event1Id = "1";
        String event2Id = "2";
        NeoneId loUri = NeoneId.fromUri(new URI("http://someserver/logistics-objects/" + loId));

        when(idProvider.getUriForLoId(loId)).thenReturn(loUri);
        when(logisticsEventService.findLogisticsEvents(any(), any(), any(), any(), any(), any(), any()))
            .thenReturn(List.of());

        // when
        InputStream response = given()
            .pathParam("loId", loId)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get()
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        // then
        Model model = Rio.parse(response, RDFFormat.JSONLD);
        assertThat(model.contains(null, RDF.TYPE, API.Collection), is(true));
        assertThat(model.contains(null, API.hasTotalItems, Values.literal(0)), is(true));
        assertThat(model.contains(null, API.hasItem, null), is(false));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_404_if_events_for_non_existing_lo_are_queried() {
        when(logisticsEventService.findLogisticsEvents(any(), any(), any(), any(), any(), any(), any()))
            .thenThrow(new SubjectNotFoundException(""));
        when(idProvider.getUriForLoId(anyString())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080/_data-holder")));
        when(idProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));

        given()
            .pathParam("loId", "does-not-exist")
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get()
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_specific_event() throws Exception {
        // given
        String loId = "1";
        String eventId = "1";
        NeoneId eventUri = NeoneId.fromUri(new URI("http://someserver/logistics-objects/"
            + loId + "/logistics-events/" + eventId));
        LogisticsEvent event = handler.fromModel(
            eventUri.getIri(),
            TestFixtures.asModel(TestFixtures.SIMPLE_EVENT, RDFFormat.JSONLD));

        when(idProvider.getUriForLoId(anyString())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080/_data-holder")));
        when(idProvider.getUriForEventId(loId, eventId)).thenReturn(eventUri);
        when(idProvider.createInternalIri()).thenReturn(NeoneId.fromIri(Values.iri("neone:1")));
        when(logisticsEventService.getLogisticsEvent(any(), eq(false), eq(null)))
            .thenReturn(event);

        // when
        InputStream response = given()
            .pathParam("loId", loId)
            .pathParam("eventId", eventId)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get("/{eventId}")
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract().asInputStream();

        // then
        LogisticsEvent model = handler.fromModel(event.iri(), Rio.parse(response, RDFFormat.JSONLD));
        assertThat(model, is(equalTo(event)));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_404_if_specific_event_does_not_exist() throws Exception {
        // given
        String loId = "1";
        String eventId = "1";
        NeoneId eventUri = NeoneId.fromUri(new URI("http://someserver/logistics-objects/"
            + loId + "/logistics-events/" + eventId));

        when(idProvider.getUriForLoId(anyString())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080/_data-holder")));
        when(idProvider.getUriForEventId(loId, eventId)).thenReturn(eventUri);
        when(idProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(logisticsEventService.getLogisticsEvent(any(), eq(false), eq(null)))
            .thenThrow(new SubjectNotFoundException(""));

        given()
            .pathParam("loId", loId)
            .pathParam("eventId", eventId)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().get("/{eventId}")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
}
