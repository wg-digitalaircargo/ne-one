// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.InjectMock;
import io.quarkus.test.security.TestSecurity;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openlogisticsfoundation.neone.client.NotificationClient;
import org.openlogisticsfoundation.neone.client.RestClientBuilderProducer;
import org.openlogisticsfoundation.neone.model.Notification;

import jakarta.inject.Inject;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(NotificationController.class)
public class NotificationControllerTest {

    @InjectMock
    RestClientBuilderProducer producer;

    @Inject
    NotificationController controller;

    @Test
    @TestSecurity(user = "test")
    public void should_orchestrate_properly() {

        NotificationClient client = mock(NotificationClient.class);
        doNothing().when(client).notify(any());
        RestClientBuilder builder = mock(RestClientBuilder.class, Mockito.RETURNS_DEEP_STUBS);
        when(builder
            .baseUri(any())
            .connectTimeout(anyLong(), any(TimeUnit.class))
            .readTimeout(anyLong(), any(TimeUnit.class))
            .build(any()))
            .thenReturn(client);
        when(producer.restClientBuilder()).thenReturn(builder);

        var notification = new Notification(
            Values.iri("some:iri"), Values.iri("some:event-type"), Optional.of("some topic"),
            Optional.empty(), Optional.empty(), Set.of()
        );
        controller.handleNotification(new NotificationMessage().withNotification(notification));
        verify(client, times(1)).notify(any());
    }
}
