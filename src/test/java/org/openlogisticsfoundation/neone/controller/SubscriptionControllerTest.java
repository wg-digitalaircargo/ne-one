// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.service.SubscriptionService;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(SubscriptionController.class)
public class SubscriptionControllerTest {

    @InjectMock
    private SubscriptionService subService;

    @Test
    @TestSecurity(user = "test")
    public void should_return_200_on_valid_subscription_request() {

        when(subService.handleSubscription(any()))
            .thenReturn(TestFixtures.getPendingSubscriptionRequest());

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.MINIMAL_SUBSCRIPTION)
            .when().post()
            .then()
            .statusCode(201);
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_400_on_invalid_subscription_request() {

        when(subService.handleSubscription(any()))
            .thenReturn(TestFixtures.getPendingSubscriptionRequest());

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            // Just leave required body empty.
            .when().post()
            .then()
            .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}
