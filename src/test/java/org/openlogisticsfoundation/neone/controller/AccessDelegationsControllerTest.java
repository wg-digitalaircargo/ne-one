//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.eclipse.rdf4j.model.util.Values;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.exception.InvalidAccessDelegationObjectException;
import org.openlogisticsfoundation.neone.model.AccessDelegationRequest;
import org.openlogisticsfoundation.neone.model.RequestStatus;
import org.openlogisticsfoundation.neone.service.AccessDelegationService;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
@TestHTTPEndpoint(AccessDelegationsController.class)
public class AccessDelegationsControllerTest {

    @InjectMock
    AccessDelegationService accessDelegationService;

    @Test
    @TestSecurity(user = "test")
    public void should_return_location_of_actionrequest() {
        var uri = Values.iri("http://testuri");
        var ad = new AccessDelegationRequest(uri, Optional.empty(), RequestStatus.REQUEST_PENDING,
            Instant.now(), null, Optional.empty(), Optional.empty(), null);
        when(accessDelegationService.handleAccessDelegation(any())).thenReturn(ad);

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACCESS_DELEGATION)
            .when().post()
            .then()
            .statusCode(201)
            .header("Location", CoreMatchers.is(CoreMatchers.equalTo(uri.toString())));
    }

    @Test
    public void should_return_401_status() {
        var uri = Values.iri("http://testuri");
        var ad = new AccessDelegationRequest(uri, Optional.empty(), RequestStatus.REQUEST_PENDING,
            Instant.now(), null, Optional.empty(), Optional.empty(), null);

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACCESS_DELEGATION)
            .when().post()
            .then()
            .statusCode(401);
    }

    @Test
    public void should_return_415_status_if_media_type_is_not_supported() {
        given()
            .contentType("application/unsupported")
            .when().post()
            .then()
            .statusCode(415);
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_badRequest_if_Lo_does_not_exist() {
        when(accessDelegationService.handleAccessDelegation(any()))
            .thenThrow(new InvalidAccessDelegationObjectException(Set.of(Values.iri("http://some/iri"))));

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.ACCESS_DELEGATION)
            .when().post()
            .then()
            .statusCode(400);
    }
}
