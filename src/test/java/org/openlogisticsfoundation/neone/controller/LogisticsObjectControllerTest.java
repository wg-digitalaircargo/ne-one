// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.controller;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.iata.onerecord.cargo.CARGO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.openlogisticsfoundation.neone.NeoneId;
import org.openlogisticsfoundation.neone.TestFixtures;
import org.openlogisticsfoundation.neone.exception.BadRevisionNumberException;
import org.openlogisticsfoundation.neone.exception.MissingMetadataException;
import org.openlogisticsfoundation.neone.exception.SubjectNotFoundException;
import org.openlogisticsfoundation.neone.model.LogisticsObject;
import org.openlogisticsfoundation.neone.model.LogisticsObjectMetadata;
import org.openlogisticsfoundation.neone.model.Snapshot;
import org.openlogisticsfoundation.neone.repository.AclAuthorizationRepository;
import org.openlogisticsfoundation.neone.repository.RepositoryTransaction;
import org.openlogisticsfoundation.neone.service.LogisticsObjectService;
import org.openlogisticsfoundation.neone.service.SnapshotService;
import org.openlogisticsfoundation.neone.service.internal.LogisticsObjectMetadataService;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import java.io.StringReader;
import java.net.URI;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.openlogisticsfoundation.neone.NeoneId.fromUri;

@QuarkusTest
@TestHTTPEndpoint(LogisticsObjectController.class)
class LogisticsObjectControllerTest {

    @InjectMock
    LogisticsObjectService loService;

    @InjectMock
    IdProvider loIdProvider;

    @InjectMock
    LogisticsObjectMetadataService metadataService;

    @InjectMock
    AclAuthorizationRepository aclAuthorizationRepository;

    @Inject
    RepositoryTransaction tx;

    @InjectMock
    SnapshotService snapshotService;

    @BeforeEach
    public void beforeEach() {
        // mock acls
        when(aclAuthorizationRepository.exists(any(), any())).thenReturn(true);
        when(aclAuthorizationRepository.aclExists(any(), any(), any(), any())).thenReturn(true);
        when(loIdProvider.parse(any(IRI.class))).thenReturn(NeoneId.fromString("some:id", true, true));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_location_of_posted_LO_as_JSON_LD() throws Exception {
        URI uri = new URI("http://testuri");
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.createUniqueLoUri()).thenReturn(NeoneId.fromUri(uri));

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.PIECE_WITH_ONE_HI_JSON_LD)
            .when().post()
                .then()
                    .statusCode(201)
                    .header("Location", is(equalTo(uri.toString())));
    }


    @Test
    @TestSecurity(user = "test")
    public void should_add_nested_LOs() throws Exception {
        URI uri1 = new URI("http://testuri1");
        URI uri2 = new URI("http://testuri2");
        reset(loIdProvider);
        when(loIdProvider.parse(eq(Values.iri(uri1.toString()))))
            .thenReturn(NeoneId.fromString(uri1.toString(), false, true));
        when(loIdProvider.parse(eq(Values.iri(uri2.toString()))))
            .thenReturn(NeoneId.fromString(uri2.toString(), false, true));
        when(loIdProvider.parse(argThat((ArgumentMatcher<IRI>) argument -> argument != null && argument.stringValue().startsWith("neone"))))
            .thenReturn(NeoneId.fromString(uri2.toString(), true, true));
        when(loIdProvider.parse(argThat((ArgumentMatcher<IRI>) argument -> argument != null && argument.stringValue().startsWith(CARGO.NAMESPACE))))
            .thenReturn(NeoneId.fromString(uri2.toString(), false, false));
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.createUniqueLoUri()).thenReturn(NeoneId.fromUri(uri1), NeoneId.fromUri(uri2));

        given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.WAYBILL_WITH_SHIPMENT)
            .when().post()
            .then()
            .log().ifValidationFails(LogDetail.BODY)
            .statusCode(201)
            .header("Location", anyOf(equalTo(uri1.toString()), equalTo(uri2.toString())));

        verify(loService).createLogisticsObjects(argThat(argument -> argument.logisticsObjects().size() == 2), eq(false));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_location_of_posted_LO_as_X_TTL() throws Exception {
        URI uri = new URI("http://testuri");
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.createUniqueLoUri()).thenReturn(NeoneId.fromUri(uri));

        given()
            .config(RestAssured.config()
                .encoderConfig(EncoderConfig.encoderConfig()
                    .encodeContentTypeAs("application/x-turtle", ContentType.TEXT)))
            .contentType(BodyHandlerBase.APPLICATION_X_TURTLE)
            .body(TestFixtures.PIECE_WITH_ONE_HI_TURTLE)
            .when().post()
            .then()
            .statusCode(201)
            .header("Location", is(equalTo(uri.toString())));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_location_of_posted_LO_as_TTL() throws Exception {
        URI uri = new URI("http://testuri");
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.createUniqueLoUri()).thenReturn(NeoneId.fromUri(uri));

        given()
            .contentType(BodyHandlerBase.TEXT_TURTLE)
            .body(TestFixtures.PIECE_WITH_ONE_HI_TURTLE)
            .when().post()
            .then()
            .statusCode(201)
            .header("Location", is(equalTo(uri.toString())));
    }

    @Test
    public void should_return_415_status_if_media_type_is_not_supported() {
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        given()
            .contentType("application/unsupported")
            .when().post()
            .then()
            .statusCode(415);
    }

    @Test
    public void should_return_all_types_in_response_header() throws Exception {

        URI uri = new URI("http://testuri");
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.createUniqueLoUri()).thenReturn(NeoneId.fromUri(uri));

        var types = given()
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .body(TestFixtures.PIECE_WITH_TWO_TYPES_JSON_LD)
            .when().post()
            .then()
            .statusCode(201)
            .extract().headers().getList("Type");
        assertThat(types.size(), is(2));
    }

    @Test
    @Disabled
    public void should_return_LO_as_TTL() throws Exception {
        IRI loIri = iri("some:iri");
        LogisticsObject input = new LogisticsObject(loIri,
            Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ID_JSONLD), RDFFormat.JSONLD));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(iri("neone:some")));
        when(loService.getLogisticsObject(Mockito.any(), Mockito.any())).thenReturn(input);
        LogisticsObjectMetadata metadata = new LogisticsObjectMetadata(iri("neone:meta"), loIri, 1,
            Instant.now(), Optional.empty());
        when(metadataService.getMetadataOf(loIri)).thenReturn(Optional.of(metadata));

        String body = given()
            .pathParam("id", "4711")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().get("/{id}")
            .then()
            .statusCode(200)
            .header(LogisticsObjectController.LATEST_REVISION_HEADER, "1")
            .extract().asString();

        Model output = Rio.parse(new StringReader(body), RDFFormat.TURTLE);
        assertThat(output, is(equalTo(input.model())));
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_404_if_lo_not_found() throws Exception {
//        tx.transactionallyDo(connection -> {
//            aclAuthorizationRepository.add(Values.iri("neone:acl"), RDF.TYPE, ACL.Authorization, connection);
//            aclAuthorizationRepository.add(Values.iri("neone:acl"), ACL.agent, Values.iri("http://localhost:8080"), connection);
//            aclAuthorizationRepository.add(Values.iri("neone:acl"), ACL.accessTo, Values.iri("http://localhost:8080"), connection);
//        });
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.getUriForLoId(any())).thenReturn(fromUri(new URI("http://localhost:8080")));
        when(loService.getLogisticsObject(Mockito.any(), Mockito.anyBoolean())).thenThrow(new SubjectNotFoundException(""));

        given()
            .pathParam("id", "4711")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().get("/{id}")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @TestSecurity(user = "test")
    public void should_return_400_on_illegal_at_parameter() {
//        tx.transactionallyDo(connection -> {
//            aclAuthorizationRepository.add(Values.iri("neone:acl"), RDF.TYPE, ACL.Authorization, connection);
//            aclAuthorizationRepository.add(Values.iri("neone:acl"), ACL.agent, Values.iri("http://localhost:8080/_data-holder"), connection);
//            aclAuthorizationRepository.add(Values.iri("neone:acl"), ACL.accessTo, Values.iri("http://localhost:8080"), connection);
//        });
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080/_data-holder")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        given()
            .pathParam("id", "4711")
            .queryParam("at", "20230404-000000")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().get("/{id}")
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }


    @Test
    @Disabled
    public void should_return_404_when_LO_to_patch_not_found() {
        when(loIdProvider.createInternalIri()).thenCallRealMethod();
        doThrow(new SubjectNotFoundException(""))
            .when(loService).createChangeRequest(any(), any());

        given()
            .pathParam("id", "4711")
            .body(TestFixtures.DELETE_PATCH_RQ)
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().patch("/{id}")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @Disabled
    public void should_return_400_for_invalid_patch_request() {
        when(loIdProvider.createInternalIri()).thenCallRealMethod();
        given()
            .pathParam("id", "4711")
            .body(TestFixtures.INVALID_PATCH_RQ)
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().patch("/{id}")
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @Disabled
    public void should_return_422_when_wrong_revision_number_is_sent_in_patch_rq() {
        when(loIdProvider.createInternalIri()).thenCallRealMethod();
        doThrow(new BadRevisionNumberException(2))
            .when(loService).createChangeRequest(any(), any());

        given()
            .pathParam("id", "4711")
            .body(TestFixtures.DELETE_PATCH_RQ)
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .when().patch("/{id}")
            .then()
            .statusCode(422);
    }

    
    @Test
    @TestSecurity(user = "test")    
    public void should_return_headers_without_body_for_HEAD_request() throws Exception {
        IRI loIri = iri("http://some/lovely/id");
        LogisticsObject input = new LogisticsObject(loIri, Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ID_JSONLD), RDFFormat.JSONLD));
        when(loService.getLogisticsObject(Mockito.any(), Mockito.anyBoolean())).thenReturn(input);

        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(iri("neone:some")));        
        

        Snapshot snapshot = new Snapshot(Values.iri("neone:snapshot"), loIri, "payload", Instant.now(), 1);
        when(snapshotService.getLatestSnapshot(Mockito.any())).thenReturn(Optional.of(snapshot));
        
        LogisticsObjectMetadata metadata = new LogisticsObjectMetadata(iri("neone:meta"), loIri, 1, Instant.now(), Optional.empty());
        when(metadataService.getMetadataOf(Mockito.any())).thenReturn(Optional.of(metadata));

        String body = given()
            .pathParam("id", "4711")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)            
            .when().head("/{id}")
            .then()
            .statusCode(200)
            .header(LogisticsObjectController.LATEST_REVISION_HEADER, "1")
            .header(LogisticsObjectController.REVISION_HEADER, "1")
            .extract().asString();

        assertEquals("", body);
    }

    @Test
    @TestSecurity(user = "test")        
    public void should_return_404_if_lo_not_found_for_HEAD_request() throws Exception {
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.getUriForLoId(any())).thenReturn(fromUri(new URI("http://localhost:8080")));                
        when(metadataService.getMetadataOf(Mockito.any())).thenThrow(new MissingMetadataException(""));        

        given()
            .pathParam("id", "4711")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().head("/{id}")
            .then()
            .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @TestSecurity(user = "test")    
    public void should_return_400_on_illegal_at_parameter_for_HEAD_request() {

        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(Values.iri("http://localhost:8080/_data-holder")));
        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        given()
            .pathParam("id", "4711")
            .queryParam("at", "20230404-000000")
            .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
            .accept(BodyHandlerBase.TEXT_TURTLE)
            .when().head("/{id}")
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    public void should_return_401_if_user_presents_invalid_token_lacks_permission_for_HEAD_request() throws Exception {
        given()
        .pathParam("id", "4711")
        .header("Authorization", "Bearer invalid")
        .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
        .accept(BodyHandlerBase.TEXT_TURTLE)
        .when().head("/{id}")
        .then()
        .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    @TestSecurity(user = "test")
    @Disabled
    public void should_return_403_if_user_has_not_sufficient_permissions_for_HEAD_request() throws Exception {
        IRI loIri = iri("http://some/lovely/id");
        LogisticsObject input = new LogisticsObject(loIri, Rio.parse(new StringReader(TestFixtures.PIECE_WITH_ID_JSONLD), RDFFormat.JSONLD));
        when(loService.getLogisticsObject(Mockito.any(), Mockito.anyBoolean())).thenReturn(input);

        when(loIdProvider.createInternalIri()).thenAnswer(invocation -> NeoneId.fromIri(Values.iri("neone:" + UUID.randomUUID())));
        when(loIdProvider.getUriForLoId(any())).thenReturn(NeoneId.fromIri(iri("neone:some")));        
        

        Snapshot snapshot = new Snapshot(Values.iri("neone:snapshot"), loIri, "payload", Instant.now(), 1);
        when(snapshotService.getLatestSnapshot(Mockito.any())).thenReturn(Optional.of(snapshot));
        
        LogisticsObjectMetadata metadata = new LogisticsObjectMetadata(iri("neone:meta"), loIri, 1, Instant.now(), Optional.empty());
        when(metadataService.getMetadataOf(Mockito.any())).thenReturn(Optional.of(metadata));

        when(aclAuthorizationRepository.exists(any(), any())).thenReturn(false);

        given()
        .pathParam("id", "4711")
        .contentType(BodyHandlerBase.APPLICATION_LD_JSON)
        .accept(BodyHandlerBase.TEXT_TURTLE)
        .when().head("/{id}")
        .then()
        .statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }
}
