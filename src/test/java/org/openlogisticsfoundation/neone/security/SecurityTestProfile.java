// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.quarkus.test.junit.QuarkusTestProfile;

import java.util.Map;

public class SecurityTestProfile implements QuarkusTestProfile {
    @Override
    public Map<String, String> getConfigOverrides() {
        return Map.of(
            "auth.valid-issuers.testlocal", "http://localhost:9898/realms/neone",
            "auth.issuers.testlocal.publickey.location", "http://localhost:9898/realms/neone/protocol/openid-connect/certs",
            "auth.valid-issuers.testlocal2", "http://localhost:9898/realms/neone2",
            "auth.issuers.testlocal2.publickey.location", "http://localhost:9898/realms/neone2/protocol/openid-connect/certs"
        );
    }
}


