// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.openlogisticsfoundation.neone.controller.BodyHandlerBase;
import org.openlogisticsfoundation.neone.model.ServerInformation;
import org.openlogisticsfoundation.neone.service.ServerInformationService;

import java.security.NoSuchAlgorithmException;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.when;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.openlogisticsfoundation.neone.security.TestSecurityUtils.generateJwks;
import static org.openlogisticsfoundation.neone.security.TestSecurityUtils.generateJwtToken;


@QuarkusTest
@TestProfile(SecurityTestProfile.class)
public class MultiIssuerJwtCallerPrincipalFactoryTest {


    @InjectMock
    ServerInformationService serverInformationService;


    private static ClientAndServer mockServer;

    @BeforeAll
    public static void startMockServer() throws NoSuchAlgorithmException {
        Set<String> issuers = Set.of("http://localhost:9898/realms/neone", "http://localhost:9898/realms/neone2");
        mockServer = startClientAndServer(9898);

        for (String issuer : issuers) {
            String path = issuer.substring(issuer.indexOf("/realms/"));
            String jwks = generateJwks(issuer);
            mockServer.when(
                request()
                    .withMethod("GET")
                    .withPath(path + "/protocol/openid-connect/certs")
            ).respond(
                response()
                    .withStatusCode(200)
                    .withBody(jwks)
            );
        }
    }

    @AfterAll
    public static void stopMockServer() {
        mockServer.stop();
    }

    @Test
    public void should_return_200_get_server_information_with_valid_tokens_multiple_trusted_issuers() {
        when(serverInformationService.getServerInformation()).thenReturn(
            new ServerInformation(Values.iri("some://iri"), new ModelBuilder().build()));

        String token = generateJwtToken("http://localhost:9898/realms/neone", "http://localhost:8080/logistics-objects/_data-holder");

        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .auth().oauth2(token)
            .when().get()
            .then()
            .statusCode(200);

        String token2 = generateJwtToken("http://localhost:9898/realms/neone2", "http://localhost:8081/logistics-objects/_data-holder");

        assertThat(token, is(not(token2)));

        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .auth().oauth2(token2)
            .when().get()
            .then()
            .statusCode(200);
    }

    @Test
    public void should_return_401_get_server_information_with_token_from_untrusted_issuer() {
        String token = generateJwtToken("http://localhost:9898/realms/neone3", "http://localhost:8080/logistics-objects/_data-holder");
        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .auth().oauth2(token)
            .when().get()
            .then()
            .statusCode(401);
    }

    @Test
    public void should_return_401_get_server_information_with_expired_token() {
        String expiredToken = "eyJraWQiOiJhYWMyMWMxNy04MzczLTNmZjMtODIwZi1hYjFiNjU4ZDI2N2YiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0Ojg5ODkvcmVhbG1zL25lb25lIiwibG9naXN0aWNzX2FnZW50X3VyaSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9sb2dpc3RpY3Mtb2JqZWN0cy9fZGF0YS1ob2xkZXIiLCJhdWQiOiJuZW9uZSIsInN1YiI6IjBhZTg5OGYzLWQyNDgtNGVhYy04NjgxLWIwMzgxYzgyZDZjMCIsInNjb3BlIjoiYW55dGhpbmciLCJpYXQiOjE3MTEzNjUyNzIsImV4cCI6MTcxMTM2NTI3MywianRpIjoiODY4OTEwNjEtYTE3MS00NGM3LWEwOTktZTMzN2VjOWQ5M2E2In0.Jj4YuCxOe1X7V9Nf2ovaDR0ZwZDel57MuFgWn77aNo8pz3A_0GvoNqvhjMDlnHbhcz0j7OXNoQW5L9CbNj0_KWhw8AS90Ln7jr87IBitg9WIFtzSdKX1FttaAcg25TNrlygPJmQ066Youij4VFlwh6e2zt_WhxDBLIt-0S7DWC9neNjJ34a2bg7S4SFTs3GJzLmiujcUksar8d1915qiQmq1Wd4XHWfAM2-m4KTX2ANYS3tI0KGETjTTOxpkdTjrUfZWL8oaQ_RnZPVtpzgbWVOMhhmn0URnaWgZ9MxnKcFhT5jdBjYdEOD7FVyjr_Krj5HIFN3SlG7_XHoVSjqRiQ";
        given()
            .accept(BodyHandlerBase.APPLICATION_LD_JSON)
            .auth().oauth2(expiredToken)
            .when().get()
            .then()
            .statusCode(401);
    }


    @Test
    public void should_return_401_get_server_information_with_not_matching_kid_token() {
        String expiredToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXMUxnTHh5cEU4SkZyZWF2OU9EZjktVDA0cUNYeTdKa3djdmFBUER6T1RJIn0.eyJleHAiOjE3MDk5MzI2ODMsImlhdCI6MTcwOTg5NjY4NCwianRpIjoiMDAzZWI3ZmQtM2ExYy00YWUxLWJmY2ItYWViZjc1OTI5YjQzIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4OTg5L3JlYWxtcy9uZW9uZSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiIwYWU4OThmMy1kMjQ4LTRlYWMtODY4MS1iMDM4MWM4MmQ2YzAiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJuZW9uZS1jbGllbnQiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1uZW9uZSIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiY2xpZW50SG9zdCI6IjE5Mi4xNjguNjUuMSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibG9naXN0aWNzX2FnZW50X3VyaSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9sb2dpc3RpY3Mtb2JqZWN0cy9fZGF0YS1ob2xkZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzZXJ2aWNlLWFjY291bnQtbmVvbmUtY2xpZW50IiwiY2xpZW50QWRkcmVzcyI6IjE5Mi4xNjguNjUuMSIsImNsaWVudF9pZCI6Im5lb25lLWNsaWVudCJ9.oAZqvt70mDIAW03ZjfwaEIhdEwX6ukqMqIO0VdX8vBVVbA1oeCOjTUL4qLiUWxZftfoQ0vQgin3YZP5dWEMXKyyUw0Ga6LwOj1mXsXqUOWQ4j0yCpdw4JsRUID-yJarFKeZjNWoLnpYhKPh98Vir1Hz_xlzoBdejVzYBx96v04RKs73b0FebHPSW90P-4CGqFozYgo9ntqMiI5Zx05gWjwQCH1B2D-ZRK7xQjLfYtnfWI_H0TzRRqaN_IOlG4IFsY7ia8dsJt02cHZg88mMCCp_pGcKsPdk6DGIlRO6gL5Ww0gn1yScBDkoDwk6td6TvNaaty14a9zazxsGR0-uOxA";
        given()
            .auth().oauth2(expiredToken)
            .when().get()
            .then()
            .statusCode(401);
    }
}
