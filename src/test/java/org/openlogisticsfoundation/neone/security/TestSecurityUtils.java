// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.openlogisticsfoundation.neone.security;

import io.smallrye.jwt.build.Jwt;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import java.security.MessageDigest;
import java.security.interfaces.RSAPublicKey;

public class TestSecurityUtils {

    private static final Map<String, KeyPair> keyPairsPerIssuer = new HashMap<>();
    private static final Map<String, String> kidsPerIssuer = new HashMap<>();

    public static KeyPair getKeyPairForIssuer(String issuer) {
        return keyPairsPerIssuer.computeIfAbsent(issuer, k -> {
            try {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(2048);
                KeyPair keyPair = keyPairGenerator.genKeyPair();
                String kid = UUID.nameUUIDFromBytes(issuer.getBytes()).toString();
                kidsPerIssuer.put(issuer, kid);
                return keyPair;
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static String generateJwtToken(String issuer, String logisticsAgentUri) {
        KeyPair keyPair = getKeyPairForIssuer(issuer);
        String kid = kidsPerIssuer.get(issuer);
        return Jwt.issuer(issuer)
            .claim("logistics_agent_uri", logisticsAgentUri)
            .audience("neone")
            .expiresIn(36000) // 10 hours
            .subject("0ae898f3-d248-4eac-8681-b0381c82d6c0")
            .scope("anything")
            .jws()
            .keyId(kid) // Use the generated or existing kid
            .sign(keyPair.getPrivate());
    }

    public static String generateJwks(String issuer) throws NoSuchAlgorithmException {
        KeyPair keyPair = getKeyPairForIssuer(issuer);
        String kid = kidsPerIssuer.get(issuer);
        RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();

        // Encode the modulus and exponent of the public key
        String n = Base64.getUrlEncoder().withoutPadding().encodeToString(rsaPublicKey.getModulus().toByteArray());
        String e = Base64.getUrlEncoder().withoutPadding().encodeToString(rsaPublicKey.getPublicExponent().toByteArray());

        // Calculate x5t and x5t#S256
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] keyBytes = keyPair.getPublic().getEncoded();
        String x5t = Base64.getUrlEncoder().withoutPadding().encodeToString(sha1.digest(keyBytes));
        String x5tS256 = Base64.getUrlEncoder().withoutPadding().encodeToString(sha256.digest(keyBytes));

        return "{\"keys\": [{"
            + "\"kty\": \"RSA\","
            + "\"kid\": \"" + kid + "\","
            + "\"use\": \"sig\","
            + "\"alg\": \"RS256\","
            + "\"n\": \"" + n + "\","
            + "\"e\": \"" + e + "\","
            + "\"x5c\": []," // Assuming x5c is an empty array if not provided
            + "\"x5t\": \"" + x5t + "\","
            + "\"x5t#S256\": \"" + x5tS256 + "\""
            + "}]}";
    }
}



