# How to contribute

So you want to help out making NE:ONE better. That's great, we welcome all contributions! 
Before you dive in, here are some things you need to know.

**Table of Contents**  

- [Legal stuff](#legal-stuff)
- [Creating your contribution](#creating-your-contribution)
- [Workflow](#workflow) 
 - [Releases](#releases)
  - [Release branches](#release-branches)
	
## Legal stuff

NE:ONE is a project governed by the DTAC within the [Open Logistics Foundation](https://openlogisticsfoundation.org), which has strict policies and guidelines regarding contributions.

In order for any contributions to NE:ONE to be accepted, you MUST do the following things:

1. Sign the [Contributer License Agreement (CLA)](https://openlogisticsfoundation.org/licenses/). You can do this as follows: 

  * If you haven't done so already, [register an GitLab account](https://git.openlogisticsfoundation.org/).
  * Sign the CLA and send it to [Andreas Nettstraeter](mailto:andreas.nettstraeter@openlogisticsfoundation.org?subject=[GitLab-signed-CLA])

2. Message the maintainer of the repository you want to contribute to, in this case [Oliver Ditz](mailto:oliver.ditz@iml.fraunhofer.de?subject=[GitLab-NE:ONE-contribution]), so he can grant you developer access.

## Creating your contribution

Once the legalities are out of the way you can dig in. Here's how:

1. Create an issue in the [NE:ONE GitLab issue tracker](https://git.openlogisticsfoundation.org/wg-digitalaircargo/ne-one/-/issues) that describes your improvement, new feature, or bug fix. Alternatively, comment on an existing issue to indicate you're keen to help solve it.
2. Fork the repository on GitLab.
3. Create a new branch for your changes starting from the `develop` branch. See [Workflow](#workflow) for details.
4. Make your changes. Apply generally good practices of writing and documenting code.
5. Make sure you include tests.
6. Make sure the test suite passes after your changes.
7. Commit your changes into the branch. Use meaningful commit messages. Reference the issue number in the commit message (for example "issue #276: added null check").
8. **Sign off** every commit you do, using the `-s` flag (as explained in the [legal stuff](#legal-stuff).
9. Optionally squash your commits (not necessary, but if you want to clean your commit history a bit, _this_ is the point to do it).
10. Push your changes to your branch in your forked repository.
11. If your contribution is complete, use GitLab to submit a merged request (MR)
	for your contribution back to `develop` in the central NE:ONE repository.
	Once you have submitted your PR, do not use your branch for any other
	development (unless asked to do so by the reviewers of your PR). 

Once you've put up a MR, we will review your contribution, possibly make some
suggestions for improvements, and once everything is complete it will be merged
into the `develop` branch, to be included in the next minor or major release towards the `main` branch. If
your contribution is a bug fix in an existing release, we will also schedule it
for inclusion in a patch release.

## Workflow

The short-short version for contributors: work from the `develop` branch. 

The more complete version: NE:ONE uses a git branching model where feature
development takes place on branches from the `develop` branch. This is where all
development for the next (minor or major) release happens.

Every issue, no matter how small, gets its own branch while under development.
Issue branch names are always prefixed with `issues/`, followed by the issue
number in the [NE:ONE GitLab issue tracker](https://git.openlogisticsfoundation.org/wg-digitalaircargo/ne-one/-/issues),
followed by one or two dash-separated keywords for the issue. 

For example: `issues/#276-rdfxml-sax` is the branch for a fix for
GitHub issue #276, which has to do with RDF/XML and the SAX parser.

Once a feature/fix is complete, tested, and reviewed (via a Merge Request), it
is merged into develop, and the issue branch is closed.

### Releases

As of right now NE:ONE releases are beeing done on an on-demand basis once there are major updates or important bug fixes. This might change how ever to a regular release cycle once the development speed for NE:ONE picks up or demand for this becomes clear.


#### Release branches

A few days before release of a minor or major version, we create a *release
branch* from the current head of the develop branch. The purpose of this release
branch is to perform last-minute tweaks and little fixes. Once we are ready to
release, the latest commit of the release branch is tagged with the version number, and
the release branch is merged back to master, then closed.