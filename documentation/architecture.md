# NE:ONE Architecture Documentation

The NE:ONE server implements the [IATA ONE Record standard](https://onerecord.iata.org).

# Introduction and Goals

An overview about the goals of the NE:ONE server can be found in the
[IATA ONE Record presentation](https://www.iata.org/contentassets/a1b5532e38bf4d6284c4bf4760646d4e/iata_one_record_presentation_en.pdf).

# Architecture Constraints

The data model of the system is fixed and follows the ONE Record data model standard. The ONE Record data model is
described as ontology. The data model thus cannot be altered or reworked as part of this project.

The ONE Record API is also modeled as an ontology and defines the interactions that have to be supported by the
NE:ONE server. The specification is freely available from the corresponding
[IATA Github repository](https://github.com/IATA-Cargo/ONE-Record).

The API specification mandates the use of JSON-LD as format of the request and responses. Using JSON-LD, there is no
simple way of describing the data model sent to/from the NE:ONE server using the OpenAPI specification.

# System Scope and Context

In order to fulfill the ONE Record standard the NE:ONE server needs data from systems not part of the standard.
The evaluation of which partners to notify about LOs is therefore mandated to a third party system. Also, the actions
that are to be taken when notifications about created or changed LOs are received are delegated to a third party
system. Last, the decision about whether to subscribe to a given topic or not is also relayed to a third party system.

## Technical Context

The following diagram shows the context of the NE:ONE server as it may be deployed.

```mermaid
C4Context
  title System Context diagram for the NE:ONE server
  Enterprise_Boundary(b0, "NE:ONE Enterprise") {

    System_Ext(partnerManagement, "Partner Management System", "Holds information about partners")

    System(neone, "NE:ONE server")

    System_Ext(notificationProcessor, "Notification Processing Server", "Handles incoming notifications from other ONE Record servers")
    System_Ext(subscriptionProcessor, "Subscription Processing Server", "Handles subscription proposals")
  }

  Enterprise_Boundary(b2, "ONE Record Enterprise") {
    System_Ext(1RServerExt, "ONE Record server")
  }

  Rel(neone, partnerManagement, "fetch partners for subscriptions", "HTTPS")
  Rel(neone, notificationProcessor, "notifies about subscribed LO changes", "HTTPS")
  Rel(neone, subscriptionProcessor, "notifies about subscription proposals", "HTTPS")
  Rel(neone, 1RServerExt, "send notifications about LO changes", "HTTPS")

  UpdateRelStyle(neone, subscriptionProcessor, $offsetX="-210", $offsetY="-100")

  UpdateLayoutConfig($c4ShapeInRow="1", $c4BoundaryInRow="2")
```

| Item                           | Description                                                                       |
|--------------------------------|-----------------------------------------------------------------------------------|
| NE:ONE server                  | The NE:ONE server                                                                 |
| Partner Management System      | System that provides information about partners, used for subscription proposals  |
| Notification Processing Server | System that handles notifications about changed LOs from other ONE Record servers |
| Subscription Processing Server | System that handles subscription proposals from other ONE Record Servers          |
| ONE Record Server              | ONE Record server of partner companies                                            |



# Solution Strategy

The NE:ONE ONE Record server processes linked data in the form of [JSON-LD](https://json-ld.org) via HTTP. Therefore,
as base technologies

 * [Quarkus](https://quarkus.io) and
 * [RDF4J](https://rdf4j.org)

have been chosen.

As the ONE Record standard is about inter linking individual data objects (Logistics Objects) between different
ONE Record servers the Quarkus framework, being developer friendly and resource effective, is used to provide the
handling of requests sent via HTTP using a REsTful style.

For the data storage RDF4J is used. RDF4J natively supports JSON-LD and is able to store and query the data as
triple statements. As RDF is a way of describing and linking data in form of triples (subject, predicate, object) and
storing these triples in a relational database makes common queries hard, RDF4J provides a storage layer to support
efficient querying of this data.

# Building Block View

## Whitebox Overall System

```mermaid
C4Container
  System_Ext(neone1RClient, "ONE Record Client", "ONE Record", "A client that wants to use the ONE Record server")
  System(neoneClient, "NE:ONE Server Client", "Manages the NE:ONE Server")
  Container_Boundary(neoneBoundary, "NE:ONE Server") {
    Container(1rApi, "ONE Record API")
    Container(intApi, "Internal NE:ONE API")
    Container(scheduler, "Scheduler")
    ContainerDb(rdf4jStore, "RDF4J Triplestore")
  }

  BiRel(neone1RClient, 1rApi, "uses", "HTTP")
  Rel(neoneClient, intApi, "uses", "HTTP")
  Rel(1rApi, rdf4jStore, "reads/writes")
  Rel(intApi, rdf4jStore, "reads/writes")

  UpdateLayoutConfig($c4ShapeInRow="2", $c4BoundaryInRow="2")
```

## ONE Record API

### Logistics Objects

```mermaid
C4Component
  Container_Boundary("neone", "NE:ONE") {
    Component(loController, "LogisticsObjectController", "Controller handling requests concerning LOs")
    Component(loService, "LogisticsObjectService", "Business logic concerning LOs")
    Component(loRepository, "LogisticsObjectRepository", "Persistence of LOs")

    Component(eventBus, "EventBus", "Internal Event Bus")

    ContainerDb(rdf4j, "RDF4J storage", "Triplestore")

    Rel(loController, loService, "uses")
    Rel(loService, loRepository, "uses")
    Rel(loService, eventBus, "publishes Events")
    Rel(loRepository, rdf4j, "persists/queries")

    UpdateLayoutConfig($c4ShapeInRow="2", $c4BoundaryInRow="2")
  }
```

#### Create

Not strictly part of the specification but implemented using only ONE Record standard types.

```mermaid
sequenceDiagram

  participant C as Client
  participant LOC as LogisticsObjectController
  participant LOS as LogisticsObjectService
  participant LOR as LogisticsObjectRepository
  participant EVR as EventRepository
  participant BUS as EventBus

  C->>LOC: createLogisticsObject
  LOC->>LOC: createLoUri
  LOC->>LOS: createLogisticsObject(model, uri)
  LOS->>LOR: persist(lo)
  LOS->>EVR: persist(event)
  LOS->>BUS: publish(event)
```

#### Patch

```mermaid
sequenceDiagram

  participant C as Client
  participant LOC as LogisticsObjectController
  participant LOS as LogisticsObjectService
  participant LOR as LogisticsObjectRepository
  participant EVR as EventRepository
  participant BUS as EventBus

  C->>LOC: updateLogisticsObject
  LOC->>LOS: updateLogisticsObject(uri, patchRq)
  LOS->>LOR: findByIri(iri)
  LOS->>LOS: checkRevision(rqRevision, loRevision)
  loop Delete Operations
    LOS->>LOR: deleteStatement(iri, p, oldValue)
  end
  loop Add Operations
    LOS->>LOR: addStatement(iri, p, value)
  end
  LOS->>LOS: incrementRevision(lo)

  LOS->>LOR: persistEvent(event)

  LOS->>BUS: publish(event)
```

#### Retrieve

```mermaid
sequenceDiagram

  participant C as Client
  participant LOC as LogisticsObjectController
  participant LOS as LogisticsObjectService
  participant LOR as LogisticsObjectRepository

  C->>LOC: getLogisticsObject
  LOC->>LOS: getLogisticsObject(uri)
  LOS->>LOR: findByIri(iri)
```

#### Publish Notifications

First all notifications that have to be sent are persisted in the RDF store. Each notification is linked to data
from the ONE Record data model and the NE:ONE internal data model is used to build up additional metadata. As the
ONE Record data model must not be changed the NE:ONE data model is built to only link to the ONE Record data model
as depicted in the following diagram.

```mermaid
classDiagram
    LogisticsObject <|-- LogisticsObjectEvent
    LogisticsObject <|-- Notification
    Notification <|-- LogisticsObjectEvent
    Notification <|-- Callback

    class LogisticsObject {
        <<ONE Record Datamodel>>
    }

    class Notification {
        <<ONE Record Datamodel>>
    }

    class LogisticsObjectEvent {
        <<NE:ONE Datamodel>>
    }

    class Callback {
        <<NE:ONE Datamodel>>
    }
```

```mermaid
sequenceDiagram

  participant BUS as EventBus
  participant EVS as LogisticsObjectEventService
  participant NOS as NotificationService
  participant NOR as NotificationRepository
  participant SUS as SubscriptionService
  participant SUR as SubscriptionRepository
  participant CBR as CallbackRepository
  participant EVR as EventRepository
  participant PAM as PartnerManagement


  BUS->>EVS: onLogisticsObjectChanged(loEvent)
  EVS->>NOS: getCompanyIdsToNotify(loId)
  NOS->>PAM: GET /notification/partner?topic=[TYPE]
  EVS->>SUS: getSubscriptionCallbackUrls(companyIds, topic)
  SUS->>SUR: getSubscriptionCallbackUrls()
  EVS->>NOS: enqueue(event, callbackUrls)
  loop for each callback URL
    NOS->>NOR: persist(notification)
    NOS->>EVR: persist(event)
    NOS->>CBR: persist(callback)
  end
  NOS->>EVR: setEventState(eventId, PENDING)
```

A scheduler then invokes the actual sending of the notifications. This way the notifications that did not get
acknowledged by the partner ONE Record server will be resent during the next scheduler cycle. Afterwards all
notifications are sent, the rdf store is cleaned, deleting all processed notifications and callbacks as well as
setting the event to `PROCESSED` state.

```mermaid
sequenceDiagram

  participant NOS as NotificationService
  participant NOR as NotificationRepository
  participant CBR as CallbackRepository
  participant ORC as OneRecordCient
  participant EVR as EventRepository
  participant ORS as ONERecordServer


  loop every scheduled interval
    NOS->>NOR: getPendingNotifications()
    loop for each notification
      NOS->>CBR: findCallback(id)
      NOS->>ORC: sendNotification(notification)
      ORC->>ORS: [POST] callback
      NOS->>EVR: deleteNotification(eventId, notificationId)
      NOS->>NOR: deleteAll(notificationId)
      NOS->>CBR: deleteAll(callbackId)
    end
  end
  NOS->>NOS: cleanUp()
  NOS->>EVR: getPendingEventsWithoutNotifications()
  loop for each event
    NOS->>EVR: setEventState(eventId, PROCESSED)
  end
```

#### Receive Notifications

When the NE:ONE server gets notified about changed LOs it is subscribed to, the notification gets send to a server
which makes decisions about how to handle the notification.

```mermaid
sequenceDiagram

  participant ORS as ONERecordServer
  participant NOC as NotificationController
  participant NCL as NotificationClient
  participant NPS as NotificationProcessingServer

  ORS->>NOC: [POST] notification
  NOC->>NCL: notify(lo)
  NCL->>NPS: [POST] notification
```

## NE:ONE API

### Add partner subscription

In order to ask partner ONE Record companies to subscribe to a certain type of LOs the NE:ONE server implements an
API to add partner subscriptions. Upon receiving a request a partner ONE Record server is queried about whether it
wants to subscripe to the LO type or not.

```mermaid
sequenceDiagram

  participant C as Client
  participant SMC as SubscriptionManagementController
  participant SUS as SubscriptionService
  participant ORC as OneRecordClient
  participant ORS as ONERecordServer
  participant SUR as SubscriptionRepository

  C->>SMC: [POST] /subscription/proposal
  SMC->>SUS: proposeSubscription()
  SUS->>ORC: proposeSubscription()
  ORC->>ORS: [GET] /?topic=[TOPIC]
  SUS->>SUR: persistSubscription(subscription)
```

# Architecture Decisions

## Scalability

Scalability is not in scope at the moment, the scheduler for instance is implemented as a local scheduler running
on the instance itself.

# Glossary

| Term | Definition       |
|------|------------------|
| LO   | Logistics Object |
