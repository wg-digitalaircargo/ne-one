### Summary
[Briefly describe the issue or feature request.]

### Description
[Provide a detailed description of the issue or feature request.]

### Steps to Reproduce (for bugs)
[If applicable, provide a clear set of steps to reproduce the issue.]

### Expected Behavior
[Describe what you expected to happen.]

### Actual Behavior
[Describe what actually happened.]

### Screenshots (if applicable)
[Attach any relevant screenshots to help illustrate the issue.]

### Environment
[List any relevant information about your environment, such as operating system, browser version, etc.]

### Possible Solution (optional)
[Suggest a possible solution or approach for the issue.]

### Additional Information (optional)
[Include any other relevant information or context about the issue.]

### Checklist
- [ ] I have searched for similar issues and confirmed this is not a duplicate.
- [ ] I have reproduced the issue with the latest version.
- [ ] I have included all the necessary information and context.
- [ ] I have tagged the appropriate labels and assignees.

/label ~Bug